# Notice

Code and documentation Copyright (C) 2015-2019 Creare. Code released under the Apache v2 License, provided in [LICENSE](https://gitlab.com/creare-com/tabsint/blob/master/LICENSE). All rights reserved.

Creare has used commercially reasonable efforts in preparing the
TabSINT Software but makes no guarantee or warranty of any nature
with regard to its use, performance, or operation.

Creare makes no representations or warranties, and Creare shall
incur no liability or other obligation of any nature whatsoever to
any person from any and all actions arising from the use of this
software.  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE HEREBY EXPRESSLY EXCLUDED. The final
responsibility for the proper use and functioning of the TabSINT
Software shall rest solely with the USAMRAA.

## Awknowledgement

This code development was supported by the US Army Medical Research Materiel Command and the Army Public Health Command under SBIR Phase III Award #W81XWH-13-C-0194 to [Creare LLC](www.creare.com). In particular, we gratefully acknowledge the support and contributions of the Audiology and Speech Center at the Walter Reed National Military Medical Center and the Department of Defense Hearing Center of Excellence in the development and extensive testing of this software.

## Privacy Policy

TabSINT is a platform for configuring and administering hearing-related exams, as well as general-purpose questionnaires. As a flexible platform, TabSINT may be configured to automatically collect many types of data, including images, audio, and tablet location. This data can be uploaded to a back end data repository only if configured by a TabSINT Administrator. The TabSINT administrator is responsible for configuring the application, developing and testing the protocol, and managing test results. Data is never transferred outside the application unless configured by the TabSINT administrator, or if TabSINT is configured to upload application logs for debugging purposes.

### Logging

The TabSINT application includes a setting to upload application loags to private server to help debug remote issues. By default, this setting is disabled.  If this setting is enabled, the application may record device state, location, network status, or test results in the application log and upload this data to our server. Please ensure logging is set to the desired state before using the application. 

If you have questions regarding these policies, please contact tabsint@creare.com.


## Third-Party Licenses

The TabSINT Software relies on many open source libraries.
We recommend you read their licenses, as their terms may differ from the terms described in our [LICENSE](https://gitlab.com/creare-com/tabsint/blob/master/LICENSE):

* AngularJS ([MIT license](https://github.com/angular/angular.js/blob/master/LICENSE))
* Cordova  ([Apache V2 License ](https://github.com/apache/cordova-android/blob/master/LICENSE))
* jquery ([License](https://github.com/jquery/jquery/blob/master/LICENSE.txt))
* Angular-UI ([MIT](https://github.com/angular-ui/bootstrap/blob/master/LICENSE))
* Bootstrap ([MIT](https://github.com/twbs/bootstrap/blob/master/LICENSE))
* Lodash ([MIT](https://raw.githubusercontent.com/lodash/lodash/4.12.0/LICENSE))
* RequireJs ([BSD, MIT](https://github.com/jrburke/requirejs/blob/master/LICENSE))
* d3 ([License](https://github.com/mbostock/d3/blob/master/LICENSE))
* ngStorage ([MIT](https://github.com/gsklee/ngStorage/blob/master/LICENSE))
* ZXing barcode scanning library ([Apache V2](https://github.com/zxing/zxing/wiki/License-Questions))
* CryptoJS ([MIT](https://github.com/brix/crypto-js/blob/develop/LICENSE))

Additional files included in `/node_modules`, `/www/bower_components`, `/plugins`, and `tabsint_plugins` directories are externally maintained libraries used by this software, which have their own licenses. 
