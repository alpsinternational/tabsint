package com.creare.tabsintnative;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

//import android.app.Activity;
//import android.content.Intent;
//import android.util.Log;

import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaWebView;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Build;
import android.os.Handler;
import android.webkit.WebView;

import android.util.Log;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;

// for Immersive view...
import android.view.View;
import android.view.WindowManager;

// for Volume buttons
import android.media.AudioManager;

// for intent send data to another app
import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;

import android.provider.MediaStore;
import android.database.Cursor;
import android.content.ClipData;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.net.Uri;

// Usb imports
import android.hardware.usb.UsbManager;
import android.hardware.usb.UsbDevice;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import java.util.HashMap;

// Flic imports
import io.flic.flic2libandroid.Flic2Manager;
import io.flic.flic2libandroid.Flic2ButtonListener;
import io.flic.flic2libandroid.Flic2ScanCallback;
import io.flic.flic2libandroid.Flic2Button;

import android.content.ContentResolver;
import android.webkit.MimeTypeMap;

/**
 * This calls out to the ZXing barcode reader and returns the result.
 *
 * @sa https://github.com/apache/cordova-android/blob/master/framework/src/org/apache/cordova/CordovaPlugin.java
 */
public class TabSINTNative extends CordovaPlugin {
  public static final String ACTION_INITIALIZE = "initialize";
  public static final String ACTION_RESET_AUDIO = "resetAudio";
  public static final String ACTION_SET_AUDIO = "setAudio";
  public static final String ACTION_GET_AUDIO_VOLUME = "getAudioVolume";
  public static final String ACTION_SEND_EXTERNAL_DATA = "sendExternalData";
  public static final String ACTION_GET_EXTERNAL_DATA_INTENT = "getExternalDataIntent";
  public static final String ACTION_SET_EXTERNAL_DATA_INTENT_HANDLER = "setExternalDataIntentHandler";
  public static final String ACTION_REGISTER_USB_DEVICE_LISTENER = "registerUsbDeviceListener";
  public static final String ACTION_UNREGISTER_USB_DEVICE_LISTENER = "unregisterUsbDeviceListener";
  public static final String ACTION_SCAN_FLIC_BUTTON = "scanFlicButton";
  public static final String ACTION_DISCONNECT_FLIC_BUTTON = "disconnectFlicButton";
  public static final String ACTION_GET_CONNECTED_FLIC_BUTTONS = "getConnectedFlicButtons";


  private static final String TAG = "ReceiverPlugin";

  private CallbackContext onNewIntentCallbackContext = null;
  private CallbackContext volumeCallbackContext = null;

  // Usb device related variables
  private UsbManager mUsbManager;
  private UsbDevice mUsbDevice;
  private boolean mUsbConnected = false;
  private BroadcastReceiver mUsbReceiver;
  private CallbackContext mUsbDeviceCallbackContext = null;
  private CordovaInterface mCordova;
  private IntentFilter mFilter;

  private AudioManager mAudioManager;

  // Flic variables
  private boolean isScanning;
  private Flic2Manager flicManager = null;
  private CallbackContext flicCallbackContext = null;

  /**
   * Constructor.
   */
  public TabSINTNative() {
  }


  /**
   * Sets the context of the Command. This can then be used to do things like
   * get file paths associated with the Activity.
   *
   * @param cordova The context of the main Activity.
   * @param webView The CordovaWebView Cordova is running in.
   */
  @Override
  public void initialize(CordovaInterface cordova, CordovaWebView webView) {
    super.initialize(cordova, webView);

    mUsbManager = (UsbManager)cordova.getActivity().getSystemService(Context.USB_SERVICE);
    mCordova = cordova;
    flicManager = Flic2Manager.initAndGetInstance(mCordova.getActivity().getApplicationContext(), new Handler());
    mFilter = new IntentFilter();
    mFilter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
    mFilter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
    mUsbReceiver = new BroadcastReceiver() {
      public void onReceive(Context context, Intent intent) {
          String action = intent.getAction();
          if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {
              mUsbDevice = null;
              sendUsbDeviceInfo();
          } else if (UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action)) {
              mUsbDevice = (UsbDevice)intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
              sendUsbDeviceInfo();
          }
      }
    };
    mAudioManager = (AudioManager)this.cordova.getActivity().getSystemService(Context.AUDIO_SERVICE);
  }



  /**
   * Executes the request.
   *
   * This method is called from the WebView thread. To do a non-trivial amount of work, use:
   *     cordova.getThreadPool().execute(runnable);
   *
   * To run on the UI thread, use:
   *     cordova.getActivity().runOnUiThread(runnable);
   *
   * @param action          The action to execute.
   * @param args            The exec() arguments.
   * @param callbackContext The callback context used when calling back into JavaScript.
   * @return                Whether the action was valid.
   *
   * @sa https://github.com/apache/cordova-android/blob/master/framework/src/org/apache/cordova/CordovaPlugin.java
   */
  @Override
  public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {

    if (action.equals(ACTION_INITIALIZE) || action.equals(ACTION_RESET_AUDIO) || action.equals(ACTION_SET_AUDIO)) {
      int volume = action.equals(ACTION_SET_AUDIO) ? args.getInt(0) : 100;
      setAudio(volume);
      if (getAudioVolume() != volume) {
        JSONObject r = new JSONObject();
        r.put("error", false);
        r.put("code", 99);
        r.put("volume", getAudioVolume());
        r.put("message", "Volume not properly set.");
        callbackContext.error(r);
        return false;
      } else {
        callbackContext.success();
      }
      return true;
    } else if (action.equals(ACTION_GET_AUDIO_VOLUME)) {
      JSONObject r = new JSONObject();
      r.put("success",true);
      r.put("volume", getAudioVolume());
      callbackContext.success(r);
      return true;
    } else if (action.equals(ACTION_SEND_EXTERNAL_DATA)) {
      return sendExternalData(args.getString(0), args.getString(1), callbackContext);
    } else if (action.equals(ACTION_GET_EXTERNAL_DATA_INTENT)) {
      return getExternalDataIntent(callbackContext);
    } else if (action.equals(ACTION_SET_EXTERNAL_DATA_INTENT_HANDLER)) {
      return setExternalDataIntentHandler(callbackContext);
    } else if (action.equals(ACTION_REGISTER_USB_DEVICE_LISTENER)) {
      return registerUsbDeviceListener(callbackContext);
    } else if (action.equals(ACTION_UNREGISTER_USB_DEVICE_LISTENER)) {
      return unregisterUsbDeviceListener(callbackContext);
    } else if (action.equals(ACTION_SCAN_FLIC_BUTTON)) {
      return scanFlicButton(callbackContext);
    } else if (action.equals(ACTION_DISCONNECT_FLIC_BUTTON)) {
      return disconnectFlicButton(args.getString(0), callbackContext);
    } else if (action.equals(ACTION_GET_CONNECTED_FLIC_BUTTONS)) {
      return getConnectedFlicButtons(callbackContext);
    } else {
      callbackContext.error("Invalid action");
      return false;
    }
  }

  /**
   * Scan for and connect to a Flic2 button.
   */
  public boolean scanFlicButton(final CallbackContext callbackContext) {
    if (flicCallbackContext == null) {
      flicCallbackContext = callbackContext;
    }
    if (isScanning) {
        flicManager.stopScan();
        isScanning = false;
    } else {
        // int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        // if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
        //     requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        //     return;
        // }
        isScanning = true;

        flicManager.startScan(new Flic2ScanCallback() {
            @Override
            public void onDiscoveredAlreadyPairedButton(Flic2Button button) {
            }

            @Override
            public void onDiscovered(String bdAddr) {
            }

            @Override
            public void onConnected() {
            }

            @Override
            public void onComplete(int result, int subCode, Flic2Button button) {
                isScanning = false;
                PluginResult pluginResult;
                if (result == Flic2ScanCallback.RESULT_SUCCESS) {
                    JSONObject r = new JSONObject();
                    try {
                        r.put("success", true);
                        r.put("event", "addButton");
                        r.put("button", button.getBdAddr());
                    } catch (JSONException e) {

                    }
                    pluginResult = new PluginResult(PluginResult.Status.OK, r);
                    pluginResult.setKeepCallback(true);
                    callbackContext.sendPluginResult(pluginResult);
                    button.addListener(new Flic2ButtonListener() {
                        @Override
                        public void onButtonUpOrDown(final Flic2Button button, boolean wasQueued, boolean lastQueued, long timestamp, boolean isUp, boolean isDown) {
                            if (wasQueued && button.getReadyTimestamp() - timestamp > 15000) { // This was from their sample app, but doesn't make sense to me...
                                // timestamp is supposedly already the time in milliseconds since this boot time, so this calculation doesn't make sense to me.
                                // Drop the event if it's more than 15 seconds old
                                return;
                            }
                            String eventValue = isDown ? "isDown" : "isUp";
                            JSONObject eventResponse = new JSONObject();
                            try {
                                eventResponse.put("success", true);
                                long ts = System.currentTimeMillis();
                                eventResponse.put("timestamp", ts);
                                eventResponse.put("event", eventValue);
                                eventResponse.put("button", button.getBdAddr());
                            } catch (JSONException e) {

                            }
                            PluginResult buttonEventResult = new PluginResult(PluginResult.Status.OK, eventResponse);
                            buttonEventResult.setKeepCallback(true);
                            flicCallbackContext.sendPluginResult(buttonEventResult);
                        }
                    });
                } else {
                    pluginResult = new PluginResult(PluginResult.Status.ERROR, "TabSINTNative.scanFlicButton error: " + Flic2Manager.errorCodeToString(result));
                    pluginResult.setKeepCallback(true);
                    callbackContext.sendPluginResult(pluginResult);
                }
            }
        });
    }
    return true;
  }

  public boolean disconnectFlicButton(String buttonId, CallbackContext callbackContext) {
      List<Flic2Button> allButtons = flicManager.getButtons();
      for (Flic2Button aButton : allButtons) {
          if (aButton.getBdAddr().equals(buttonId)) {
              flicManager.forgetButton(aButton);
              callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, "successfully removed button: " + buttonId));
              return true;
        }
      }
      callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "Did not disconnect, could not find button with id: " + buttonId));
      return false;
  }

  public boolean getConnectedFlicButtons(CallbackContext callbackContext) {
      List<Flic2Button> allButtons = flicManager.getButtons();
      JSONObject r = new JSONObject();
      JSONArray buttons = new JSONArray();
      for (Flic2Button aButton : allButtons) {
          buttons.put(aButton.getBdAddr());
      }
      try {
          r.put("success",true);
          r.put("buttons", buttons);
      } catch (JSONException e) {

      }

      callbackContext.success(r);
      return true;
  }

  public void setAudio(int volume) {
    int maxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
    int sysVolume = Math.round((float) (volume * (float )maxVolume / 100));
    if (sysVolume > maxVolume) {
        sysVolume = maxVolume;
    }
    mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, sysVolume, 0);
  }

  public int getAudioVolume() {
    int maxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
    int currSystemVol = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
    int volLevel = Math.round((float) (currSystemVol * 100.0 / (float) maxVolume));
    return volLevel;
  }


  /**
   * Send data to another app
   * This method requires another installed app with appropriate intent filters.
   *
   * @param appName         string.  ex:  com.creare.receiver
   * @param data            string.  data = JSON.stringify(dataObject)
   * @param callbackContext The callback context used when calling back into JavaScript.
   * @return                Boolean regarding success
   */
  public boolean sendExternalData(final String appName, final String data, final CallbackContext callbackContext){
    if (data != null && (data instanceof String)) {
      if (appName != null && (appName instanceof String)) {
        Context activityContext = this.cordova.getActivity().getApplicationContext();
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.setPackage(appName);
        sendIntent.putExtra("TABSINT_DATA_JSON_STRING", data);
        sendIntent.setType("text/plain");
        sendIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activityContext.startActivity(sendIntent);
        try {
          JSONObject r = new JSONObject();
          r.put("success",true);
          r.put("message", "Receiver.sendDataExternal");
          r.put("data", data);
          callbackContext.success(r);
        } catch (JSONException e) {

        }
        return true;
      } else {
        try {
          JSONObject r = new JSONObject();
          r.put("success",false);
          r.put("message", "Could not send data to another app - no appName specified.  Name format is 'com.myorg.myapp'.");
          callbackContext.error(r);
        } catch (JSONException e) {

        }
        return false;
      }
    } else {
      try {
        JSONObject r = new JSONObject();
        r.put("success",false);
        r.put("message", "Could not send data to another app - Data must be a string.  If data is any other type, use JSON.stringify().");
        callbackContext.error(r);
      } catch (JSONException e) {

      }
      return false;
    }
  }


  /**
   * Send a JSON representation of the cordova intent back to the caller
   *
   * @param callbackContext
   */
  public boolean getExternalDataIntent (final CallbackContext callbackContext) {
      Intent intent = cordova.getActivity().getIntent();
      String action = intent.getAction();
      String type = intent.getType();
      if (Intent.ACTION_SEND.equals(action) && type != null) {
        callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, getIntentJson(intent)));
        return true;
      } else {
        callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.NO_RESULT));
        return true;
      }

  }

  /**
   * Send the latest device info to the app.
  */
  private void sendUsbDeviceInfo() {
      if (mUsbDeviceCallbackContext != null) {
          mUsbConnected = mUsbDevice != null && mUsbDevice.getVendorId() == 11799 && mUsbDevice.getProductId() == 40963;
          PluginResult result;
          JSONObject usbJSON = new JSONObject();
          try {
              usbJSON.put("isUsbConnected", mUsbConnected);
              usbJSON.put("mUsbDevice", mUsbDevice != null ? mUsbDevice.getManufacturerName() : "undefined");
          }
          catch(Exception e) {
              result = new PluginResult(PluginResult.Status.ERROR, "TabSINTNative.sendUsbDeviceInfo error: " + e.getMessage());
              result.setKeepCallback(true);
              mUsbDeviceCallbackContext.sendPluginResult(result);
          }
          result = new PluginResult(PluginResult.Status.OK, usbJSON);
          result.setKeepCallback(true);
          mUsbDeviceCallbackContext.sendPluginResult(result);
      }

  }

  private boolean unregisterUsbDeviceListener(CallbackContext callbackContext) {
      if (mUsbDeviceCallbackContext != null) {
          mUsbDevice = null;
          mCordova.getActivity().getApplicationContext().unregisterReceiver(mUsbReceiver);
          mUsbDeviceCallbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, "This context is being removed."));
          callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, "Success: Removed UsbDeviceReceiver"));
          mUsbDeviceCallbackContext = null;
          return true;
      }
      callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "There is no UsbDeviceReceiver to remove."));
      return false;
  }

  private boolean registerUsbDeviceListener(final CallbackContext callbackContext) {
      if (mUsbDeviceCallbackContext == null) {
          mUsbDeviceCallbackContext = callbackContext;
          mCordova.getActivity().getApplicationContext().registerReceiver(mUsbReceiver, mFilter);

          // Check for current USB device
          mUsbDevice = null;
          HashMap<String, UsbDevice> usbDevices = mUsbManager.getDeviceList();
          if (usbDevices.values().size() > 0) {
              for (UsbDevice d : usbDevices.values()) {
                  mUsbDevice = d;
                  break;
              }
          }
          sendUsbDeviceInfo();
          return true;
      }
      mUsbDeviceCallbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, "UsbDeviceReceiver already registered."));
      return false;

  }

  /**
   * Register handler for onNewIntent event
   *
   * @param data
   * @param callbackContext
   * @return
   */
  public boolean setExternalDataIntentHandler (final CallbackContext callbackContext) {
      this.onNewIntentCallbackContext = callbackContext;

      PluginResult result = new PluginResult(PluginResult.Status.NO_RESULT);
      result.setKeepCallback(true);
      callbackContext.sendPluginResult(result);

      return true;
  }

  /**
   * Triggered on new intent
   *
   * @param intent
   */
  @Override
  public void onNewIntent(Intent intent) {
      PluginResult pluginResult;
      String action = intent.getAction();
      String type = intent.getType();
      if (this.onNewIntentCallbackContext != null) {
          if (Intent.ACTION_SEND.equals(action) && type != null) {
            pluginResult = new PluginResult(PluginResult.Status.OK, getIntentJson(intent));
            pluginResult.setKeepCallback(true);
            this.onNewIntentCallbackContext.sendPluginResult(pluginResult);
          } else {
            pluginResult = new PluginResult(PluginResult.Status.NO_RESULT);
            pluginResult.setKeepCallback(true);
            this.onNewIntentCallbackContext.sendPluginResult(pluginResult);
          }
      }
  }

  /**
   * Return JSON representation of intent attributes
   *
   * @param intent
   * @return
   */
  private JSONObject getIntentJson(Intent intent) {
      JSONObject intentJSON = null;

      try {
          intentJSON = new JSONObject();

          intentJSON.put("type", intent.getType());
          intentJSON.put("data", intent.getStringExtra("TABSINT_DATA_JSON_STRING"));

          return intentJSON;
      } catch (JSONException e) {
          Log.d(TAG, TAG + " Error thrown during intent > JSON conversion");
          Log.d(TAG, e.getMessage());
          Log.d(TAG, Arrays.toString(e.getStackTrace()));

          return null;
      }
  }

}
