(Fill out fields that are applicable)

## Summary

- TabSINT Version:
- WAHTS Firmware: 
- UUID: 
- Backend (TabSINT Server/Gitlab):
- Site / Group:
- Protocol: (drag and drop protocol file)

## Description

(Short description of the issue)

## Steps to reproduce

(How one can reproduce the issue - this is very important)

## Other

- Upload logs or attach the exported logs to the issue (drag and drop)
- Attach any relevant screenshots (drag and drop)

/label ~bug
