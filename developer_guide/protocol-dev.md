# Protocol Development Tools

The [TabSINT user guide](https://tabsint.org/docs/user-guide/protocols.html) goes into great depth about writing and developing protocols.
The source code has many features to help you debug and refine your protocols.

This document will describe more advanced tools to help in the protocol development process. 

## Validating a Protocol

The npm scripts have a built in validator to check the your protocol conforms to the master schema. To validate a protocol, run:

``` bash
$ npm run protocol-directory   # shorthand if protocol directory is in `www/protocols`
$ npm run validate path/to/protocol-directory
```

If the input is not a path (i.e. `mockProtocol`), the validator will look in the `www/protocols/` directory.

If the input is a path (i.e. `other-directory/mock`), the validator will look for the protocol inside that directory.
All input paths must be relative to the tabsint root directory.

## Testing a Protocol in the Browser

Serving the app provides a great way to test and iterate protocols quickly.
As described in [Building and Running](building-running.md), you can serve the app in the browser using the command: `npm run serve`
This server will update every time a change is in the source code.

#### Load the Protocol in the App

Create a new directory in the `src/` directory of the cloned tabsint repository titled `protocols`. Inside this new directory, make a new directory named for your protocol. Place the protocol files in this directory, including your `protocol.json`. 

Now, in your config file (`/src/tabsintConfig.json`) include a new key named `protocols` and set the value to be an array of all the protocol directory names you want to load. For instance, if your protocol is in a directory `myProtocol`, you would write into your config file:

```
{
  "build": "tabsint",
  "description": "Official tabsint release for android",
  "platform": "android",
  "protocols": [
    "myProtocol"
  ],
  ...
```

Now that your protocol is available to the source code, open TabSINT in the browser, ensure that you are in `AdminMode`, and navigate to the *Protocols* section of the **Admin Page**. You should see your protocol listed in the table. Select your protocol in this pane, then click `Load`. You should now be able to switch to Exam View and begin testing your protocol.

## Converting a calibrated streaming protocol to a WAHTS native protocol
#### Goal
Create a procedure to modify a protocol that currently uses a connected  VicFirth headset (with calibration) to play files, and instead use the “almost” identical protocol to play files from the SD card at the same target levels. 

#### First, write the media on the WAHTS SD card. [Example](https://gitlab.com/creare-com/tabsint/blob/master/tools/matlab/write_media.m)

  - Rescale and resample the media. 
  - Rename files according the WAHTS naming convention  which follows the [FAT 8.3 naming scheme](https://en.wikipedia.org/wiki/8.3_filename).

		* Limited to at most eight characters 
	    * Legal characters:
			- Upper case letters A–Z
			- Numbers 0–9
			- Space (though trailing spaces in either the base name or the extension are considered to be padding and not a part of the filename, also filenames with spaces in them must be enclosed in quotes to be used on a DOS command line, and if the DOS command is built programmatically, the filename must be enclosed in quadruple quotes when viewed as a variable within the program building the DOS command.)
			- ! # $ % & ' ( ) - @ ^ _ ` { } ~
		
		* Illegal characters:
			- " * + , / : ; < = > ? \ [ ] | 
  - [Put the media on the WAHTS](http://tabsint.org/docs/user-guide/cha.html#loading-audio-files)
  

#### Second, update `protocol.json`.

  - Replace `headset` parameter from `VicFirth` to `WAHTS`.
  - Delete `calibration` array.
  - Replace `wavfiles` array on each page with `chaWavFiles`.
  - Replace `wavfile` properties with `chaWavFiles` properties.
	  * `path` stays the same. Ensure it is the correct path that was writen to the WAHTS SD card.
	  * Set `UseMetaRMS` to true.
	  * Specify `Leq`. Default is [72, 72]. 
