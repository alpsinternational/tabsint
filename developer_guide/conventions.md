# Conventions

This section will detail the various conventions we would like to enforce in the TabSINT repository.

## Commit Messages

### Format

The format of the commit messages is sourced from [AngularJS Commit Message Conventions](https://gist.github.com/stephenparish/9941e89d80e2bc58a153)

```
[type]([reference]): [subject]

[body]

[footer]
```

Any line of the commit message should not be longer than 100 characters. This allows the message to be easier to read on github as well as in various git tools.

- `[type]`
  + feat (feature)
  + enh (enhancement)
  + fix (bug fix)
  + docs (documentation)
  + style (formatting, missing semi colons, …)
  + refactor
  + test (when adding missing tests)
  + chore (maintain)
- `reference`
  + Reference should a reference to an issue (i.e. #104) or a MR (i.e. !71)
- `[subject]` text
  + use imperative, present tense: `change` not `changed` nor `changes`
  + don't capitalize first letter
  + no dot (.) at the end
- `[body]`
  + just as in [subject] use imperative, present tense: `change` not `changed` nor `changes`
  + includes motivation for the change 
- `[footer]`
  + Breaking changes
    + All breaking changes have to be mentioned in footer with the description of the change, justification and migration notes

    ```
    BREAKING CHANGE: isolate scope bindings definition has changed and
        the inject option for the directive controller injection was removed.
        
        To migrate the code follow the example below
        
        Before:
        
        scope: {
          myAttr: 'attribute',
        }
        
        After:
        
        scope: {
          myAttr: '@',
        }
        
        The removed `inject` wasn't generally useful for directives so there should be no code using it.
    ```
  + Referencing issues
    + Closed bugs should be listed on a separate line in the footer prefixed with "Closes" keyword like this:
   
    ```
    Closes #234
    ```

### Examples

```
feat(#103): onUrlChange event (popstate/hashchange/polling)

Added new event to $browser:
- forward popstate event if available
- forward hashchange event if popstate not available
- do polling when neither popstate nor hashchange available

Breaks $browser.onHashChange, which was removed (use onUrlChange instead)
```


## Error Handling in App

Error handling in the app can consist of many pieces:

- logging error messages
- rejecting promises
- notifying the user of an error via an `alert`

A few conventions to maintain while writing error handlers in TabSINT

- Errors should always be passed as objects
- Error objects should adhere to the following format:

```
{
  msg: "string (required): formal user digestable error message that can be displayed to the user via a notification pop up",
  code: "string|number (optional): informal error code or error name that can be used by consuming methods",
  status: "string|number (optional): same as `code`, but `code` is preferred"
  err: "object (optional): error object returned from outside method to be passed to consuming method"
  data: "object (optional): data object with other pertinent fields in the error block",
}
```

### Error Notifications

- All error notifications should be spoken from the `TabSINT` perspective
  - i.e. "TabSINT encountered an issue...", "TabSINT failed to..."
