# FAQ / Common Issues

### When building the app on the tablet, the build scripts say `Build Success`, but no app shows up on the tablet.

The most likely cause of the error is that you have a version of TabSINT on the tablet that won't let you install a debug version over it. 
This error looks like `[FAILED_INCONSISTENT_CERTIFICATES]`. In this case, you should uninstall the version of TabSINT on the tablet, then try running the command again.

Cordova run commands can also fail when the android platform in the working copy is outdated. 
In this case, you should re-install the cordova platform by running the following commands in order in the tabsint root directory (android is used in this example):

```bash
$ npm run cordova -- platform rm android
$ npm run cordova -- platform add android
```
