
'use strict';

// tiny validator
var tv4 = require('tv4');
var _ = require('lodash');
var log = require('./util/log.js');
var process = require('process');

if (require.main === module) {
  // input protocol directory from command line
  var protocolDir = process.argv.slice(2)[0];  // Either a path to a directory or a directory in www/res/protocol/
  log.info('Validating protocol ' + protocolDir);

  var protocolFile;
  if (protocolDir.indexOf('/') > -1) {
    protocolFile = protocolDir.endsWith('/') ? protocolDir + 'protocol.json' : protocolDir + '/protocol.json';
  } else {
    protocolFile = 'www/protocols/' + protocolDir + '/protocol.json';
  }

  // try to load this file
  var p;
  try {
    p = require('../../'+protocolFile);
  } catch(e) {
    console.log('Failed to load protocol file from ' + protocolFile + ' with error: ' + JSON.stringify(e));
    process.exit(2);
  }

  // start validation process
  var errors = [];
  var msg;

  function addSchema(s) {

    // derive schema path, remove precedig 'definitions' if tv4 adds it
    var sPath = s.endsWith('.json') ? s : s + '.json';
    sPath = sPath.startsWith('definitions/definitions/') ? sPath.slice(12) : sPath;

    // load schema
    var schema = require('../../src/res/protocol/schema/' + sPath);

    // add schema
    tv4.addSchema(s, schema);

    // find out dependent schemas
    var missing = tv4.getMissingUris();

    // handle missing schemas one by one
    if (missing.length > 0) {
      addSchema(missing[0]);
    }

    return schema;
  }

  // convienence function for interpreting nested tv4 errors
  function getErrors(err) {
    if (!err.subErrors) {
      errors.push({dataPath: err.dataPath, message: err.message});
    } else {
      _.forEach(err.subErrors, function(sub) {
        getErrors(sub);
      });
    }
  }

  // add root, recursively will add all dependenent schemas
  var protocolSchema = addSchema('protocol_schema');

  // validate
  var valid = tv4.validate(p, protocolSchema, false, true);


  // handle missing schemas
  if (tv4.missing.length > 0) {
    errors.push('The following schemas are missing from validation: ' + JSON.stringify(tv4.missing, null, 2));
    valid = false;
  }

  // collect actual errors
  if (tv4.error) {
    getErrors(tv4.error);
  }

  // handle invalid
  if (!valid) {
    log.error('Protocol schema validation failed with the following errors: \n' + JSON.stringify(errors, null, 2));
    process.exit(1);
  } else {
    log.info('Protocol ' + protocolDir + ' successfully validated');
  }
}
