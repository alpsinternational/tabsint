
/**
 * Config file handling module
 * @module
 */
var config = module.exports = {};

// module depedencies
var log = require('./log.js'); // note that when you "require" paths are relative to this specific file
var fs = require('fs-extra');
var tv4 = require('tv4');
var process = require('process');

/**
 * Read config.json file into a javascript object
 *
 * @return {object} configuration object read from config.json
 */
config.read = function() {
  return JSON.parse(fs.readFileSync('src/tabsintConfig.json'));
};

/**
 * Write javascript config object out to config.json file
 *
 * @param {object} c - configuration object
 */
config.write = function(c) {
  fs.writeFileSync('src/tabsintConfig.json', JSON.stringify(c, null, 2));
};


/**
 * Load config.json into object and validate
 *
 * @return {object} Loaded config file that has been succesfully validated against the config-schema.json
 */
config.load = function() {

  var c = {};

  try {
    c = config.read();
  } catch(e) {
    log.debug('No config.json file available. Using example_config.json');
    config.copy('example_config.json');
    c = config.read();
  }

  return c;
};


/**
 * Copy custom configuration file into "src/tabsintConfig.json"
 * The method can only look for configuration files that are within the /config directory.
 * After copying the file, the method will validate the config file against the config_shema
 *
 * @param {string} [filename] - config filename to copy
 */
config.copy = function(filename) {

  // error our if no config file specified
  if (!filename) {
    log.error('Config filename required');
    process.exit(1);
  }

  // append .json to filename
  if (!filename.endsWith('.json')) {
    filename = filename + '.json';
  }

  // support --tabsint-admin from cli
  if (process.env.npm_config_tabsint_admin) {
    process.env.npm_config_tabsintadmin = process.env.npm_config_tabsint_admin
  }

  // append path [all paths relative to root directory]
  if (process.env.npm_config_tabsintadmin && process.env.npm_config_tabsintadmin !== 'true'){
    log.debug('--tabsintadmin path set to: ' + process.env['npm_config_tabsintadmin']);
    directory = process.env['npm_config_tabsintadmin'] + '/config/';
  } else if (process.env.npm_config_tabsintadmin && process.env['TABSINT_ADMIN']) {
    log.debug('--tabsintadmin path set to environment variable: ' + process.env['TABSINT_ADMIN']);
    directory = process.env['TABSINT_ADMIN'] + '/config/';
  } else if (process.env.npm_config_tabsintadmin && !process.env['TABSINT_ADMIN']) {
    log.error('No environment variable has been defined for TABSINT_ADMIN');
    directory = process.env['TABSINT_ADMIN'] + '/config/';
  } else if (!filename.startsWith('config/')) {
    directory = 'config/';
  }

  filename = directory + filename;
  log.debug('Copying config file from: ' + filename);

  // copy file to config.json
  try {
    fs.copySync(filename, 'src/tabsintConfig.json');
    log.info('Copied ' + filename + ' to src/tabsintConfig.json');
  } catch(e) {
    if (e.errno === -4058) {
      log.error('No config file found at path: ' + filename);
      process.exit(2);
    } else {
      log.error('Failed to copy config file: ' + filename, e);
      process.exit(2);
    }
  }

  var c;
  try {
    c = config.read();
    c.filename = filename.split('/').slice(-1)[0];
    config.write(c);
  } catch(e) {
    log.error('Failed to load config file ' + filename, e);
    process.exit(2);
  }

  config.validate(c);
};


/**
 * Validate config file against the config-schema
 * @param  {object} c - config object from config.json
 */
config.validate = function(c) {

  // check input
  if (!c) {
    log.error('No config object provided to validator');
    process.exit(1);
  }

  // load schema file
  try {
    var schema = JSON.parse(fs.readFileSync('config/config_schema.json'));
  } catch(e) {
    log.error('Failed while loading config schema: ', e);
    process.exit(1);
  }

  // validate config file
  var valid = tv4.validate(c, schema);

  // convienence function for interpreting nested tv4 errors
  var errors = [];
  function getErrors(err) {
    if (!err.subErrors) {
      errors.push({dataPath: err.dataPath, message: err.message});
    } else {
      _.forEach(err.subErrors, function(sub) {
        getErrors(sub);
      });
    }
  }

  // collect actual errors
  if (tv4.error) {
    getErrors(tv4.error);
  }

  // handle invalid
  if (!valid) {
    log.error('Config validation failed with the following errors', JSON.stringify(errors, null, 2));
    process.exit(1);
  }

  log.debug('Config validation passed');

};
