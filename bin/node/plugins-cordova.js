#!/usr/bin/env node

"use strict";

var config = require("./util/config.js");
var cmd = require("./util/cmd.js");
var log = require("./util/log.js");
var process = require("process");
var _ = require("lodash");

if (require.main === module) {
  var c = config.load();

  if (c.cordovaPlugins && c.cordovaPlugins.length > 0) {
    // iterate through each plugin
    _.forEach(c.cordovaPlugins, function(p) {
      // remove old plugin, don't fail if it doesn't exit
      log.info("Removing cordova plugin for plugin: " + p.package);
      var ret = cmd.run("npm run cordova -- plugin rm " + p.package, true);

      if (ret.error && ret.stderr.indexOf("not present in the project") !== -1) {
        log.info('No plugin "' + p.package + '" installed');
      } else if (ret.err) {
        log.error('Failed to remove plugin: "' + p.package + '"', ret.stderr);
        process.exit(2);
      }

      // install new plugin
      log.info("Installing cordova plugin: " + p.src);
      var ret = cmd.run("npm run cordova -- plugin add " + p.src + " --nosave");

      if (ret.error) {
        log.error("Failed to install cordova plugin for plugin: " + p.package, ret.stderr);
        process.exit(2);
      }
    });
  }
}
