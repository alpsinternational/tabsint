% Example analysis script that shows examples of how to use the TabsintResults processor
%
% Put some results files (.json) in this directory, then run this script
% For help, type help TabsintResults
% or help TabsintResults.[method]

clear all;

tr = TabsintResults;

% % load results
tr.loadresults();

% % load responses
% output = tr.loadresponses('page.responseArea'); % load page.responseArea struct as page_responseArea in addition to other default response fields

% % run functions
% output = tr.runfunctions('demo');

% % load audiometry
% output = tr.loadaudiometry();                       % load all audiometry tests
% output = tr.loadaudiometry('HughsonWestlake');    % load only HughsonWestlake tests
% output = tr.loadaudiometry('BekesyFrequency');    % load only BekesyFrequency tests
% output = tr.loadaudiometry('BHAFT');              % load only BHAFT tests

% % audiometry shorthand methods:
% output = tr.loadhw();          % load hughson westlake level tests ('HughsonWestlake')
% output = tr.loadflft();        % load original flft tests ('BekesyFrequency')
% output = tr.loadbhaft();       % load adaptive flft tests ('BHAFT')