% TabSINT Results Processor for MATLAB
%
% Enter 'methods TabsintResults' to see available methods.  Then enter
%  'help TabsintResults.X' to see detailed help a particular method X.
%
% This library relies on the MATLAB File Exchange file JSON encode/decode version 1.1 (8.79 KB) by L�a Strobino:  
%   - https://www.mathworks.com/matlabcentral/fileexchange/56214-json-encode-decode
%

classdef TabsintResults < handle
    
    % Public read-only variables
    properties (GetAccess=public, SetAccess=private)
        privatekey;
        publickey;
        raw;
        results;
        responses;
        audiometry;
        hw;
        bhaft;
        flft;
        hint;
        fetch;
        computro;
    end

    % Public read/write variables
    properties (Access=public)
        output;
        functions;
    end

    % Private variables
    % properties (Access=private)

    % end
    
	% Public Methods
	methods
    
		%% Constructor
		function this = TabsintResults()
            filepath = which('TabsintResults'); % In case you aren't in the same directory for analysis
            addpath([filepath(1:end-16),'json']);  % add json4mat to MATLAB path
        end
        
        %% Core Methods
        function publickey = generatekey(this, varargin)
        % generatekey generates encryption keys for for tabsint protocols
        % 
        % returns text to insert into `publicKey` field of tabsint protocol
        % generate private key file (defaults to `tabsint.pem`)
        % and public key file (defaults to `tabsint.pub`)
        % saves public and private key in current Matlab directory
        %
        % securely store the private key for results decryption
        %
        % EXAMPLE:
        %   tp = TabsintResults;
        %   publickey = tp.generatekey();  % generates private, public keys and loads public key text
        %   publickey = tp.generatekey('myprivate.pem');  % gerates specific private key `myprivate.pem` and loads public key text
   
            % confirm openssl installed
            this.checkopenssl(this);
            
            % get private key filename
            if ~isempty(varargin)
                if ischar(varargin{1})
                    privatekeyFilename = varargin{1};
                else
                    error('input filename must be a string');
                end
            else
                privatekeyFilename = 'tabsint.pem';
            end

            % always set publickey to private key name .pub
            publickeyFilename = strcat([privatekeyFilename(1:end-4), '.pub']);
            
            this.trlog(strcat(['private key: ', privatekeyFilename]));
            this.trlog(strcat(['public key: ', publickeyFilename]));

            % define some functions
            function [status, stdout] = opensslkeypair(privatekeyFilename)
                cmd = strcat(['openssl genrsa -out ', privatekeyFilename, ' 2048']);
                [status, stdout] = system(cmd);
                
                if status ~= 0
                    this.trlog(strcat(['failed to generate private key with status ', num2str(status)]));
                    this.trlog(strcat(['Output: ', stdout]));
                    error('failed to generate private key');
                end
            end
            
            function [status, stdout] = opensslpublickey(privatekeyFilename, publickeyFilename)
                cmd = strcat(['openssl rsa -in ', privatekeyFilename, ' -pubout -out ', publickeyFilename]);
                [status, stdout] = system(cmd);
                
                if status ~= 0
                    this.trlog(strcat(['failed to generate public key with status ', num2str(status)]));
                    this.trlog(strcat(['Output: ', stdout]));
                    error('failed to generate public key');
                end
            end
            
            function publickey = loadpublickey(publickeyFilename)
                publickey = fileread(publickeyFilename);
            end
            
            % check if privatekeyFilename already exists
            keyfile = dir(privatekeyFilename);
            
            % if keyfile exists, generate public key and load
            if ~isempty(keyfile)
                [~, ~] = opensslpublickey(privatekeyFilename, publickeyFilename);                
                
            % if it doesn't, generate it
            else
                [~, ~] = opensslkeypair(privatekeyFilename);
                [~, ~] = opensslpublickey(privatekeyFilename, publickeyFilename);
            end
            
            % save the public key filename
            this.publickey = publickeyFilename;
                        
            % load the public key as a string
            publickey = loadpublickey(publickeyFilename);

        end
        
        function decrypt(this, varargin)
        % decrypt result files using a private key that corresponds to the public key used in the protocol
        % 
        %
        % EXAMPLE:
        %   tr = TabsintResults;
        %   tr.decrypt();                % by default uses any *.pem file in the current directory
        %   tr.decrypt('tabsint2.pem');  % specify filename of private key
            
            % confirm openssl installed
            this.checkopenssl(this);

            % get private key filename
            if ~isempty(varargin)
                if ischar(varargin{1})
                    this.privatekey = varargin{1};
                else
                    error('private key filename must be a string');
                end
            else
                privatekeys = dir('*.pem');
                if length(privatekeys) < 1
                    error('no private keys (*.pem) found in current directory');
                elseif length(privatekeys) > 1
                    error('more than 1 private key (*.pem) found in current directory - specify the private key as an input to decrypt function (i.e. `tr.decrypt("tabsint.pem")`');
                else
                    this.privatekey = privatekeys(1).name;
                end
            end
            
            % load public key (we don't actually have to do this, but its a
            % good check that the public/private keys match)
            % this.generatekey(this.privatekey);
            this.trlog(strcat(['private key: ', this.privatekey]));
            
            % generic functions to run openssl commands
            function [status, stdout] = opensslcmd(cmd)
                [status, stdout] = system(cmd);
                
                if status ~= 0
                    this.trlog(strcat(['failed to run command with status ', num2str(status)]));
                    this.trlog(strcat(['Output: ', stdout]));
                    error(strcat(['failed to run openssl command: ', cmd]));
                end
            end
            
            % decrypt aes keys
            keyFiles = dir('*.key.enc');
            for i = 1:length(keyFiles)
                filename = keyFiles(i).name;
                binKeyFilename = strcat([filename(1:end-4), '.bin']);
                keyFilename = filename(1:end-4);
                
                cmd = strcat(['openssl base64 -d -A -in ', filename, ' -out ', binKeyFilename]);
                opensslcmd(cmd);
                cmd = strcat(['openssl rsautl -decrypt -inkey ', this.privatekey, ' -in ', binKeyFilename, ' -out ', keyFilename]);
                opensslcmd(cmd);
            end

            % decrypt results
            resultFiles = dir('*.json.enc');
            for i = 1:length(resultFiles)
                filename = resultFiles(i).name;
                resultFilename = filename(1:end-4);
                keyFilename = strcat([resultFilename, '.key']);
                
                cmd = strcat(['openssl enc -d -base64 -A -aes-256-cbc -md md5 -pass file:', keyFilename, ' -in ', filename, ' -out ', resultFilename]);
                opensslcmd(cmd);
            end
        
        end

        function raw = loadraw(this)
        % loadraw loads raw json result files in current directory.
        % 
        % No flatenning or filtering is performed on results.
        % The json files may contain single or multiple results.
        % This method creates a struct containing aggregated results and 
        % Saves the struct on the `raw` property.
        %
        % returns the `raw` struct containing all un-flattened exam results 
        %
        % EXAMPLE:
        %   tr = TabsintResults;
        %   raw = tr.loadraw();

            this.trlog('Loading TabSINT results');

            % Get list of files in the users current active directory
            fileList = dir('*.json');

            % Create an array of structs containing exam results
            raw = [];
            
            for i = 1:length(fileList)

                % read file
                fileString = fileread(fileList(i).name);

                % Check for files with single results, and prepend and append
                % with square brackets to make it an array of 1 result
                if fileString(1) == '{'
                    fileString = strcat('[',fileString,']');
                end

                % decode json results into matlab struct using json4mat `json_decode`
                % transpose so its compatible with Matlab 2017 `jsondecode`
                try
                    % uses `json4mat` library included in the same directory as `TabsintResults.m`
                    fileCellArray = json_decode(fileString);
                    fileStruct = transpose(cell2mat(fileCellArray));
                catch Exception
                    this.trlog(['Failed to decode json file: ', fileList(i).name]);
                    this.trlog('TabsintResults.m requires the library json4mat with method `json_decode`');
                    this.trlog('    json4mat is included in the tabsint repository in the `json` directory at this link: https://gitlab.com/creare-com/tabsint/tree/master/tools/matlab');
                    this.trlog('    Download the `json` direcotry and place it in the same directory as TabsintResults.m');
                    rethrow(Exception);

                    % try
                    %     % use Matlab2017 built in `jsondecode`
                    %     % This causes problems downstream - sticking to json_decode for now
                    %     fileStruct =  jsondecode(fileString);
                    % catch Exception
                    %     this.trlog('MATLAB 2017 and above includes this library with method `jsondecode`');
                    %     % error('Failed to decode json file');
                    % end
                end

                % Handle missing struct fields before concatenation
                % If struct to append has fewer fields:
                if length(raw)>0
                    
                    if all(ismember(fields(raw),fields(fileStruct)))==0
                        f = fields(raw);
                        idx = find(~ismember(fields(raw),fields(fileStruct)));
                        for l = 1:length(idx)
                            fileStruct(1).(f{idx(l)}) = [];
                        end   
                    end
                % If struct to append has more fields:   
                    if all(isfield(raw,fields(fileStruct))) == 0
                        missingKey = find(isfield(raw,fields(fileStruct))==0);
                        structFields = fields(fileStruct);
                        missingFields = structFields(missingKey);
                        for j=1:length(raw)
                            for k=1:length(missingFields)
                                raw(j).(missingFields{k}) = [];
                            end
                        end
                    end
                end
                
                raw = [raw; fileStruct];
                
                % store on object
                this.raw = raw;
            end

            if isempty(this.raw)
                this.trlog('No results found in current directory. Place result json files in this directory to load.')
                error('No results to load');
            end
        end

        function results = loadresults(this)
        % loadresults loads json result files in current directory and lightly flattens results for simpler processing.
        % 
        % The json files may contain single or multiple results.
        % This method creates a struct containing aggregated results and 
        % Saves the struct on the `results` property. 
        %
        % returns the `results` struct containing all exam results 
        %
        % EXAMPLE:
        %   tr = TabsintResults;
        %   results = tr.loadresults();  % loads all result files
            

            % load raw result files into MATLAB struct
            this.loadraw();
            
            this.trlog('Flattenning results');

            % save raw results to a new output variable for filtering/analysis
            results = this.raw;

            % move `results.testResults` keys to top level (to flatten struct slightly)
            keys = fieldnames(results(1).testResults);
            for i = 1:length(results)
                for j = 1:length(keys)
                    try
                        results(i).(keys{j}) = results(i).testResults.(keys{j});
                    catch Exception
                        results(i).(keys{j}) = [];
                        %results(i) = setfield(results(i), getfield(results(i).testResults, keys(j)));
                    end
                end
            end
            results = rmfield(results, 'testResults');  % remove sublevel `testResults` field

            % reorder structure fields to that less used columns are near the right
            % TODO

            % store on object
            this.results = results;

        end
        
        function output = runfunctions(this, varargin)
        % runfunctions runs loaded results through arbitrary functions 
        %
        % Each function must take one input argument that refers to this TabsintResults class (i.e. `tr`).
        % `tr.results` is the results struct loaded in the method `loadresults`.
        % `tr.responses` is the responses struct loaded in the method `loadresponses`.
        % `tr.output` is a struct for the user to customize and save analysis results 
        %
        % If a subdirectory `./functions` exists, this method will add this directory to the path.
        % Functions are evaluated in alphabetical order and must NOT begin with numerical characters.
        % 
        % returns the `tr.output` struct
        %
        % EXAMPLE:
        %   tr = TabsintResults;
        %   results = tr.loadresults();         % loads all result files  
        %   output = tr.runfunctions();         % runs all functions in `./functions` subdirectory
        %   output = tr.runfunctions('demo');   % runs only the 'demo.m' function (must be in ./functions directory or on MATLAB path)

            this.trlog('Running functions');

            % return if results have not been loaded
            this.resultsAvailable(this);

            % get function list from input, if specified
            if length(varargin) > 0
                this.functions = varargin;
            end

            % see if there is a subdirectory `./functions` and load those functions
            if length(dir('./functions')) > 0

                % add './functions/' to path
                addpath('./functions');

                % if the user does not specify a list of functions, then 
                % get a list of all functions in the subdirectory `./functions`
                if isempty(this.functions)
                    functionList = dir('functions/*.m');
                    this.functions = cell(1, length(functionList));         % initialize empty array
                    for i = 1:length(functionList)
                        this.functions{i} = regexprep(functionList(i).name, '\.[^\.]*$', '');  % remove .m from the end of function name
                    end
                end
            end

            % run each function with results
            for i = 1:length(this.functions)
                func = regexprep(this.functions{i}, '\.[^\.]*$', '');  % remove .m from the end of function name
                try
                    this.trlog(['Running function:    ', func, '.m']);
                    eval([func, '(this)']);   % run function(results, output)
                catch Exception
                    this.trlog(['Error while running function:    ', func, '(TabsintResults)']);

                    if any(strcmpi(func(1), {'0','1','2','3','4','5','6','7','8','9'}))
                        this.trlog('     --> function name cannot start with a number');
                    end

                    rethrow(Exception);
                end
            end

            % rm './functions/' from path
            rmpath('./functions');

            % store on object
            output = this.output;
        end

        function responses = loadresponses(this, varargin)
        % responses unrolls the `responses` array within each result into a single table of responses indexed by test
        %
        % the user can add fields the output struct by passing in field names as an input e.g. loadresponses('examType', 'ResultType') 
        % fields that are nested can be passed in using the dot notation i.e. loadresponses('examProperties.OutputChannel')
        % fields can be passed as a cell array i.e. loadresponses({'examType', 'ResultType', 'examProperties.OutputChannel'})
        %
        % returns the `responses` struct containg all responses
        %
        % EXAMPLE:
        %   tr = TabsintResults;
        %   results = tr.loadresults();                 % load results
        %   responses = tr.loadresponses();             % flatten responses into new struct
        %   responses = tr.loadresponses('examType');   % flatten responses into new struct and include page property `examType` in the responses struct
        %   responses = tr.loadresponses({'examType', 'ResultType', 'examProperties.OutputChannel'});   % flatten responses into new struct and include page properties in the responses struct            
        
            this.trlog('Loading responses');

            % initialize empty struct
            responses = struct();

            % return if results have not been loaded
            this.resultsAvailable(this);

            % default properties to put in responses table
            props = {'presentationId', 'page', 'response', 'responseStartTime', 'responseElapTimeMS'};
            
            % get extra properties from input, if specified, and concat
            inputProps = this.getVarargin(varargin);
            props = [props, inputProps];

            idx = 0;
            for i=1:length(this.results)
                for j=1:length(this.results(i).responses)
                    idx = idx + 1;

                    % set the result and response index value
                    responses(idx).resultidx = i;
                    responses(idx).responseidx = j;

                    % for property values within the response object
                    for prop = props
                        try
                            % if prop contains a '.', split it
                            prop = cell(java.lang.String(prop{1}).split('\.'));

                            % get response from either struct or cell array
                            if isstruct(this.results(i).responses)
                                response = this.results(i).responses(j);
                            else
                                response = this.results(i).responses{j};
                            end

                            % make sure property is actually in response, otherwise set to empty array
                            if any(ismember(fieldnames(response), prop{1}))

                                % if "prop" is length 2 (i.e. 'chaInfo.serialNumber'}), then recurse into object. Join the fields with an _
                                if length(prop) > 1 && any(ismember(fieldnames(response.(prop{1})), prop{2}))
                                    responses(idx).([prop{1}, '_', prop{2}]) = response.(prop{1}).(prop{2});

                                % otherwise just grab the prop value
                                else
                                    responses(idx).(prop{1}) = response.(prop{1});
                                end
                            elseif any(ismember(fieldnames(this.results(i)), prop{1}))  
                                 responses(idx).(prop{1}) = this.results(i).(prop{1});
                            else
                                responses(idx).(prop{1}) = [];
                            end

                        catch Exception
                            responses(idx).(prop{1}) = [];
                            this.trlog(['Error processing property "', prop{1}, '" in result ', num2str(i), ' - response ', num2str(j)]);
                            rethrow(Exception);
                        end
                    end
                end
            end

            % set responses to property
            this.responses = responses;
                
        end

        function csv = tocsv(this, varargin)
        % output csv file based on input struct. Defaults to `responses` property
        %
        % the user can add fields the output struct by passing in field names as an input e.g. loadresponses('examType', 'ResultType') 
        % fields that are nested can be passed in using the dot notation i.e. loadresponses('examProperties.OutputChannel')
        % fields can be passed as a cell array i.e. loadresponses({'examType', 'ResultType', 'examProperties.OutputChannel'})
        %
        % returns the `responses` struct containg all responses
        %
        % EXAMPLE:
        %   tr = TabsintResults;
        %   results = tr.loadresults();                 % load results
        %   responses = tr.loadresponses();             % flatten responses into new struct
        %   tr.tocsv();                                 % output csv
        %   tr.tocsv(results);                          % output results struct as csv
        %   tr.tocsv(results, 'filename.csv');          % output results struct as csv with filename
        %   tr.tocsv(results, 'filename.xls');          % output results struct as xls with filename
        %   tr.tocsv(results, 'filename.xls');          % output results struct as xls with filename
        

            % return if results have not been loaded
            this.resultsAvailable(this);

            % defaults 
            obj = this.responses;
            obj = rmfield(obj, 'page'); % remove struct inside

            filename = 'output.csv';

            % get extra properties from input, if specified, and concat
            args = this.getVarargin(varargin);

            % handle inputs
            if length(args) > 0
                 for i = 1:length(args)
                    if i == 1 && ~isempty(args(i))
                        obj = args(i);
                    end

                    if i == 2 && ~isempty(args(i))
                        filename = args(2);
                    end
                end
            end

            % TODO: remove any struct sub fields

            % create table from struct
            table = struct2table(obj)

            this.trlog(['Outputting to file "', filename, '"']);

            % write output
            writetable(table, filename)

        end

        
        %% Test Specific Parsers
        
        function audiometry = loadaudiometry(this, varargin)
        % Loads audiometry results
        %
        % There are multiple types of audiometry exams: 
        %   - "HughsonWestlake": Hughson Westlake algorithm at fixed frequency with level tracking
        %   - "HughsonWestlakeFrequency": Hughson Westlake algorithm at fixed level with frequency tracking
        %   - "BekesyLike": Bekesy algorithm at fixed frequency with level tracking
        %   - "BekesyFrequency": Bekesy algorithm at fixed level with frequency tracking (FLFT)
        %   - "BHAFT": Bekesy algorithm at fixed level with adaptive frequency tracking that transitions 
        %       to level tracking at the highest freuquency. 
        %
        % This method takes one optional input specifying the type of exam to load and process.
        % The input corresponds to the exam type field, which is retained in `this.responses.examType`.
        % 
        % EXAMPLE:
        %
        %   tr = TabsintResults;
        %   results = tr.loadresults();                         % load results
        %   audiometry = tr.loadaudiometry();                   % load all audiometry exam types
        %   audiometry = tr.loadaudiometry('HughsonWestlake');  % Loads only "HughsonWestlake"
        %   audiometry = tr.loadaudiometry('HughsonWestlakeFrequency');  % Loads only "HughsonWestlakeFrequency"
        %   audiometry = tr.loadaudiometry('BekesyLike');       % Loads only "BekesyLike"
        %   audiometry = tr.loadaudiometry('BekesyFrequency');  % Loads only "BekesyFrequency"
        %   audiometry = tr.loadaudiometry('BHAFT');            % Loads only "BHAFT" (adaptive FLFT)

            % return if results have not been loaded
            this.resultsAvailable(this);
            
            % get input
            args = this.getVarargin(varargin);

            % load responses with flft specific columns (some extra columns are pre-specified here to retain column order)
            props = {'subjectId', 'examType', 'Ear', 'ResultType', 'Threshold', 'ThresholdFrequency', 'Units', 'L', 'F', ...
                     'RetSPL', 'FalsePositive', 'examProperties', 'examProperties.F', 'examProperties.OutputChannel', 'examProperties.Level',  ... 
                     'chaInfo', 'chaInfo.serialNumber', 'slm','svantek'};
            this.loadresponses(props);
            
            % get responses using input examType (by default load all)
            inputs = {
                'HughsonWestlake',
                'HughsonWestlakeFrequency',
                'BekesyLike',
                'BekesyFrequency',
                'BHAFT'
            };

            % log
            if length(args) > 0
                this.trlog(['Loading audiometry responses for exams "', strjoin(args, ' '), '"']);
            else
                this.trlog('Loading audiometry responses');
            end


            % default to return none
            audrows = zeros(1, length(this.responses));

            if length(args) > 0
                 for i = 1:length(args)
                    if any(ismember(inputs, args{i}))
                        audrows = audrows | strcmp({this.responses.examType},args{i});

                    % unknown input type
                    else
                        this.trlog(['Input to loadaudiometry() ("', strjoin(args, ' '), '") is not one of "', strjoin(inputs, '" "'), '"']);
                        error('Unknown input');
                    end
                 end

            % load all types
            else
                for i = 1:length(inputs)
                    audrows = audrows | strcmp({this.responses.examType},inputs{i});            
                end
            end

            % get the specified rows
            audiometry = this.responses(find(audrows));
            
            % Process each response
            for i = 1:length(audiometry)
                
                % get subject ids from results
                audiometry(i).subjectId = this.results(audiometry(i).resultidx).subjectId;
                
                % make an "ear" column
                if isempty(audiometry(i).examProperties_OutputChannel)
                    audiometry(i).Ear = [];
                else
                    audiometry(i).Ear = audiometry(i).examProperties_OutputChannel(3);
                end

                % convert L into array instead of cell array
                if ~isempty(audiometry(i).L) && iscell(audiometry(i).L)
                    audiometry(i).L = cell2mat(audiometry(i).L);
                end  
                
                % convert F into array instead of cell array
                if ~isempty(audiometry(i).F) && iscell(audiometry(i).F)
                    audiometry(i).F = cell2mat(audiometry(i).F);
                end 

                % convert FalsePositive into an array instead of cell array
                if ~isempty(audiometry(i).FalsePositive)
                    audiometry(i).FalsePositive = cell2mat(audiometry(i).FalsePositive);
                end
            
                % process responses specific to examType
                if ( strcmp(audiometry(i).examType, 'HughsonWestlake') | strcmp(audiometry(i).examType, 'BekesyLike') )

                    % rename Frequency
                    audiometry(i).F = audiometry(i).examProperties_F;

                elseif ( strcmp(audiometry(i).examType ,'HughsonWestlakeFrequency') | strcmp(audiometry(i).examType ,'BekesyFrequency') )

                    % rename Threshold to ThresholdFrequency
                    audiometry(i).ThresholdFrequency = audiometry(i).Threshold;
                    audiometry(i).L = audiometry(i).examProperties_Level;

                    % remove Threshold for clarity
                    audiometry(i).Threshold = [];


                elseif strcmp(audiometry(i).examType ,'BHAFT')

                    % reset response and set to ThresholdFrequency or ResultType
                    audiometry(i).response = [];
                    if ~isempty(audiometry(i).ThresholdFrequency)
                        audiometry(i).response = audiometry(i).ThresholdFrequency;
                    else
                        audiometry(i).response = audiometry(i).ResultType;
                    end

                    % Calculate high frequency threshold if level Threshold level < 80 dB
                    if audiometry(i).Threshold < 80
                        audiometry(i).calcThreshFreq = 19023*2^((80-audiometry(i).Threshold)/90);
                    else
                        audiometry(i).calcThreshFreq = audiometry(i).ThresholdFrequency;
                    end
                end
            end
            
            % change names of fields to coorespond to the spec
            old = {};
            new = {};
            audiometry = this.rename(audiometry, old, new);

            % remove some fields
            rmfields = {
                'examProperties_OutputChannel',
                'examProperties_F',
                'examProperties_Level'
            };
            for i = 1:length(rmfields)
                try
                    audiometry = rmfield(audiometry, rmfields{i});
                catch
                end
            end

            % put object on struct
            this.audiometry = audiometry;
        end

        function hw = loadhw(this)
        % Loads Hughson Westlake Level ('HughsonWestlake') results
        % 
        % This is equivalent to `loadaudiometry('HughsonWestlake')`
        % 
        % EXAMPLE:
        %
        %   tr = TabsintResults;
        %   results = tr.loadresults();     % load results
        %   hw = tr.loadhw();               % process hughson westlake

            % wrap loadaudiometry
            hw = this.loadaudiometry('HughsonWestlake');
            
            % put object on struct
            this.hw = hw;
        end

        function bhaft = loadbhaft(this)
        % Loads Bekesy Highest Audible Frequency ('BHAFT') results and 
        % calculate the estimated high frequency threshold.
        % 
        % This is equivalent to `loadaudiometry('BHAFT')`
        %
        % See also `loadflft()` to load results from the original flft test ("BekesyFrequency")
        % 
        % EXAMPLE:
        %
        %   tr = TabsintResults;
        %   results = tr.loadresults();   % load results
        %   bhaft = tr.loadbhaft();       % process bhaft

            % wrap loadaudiometry
            bhaft = this.loadaudiometry('BHAFT');
            
            % put object on struct
            this.bhaft = bhaft;
        end
      
        function flft = loadflft(this, varargin)
        % Load Bekesy fixed level frequency tracking exam results ('BekesyFrequency')
        %
        % This method is equivalent to `loadaudiometry('BekesyFrequency')`
        %
        % See also `loadbhaft()` to load results from the adaptive flft test ('BHAFT')
        %
        % EXAMPLE:
        %
        %   tr = TabsintResults;
        %   results = tr.loadresults();             % load results
        %   flft = tr.loadflft();                   % load flft results

            % load flft
            flft = this.loadaudiometry('BekesyFrequency');

            % put object on struct
            this.flft = flft;
        end

        function hint = loadhint(this)
        % Loads HINT results and calculates word intelligability score (WI)
        % 
        % EXAMPLE:
        %   tr = TabsintResults;
        %   results = tr.loadresults();           % load results
        %   hint = tr.loadhint();     % process hint
            
            % return if results have not been loaded
            this.resultsAvailable(this);
            
            % load responses with audiometry specific columns (some extra columns are pre-specified here to retain column order)
            props = {'protocolId','protocolName','page','examType','examProperties','presentationId',...  
                    'responseStartTime', 'responseElapTimeMS','response','selectedWords'...
                    'SentencePath','CurrentSentence','CurrentSNR','numberCorrect','wordCount','examType'};
            this.loadresponses(props);
            
            this.trlog('Loading hint');
            
            % map responses to audiometry struct, taking only the HW presentations
            hint = this.responses(find(strcmp({this.responses.examType},'HINT')));
            
            % Process each response
            for i = 1:length(hint)
                
                % get subject ids from results
                %hint(i).subjectId = this.results(hint(i).resultidx).subjectId;
                
                hint(i).WIScore = hint(i).numberCorrect/hint(i).wordCount;
            end
            
            % put object on struct
            this.hint = hint;
        end
        function computro = loadcomputro(this)
        % Loads Computro results.
        % 
        % EXAMPLE:
        %   tr = TabsintResults;
        %   results = tr.loadresults(); % load results
        %   computro = tr.loadcomputro();     % process computro
            
            % return if results have not been loaded
            this.resultsAvailable(this);
            
%             props = {'subjectId', 'examType', 'Ear', 'ResultType', 'Threshold', 'ThresholdFrequency', 'Units', 'L', 'F', ...
%                      'RetSPL', 'FalsePositive', 'examProperties', 'examProperties.F', 'examProperties.OutputChannel', 'examProperties.Level',  ... 
%                      'chaInfo', 'chaInfo.serialNumber', 'slm','svantek'};
%             this.loadresponses(props);
            
            % load responses
            this.loadresponses({'tabletUUID'});
            
            % find indices of responses with presentationId ==
            % 'externalAppResponseArea and message == 'ThisisdatafromComputro'
            ind = [];
            ext = find(strcmp({this.responses.presentationId},'computro'));
            for jj = ext
                if isempty(this.responses(jj).response)
                    %do nothing
                elseif strcmp(this.responses(jj).response.message, 'This is data from Computro')
                    ind = [ind,jj];
                end
            end
            computro = struct([]);
            for residx = ind
                levels = struct([]);
                levels = this.responses(residx).response.data.levels;
                tabletUUID = this.responses(residx).tabletUUID;
                for level = 1:length(levels)
                    clips = struct([]);
                    clips = levels{1,level}.clips;
                    for clip = 1:length(clips)
                        wordPairs = struct([]);
                        wordPairs = clips{1,clip};
                        tmp(1:length(wordPairs.wordPairs)) = this.responses(residx).page.responseArea.dataOut.data.levels{level};
                        for i =1:length(wordPairs.wordPairs)
                            tmp(i).tabletUUID = tabletUUID;
                            tmp(i).resultidx = this.responses(residx).resultidx;
                            findsub = strcmp({this.responses.presentationId},'subjectId');
                            findres = [this.responses.resultidx] == tmp(i).resultidx;
                            if isempty(find(findsub & findres))== 0
                                tmp(i).subjectId = this.responses(find(findsub & findres)).response;
                            end
                            tmp(i).responseStartTime = this.responses(residx).responseStartTime;
                            tmp(i).responseElapTimeMS = this.responses(residx).responseElapTimeMS;
                            tmp(i).levelNum = level;
                            tmp(i).clipNum = clip;
                            tmp(i).wordPairNum = i;
                            tmp(i).targetAudioName = wordPairs.targetAudioName;
                            tmp(i).leftWord = wordPairs.wordPairs{i}.leftWord.content;
                            tmp(i).rightWord = wordPairs.wordPairs{i}.rightWord.content;
                            tmp(i).userSelectedWord = wordPairs.wordPairs{i}.userSelectedWord.content;
                            tmp(i).scoredAsCorrect = wordPairs.wordPairs{1,i}.scoredAsCorrect;
                            tmp(i).timedOut = wordPairs.wordPairs{1,i}.timedOut;
                            tmp(i).timedStart = wordPairs.wordPairs{1,i}.timeStart;
                            tmp(i).timedEnd = wordPairs.wordPairs{1,i}.timeEnd;
                            tmp(i).timeEndInClip = wordPairs.wordPairs{1,i}.timeEndInClip;
                            tmp(i).responseTime = wordPairs.wordPairs{1,i}.responseTime;
                            if isfield(levels{1,level},'activeSettings')
                                
                                tmp(i).activeSettings = levels{1,level}.activeSettings;
                            end
                            
                        end
                        clear i
                        if isempty(computro)==0
                            if isfield(computro, ['tabletUUID' tabletUUID])==0
                                computro(1).(['tabletUUID' tabletUUID])=struct([]); 
                            end
                        elseif isfield(computro, ['tabletUUID' tabletUUID])==0 
                            computro(1).(['tabletUUID' tabletUUID])=struct([]); 
                        end
                        computro(1).(['tabletUUID' tabletUUID]) = [computro(1).(['tabletUUID' tabletUUID]),tmp];  
                        clear tmp
                    end
                end
            end
            this.computro = computro;
            this.trlog(['Loading Computro: ' num2str(length(computro)) ' responses from ' num2str(length(ind)) ' results']);
        end
        
        function fetch = loadfetch(this)
        % Loads Fetch results.
        % 
        % EXAMPLE:
        %   tr = TabsintResults;
        %   results = tr.loadresults(); % load results
        %   fetch = tr.loadfetch();     % process fetch
            
            % return if results have not been loaded
            this.resultsAvailable(this);
            
            % load responses
            this.loadresponses({'tabletUUID'});
            
            % find indices of responses with presentationId ==
            % 'externalAppResponseArea and message == 'ThisisdatafromFetch'
            ext = find(strcmp({this.responses.presentationId},'fetch'));
            ind = [];
            for jj = ext
               if isempty(this.responses(jj).response)
                    %do nothing
               elseif strcmp(this.responses(jj).response.message, 'This is data from Fetch')
                    ind = [ind,jj];
               end
            end
            fetch = struct([]);
            for j = ind
                tabletUUID=this.responses(j).tabletUUID;
                tmp1 = this.responses(j).response.data;
                for i = 1:length(tmp1)
                    tmp2(1:length(tmp1{1,i}.turns)) = this.responses(j).page.responseArea.dataOut.data;
                    if length(tmp1{1,i}.turns)>0
                        for ii = 1:length(tmp1{1,i}.turns);
                            tmp2(ii).tabletUUID = tabletUUID;
                            tmp2(ii).resultidx = this.responses(j).resultidx;
                            findsub = strcmp({this.responses.presentationId},'subjectId');
                            findres = [this.responses.resultidx] == tmp2(ii).resultidx;
                            if isempty(find(findsub & findres))== 0
                                tmp2(ii).subjectId = this.responses(find(findsub & findres)).response;
                            end
                            tmp2(ii).responseStartTime = this.responses(j).responseStartTime;
                            tmp2(ii).responseElapTimeMS = this.responses(j).responseElapTimeMS;
                            tmp2(ii).dataNum = i;
                            tmp2(ii).levelNum = tmp1{1,i}.levelNum;
                            % add masker and signal SPL to results
                            currentSNR = tmp2(ii).EasiestSnrDb-(tmp2(ii).levelNum-1)*(tmp2(ii).EasiestSnrDb-tmp2(ii).HardestSnrDb)/tmp2(ii).MostDifficultLevel;
                            tmp2(ii).signalSPL = 50;
                            tmp2(ii).maskerSPL = tmp2(ii).signalSPL - currentSNR;
                            tmp2(ii).challengeSettings = tmp1{1,i}.challengeSettings;
                            tmp2(ii).levelEndReason = tmp1{1,i}.levelEndReason;
                            tmp2(ii).livesAtStart = tmp1{1,i}.livesAtStart;
                            tmp2(ii).turnNum = ii;
                            tmp2(ii).spokenStreetName = tmp1{1,i}.turns{1,ii}.spokenStreetName;
                            tmp2(ii).turnChoices = tmp1{1,i}.turns{1,ii}.turnChoices;
                            tmp2(ii).userChosenStreetName = tmp1{1,i}.turns{1,ii}.userChosenStreetName;
                            tmp2(ii).timeFromCueToChoice = tmp1{1,i}.turns{1,ii}.timeFromCueToChoice;
                            tmp2(ii).advanceDistanceChoiceMade = tmp1{1,i}.turns{1,ii}.advanceDistanceChoiceMade;
                            tmp2(ii).madeCorrectChoice = tmp1{1,i}.turns{1,ii}.madeCorrectChoice;
                            % move `challengeSettings` keys to top level (to flatten struct slightly)
                            keys = fieldnames(tmp2(ii).challengeSettings);
                            for k = 1:length(keys)
                                try
                                    tmp2(ii).(keys{k}) = tmp2(ii).challengeSettings.(keys{k});
                                catch Exception
                                    tmp2(ii).(keys{k}) = [];
                                end
                            end
                            % move `turnChoices` keys to top level (to flatten struct slightly)
                            try
                                tmp2(ii).turnChoice1 = tmp2(ii).turnChoices{1};
                                tmp2(ii).turnChoice2 = tmp2(ii).turnChoices{2};
                            catch
                                tmp2(ii).turnChoice1 = [];
                                tmp2(ii).turnChoice2 = [];
                            end
                        end
                    else
                        tmp2(1).tabletUUID = tabletUUID;
                        tmp2(1).resultidx = this.responses(j).resultidx;
                        findsub = strcmp({this.responses.presentationId},'subjectId');
                        findres = [this.responses.resultidx] == tmp2(1).resultidx;
                        if isempty(find(findsub & findres))== 0
                            tmp2(1).subjectId = this.responses(find(findsub & findres)).response;
                        end
                        tmp2(1).responseStartTime = this.responses(j).responseStartTime;
                        tmp2(1).responseElapTimeMS = this.responses(j).responseElapTimeMS;
                        tmp2(1).dataNum = i;
                        tmp2(1).levelNum = tmp1{1,i}.levelNum;
                        % add masker and signal SPL to results
                        currentSNR = tmp2(1).EasiestSnrDb-(tmp2(1).levelNum-1)*(tmp2(1).EasiestSnrDb-tmp2(1).HardestSnrDb)/tmp2(1).MostDifficultLevel;
                        tmp2(1).signalSPL = 50;
                        tmp2(1).maskerSPL = tmp2(1).signalSPL - currentSNR;
                        tmp2(1).challengeSettings = tmp1{1,i}.challengeSettings;
                        tmp2(1).levelEndReason = tmp1{1,i}.levelEndReason;
                        tmp2(1).livesAtStart = tmp1{1,i}.livesAtStart;
                        tmp2(1).turnNum = 0;
                        tmp2(1).spokenStreetName = [];
                        tmp2(1).turnChoices = [];
                        tmp2(1).userChosenStreetName = [];
                        tmp2(1).timeFromCueToChoice = [];
                        tmp2(1).advanceDistanceChoiceMade = [];
                        tmp2(1).madeCorrectChoice = [];
                        
                        % move `challengeSettings` keys to top level (to flatten struct slightly)
                        keys = fieldnames(tmp2(1).challengeSettings);
                        for k = 1:length(keys)
                            try
                                tmp2(1).(keys{k}) = tmp2(1).challengeSettings.(keys{k});
                            catch Exception
                                tmp2(1).(keys{k}) = [];
                            end
                        end
                        
                        % move `turnChoices` keys to top level (to flatten struct slightly)
                        try
                            tmp2(1).turnChoice1 = tmp2(1).turnChoices{1};
                            tmp2(1).turnChoice2 = tmp2(1).turnChoices{2};
                        catch
                            tmp2(1).turnChoice1 = [];
                            tmp2(1).turnChoice2 = [];
                        end
                    end
                    tmp2 = rmfield(tmp2, 'challengeSettings');  % remove sublevel `challengeSettings` field
                    tmp2 = rmfield(tmp2, 'turnChoices');  % remove sublevel `turnChoices` field
                    tmp2 = rmfield(tmp2, 'targets');  % remove sublevel `targets` field
                    tmp2 = rmfield(tmp2, 'masker');  % remove sublevel `masker` field
                           
                    if isempty(fetch)==0
                        if isfield(fetch, ['tabletUUID' tabletUUID])==0
                            fetch(1).(['tabletUUID' tabletUUID])=struct([]); 
                        end
                    elseif isfield(fetch, ['tabletUUID' tabletUUID])==0 
                        fetch(1).(['tabletUUID' tabletUUID])=struct([]); 
                    end
                    fetch(1).(['tabletUUID' tabletUUID]) = [fetch(1).(['tabletUUID' tabletUUID]),tmp2];  
                    clear tmp2
                end 
            end 
            
            this.fetch = fetch;
            this.trlog(['Loading Fetch: ' num2str(length(fetch)) ' responses from ' num2str(length(ind)) ' results']);
        end
    end
    


    % Private Methods
    methods (Access=private, Static=true)

        function trlog(msg)
        % display messages to the console
            fprintf(msg);
            fprintf('\n');
        end

        function resultsAvailable(this)
        % throw error if not results available
            if isempty(this.results)
                this.trlog('Results have not been loaded. Use `loadresults()` to load test results.');
                error('No Results');
            end
        end

        function obj = rename(obj, old, new)
        % rename all fields in an object
            if length(old) ~= length(new)
                error('Error while trying to rename fields in object. {old} labels are not the same length as {new}')
            end

            for i = 1:length(old)
                [obj.(new{i})] = obj.(old{i});
                obj = rmfield(obj,old{i});
            end
        end

        function args = getVarargin(inputArgs)
        % interpret varargin when passed from one method
        % on to the next
            if length(inputArgs) > 0
                if iscell(inputArgs{1})
                    args = inputArgs{1};
                else 
                    args = inputArgs;
                end
            else
                args = [];
            end
        end
        
        function checkopenssl(this)
        % see if openssl is installed on the system
            [status, ~] = system('openssl help');
            if status ~= 0
                this.trlog(strcat('This function requires `openssl v1.0.2` to be installed \n', + ...
                           'and on your system path. Please see https://slproweb.com/products/Win32OpenSSL.html \n', + ...
                           'and dowload "Win64 OpenSSL v1.0.2q" for a 64-bit system or \n', + ...
                           '"Win32 OpenSSL v1.0.2q" for a 32-bit system.\n\n', + ...
                           'Make sure the `openssl` binary is on your system path'));
                error('no openssl');
            end
        end

    end

end
