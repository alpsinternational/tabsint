
tr = TabsintResults;
results = tr.loadresults(); % load results
fetch = tr.loadfetch();     % process fetch % create table from struct
fields=fieldnames(fetch);
for f=1:length(fields)
    fieldname=fields{f};
    table = struct2table(fetch.(fieldname));
    writetable(table, ['fetch_' fieldname '.xls']); % write output
end

computro = tr.loadcomputro();     % process computro % create table from struct
fields=fieldnames(computro);
for f=1:length(fields)
    fieldname=fields{f};
    table = struct2table(computro.(fieldname));
    writetable(table, ['computro_' fieldname '.xls']); % write output
end