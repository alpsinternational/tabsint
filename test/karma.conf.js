// Karma configuration
// http://karma-runner.github.io/0.10/config/configuration-file.html

module.exports = function (config) {
  config.set({
    // base path, that will be used to resolve files and exclude
    basePath: '../',

    // Start these browsers, currently available:
    browsers: ['ChromeHeadlessCI'],

    // testing framework to use (jasmine/mocha/qunit/...)
    frameworks: ['jasmine', 'es6-shim'],
    reporters: ['mocha'],
    mochaReporter: {
      output: 'minimal',
      ignoreSkipped: true
    },

    // Chrome custom flags
    customLaunchers: {
      ChromeHeadlessCI: {
        base: 'ChromeHeadless',
        flags: ['--no-sandbox']
      }
    },

    client: { captureConsole: false },   // True: Show console.log messages, False: suppress console.log messages
    logLevel: config.LOG_ERROR,  // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG

    // timeout - sometimes this kills the tests
    browserNoActivityTimeout: 100000,

    // list of files / patterns to load in the browser
    files: [
      {pattern: 'www/test.spec.js', included: true},
      {pattern: 'www/**/*.js', included: false},
      {pattern: 'www/**/*.json', included: false}
    ],

    // generate js files from html templates
    preprocessors: {
      'www/views/*.html': 'ng-html2js',
      'www/scripts/**/*.html': 'ng-html2js',
      'www/scripts/**/*.js': ['coverage']
    },

    ngHtml2JsPreprocessor: {
      // strip this from the file path
      stripPrefix: 'www/',

      // setting this option will create only a single module that contains templates
      // from all the files, so you can load them all with module('foo')
      moduleName: 'htmls'
    },


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: false,


    // Continuous Integration mode
    // if true, it capture browsers, run tests and exit
    singleRun: true
  });
};
