/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

import "./indicators/indicators";
import "./build-details/build-details";
import "./debug-view/debug-view";
import "./header/header";
import "./active-tasks/active-tasks";
import "./authorize/authorize";
import "./results-warnings/results-warnings";
import "./response-areas/response-areas";
import "./change-pin/change-pin";
import "./log-maxRows/log-maxRows";
import "./partial-result/partial-result";
import "./cha-admin/cha-admin";
import "./cha-indicator/cha-indicator";
import "./cha-info/cha-info";
import "./svantek-admin/svantek-admin";
import "./flic-admin/flic-admin";

angular.module("tabsint.components", [
  "tabsint.components.response-areas",
  "tabsint.components.indicators",
  "tabsint.components.build-details",
  "tabsint.components.debug-view",
  "tabsint.components.header",
  "tabsint.components.active-tasks",
  "tabsint.components.authorize",
  "tabsint.components.results-warnings",
  "tabsint.components.partial-result",
  "tabsint.components.change-pin",
  "tabsint.components.log-maxRows",
  "tabsint.components.cha.admin",
  "tabsint.components.cha.indicator",
  "tabsint.components.cha.info",
  "tabsint.components.svantek.admin",
  "tabsint.components.flic.admin"
]);
