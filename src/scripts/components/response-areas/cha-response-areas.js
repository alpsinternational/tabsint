/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

/* jshint bitwise: false */
/* globals ChaWrap: false */

angular
  .module("tabsint.components.response-areas.cha-response-areas", [])
  .controller("ChaResponseAreaCtrl", function(
    $q,
    $scope,
    $timeout,
    adminLogic,
    cha,
    chaCheck,
    chaExams,
    disk,
    examLogic,
    logger,
    page,
    svantek
  ) {
    // Assume $scope.dm is defined from examLogic in parent scope.
    $scope.debugMode = disk.debugMode;
    $scope.cha = cha;
    $scope.hideExamProperties = page.dm.responseArea.hideExamProperties;
    $scope.chaExams = chaExams;

    if (page.result.response !== undefined) {
      page.dm.isSubmittable = true;
    } else {
      page.dm.isSubmittable = false;
    }

    // set callback for dropped connections
    cha.connectionDropCB = function() {
      logger.debug("CHA - Running cha connectionDropCB to reset Exam");
      cha.abortExams();
      resetExam();
    };

    function resetExamNested() {
      var deferred = $q.defer();
      chaExams.resetPage();
      $scope.chaError = false; // initialize errors to false

      page.result.chaInfo = chaExams.getChaInfo();

      $scope.skippable = page.dm.responseArea.skip;
      $scope.pausable = page.dm.responseArea.pause;

      // Exam definitions
      if (chaExams.complexExamType) {
        // do nothing
      } else {
        var examType = page.dm.responseArea.type.substring(3); // remove 'cha' from prefix
        chaExams.setup(examType, page.dm.responseArea.examProperties); // defines chaExams.examType and chaExams.examProperties
      }

      $scope.isAudiometryExam = chaCheck.isAudiometryExam(chaExams.examType);
      deferred.resolve();
      return deferred.promise;
    }

    function resetExam() {
      return resetExamNested()
        .then(function() {
          if (page.dm.responseArea.measureBackground === "ThirdOctaveBands") {
            chaExams.state = "measuringBackgroundNoise";
            return chaExams.runTOBifCalledFor();
          }
        })
        .then(function() {
          chaExams.state = undefined;
          if (page.dm.responseArea.autoBegin && cha.state === "connected") {
            return $scope.startExam(chaExams.examType, chaExams.examProperties);
          } else {
            chaExams.state = "start";
          }
        });
    }

    $scope.repeatChaExam = function() {
      resetExam();
      $timeout(function() {
        return $scope.startExam(chaExams.examType, chaExams.examProperties);
      }, 0); //todo $timeout may not be needed
    };

    // skip exam
    $scope.skipExam = function() {
      page.result.response = "skipped";
      examLogic.skip();
    };

    function switchToExamView() {
      chaExams.state = "exam";

      // reset the MainText and SubText to the original page. This will happen when a test is failed and the person repeats the test
      page.dm.instructionText = page.dm.examInstructions; // custom instructions for each response area - blank if no instructions provided
    }

    function startNoiseIfCalledFor() {
      if (angular.isDefined(page.dm.responseArea.maskingNoise)) {
        cha.startNoiseFeature(page.dm.responseArea.maskingNoise);
      }
    }

    // Start exam, pass exam properties to cha
    $scope.startExam = function(examType, examProperties) {
      return cha
        .requestStatusBeforeExam()
        .then(switchToExamView)
        .then(startNoiseIfCalledFor)
        .then(function() {
          if (angular.isDefined(page.dm.responseArea.delay)) {
            return $timeout(function() {
              return cha.queueExam(examType, examProperties);
            }, page.dm.responseArea.delay);
          } else {
            return cha.queueExam(examType, examProperties);
          }
        })
        .catch(function(err) {
          cha.errorHandler.main(err);
        });
    };

    cha.errorHandler.responseArea = function(err) {
      // Display error to user
      $scope.chaError = true;

      if (err.msg) {
        $scope.errorMessage = "Error: " + angular.toJson(err.msg);
      } else {
        $scope.errorMessage = "Internal error, please restart the exam";
      }

      // make page submittable
      page.dm.isSubmittable = true;
    };

    resetExam();
  });
