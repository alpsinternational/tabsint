/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as $ from "jquery";

/* jshint bitwise: false */
/* globals ChaWrap: false */

angular
  .module("tabsint.components.response-areas.cha.dpoae", [])
  .directive("dpoaeExam", function() {
    return {
      restrict: "E",
      templateUrl: "scripts/components/response-areas/dpoae/dpoae.html",
      controller: "DPOAEExamCtrl"
    };
  })
  .controller("DPOAEExamCtrl", function($scope, $q, cha, page, disk, chaExams, chaResults, logger, examLogic) {
    var Frequencies1, Frequencies2, nExams, currentN;
    var storedResults = {
      resultList: undefined
    };

    function startExam() {
      // TODO: refactor DPOAEs such that all exam properties are specified in the protocol and handled by TabSINT.
      Frequencies2 = [5000, 4000, 3000];
      Frequencies1 = [];
      for (var i = 0; i < Frequencies2.length; i++) {
        Frequencies1[i] = Frequencies2[i] / 1.2;
      }
      nExams = Frequencies2.length + 1;
      currentN = 1;
      storedResults.resultList = [];
      page.dm.isSubmittable = false;
      //if (disk.debugMode) {
      $scope.page.dm.questionMainText = "Running DPOAE exam " + currentN++ + "/" + nExams;
      //}
    }

    function finishExam() {
      if (disk.debugMode) {
        $scope.page.dm.questionMainText = "DPOAE exam(s) complete";
      }
      return examLogic.submit();
    }

    function storeResults(dpoaeResults) {
      dpoaeResults.examProperties = chaExams.examProperties;
      storedResults.resultList.push(dpoaeResults);
    }

    function presentResults() {
      chaExams.state = "results";
      page.dm.hideProgressbar = true;

      // Update page view, get rid of instructions
      $scope.page.dm.title = "Results";
      if (page.dm.id.indexOf("LEFT") > 0) {
        $scope.page.dm.questionMainText = "DPOAE Results - Left Ear";
        storedResults.channel = "left";
      } else if (page.dm.id.indexOf("RIGHT") > 0) {
        $scope.page.dm.questionMainText = "DPOAE Results - Right Ear";
        storedResults.channel = "right";
      } else {
        $scope.page.dm.questionMainText = "DPOAE Results, Channel Unknown";
        storedResults.channel = "unknown";
      }

      $scope.page.dm.instructionText = "";

      // convert integers to string results, save exam results to structure
      storedResults.chaInfo = chaExams.getChaInfo();

      if (storedResults.resultList[0].DpLow === angular.undefined) {
        storedResults.dataError = "No results returned";
        $scope.dataError = storedResults.dataError;
      }

      storedResults.examProperties = chaExams.examProperties;
      // push all results onto the dm
      page.result = $.extend({}, page.result, storedResults);

      // set default 'response' field
      page.result.response = "continue";

      $scope.dpoaeData = chaResults.createDPOAEData(page.result);
      page.dm.isSubmittable = true;
    }

    var waitFunction = function() {
      var runNextFrequency = function() {
        // reset state and results
        if (Frequencies1.length > 0) {
          logger.debug("running next F1: " + Frequencies1[Frequencies1.length - 1]);
          $scope.page.dm.questionMainText = "Running DPOAE exam " + currentN++ + "/" + nExams;

          // overwrite pertinent exam properties
          chaExams.examProperties.F1 = Frequencies1.pop();
          chaExams.examProperties.F2 = Frequencies2.pop();

          //            chaExams.runTOBifCalledFor()
          cha
            .requestStatusBeforeExam()
            .then(function() {
              return cha.queueExam("DPOAE", chaExams.examProperties);
            })
            .then(waitFunction)
            .catch(function(err) {
              cha.errorHandler.main(err);
            });
        } else {
          logger.debug("Finished frequency list");
          presentResults()
            .then(finishExam)
            .catch(function(err) {
              cha.errorHandler.main(err);
            });
        }
      };

      chaExams.wait
        .forReadyState()
        .then(function() {
          return cha.requestResults();
        })
        .then(function(results) {
          storeResults(results);
        })
        .catch(function(err) {
          cha.errorHandler.main(err);
        })
        .finally(runNextFrequency);
    };

    startExam();

    waitFunction();
  })

  .directive("dpoaePlot", function() {
    return {
      restrict: "E",
      template: '<div id="dpoaePlotWindow"></div>',
      controller: "DPOAEPlotCtrl",
      scope: {
        data: "="
      }
    };
  })

  .controller("DPOAEPlotCtrl", function($scope, d3Services) {
    d3Services.dpoaePlot("#dpoaePlotWindow", $scope.data);
  });
