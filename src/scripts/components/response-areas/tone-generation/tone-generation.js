/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

/* jshint bitwise: false */
/* globals ChaWrap: false */

angular
  .module("tabsint.components.response-areas.cha.tone-generation", [])
  .directive("toneGenerationExam", function() {
    return {
      restrict: "E",
      templateUrl: "scripts/components/response-areas/tone-generation/tone-generation.html",
      controller: "ToneGenerationExamCtrl"
    };
  })
  .controller("ToneGenerationExamCtrl", function($scope, $q, cha, chaExams, page, disk, logger) {
    $scope.chaExamInProgress = false;
    var pollInterval;

    function startExam() {
      pollInterval = setInterval(pollExam, 500);
      page.dm.isSubmittable = false;
      var duration = "" + chaExams.examProperties.ToneDuration / 1000;
      if (disk.debugMode) {
        $scope.page.dm.questionMainText +=
          "Generating a " + chaExams.examProperties.F + "kHz tone for " + duration + " seconds.";
      }
    }

    function finishExam() {
      $scope.chaExamInProgress = false;
      page.dm.isSubmittable = true;
      clearInterval(pollInterval);
    }

    function pollExam() {
      cha
        .requestStatus()
        .then(function(status) {
          if (status.State === 2) {
            // still running
          } else if (status.State !== 1) {
            return $q.reject({ code: 66, msg: "Cha is in an unknown state" });
          } else if (status.State === 1) {
            // done
            finishExam();
          }
        })
        .catch(function(err) {
          finishExam();
          cha.errorHandler.main(err);
        });
    }

    $scope.stopTone = function() {
      finishExam();
      try {
        cha.setSoftwareButtonState(1);
      } catch (e) {
        logger.warn("CHA Error aborting exams manually: " + e);
      }
    };

    startExam();
  });
