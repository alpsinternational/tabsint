/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";
import "angular-mocks";
import "../../app";

beforeEach(angular.mock.module("tabsint"));

describe("Response Areas", function() {
  var $rootScope, examLogic, page, results, protocol, timeout, json, config, version;

  beforeEach(
    angular.mock.module(function($provide) {
      $provide.value("authorize", {
        modalAuthorize: function(targetFn) {
          targetFn();
        } // assume authorized.
      });
    })
  );
  beforeEach(
    angular.mock.module(function($provide) {
      $provide.value("chooseCha", {
        discover: function() {}
      });
    })
  );
  var scope;

  beforeEach(
    angular.mock.inject(function(
      _$rootScope_,
      $injector,
      _examLogic_,
      _page_,
      _results_,
      _protocol_,
      _json_,
      _config_,
      _version_,
      _$httpBackend_
    ) {
      $rootScope = _$rootScope_;
      examLogic = _examLogic_;
      page = _page_;
      results = _results_;
      protocol = _protocol_;
      timeout = $injector.get("$timeout");
      json = _json_;
      config = _config_;
      version = _version_;
      _$httpBackend_.whenGET("res/translations/translations.json").respond(200, "a string");

      config.load();
      version.load();
      $rootScope.$digest();
      protocol.override("res/protocol/mock", false).then(examLogic.reset);
      $rootScope.$digest();
    })
  );

  function flushSubmitTimeout() {
    if (page.result !== undefined) {
      if (page.result.response !== undefined) {
        timeout.flush(5001);
      }
    }
  }

  function flushActivateTimeout() {
    timeout.flush(101);
  }

  describe("Controller: MrtResponseArea", function() {
    // Initialize the controller and a mock scope
    beforeEach(
      angular.mock.inject(function($controller, $rootScope) {
        scope = $rootScope.$new(); // start a new scope

        examLogic.begin();
        flushActivateTimeout();
        examLogic.testing.goToId("ref1");
        flushActivateTimeout();

        $controller("MrtResponseAreaCtrl", {
          $scope: scope
        });
      })
    );

    it("should have two rows defined.", function() {
      expect(scope.rows).toBeDefined();
      expect(scope.rows.length).toEqual(2);
    });

    describe("BasicChoiceController", function() {
      // define two separate choices, and do some tests to make sure
      // the logic is correct.
      var choice1, choice2, ctrl1, ctrl2, scope1, scope2, result;
      choice1 = { id: "choice1" };
      choice2 = { id: "choice2" };
      result = {};
      scope1 = { choice: choice1, result: result };
      scope2 = { choice: choice2, result: result };

      beforeEach(
        angular.mock.inject(function($controller) {
          ctrl1 = $controller("BasicChoiceController", {
            $scope: scope1
          });
          ctrl2 = $controller("BasicChoiceController", {
            $scope: scope2
          });
        })
      );

      it("should initially not be chosen.", function() {
        expect(scope1.chosen()).toBeFalsy();
        expect(scope2.chosen()).toBeFalsy();
        expect(scope1.btnClass()).toEqual(scope2.btnClass());
      });

      //        it('should sense when one is chosen.', function () {
      //          scope1.choose();
      //          expect(scope1.chosen()).toBeTruthy();
      //          expect(scope2.chosen()).toBeFalsy();
      //          expect(scope1.btnClass()).not.toEqual(
      //            scope2.btnClass());
      //        });
      //        it('should toggle and switch correctly.', function () {
      //          scope1.choose();
      //          expect(scope1.chosen()).toBeTruthy();
      //          expect(scope2.chosen()).toBeFalsy();
      //          scope2.choose();
      //          expect(scope2.chosen()).toBeTruthy();
      //          expect(scope1.chosen()).toBeFalsy();
      //          scope2.choose();
      //          expect(scope1.chosen()).toBeFalsy();
      //          expect(scope2.chosen()).toBeFalsy();
      //        });
    });
  });

  describe("Controller: YesNoResponseArea", function() {
    // Initialize the controller and a mock scope
    beforeEach(
      angular.mock.inject(function($controller, $rootScope) {
        scope = $rootScope.$new();

        examLogic.begin();
        flushActivateTimeout();
        examLogic.testing.goToId("ref2");
        flushActivateTimeout();

        $controller("MultipleChoiceResponseAreaCtrl", {
          $scope: scope
        });
      })
    );

    it("should have two appropriate choices defined.", function() {
      scope.$digest();
      expect(scope.choices).toBeDefined();
      expect(scope.choices.length).toEqual(2);
      expect(scope.choices[0].id).toEqual("yes");
      expect(scope.choices[1].id).toEqual("no");
    });
  });

  describe("Controller: MultipleChoiceResponseArea", function() {
    // Initialize the controller and a mock scope
    beforeEach(
      angular.mock.inject(function($controller, $rootScope) {
        scope = $rootScope.$new();

        examLogic.begin();
        flushActivateTimeout();
        examLogic.testing.goToId("ref2");
        flushActivateTimeout();
      })
    );

    it("should have some appropriate choices defined 1.", function() {
      examLogic.testing.goToId("mc001");
      flushActivateTimeout();
      inject(function($controller) {
        $controller("MultipleChoiceResponseAreaCtrl", {
          $scope: scope
        });
      });
      scope.$digest();
      expect(scope.choices).toBeDefined();
      expect(scope.choices.length).toEqual(4);
      expect(scope.result.otherResponse).not.toBeDefined();
    });

    it('should show and define "other" if told to.', function() {
      examLogic.testing.goToId("mc002");
      flushActivateTimeout();
      inject(function($controller) {
        $controller("MultipleChoiceResponseAreaCtrl", {
          $scope: scope
        });
      });
      scope.$digest();
      expect(scope.choices.length).toEqual(4);
      expect(scope.choices[3].id).toEqual("Other");
      expect(scope.choices[3].text).toEqual("Another Color...");
      expect(scope.result.otherResponse).not.toBeDefined();
      expect(scope.enableOtherText).toBeFalsy();
      scope.result.response = "Other";
      scope.$digest();
      expect(scope.enableOtherText).toBeTruthy();
      page.result.response = "NotOther";
      expect(scope.result.otherResponse).not.toBeDefined();
    });
  });

  describe("Controller: buttonGridResponseArea", function() {
    // Initialize the controller and a mock scope
    beforeEach(
      angular.mock.inject(function($controller, $rootScope) {
        scope = $rootScope.$new();

        examLogic.begin();
        flushActivateTimeout();
        examLogic.testing.goToId("ref2");
        flushActivateTimeout();
      })
    );

    it("should have some appropriate choices defined 2.", function() {
      examLogic.testing.goToId("btngrid001");
      flushActivateTimeout();

      inject(function($controller) {
        $controller("ButtonGridResponseAreaCtrl", {
          $scope: scope
        });
      });

      scope.$digest();
      expect(scope.rows).toBeDefined();
      expect(scope.rows[0].choices).toBeDefined();
      expect(scope.rows[0].choices[0].id).toEqual("A");
    });
  });

  describe("Controller: imageMapResponseArea", function() {
    // Initialize the controller and a mock scope
    beforeEach(
      angular.mock.inject(function($controller, $rootScope) {
        scope = $rootScope.$new();

        examLogic.begin();
        flushActivateTimeout();
        examLogic.testing.goToId("ref2");
        flushActivateTimeout();
        examLogic.testing.goToId("imagemap001");
        flushActivateTimeout();

        $controller("ImageMapResponseAreaCtrl", {
          $scope: scope
        });
      })
    );

    it("should have some appropriate choices defined 3.", function() {
      scope.$digest();
      expect(scope.hotspots).toBeDefined();
      expect(scope.hotspots[0].shape).toEqual("rect");
      expect(scope.hotspots[3].coordinates).toEqual("50, 50, 20");
    });
  });

  describe("Controller: CheckboxResponseArea", function() {
    // Initialize the controller and a mock scope
    beforeEach(
      angular.mock.inject(function($rootScope) {
        scope = $rootScope.$new();

        examLogic.begin();
        flushActivateTimeout();
        examLogic.testing.goToId("ref2");
        flushActivateTimeout();
      })
    );

    it("should have some appropriate choices defined 4.", function() {
      examLogic.testing.goToId("cb001");
      flushActivateTimeout();

      inject(function($controller) {
        $controller("CheckboxResponseAreaCtrl", {
          $scope: scope
        });
      });

      scope.$digest();
      expect(scope.choices).toBeDefined();
      expect(scope.choices.length).toEqual(3);
      page.dm.isSubmittable = examLogic.getSubmittableLogic(page.dm.responseArea);
      expect(page.dm.isSubmittable).toBeTruthy();
    });

    it('should show and define "other" if told to.', function() {
      examLogic.testing.goToId("cb002");
      flushActivateTimeout();

      inject(function($controller) {
        $controller("CheckboxResponseAreaCtrl", {
          $scope: scope
        });
      });

      scope.$digest();
      expect(scope.choices.length).toEqual(4);
      expect(scope.choices[3].id).toEqual("Other");
      expect(scope.choices[3].text).toEqual("Some other color.");
      expect(page.result.otherResponse).not.toBeDefined();
      expect(scope.enableOtherText).toBeFalsy();
      page.dm.isSubmittable = examLogic.getSubmittableLogic(page.dm.responseArea);
      expect(page.dm.isSubmittable).toBeFalsy();
      page.result.response = '["Red","Other"]';
      scope.$digest();
      expect(page.dm.responseArea.other && _.includes(scope.responseAsList(), "Other")).toBeTruthy();
      page.result.response = '["Red"]';
      expect(page.result.otherResponse).not.toBeDefined();
      page.dm.isSubmittable = examLogic.getSubmittableLogic(page.dm.responseArea);
      expect(page.dm.isSubmittable).toBeTruthy();
    });
  });

  describe("Controller: IntegerResponseArea", function() {
    // Initialize the controller and a mock scope
    beforeEach(
      angular.mock.inject(function($controller, $rootScope) {
        scope = $rootScope.$new();

        examLogic.begin();
        flushActivateTimeout();
        examLogic.testing.goToId("ref2");
        flushActivateTimeout();
        examLogic.testing.goToId("int001");
        flushActivateTimeout();

        $controller("IntegerResponseAreaCtrl", {
          $scope: scope
        });
      })
    );

    it("should have an empty keypad at the start.", function() {
      scope.$digest();
      expect(scope.response).toEqual(undefined);
    });

    it("should set a number properly.", function() {
      scope.$digest();
      scope.response = "4";
      scope.$digest();
      expect(scope.response).toEqual("4");
    });

    it("should enforce min settings.", function() {
      scope.response = "-1";
      scope.$digest();
      expect(page.dm.isSubmittable).toBeFalsy();
      scope.response = "0";
      scope.$digest();
      expect(page.dm.isSubmittable).toBeTruthy();
    });

    it("should enforce max settings.", function() {
      scope.response = "300";
      scope.$digest();
      expect(page.dm.isSubmittable).toBeFalsy();
      scope.response = "120";
      scope.$digest();
      expect(page.dm.isSubmittable).toBeTruthy();
    });

    it('should enforce it being an integer when "float" flag is false (or undefined).', function() {
      scope.response = undefined;
      scope.$digest();
      expect(page.dm.isSubmittable).toBeFalsy();
      scope.response = "111.11";
      scope.$digest();
      expect(page.dm.isSubmittable).toBeFalsy();
      scope.response = "111";
      scope.$digest();
      expect(page.dm.isSubmittable).toBeTruthy();
    });
  });

  describe("Controller: LikertResponseArea", function() {
    // Initialize the controller and a mock scope
    beforeEach(
      angular.mock.inject(function($controller, $rootScope) {
        scope = $rootScope.$new();

        examLogic.begin();
        flushActivateTimeout();
        examLogic.testing.goToId("ref2");
        flushActivateTimeout();
        examLogic.testing.goToId("likert001");
        flushActivateTimeout();

        $controller("LikertResponseAreaCtrl", {
          $scope: scope
        });
      })
    );

    it("should push likert definition into a questions array", function() {
      expect(scope.questions.length).toEqual(1);
    });

    it('should push the responseArea properties into the first likert question if no "questions" array is defined in protocol', function() {
      expect(scope.questions[0].levels.length).toEqual(5);
      expect(scope.questions[0].text).toEqual(page.dm.responseArea.questionMainText);
    });

    it("should detect spaciousness property and redefine labels", function() {
      expect(scope.questions[0].topLabelsAreSpacious).toBeTruthy();
      expect(scope.questions[0].bottomLabelsAreSpacious).toBeTruthy();
      expect(scope.questions[0].topLabels.length).toEqual(3);
      expect(scope.questions[0].bottomLabels.length).toEqual(3);
    });

    it("should have defined some appropriate fields in the first question ", function() {
      expect(scope.questions[0].topLabels[0]).toEqual("It's awful!");
      expect(scope.questions[0].topLabels[1]).toEqual("It's acceptable.");
      expect(scope.questions[0].bottomLabels[2]).toEqual("It's THAT good.");
    });
  });

  describe("Controller: OmtResponseArea", function() {
    // Initialize the controller and a mock scope
    beforeEach(
      angular.mock.inject(function($rootScope) {
        scope = $rootScope.$new();

        examLogic.begin();
        flushActivateTimeout();
        examLogic.testing.goToId("ref2");
        flushActivateTimeout();
      })
    );

    it("should let us choose results as expected and give the expected answer..", function() {
      examLogic.testing.goToId("omt001");
      flushActivateTimeout();

      inject(function($controller) {
        $controller("OmtResponseAreaCtrl", {
          $scope: scope
        });
      });

      scope.$digest();
      expect(scope.sentence).toEqual("- - - - -");
      expect(page.result.response).not.toBeDefined();
      scope.omtchoose(0, "Alan");
      expect(scope.sentence).toEqual("Alan - - - -");
      expect(page.dm.isSubmittable).toBeFalsy();
      expect(page.result.response).not.toBeDefined();
      scope.omtchoose(0, "Alan");
      expect(page.result.response).not.toBeDefined();
      scope.omtchoose(0, "Alan");
      scope.omtchoose(1, "bought");
      scope.omtchoose(2, "some");
      scope.omtchoose(3, "big");
      expect(page.dm.id).toEqual("omt001");
      scope.omtchoose(4, "beds.");
      examLogic.submit();
      flushSubmitTimeout();
      flushActivateTimeout();
      // expect(scope.sentence).toEqual('Alan bought some big beds.');
      expect(results.current.testResults.responses[0].response).toEqual("Alan bought some big beds.");
      expect(page.dm.id).not.toEqual("omt001");
    });

    it("should let us choose results as expected and give the expected answer even for american list..", function() {
      examLogic.testing.goToId("omt002");
      flushActivateTimeout();

      inject(function($controller) {
        $controller("OmtResponseAreaCtrl", {
          $scope: scope
        });
      });

      scope.$digest();
      expect(scope.sentence).toEqual("- - - - -");
      expect(page.result.response).not.toBeDefined();
      console.log("page: " + angular.toJson(page.dm));
      scope.omtchoose(0, "Doris");
      expect(scope.sentence).toEqual("Doris - - - -");
      expect(page.dm.isSubmittable).toBeFalsy();
      expect(page.result.response).not.toBeDefined();
      scope.omtchoose(0, "Doris");
      expect(page.result.response).not.toBeDefined();
      scope.omtchoose(0, "Doris");
      scope.omtchoose(1, "bought");
      scope.omtchoose(2, "nineteen");
      scope.omtchoose(3, "large");
      expect(page.dm.id).toEqual("omt002");
      scope.omtchoose(4, "sofas.");
      examLogic.submit();
      flushSubmitTimeout();
      flushActivateTimeout();
      // expect(scope.sentence).toEqual('Alan bought some big beds.');
      expect(results.current.testResults.responses[0].response).toEqual("Doris bought nineteen large sofas.");
      expect(page.dm.id).not.toEqual("omt002");
    });
  });

  describe("Controller: OmtResponseAreaCtrl - multipleChoiceSelectionResponseArea", function() {
    // Initialize the controller and a mock scope
    beforeEach(
      angular.mock.inject(function($rootScope) {
        scope = $rootScope.$new();

        examLogic.begin();
        flushActivateTimeout();
        examLogic.testing.goToId("ref2");
        flushActivateTimeout();
      })
    );

    it("should have expected column headings and default audiometry text", function() {
      examLogic.testing.goToId("omt003");
      flushActivateTimeout();

      inject(function($controller) {
        $controller("OmtResponseAreaCtrl", {
          $scope: scope
        });
      });

      scope.$digest();
      expect(scope.sentence).toEqual("You will mark -, -, -, and - please."); //4 columns
      expect(page.dm.response).not.toBeDefined();
      expect(scope.columnLabels.length).toEqual(4);
      expect(scope.rows[0].length).toEqual(4); //columns
      expect(scope.rows.length).toEqual(10); //rows
      var colLabels = ["here", "are", "four", "columns"];
      expect(scope.columnLabels).toEqual(colLabels);
      expect(page.dm.isSubmittable).toBeFalsy();
      expect(page.result.response).not.toBeDefined();
    });

    it("should have expected correct response, and should not prematurely be submittable", function() {
      examLogic.testing.goToId("omt003");
      flushActivateTimeout();

      inject(function($controller) {
        $controller("OmtResponseAreaCtrl", {
          $scope: scope
        });
      });

      scope.$digest();
      expect(scope.sentence).toEqual("You will mark -, -, -, and - please."); //4 columns
      expect(page.result.response).not.toBeDefined();
      expect(page.dm.isSubmittable).toBeFalsy();

      scope.omtchoose(0, "Allen");
      expect(scope.sentence).toEqual("You will mark Allen, -, -, and - please.");
      expect(page.dm.isSubmittable).toBeFalsy();
      expect(page.result.response).not.toBeDefined();
      scope.omtchoose(0, "Allen");
      expect(scope.sentence).toEqual("You will mark -, -, -, and - please.");
      expect(page.result.response).not.toBeDefined();
      scope.omtchoose(2, "seven");
      expect(scope.sentence).toEqual("You will mark -, -, seven, and - please.");
      expect(page.result.response).not.toBeDefined();
      scope.omtchoose(0, "Steven");
      scope.omtchoose(1, "sees");
      scope.omtchoose(3, "sofas.");
      expect(scope.sentence).toEqual("You will mark Steven, sees, seven, and sofas. please.");
      expect(page.result.response).toBeDefined();
      expect(page.dm.isSubmittable).toBeTruthy();
      scope.omtchoose(1, "sees");
      expect(page.result.response).not.toBeDefined();
      expect(page.dm.isSubmittable).toBeFalsy();
      scope.omtchoose(1, "sees");
      expect(page.dm.isSubmittable).toBeTruthy();
      expect(page.dm.id).toEqual("omt003");
      examLogic.submit();
      expect(page.result.response).toBeDefined();

      flushSubmitTimeout();
      flushActivateTimeout();

      expect(results.current.testResults.responses[0].response).toEqual("Steven sees seven sofas.");
      expect(page.dm.id).not.toEqual("omt003");
    });
  });

  xdescribe("Controller: CustomResponseArea", function() {
    // Initialize the controller and a mock scope
    var scope2, advancedProtocol, $rootScope, $controller;
    beforeEach(
      angular.mock.inject(function(_$controller_, _$rootScope_, _advancedProtocol_) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        advancedProtocol = _advancedProtocol_;
      })
    );

    beforeEach(function(done) {
      scope = $rootScope.$new();
      //scope2 = $rootScope.$new();

      examLogic.begin();
      flushActivateTimeout();

      $controller("CustomResponseAreaCtrl", {
        $scope: scope
      });

      advancedProtocol.js.ready().then(function() {
        console.log("-- js ready");

        examLogic.testing.goToId("ref2");
        flushActivateTimeout();
        examLogic.testing.goToId("custom001");
        flushActivateTimeout();
        //            $controller('CustomJs1', {
        //              $scope: scope2
        //            });
        done();
      });
    });

    it("should have appropriate fields and load the javascript successfully", function() {
      expect(scope.html).toEqual("base/www/res/protocol/mock/customHtml.html");
      console.log("hi");
      expect(page.dm.id).toEqual("custom001");
      //expect(scope2.test).toEqual('customTest');
      //        expect(scope.loadSuccess).toBeTruthy();
      //        expect(scope.loadError).toBeFalsy();
    });

    //      it('should have interpreted customJs.js appropriately', function () {
    //        scope.submit();
    //        expect(scope.page.dm.isSubmittable).toBeTruthy();
    //        expect(scope.test).toEqual('customTest');
    //      });

    //      it('should move between custom areas appropriately', function () {
    //        scope.submit();
    //        expect(scope.page.dm.isSubmittable).toBeTruthy();
    //        expect(scope.page.result.response).toBeUndefined();
    //        expect(scope.test).toEqual('customTest');
    //        scop.tabResults();
    //        expect(scope.nResponses).toEqual(0);
    //      });
  });

  //TODO: include test for natoResponseArea
});
