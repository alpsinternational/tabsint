/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

/* jshint bitwise: false */
/* globals ChaWrap: false */

angular
  .module("cha.audiometry-table", [])
  .controller("AudiometryTableCtrl", function($scope, page, chaResults, gettextCatalog) {
    page.dm.hideProgressbar = true;
    page.dm.isSubmittable = true;

    // Update page view, get rid of instructions
    $scope.showSLMNoise = page.dm.responseArea.showSLMNoise;
    $scope.showSvantek = page.dm.responseArea.showSvantek;
    $scope.page.dm.title = gettextCatalog.getString("Audiometry Results");
    $scope.page.dm.questionMainText = "";
    $scope.page.dm.instructionText = "";

    // this function caclulates audiometry results based on the display id's passed. It will return an array of [resultsLost, audiogramData]
    $scope.resultsList = chaResults.createAudiometryResults(page.dm.responseArea.displayIds)[0];

    if (angular.isUndefined($scope.resultsList)) {
      $scope.page.dm.questionSubText = gettextCatalog.getString("No Results to Show");
    }
  });
