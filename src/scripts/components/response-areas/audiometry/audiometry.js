/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import "./audiometry-service";
import "./audiogram/audiogram";
import "./audiometry-table/audiometry-table";
import "./audiometry-properties/audiometry-properties";
import "./software-button/software-button";

/* jshint bitwise: false */
/* globals ChaWrap: false */

angular
  .module("tabsint.components.response-areas.cha.audiometry", [
    "cha.audiometry-service",
    "cha.audiogram",
    "cha.audiometry-table",
    "cha.audiometry-properties",
    "cha.software-button"
  ])
  .directive("audiometryExam", function() {
    return {
      restrict: "E",
      templateUrl: "scripts/components/response-areas/audiometry/audiometry.html",
      controller: "AudiometryExamCtrl"
    };
  })

  .controller("AudiometryExamCtrl", function(
    $scope,
    $q,
    $timeout,
    cha,
    chaExams,
    chaCheck,
    chaResults,
    examLogic,
    page,
    logger,
    chaAudiometryService
  ) {
    chaAudiometryService.reset(); // set buttonPressCount and examDone, cancel any simulateTimer currently running

    $scope.useSoftwareButton = chaExams.examProperties.UseSoftwareButton;

    // Results Presentation
    function presentResults() {
      function switchToResultsState() {
        chaExams.state = "results";
        page.dm.hideProgressbar = true;
        $scope.page.dm.title = "Results";

        $scope.page.dm.questionMainText = page.dm.responseArea.resultMainText || "Results";
        $scope.page.dm.questionSubText = page.dm.responseArea.resultSubText || "";
        $scope.page.dm.instructionText = "";
      }

      // if using the softwareButton, no button press, and showMessageIfNoButtonPress, change autoSubmit to false, go to results state, and define results message
      if (
        chaExams.examProperties.UseSoftwareButton &&
        page.dm.responseArea.showMessageIfNoResponse &&
        chaAudiometryService.buttonPressCount === 0
      ) {
        logger.warn("No user button presses during this audiometry exam - showing user a message about it");
        page.dm.responseArea.autoSubmit = false; // turn off auto-submit so the user will see the message.
        switchToResultsState();
        $scope.page.dm.questionMainText =
          page.dm.responseArea.noResponseCustomMessage ||
          "It looks like you did not press the button at all during the last test.  Please make sure to press the button if you hear any sound.";
        return; // don't bother setting up all the plots/tables below - there's nothing to show because the user didn't press the button once.
      }

      if (!page.dm.responseArea.autoSubmit) {
        switchToResultsState();

        $scope.ResultType = page.result.ResultType;
        $scope.examType = page.result.examType;
        $scope.ThresholdFrequency = page.result.ThresholdFrequency;
        $scope.Threshold = page.result.Threshold;
        $scope.Units = page.result.Units;

        // plotting
        var figures = chaResults.createAudiometryResults();

        $scope.audiogramData = figures[1];

        $scope.showAudiogram = false;
        $scope.showLevelProgression = false;
        var plotProperties = page.dm.responseArea.plotProperties || {};

        if (
          plotProperties.displayAudiogram !== angular.undefined &&
          $scope.audiogramData !== angular.undefined &&
          !page.result.examProperties.Screener
        ) {
          $scope.showAudiogram = plotProperties.displayAudiogram;
        }

        if (plotProperties.displayLevelProgression && !page.result.examProperties.Screener) {
          var levelProgressionData = chaResults.createLevelProgressionData(page.result);
          $scope.levelProgressionData = levelProgressionData;
          $scope.showLevelProgression = true;
        }
      }
    }

    function repeatIfNecessary() {
      var deferred = $q.defer();
      if (page.dm.responseArea.repeatIfFailedOnce) {
        if (page.result.ResultType !== "Threshold") {
          if (page.result.failedOnce) {
            page.result.failedTwice = true;
            deferred.resolve();
          } else {
            page.result.failedOnce = true;
            if (chaAudiometryService.buttonPressCount === 0) {
              // TODO - do we want to pop an alert?  make this a responseArea option?
              $scope.page.dm.questionSubText =
                page.dm.responseArea.resultNoPressSubText ||
                "It looks like you did not press the button at all during the last test.  Please try again, and make sure to press the button if you hear any sound.";
            } else {
              var subText = "Nice work, but we need to try that test again.";
              if (chaCheck.isBekesyExam(chaExams.examType)) {
                subText +=
                  " Press and hold the red button as soon as you hear the beeping tone, and release the button as soon as you no longer hear it.";
              }
              subText += " Please make sure to listen carefully.";
              page.dm.questionSubText = subText;
            }

            chaExams.state = "start";
            deferred.reject({ code: 404, msg: "repeating exam" });
          }
        } else {
          deferred.resolve();
        }
      } else {
        deferred.resolve();
      }
      return deferred.promise;
    }

    function getNotesIfNecessary() {
      if (page.dm.responseArea.getNotesIfFailedTwice) {
        if (page.result.failedTwice) {
          page.dm.questionSubText =
            "<div>Unfortunately the last test was unsuccessful a second time.  Please hand the tablet to an administrator.<br><br>Administrators:  Please add any relevant notes about the previous audiometry test below.<div>";
          chaExams.state = "notes";
          if (page.dm.responseArea.autoSubmit) {
            page.dm.responseArea.autoSubmit = false;
          } // so they can actually enter notes!
        }
      }
    }

    $scope.pauseAudiometryExam = function() {
      logger.info("User paused CHA exam");
      chaExams.state = "start";
      cha.abortExams();
    };

    // This is a normal audiometry exam - finish it as usual
    chaAudiometryService.examDone = false;
    chaExams.wait.forReadyState().then(function() {
      chaAudiometryService.examDone = true;

      // check to make sure we have not paused.  If state is still exam, request results
      if (chaExams.state === "exam") {
        cha
          .requestResults()
          .then(chaAudiometryService.processResults)
          .then(repeatIfNecessary)
          .then(cha.stopNoiseFeature)
          .then(function() {
            if (!chaExams.complexExamType) {
              presentResults(); // only present results for individual audiometry pages - not for an audiometryList
            }
          })
          .then(getNotesIfNecessary)
          .then(function() {
            if (!chaExams.complexExamType) {
              page.dm.isSubmittable = true;
            }
            if (page.dm.responseArea.autoSubmit) {
              examLogic.submit();
            }
          })
          .catch(function(err) {
            if (err.msg !== "repeating exam") {
              cha.errorHandler.main(err);
            }
          });
      }
    });
  });
