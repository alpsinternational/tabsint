/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

/* jshint bitwise: false */
/* globals ChaWrap: false */

angular
  .module("cha.audiogram", [])
  .directive("audiogramPlot", function() {
    return {
      restrict: "E",
      template: '<div id="audiogramPlotWindow"></div>',
      controller: "AudiogramPlotCtrl",
      scope: {
        data: "="
      }
    };
  })

  .controller("AudiogramPlotCtrl", function($scope, d3Services) {
    d3Services.audiogramPlot("#audiogramPlotWindow", $scope.data);
  })
  .controller("AudiogramCtrl", function($scope, page, chaResults, gettextCatalog) {
    page.dm.hideProgressbar = true;
    page.dm.isSubmittable = true;

    // Update page view, get rid of instructions
    $scope.page.dm.title = gettextCatalog.getString("Audiogram");
    $scope.page.dm.questionMainText = "";
    $scope.page.dm.instructionText = "";

    $scope.audiogramData = chaResults.createAudiometryResults(page.dm.responseArea.displayIds)[1];

    if ($scope.audiogramData) {
      $scope.showAudiogram = true;
    }
  });
