/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";

angular
  .module("tabsint.components.response-areas.mrt", [])

  .controller("MrtResponseAreaCtrl", function($scope, page) {
    // container spacing
    $scope.spacing = {};
    if (page.dm.responseArea.verticalSpacing) {
      _.extend($scope.spacing, {
        "padding-bottom": page.dm.responseArea.verticalSpacing
      });
    }

    if (page.dm.responseArea.horizontalSpacing) {
      _.extend($scope.spacing, {
        "padding-right": page.dm.responseArea.horizontalSpacing / 2,
        "padding-left": page.dm.responseArea.horizontalSpacing / 2
      });
    }

    $scope.gradeResponse = false;
    $scope.showCorrect = false;

    function makeRows() {
      var choices = page.dm.responseArea.choices;

      // added switches for showing feedback - inside makeRows so they update each watch
      $scope.gradeResponse = false;
      $scope.showCorrect = false;

      // callback for page.dm
      if (page.dm.responseArea.feedback) {
        page.dm.showFeedback = function() {
          if (page.dm.responseArea.feedback === "gradeResponse") {
            $scope.gradeResponse = true;
          } else if (page.dm.responseArea.feedback === "showCorrect") {
            $scope.showCorrect = true;
          }
        };
      }

      // create rows if choices exists
      if (choices && typeof choices === "object" && choices.length) {
        $scope.rows = [choices.slice(0, 3), choices.slice(3, 6)];
      } else {
        $scope.rows = [];
      }
    }

    makeRows();
  });
