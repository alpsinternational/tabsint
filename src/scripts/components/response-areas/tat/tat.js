/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as $ from "jquery";

/* jshint bitwise: false */
/* globals ChaWrap: false */

angular
  .module("tabsint.components.response-areas.cha.tat", [])
  .directive("tatExam", function() {
    return {
      restrict: "E",
      templateUrl: "scripts/components/response-areas/tat/tat.html",
      controller: "tatExamCtrl"
    };
  })
  .controller("tatExamCtrl", function($scope, cha, examLogic, page, chaExams, logger, $timeout, $q) {
    /**********************************************************
     *  Setup - these steps happen as soon as the ctrl loads  *
     *********************************************************/

    var EXAM_STATES = {
      //subset of chaCodes.h exam states as defined by TAT spec
      PLAYING: 1,
      DONE: 3,
      WAITING_FOR_SUBMISSION: 4
    };
    var INITIAL_DELAY = 500; // need to wait until the exam is queued before asking for results
    var POLLING_DELAY = 400; //polling delay in ms
    var SUBMIT_DELAY = 800; // delay to show feedback
    var pollingTimeout, submitTimeout, active;
    var requestingResults = false; // need to make sure we don't request results twice

    var defaultNBlocks = 4;
    var defaultNExclude = 1;
    var defaultNPresentations = 10;

    if (angular.isUndefined(chaExams.examProperties.NBlocks)) {
      chaExams.examProperties.NBlocks = defaultNBlocks;
    }
    if (angular.isUndefined(chaExams.examProperties.NExclude)) {
      chaExams.examProperties.NExclude = defaultNExclude;
    }
    if (angular.isUndefined(chaExams.examProperties.NPresentations)) {
      chaExams.examProperties.NPresentations = defaultNPresentations;
    }

    $scope.tatInstructions = "Preparing...";
    $scope.currentPresentation = 0; //only change when exam state is returned, or exam reset; 1-based, apparently
    $scope.NPresentations = chaExams.examProperties.NPresentations; //number of presentations, 10 default
    $scope.tatState = undefined; //assigned by getChaResult, equivalent to parseInt(tatResult.State)
    $scope.selected = -1; // will be updated when user makes selection
    $scope.buttonsDisabled = true;
    $scope.readyToProcess = false; // activated when the presentation info is set in "presentationHandler"

    // Handle page submission logic.
    // Put this on the scope so it can get recognized
    $scope.submittable = false;
    page.dm.isSubmittable = $scope.submittable;

    /*********************************************
     *                                           *
     ********************************************/

    //NOTE: the conversion from Protocol values of "Left", "Right", and "Both"
    //happens in cha-exams.js.
    // const EAR = {
    //   LEFT: 0,
    //   RIGHT: 1,
    //   BOTH: 2
    // };

    var tatResult; //will be assigned by getChaResult()

    /****************************************************
     *  Functions to determine block properties on page  *
     ****************************************************/

    //determines if a block is excluded by NExclude
    $scope.excluded = function(blockNumber) {
      return (
        blockNumber < chaExams.examProperties.NExclude ||
        blockNumber >= chaExams.examProperties.NBlocks - chaExams.examProperties.NExclude
      );
    };

    //determines whether user can select block after presentation
    $scope.selectable = function() {
      return $scope.tatState === EXAM_STATES.WAITING_FOR_SUBMISSION;
    };

    //returns true if blockNumber is the sound block containing the signal pulses
    $scope.isCorrect = function(blockNumber) {
      return tatResult && blockNumber === parseInt(tatResult.ToneIndex);
    };

    //determines whether button represents active playing sound blocks
    $scope.playing = function(blockNumber) {
      return $scope.tatState === EXAM_STATES.PLAYING && blockNumber === parseInt(tatResult.CurrentBlock);
    };

    // Clean up timers on responseArea exit OR on user input
    function stopTimer() {
      if (pollingTimeout) {
        $timeout.cancel(pollingTimeout);
      }
      if (submitTimeout) {
        $timeout.cancel(submitTimeout);
      }
    }

    // onExit function, to make sure popups and timers do not remain
    $scope.$on("$destroy", function() {
      active = false; // ensure the exam will not continue looping
      stopTimer();
    });

    function resetResults() {
      var presentationId = page.result.presentationId + $scope.currentPresentation + 1;
      page.result = {};
      page.result = {
        examType: chaExams.examType,
        presentationId: presentationId,
        responseStartTime: new Date()
      };
    }

    function reset() {
      // page result
      resetResults();
      // local variables
      $scope.selected = -1; //user-input
    }

    /****************************************
     * TAT exam results/status polling logic *
     *****************************************/

    //poll CHA and get intermediate results - this gives us info on which
    //sound block contains the signal pulse train, and also state of the exam,
    //including which block is currently being played
    function getExamStatus() {
      if (active) {
        // if we are already requesting results, wait until that request is fulfilled
        if (requestingResults) {
          return;
        }

        // otherwise, set to true and start requesting results
        requestingResults = true;
        return cha
          .requestResults()
          .then(function(result) {
            requestingResults = false;
            examStatusHandler(result);
          })
          .catch(function(err) {
            requestingResults = false;
            cha.errorHandler.main(err);
          });
      }
    }

    // This is less a results handler, more an exam status handler
    //Presentation state machine logic
    function examStatusHandler(resultFromCha) {
      if (active) {
        tatResult = resultFromCha;
        $scope.currentPresentation = Math.min(
          parseInt(tatResult.CurrentPresentation),
          chaExams.examProperties.NPresentations
        ); // cha returns one higher value right as it starts to end, making the value blip to N+1/N...
        $scope.tatState = parseInt(resultFromCha.State); //this is a string, so converting

        switch ($scope.tatState) {
          case EXAM_STATES.PLAYING:
            $scope.tatInstructions = " ";

            // keep getting status while playing to check progress
            pollingTimeout = $timeout(function() {
              getExamStatus();
            }, POLLING_DELAY);
            break;

          case EXAM_STATES.WAITING_FOR_SUBMISSION:
            $scope.tatInstructions = "Please make a selection.";

            //keep polling until DONE
            pollingTimeout = $timeout(function() {
              getExamStatus();
            }, POLLING_DELAY);
            break;

          case EXAM_STATES.DONE:
            //prepare result for submission
            page.result = $.extend({}, page.result, {
              response: "Exam Results",
              score: tatResult.Score,
              chaInfo: chaExams.getChaInfo()
            });

            //result prepared, now ready to submit.
            // Auto-submit page if autoSubmit is enabled
            if (page.dm.responseArea.autoSubmit) {
              //submit after a short delay
              submitTimeout = $timeout(examLogic.submit, SUBMIT_DELAY);
            } else {
              //update instructions to instruct user to submit if not auto-submitting
              $scope.tatInstructions = "Complete, press Submit to continue.";
            }

            // activate submit button
            $scope.submittable = true;
            page.dm.isSubmittable = $scope.submittable;

            break;
          default:
            logger.error("CHA Unexpected exam state during TAT: " + resultFromCha.State);
            break;
        }
      }
    }

    // Selecting a block calls this function, delays to show feedback, submits a presentation result, then requests results
    $scope.select = function(buttonNumber) {
      $scope.selected = buttonNumber;

      // Grade Presentation Response and set up result object
      page.result = $.extend({}, page.result, {
        presentation: $scope.currentPresentation,
        userResponse: buttonNumber,
        correctResponse: parseInt(tatResult.ToneIndex),
        correct: $scope.isCorrect(buttonNumber)
      });
      // push results
      examLogic.pushResults();

      // send results to cha and start new presentation
      var submission = { UserResponse: buttonNumber };

      // the cha starts playing the next tone after receiving submission.  If feedback is desired, a delay is required here.
      function finishSubmit() {
        cha
          .examSubmission("TAT$Submission", submission)
          //            .then(chaExams.runTOBifCalledFor) //run ThirdOctaveBands if called for
          .then(reset)
          .then(getExamStatus)
          .catch(function(err) {
            logger.debug("CHA - TAT Exam error submitting user input.");
            cha.errorHandler.main(err);
          });
      }

      $timeout(finishSubmit, SUBMIT_DELAY); // user feedback delay on pressed button
    };

    // initial page setup:
    function newTATexam() {
      active = true; // loop protection
      // set up the page.result object and local variables
      reset();
      // requestResults to start handling the state
      $timeout(getExamStatus, INITIAL_DELAY);
    }

    newTATexam();
  });
