/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";

angular
  .module("tabsint.components.response-areas.omt", [])

  .controller("OmtResponseAreaCtrl", function($scope, examLogic, page, $timeout, logger) {
    $scope.gradeResponse = false;
    $scope.showCorrect = false;

    var language = page.dm.responseArea.language || "british"; // default language is british
    language = language.toLowerCase();

    // For omt response areas
    // page.result will be a sentence, and the 'correct' field will also be a sentence
    function gradeOmtResponse() {
      var responseSentence = page.result.response;
      var correctSentence = page.dm.responseArea.correct;
      page.result.correct = undefined;
      page.result.eachCorrect = undefined;
      // remove all periods
      responseSentence = responseSentence.replace(".", "");
      correctSentence = correctSentence.replace(".", "");

      // force all lower case
      responseSentence = responseSentence.toLowerCase();
      correctSentence = correctSentence.toLowerCase();

      // binary right or wrong
      if (responseSentence === correctSentence) {
        page.result.correct = true;
      } else {
        page.result.correct = false;
      }

      page.result.eachCorrect = [];
      // count of correct words
      var chosenWords = responseSentence.split(" ");
      var correctWords = correctSentence.split(" ");
      for (var i = 0; i < correctWords.length; i++) {
        if (chosenWords[i] === correctWords[i]) {
          page.result.eachCorrect[i] = true;
        } else {
          page.result.eachCorrect[i] = false;
        }
      }
      page.result.numberCorrect = 0;
      _.forEach(page.result.eachCorrect, function(current) {
        if (current) {
          page.result.numberCorrect++;
        }
      });
      page.result.numberIncorrect = 0;
      _.forEach(page.result.eachCorrect, function(current) {
        if (!current) {
          page.result.numberIncorrect++;
        }
      });
    }

    examLogic.gradeResponse = gradeOmtResponse;

    // define rows: first look to protocol, otherwise define by language
    if (page.dm.responseArea.rows) {
      $scope.rows = page.dm.responseArea.rows;
    } else {
      if (language === "british") {
        $scope.rows = [
          ["Alan", "bought", "some", "big", "beds."],
          ["Barry", "gives", "two", "cheap", "chairs."],
          ["Hannah", "got", "three", "dark", "desks."],
          ["Kathy", "has", "four", "green", "mugs."],
          ["Lucy", "kept", "five", "large", "rings."],
          ["Nina", "likes", "six", "old", "ships."],
          ["Peter", "sees", "eight", "pink", "shoes."],
          ["Rachel", "sold", "nine", "red", "spoons."],
          ["Steven", "wants", "ten", "small", "tins."],
          ["Thomas", "wins", "twelve", "thin", "toys."]
        ];
      } else if (language === "american") {
        $scope.rows = [
          ["Allen", "bought", "two", "cheap", "chairs."],
          ["Doris", "gives", "three", "dark", "desks."],
          ["Kathy", "got", "four", "green", "flowers."],
          ["Lucy", "has", "seven", "heavy", "houses."],
          ["Nina", "kept", "eight", "large", "rings."],
          ["Peter", "ordered", "nine", "old", "sofas."],
          ["Rachel", "prefers", "twelve", "pretty", "spoons."],
          ["Steven", "sees", "fifteen", "red", "tables."],
          ["Thomas", "sold", "nineteen", "small", "toys."],
          ["William", "wants", "sixty", "white", "windows."]
        ];
      }
    }

    // define column labels
    if (page.dm.responseArea.columnLabels) {
      $scope.columnLabels = page.dm.responseArea.columnLabels;
    } else {
      if (page.dm.responseArea.type === "multipleChoiceSelectionResponseArea") {
        $scope.columnLabels = _.map($scope.rows, function() {
          return " ";
        });
      } else {
        $scope.columnLabels = ["First", "Second", "Third", "Fourth", "Fifth"]; //default
      }
    }

    var correctSentence;
    var correctWords;

    function update() {
      $scope.selections = [];
      for (var k = 0; k < $scope.rows[0].length; k++) {
        $scope.selections.push("-");
      }

      setSentence();

      $scope.gradeResponse = false;
      $scope.showCorrect = false;

      if (page.dm.responseArea.correct !== angular.undefined) {
        correctSentence = page.dm.responseArea.correct;
        correctSentence = correctSentence.replace(".", "");
        correctSentence = correctSentence.toLowerCase();
        correctWords = correctSentence.split(" ");
      } else {
        correctWords = [];
      }

      $scope.isCorrect = function(iColumn, currentButtonText) {
        currentButtonText = currentButtonText.toLowerCase().replace(".", "");
        return correctWords[iColumn] === currentButtonText;
      };

      // callback for examLogic
      if (page.dm.responseArea.feedback !== angular.undefined) {
        page.dm.showFeedback = function() {
          if (page.dm.responseArea.feedback === "gradeResponse") {
            $scope.gradeResponse = true;
          } else if (page.dm.responseArea.feedback === "showCorrect") {
            $scope.showCorrect = true;
          }
        };
      }
    }
    //$scope.$watch('page.dm', update);
    update();

    $scope.omtchoose = function(iColumn, word) {
      // if the word is already selected, switch back to '-', otherwise select it
      if ($scope.selections[iColumn] === word) {
        $scope.selections[iColumn] = "-";
      } else {
        $scope.selections[iColumn] = word;
      }

      // update the view
      setSentence();

      // if all words have been selected, then set joined selections to response
      if (
        _.every($scope.selections, function(x) {
          return x !== "-";
        })
      ) {
        page.result.response = joinSelections();
        page.dm.isSubmittable = true;
      } else {
        page.result.response = undefined;
        page.dm.isSubmittable = false;
      }

      // Auto-submit omt page when sentence is finished
      if (page.dm.isSubmittable && page.dm.responseArea.autoSubmit) {
        $timeout(function() {
          return examLogic.submit();
        }, 1000);
      }
    };

    // class selection method
    $scope.omtchosen = function(iColumn, word) {
      return $scope.selections[iColumn] === word;
    };

    // set sentence to view
    function setSentence() {
      if (page.dm.responseArea.type === "multipleChoiceSelectionResponseArea") {
        var selectionsTxt = "";
        for (var j = 0; j < $scope.selections.length - 1; j++) {
          selectionsTxt += $scope.selections[j] + ", ";
        }
        selectionsTxt += "and " + $scope.selections[$scope.selections.length - 1];
        $scope.sentence = "You will mark " + selectionsTxt + " please.";
      } else {
        $scope.sentence = joinSelections();
      }
    }

    function joinSelections() {
      return $scope.selections.join(" ");
    }
  });
