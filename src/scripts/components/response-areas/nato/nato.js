/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";

/* global WAVsave */

angular
  .module("tabsint.components.response-areas.nato", [])

  .controller("NatoResponseAreaCtrl", function($scope, page, $timeout, logger, devices, media) {
    var vm = this;

    // defaults
    vm.disabled = true;
    vm.state = "preparing";
    vm.sentence = "";
    vm.error = undefined;
    vm.microphone = "internal";
    vm.autoPlay = true;
    var startTime, elapTime, filename;

    /**
     * Response Area init
     */
    vm.$onInit = function() {
      // set submittable logic
      page.dm.isSubmittable = false;

      vm.sentence = page.dm.responseArea.sentence || vm.sentence;
      vm.autoPlay = angular.isDefined(page.dm.responseArea.autoPlay) ? page.dm.responseArea.autoPlay : vm.autoPlay;
      if (page.dm.responseArea.microphone && _.includes(["internal", "external"], page.dm.responseArea.microphone)) {
        vm.microphone = page.dm.responseArea.microphone;
      } else {
        logger.debug("Undefined or invalid microphone parameter set in NATO protocol. Using internal microphone");
      }

      // WAVsave Set Up
      WAVsave.init(vm.chooseMic, function(e) {
        vm.error("Failed to initialize WAVsave", e);
      });
    };

    /**
     * WAVsave choose microphone.
     * Assumes vm.microphone is sanitized
     */
    vm.chooseMic = function() {
      if (vm.microphone === "external") {
        WAVsave.setMicAudioJack(
          function() {
            logger.info("WAVsave set mic to audio jack");
            vm.ready();
          },
          function(e) {
            vm.error("Failed while trying to set microphone to external", e);
          }
        );
      } else {
        // default is BuiltIn, so go straight to recording
        vm.ready();
      }
    };

    vm.ready = function() {
      vm.state = "ready";
    };

    /**
     * WAVsave chooseMic success callback.
     * Start recording right away
     */
    vm.record = function() {
      WAVsave.record(
        function() {
          logger.info("WAVsave started recording");
          vm.state = "recording";
          $scope.$apply();
          startTime = new Date();
        },
        function(e) {
          vm.error("Failed while trying to start recording", e);
        }
      );
    };

    /**
     * Start recording again
     */
    vm.reRecord = function() {
      if (angular.isDefined(page.dm.wavfiles)) {
        var startDelayTime = angular.isDefined(page.dm.wavfileStartDelayTime) ? page.dm.wavfileStartDelayTime : 0; // we don't want to delay in this case
        media.playWav(page.dm.wavfiles, startDelayTime);
      }

      vm.record();
    };

    /**
     * Controller tear down
     */
    vm.$onDestroy = function() {
      WAVsave.close();
    };

    /**
     * Stop recording
     */
    vm.stopRecording = function() {
      // manually add 500 ms extra to recording
      setTimeout(function() {
        WAVsave.stop(stopRecordingSuccess, function(e) {
          vm.error("Failed to stop recording", e);
        });
      }, 500);
    };

    /**
     * WAVsave stopped successfully
     */
    function stopRecordingSuccess() {
      var time = new Date()
        .toJSON()
        .replace(":", "-")
        .replace(":", "-")
        .split(".")[0];
      elapTime = new Date() - startTime; // milliseconds - not used currently
      filename = devices.shortUUID + "_" + time;
      page.result.response = filename;

      WAVsave.saveWAVfile(
        function() {
          logger.log("INFO: Successfully saved wav file to filename: " + filename);

          // change state
          vm.state = "recorded";
          $scope.$apply();
          vm.finish();
        },
        function(e) {
          vm.error("Failed while saving audio file", e);
        },
        filename
      );
    }

    /**
     * Plays last recorded audio file
     */
    vm.play = function() {
      // stop tablet audio
      media.stopAudio();

      // set volume in WAVsave
      let vol = 8.0; // ref sensimetrics
      WAVsave.setVolume(
        function() {
          play();
        },
        function(e) {
          vm.error("Failed to set volume in audio file. Playing at previously set volume. ", e);
          play();
        },
        vol
      );

      // play last wav in WAVsave
      function play() {
        WAVsave.play(
          function() {},
          function(e) {
            vm.error("Failed to start playing audio file", e);
          }
        );
      }
    };

    /**
     * Stop Playing
     */
    vm.stopPlaying = function() {
      WAVsave.stop(
        function() {},
        function(e) {
          vm.error("Failed to stop playing", e);
        }
      );
    };

    /**
     * Error handler for WavSav
     * @param  {string} msg - error message
     */
    vm.error = function(msg, err) {
      vm.state = "error";
      vm.error = msg;
      logger.log("ERROR: " + msg + ".  Return from WAVsave: " + JSON.stringify(err));
      vm.finish();
    };

    /**
     * Finish up the response-area. Make page submittable
     */
    vm.finish = function() {
      if (vm.autoPlay) {
        vm.play();
      }

      page.dm.isSubmittable = true;
    };
  });
