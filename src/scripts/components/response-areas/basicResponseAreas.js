/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";

angular
  .module("tabsint.components.response-areas.basic-response-areas", [])

  .controller("BasicChoiceController", function($scope, examLogic, page) {
    // container spacing
    $scope.spacing = {};
    if (page.dm.responseArea.verticalSpacing) {
      _.extend($scope.spacing, {
        "padding-bottom": page.dm.responseArea.verticalSpacing
      });
    }

    if (page.dm.responseArea.horizontalSpacing) {
      _.extend($scope.spacing, {
        "padding-right": page.dm.responseArea.horizontalSpacing / 2,
        "padding-left": page.dm.responseArea.horizontalSpacing / 2
      });
    }

    $scope.label = $scope.choice.text || $scope.choice.id;
    $scope.chosen = function() {
      return $scope.choice.id === page.result.response;
    };
    $scope.isCorrect = function() {
      return $scope.choice.correct === true;
    };
    $scope.btnClass = function() {
      var btnClass = "btn btn-block ";
      if ($scope.chosen()) {
        btnClass += "btn-default active ";
      } else {
        btnClass += "btn-default ";
      }
      return btnClass;
    };
    $scope.choose = function() {
      // toggle chosen/unchosen.
      //        if (page.result.response === $scope.choice.id) {
      //          page.result.response = undefined; //toggle off.
      //        } else {
      //          page.result.response = $scope.choice.id; // choose.
      //        }
      // AUTO-SUBMIT:
      page.result.response = $scope.choice.id;
      page.dm.isSubmittable = examLogic.getSubmittableLogic(page.responseArea);
      if (page.dm.isSubmittable && page.result.response !== "Other") {
        examLogic.submit = examLogic.submitDefault;
        examLogic.submit();
      }
    };
  })

  .controller("CheckboxChoiceController", function($scope, page) {
    $scope.label = $scope.choice.text || $scope.choice.id;

    // container spacing
    $scope.spacing = {};
    if (page.dm.responseArea.verticalSpacing) {
      _.extend($scope.spacing, {
        "padding-bottom": page.dm.responseArea.verticalSpacing
      });
    }

    if (page.dm.responseArea.horizontalSpacing) {
      _.extend($scope.spacing, {
        "padding-right": page.dm.responseArea.horizontalSpacing / 2,
        "padding-left": page.dm.responseArea.horizontalSpacing / 2
      });
    }

    $scope.saveListToResponse([]);

    $scope.chosen = function() {
      return _.includes($scope.responseAsList(), $scope.choice.id);
    };
    $scope.btnClass = function() {
      var btnClass = "btn btn-block ";
      if (page.dm.responseArea.buttonScheme === "markIncorrect") {
        if ($scope.chosen()) {
          btnClass += "btn-danger";
        } else {
          btnClass += "btn-success";
        }
      } else if (page.dm.responseArea.buttonScheme === "markCorrect") {
        if ($scope.chosen()) {
          btnClass += "btn-success";
        } else {
          btnClass += "btn-danger";
        }
      } else {
        if ($scope.chosen()) {
          btnClass += "btn-default active ";
        } else {
          btnClass += "btn-default ";
        }
      }
      return btnClass;
    };
    $scope.choose = function() {
      // toggle chosen/unchosen.
      if ($scope.chosen()) {
        $scope.saveListToResponse(_.without($scope.responseAsList(), $scope.choice.id)); //toggle off.
      } else {
        $scope.saveListToResponse(_.union($scope.responseAsList(), [$scope.choice.id])); //toggle off.
      }
    };
  })

  .controller("QSINController", function($scope) {
    $scope.label = $scope.choice.text || $scope.choice.id;

    $scope.saveListToResponse([]);

    $scope.chosen = function() {
      return _.includes($scope.responseAsList(), $scope.choice.id);
    };
    $scope.btnClass = function() {
      var btnClass = "btn btn-block ";
      if ($scope.chosen()) {
        btnClass += "btn-danger"; //'btn-default active ';
      } else {
        btnClass += "btn-success"; //'btn-default ';
      }
      return btnClass;
    };
    $scope.choose = function() {
      // toggle chosen/unchosen.
      if ($scope.chosen()) {
        $scope.saveListToResponse(_.without($scope.responseAsList(), $scope.choice.id)); //toggle off.
      } else {
        $scope.saveListToResponse(_.union($scope.responseAsList(), [$scope.choice.id])); //toggle off.
      }
    };
  });
