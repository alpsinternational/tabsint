/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

/* jshint bitwise: false */
/* globals ChaWrap: false */

angular
  .module("tabsint.components.response-areas.mpanls", [])
  .directive("mpanlsExam", function() {
    return {
      restrict: "E",
      templateUrl: "scripts/components/response-areas/mpanls/mpanls.html",
      controller: "MPANLsExamCtrl"
    };
  })
  .controller("MPANLsExamCtrl", function(
    $scope,
    $q,
    $timeout,
    cha,
    chaExams,
    examLogic,
    gettextCatalog,
    logger,
    notifications,
    page,
    svantek
  ) {
    $scope.state = "start";

    // Results structure:
    var startTime = new Date();
    var limits, headsetAttenuation, freqs;
    var noiseFloors = [];
    var mpanlsData = [];
    var destroy = false;
    var questionMainText = page.dm.questionMainText || undefined;
    var instructionText = page.dm.instructionText || undefined;

    // Start Exam
    $scope.newMPANLsExam = function(MPANLsDuration) {
      // if svantek is not connected, make page submittable and throw alert.
      if (!svantek.device) {
        $scope.state = "start";
        notifications.alert(gettextCatalog.getString("Svantek dosimeter is not connected."));
        return;
      }

      // if page also defines "svantek", remove it:
      if (page.dm.svantek) {
        page.dm.svantek = undefined;
      }

      $scope.state = "exam";
      page.result.duration = MPANLsDuration;
      $scope.page.dm.questionMainText = questionMainText || "Svantek Background Noise Measurement";
      $scope.page.dm.questionSubText =
        page.dm.examInstructions || "Please remain still and quiet while we measure the background noise levels.";
      $scope.page.dm.instructionText =
        "This test measures the background noise levels using the Svantek dosimeter. Wait quietly for measurement to complete.";
      page.dm.responseArea.standard = page.dm.responseArea.standard || "ANSI S3.1-R2008";
      mpanlsData.standard = page.dm.responseArea.standard;

      // MPANLs response area is only implemented for the WAHTS or overwritten headset attenuation. If a different headset is
      // specified, alert the administrator and cancel exam
      if (
        angular.isDefined(page.dm.headset) &&
        !(page.dm.headset === "WAHTS" || page.dm.headset === "Creare Headset")
      ) {
        cancelMPANLsExam(
          gettextCatalog.getString(
            "The MPANLs response area is only implemented for the WAHTS headset. If you are using a different headset, specify your headset attenuation instead of specifying a headset in the protocol."
          )
        );
        page.headset = page.dm.headset;
        return;
      } else if (angular.isDefined(page.dm.responseArea.attenuation)) {
        page.headset = "unknown"; // If the headset is not defined in the protocol and no attenuation is specified, we assume the headset is the WAHTS
      } else {
        page.headset = "WAHTS";
      }
      mpanlsData.headset = page.headset;

      // If protocol overrides F, MPANL, and/or attenuation, check that all array lenghts are the same. Otherwise,
      // cancel exam and notify administrator to fix protocol
      // At the same time, set the frequencies, attenuations and limits to display in the MPANLs
      if (
        angular.isDefined(page.dm.responseArea.F) &&
        angular.isDefined(page.dm.responseArea.MPANL) &&
        angular.isDefined(page.dm.responseArea.attenuation)
      ) {
        if (
          page.dm.responseArea.F.length === page.dm.responseArea.MPANL.length &&
          page.dm.responseArea.F.length === page.dm.responseArea.attenuation.length
        ) {
          limits = page.dm.responseArea.MPANL;
          headsetAttenuation = page.dm.responseArea.attenuation;
          freqs = page.dm.responseArea.F;
        } else {
          cancelMPANLsExam(
            gettextCatalog.getString(
              "The MPANLs octave band frequencies, limits and headset attenuation must all be arrays of equal length OR unspecified. Please verify the protocol and try again."
            )
          );
          return;
        }
      } else if (
        angular.isDefined(page.dm.responseArea.F) ||
        angular.isDefined(page.dm.responseArea.MPANL) ||
        angular.isDefined(page.dm.responseArea.attenuation)
      ) {
        if (angular.isDefined(page.dm.responseArea.F)) {
          freqs = page.dm.responseArea.F;
        } else {
          if (page.dm.responseArea.standard === "ANSI S3.1-R2008") {
            freqs = [125, 250, 500, 1000, 2000, 4000, 8000];
          } else {
            freqs = [500, 1000, 2000, 4000, 8000];
          }
        }
        if (angular.isDefined(page.dm.responseArea.MPANL)) {
          if (page.dm.responseArea.MPANL.length !== freqs.length) {
            cancelMPANLsExam(
              gettextCatalog.getString(
                "The MPANLs octave band frequencies, limits and headset attenuation must all be arrays of equal length OR unspecified. To specify a subset of these parameters, ensure that the array lengths are the same length as the noise limits standard. Please verify the protocol and try again."
              )
            );
            return;
          } else {
            limits = page.dm.responseArea.MPANL;
          }
        } else {
          if (page.dm.responseArea.standard === "ANSI S3.1-R2008") {
            limits = [35, 21, 16, 13, 14, 11, 14];
          } else if (page.dm.responseArea.standard === "DoD") {
            limits = [27, 29, 34, 39, 41];
          } else if (page.dm.responseArea.standard === "OSHA") {
            limits = [40, 40, 47, 57, 62];
          }
          if (limits.length !== freqs.length) {
            cancelMPANLsExam(
              gettextCatalog.getString(
                "The MPANLs octave band frequencies, limits and headset attenuation must all be arrays of equal length OR unspecified. To specify a subset of these parameters, ensure that the array lengths are the same length as the noise limits standard. Please verify the protocol and try again."
              )
            );
            return;
          }
        }
        if (angular.isDefined(page.dm.responseArea.attenuation)) {
          if (page.dm.responseArea.attenuation.length !== freqs.length) {
            cancelMPANLsExam(
              gettextCatalog.getString(
                "The MPANLs octave band frequencies, limits and headset attenuation must all be arrays of equal length OR unspecified. To specify a subset of these parameters, ensure that the array lengths are the same length as the noise limits standard. Please verify the protocol and try again."
              )
            );
            return;
          } else {
            headsetAttenuation = page.dm.responseArea.attenuation;
          }
        } else {
          if (page.dm.responseArea.standard === "ANSI S3.1-R2008") {
            if (page.headset === "WAHTS") {
              headsetAttenuation = [30.6, 31.6, 37.5, 39.5, 34.5, 36.0, 36.9];
            }
          } else {
            if (page.headset === "WAHTS") {
              headsetAttenuation = [37.5, 39.5, 34.5, 36.0, 36.9];
            }
          }
          if (headsetAttenuation.length !== freqs.length) {
            cancelMPANLsExam(
              gettextCatalog.getString(
                "The MPANLs octave band frequencies, limits and headset attenuation must all be arrays of equal length OR unspecified. To specify a subset of these parameters, ensure that the array lengths are the same length as the noise limits standard. Please verify the protocol and try again."
              )
            );
            return;
          }
        }
      } else {
        if (page.dm.responseArea.standard === "ANSI S3.1-R2008") {
          limits = [35, 21, 16, 13, 14, 11, 14];
          freqs = [125, 250, 500, 1000, 2000, 4000, 8000];
          if (page.headset === "WAHTS") {
            headsetAttenuation = [30.6, 31.6, 37.5, 39.5, 34.5, 36.0, 36.9];
          }
        } else {
          freqs = [500, 1000, 2000, 4000, 8000];
          if (page.dm.responseArea.standard === "DoD") {
            limits = [27, 29, 34, 39, 41];
          } else if (page.dm.responseArea.standard === "OSHA") {
            limits = [40, 40, 47, 57, 62];
          }
          if (page.headset === "WAHTS") {
            headsetAttenuation = [37.5, 39.5, 34.5, 36.0, 36.9];
          }
        }
      }

      // Check that the frequencies are octave band center frequencies. Otherwise, continue the exam but inform the administrator
      for (var i = 0; i < freqs.length; i++) {
        if (![125, 250, 500, 1000, 2000, 4000, 8000].includes(freqs[i])) {
          notifications.alert(
            gettextCatalog.getString(
              "At least one of the frequencies specified in the protocol is not an octave band center frequency between 100 and 10,000. The exam will proceed and results displayed, but please verify the protocol for frequencies accuracy."
            )
          );
        }
      }

      var noiseFloorsMap = [
        { freq: 125, floor: 41.5 },
        { freq: 250, floor: 41.2 },
        { freq: 500, floor: 41.2 },
        { freq: 1000, floor: 42.1 },
        { freq: 2000, floor: 44.1 },
        { freq: 4000, floor: 46.5 },
        { freq: 8000, floor: 50.5 }
      ];

      for (var k = 0; k < freqs.length; k++) {
        // handling mapping the frequencies that don't match with the limit frequencies
        var freqMap = Math.round(freqs[k]);
        for (var j = 0; j < noiseFloorsMap.length; j++) {
          var freqDiff = Math.abs(freqMap - noiseFloorsMap[j].freq) / freqMap;
          if (freqDiff < 0.02) {
            noiseFloors.push(noiseFloorsMap[j].floor); // set limit if it exists
          }
        }
      }

      // stop svantek, then start and record data for duration
      svantek
        .stop()
        .then(svantek.start)
        .then(function() {
          $timeout(function() {
            return handleResults();
          }, page.result.duration);
        })
        .catch(function(e) {
          $scope.state = "start";
          notifications.alert(gettextCatalog.getString("Failed to start recording from the Svantek Dosimeter."));
        });
    };

    // skip exam
    $scope.skipExam = function() {
      page.result.response = "skipped";
      examLogic.skip();
    };

    this.$onDestroy = function() {
      destroy = true;
      svantek.stop();
      $scope.page.dm.questionSubText = "";
      $scope.page.dm.questionMainText = questionMainText || "Svantek Background Noise Measurement";
      $scope.page.dm.instructionText =
        instructionText ||
        "Press 3 or 10 seconds and then remain still and quiet while we measure the background noise levels.";
    };

    // If protocol parameters are not entered appropriately, cancel exam
    function cancelMPANLsExam(msg) {
      page.result.response = "cancelled";
      examLogic.submit();
      notifications.alert(msg);
    }

    // Stop Svantek and Handle Results
    function handleResults() {
      svantek
        .stop()
        .then(addResults)
        .then(presentResults)
        .catch(function(err) {
          cha.errorHandler.main(err);
        });
    }

    // Stop Svantek and Process Results
    function addResults() {
      var data = svantek.data;
      if (!data) {
        logger.warn("No data available on svantek.data.");
        // Note: we could check page.result, but seems unsafe if svantek.data is empty.
        return;
      }

      page.result.svantek = data;
      page.result.svantek.FBand = freqs;

      // calculate the amount of noise in the octave band level
      var bandLevel = [];
      for (var i = 0; i < page.result.svantek.FBand.length; i++) {
        bandLevel.push(svantek.calculateBandLevel(data, page.result.svantek.FBand[i]));
      }
      page.result.svantek.bandLevel = bandLevel;
      page.result.responseStartTime = startTime;
    }

    // Results Presentation
    function presentResults() {
      var deferred = $q.defer();
      if (!destroy) {
        $scope.state = "results";
        page.dm.hideProgressbar = true;

        // Update page view, get rid of instructions
        $scope.page.dm.questionMainText = "Background Noise Results";
        $scope.page.dm.questionSubText = "";
        $scope.page.dm.instructionText = "";

        // set default 'response' field
        page.result.response = "continue";

        if (page.dm.responseArea.autoSubmit) {
          examLogic.submit();
          return;
        } else {
          page.dm.isSubmittable = true;
        }

        for (var k = 0; k < page.result.svantek.FBand.length; k++) {
          mpanlsData.push({
            freq: page.result.svantek.FBand[k],
            level: page.result.svantek.bandLevel[k] > 0 ? page.result.svantek.bandLevel[k].toFixed(1) : 0, // min of 0
            limit: limits[k],
            att: headsetAttenuation[k],
            levelUnderWAHTS:
              page.result.svantek.bandLevel[k] > 0 && headsetAttenuation[k] < 1000
                ? (page.result.svantek.bandLevel[k] - headsetAttenuation[k]).toFixed(1)
                : 0,
            noiseFloor: noiseFloors[k]
          });
        }

        $scope.mpanlsData = mpanlsData;
        page.result.mpanlsData = mpanlsData;
      }
      deferred.resolve();
      return deferred.promise;
    }

    // Reset and Repeat Exam
    $scope.repeatMPANLs = function(MPANLsDuration) {
      // reset state and results
      logger.debug("repeating MPANLs");
      $scope.state = "start";
      page.result = {};
      mpanlsData = [];

      $scope.newMPANLsExam(MPANLsDuration);
    };
  })

  .directive("mpanlsPlot", function() {
    return {
      restrict: "E",
      template: '<div id="mpanlsPlotWindow"></div>',
      controller: "mpanlsPlotCtrl",
      scope: {
        data: "="
      }
    };
  })

  .controller("mpanlsPlotCtrl", function($scope, d3Services) {
    d3Services.mpanlsPlot("#mpanlsPlotWindow", $scope.data);
  });
