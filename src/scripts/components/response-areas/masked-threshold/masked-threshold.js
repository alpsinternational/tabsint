/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as $ from "jquery";

angular
  .module("tabsint.components.response-areas.cha.masked-threshold", [])
  .directive("maskedThresholdExam", function() {
    return {
      restrict: "E",
      templateUrl: "scripts/components/response-areas/masked-threshold/masked-threshold.html",
      controller: "MaskedThresholdExamCtrl"
    };
  })
  .controller("MaskedThresholdExamCtrl", function($scope, cha, chaExams, examLogic, logger, page, results) {
    $scope.chaExams = chaExams;

    // wait until exam is over
    chaExams.wait.forReadyState().then(function() {
      cha
        .requestResults()
        .then(function(resultFromCha) {
          page.result = $.extend({}, page.result, resultFromCha);

          page.dm.isSubmittable = true;

          if (page.dm.responseArea.autoSubmit) {
            examLogic.submit();
          }
        })
        .catch(function(err) {
          if (err.msg !== "repeating exam") {
            cha.errorHandler.main(err);
          }
        });
    });
  });
