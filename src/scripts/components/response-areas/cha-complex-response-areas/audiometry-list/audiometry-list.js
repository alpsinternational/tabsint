/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";

angular
  .module("cha.audiometry-list", [])
  .directive("audiometryList", function() {
    return {
      restrict: "E",
      templateUrl: "scripts/components/response-areas/cha-complex-response-areas/audiometry-list/audiometry-list.html",
      controller: "AudiometryListCtrl"
    };
  })
  .controller("AudiometryListCtrl", function(
    $q,
    $scope,
    $timeout,
    cha,
    chaAudiometryService,
    chaCheck,
    chaExams,
    disk,
    examLogic,
    gettextCatalog,
    page,
    results
  ) {
    var repeatPage = {
      id: "repeat",
      title: "Please Try Again",
      questionSubText: gettextCatalog.getString(
        "Nice try, but the test needs to be repeated for one or more frequencies.  Please try again and be sure to listen carefully."
      )
    };
    var notesPage = {
      id: "notes",
      title: "Audiometry Notes",
      questionSubText:
        gettextCatalog.getString(
          "Unfortunately the last test was unsuccessful a second time.  Please hand the tablet to an administrator."
        ) +
        "<br><br>" +
        gettextCatalog.getString(
          "Administrators:  Please add any relevant notes about the previous audiometry test below."
        )
    };

    // Build List from global and listed examProperties.  Store list in a persistent location (not this controller,
    // which gets cleared when navigating away from audiometryList.  Use chaExams.storage.
    // Check upon load in case it is already populated - storage gets cleared on examReset and pageEnd
    if (!chaExams.storage.audiometryList) {
      chaExams.storage.audiometryList = page.dm.responseArea;

      var commonExamProps = page.dm.responseArea.commonExamProperties || {}; // load globals
      var responseAreaProps = page.dm.responseArea.commonResponseAreaProperties || {};

      // randomize if requested
      if (page.dm.responseArea.randomizeList) {
        chaExams.storage.audiometryList.presentationList = _.shuffle(chaExams.storage.audiometryList.presentationList);
      }

      for (var i = 0; i < chaExams.storage.audiometryList.presentationList.length; i++) {
        var presProps = chaExams.storage.audiometryList.presentationList[i];
        var props = _.extend({}, commonExamProps, presProps); // add globals to each pres, prefer pres props by putting pres last.  Use empty {} first to avoid mutating commonExamProps.

        var pres = {};
        pres.responseAreaProps = _.extend({}, responseAreaProps);
        if (i === 0) {
          pres.responseAreaProps.autoBegin = false;
          pres.responseAreaProps.autoSubmit = true;
        }
        pres.id = props.id || page.dm.id + "_" + props.F; // establish id's for each page
        pres.title = "Audiometry Exam";
        pres.questionMainText = page.dm.questionMainText;
        pres.questionSubText = undefined;
        pres.type = page.dm.audiometryType || "HughsonWestlake"; // remove 'cha' from prefix
        delete props.id; // can't pass the presentationId on to the CHA as an exam property!
        pres.examProperties = props;
        chaExams.storage.audiometryList.presentationList[i] = pres; // overwriting original element defined by protocol
      }
    }

    // Hijack submit function
    examLogic.submit = function() {
      submitResultsAndGoToNextInList();
    };

    // Initialize to the next presentation to the first in the stored list
    var nextPresentation = chaExams.storage.audiometryList.presentationList[0];
    setPresentation(nextPresentation, true);

    function submitResultsAndGoToNextInList() {
      // check for failures if we care (repeat Group
      if (page.dm.responseArea.repeatGroup) {
        // only add repeats, failed message, or notes page if the last presentation was an audiometry presentation and the current presentaion is not skipped
        if (
          chaExams.storage.audiometryList.presentationList[0].type &&
          chaCheck.isAudiometryExam(chaExams.storage.audiometryList.presentationList[0].type) &&
          !page.result.isSkipped
        ) {
          if (
            (page.result.examProperties.Screener && page.result.ResultType !== "Pass") ||
            (!page.result.examProperties.Screener && page.result.ResultType !== "Threshold")
          ) {
            // add the message page if it isn't already here and we aren't already repeating
            if (chaExams.storage.audiometryList.presentationList[0].id.indexOf("_@repeat") < 0) {
              var found = _.find(chaExams.storage.audiometryList.presentationList, function(pres) {
                return pres.id.indexOf("repeat") >= 0;
              });
              if (!found) {
                chaExams.storage.audiometryList.presentationList.push(repeatPage);
              }
            }
            // add this audiometry exam if we haven't already repeated it
            if (chaExams.storage.audiometryList.presentationList[0].id.indexOf("_@repeat") < 0) {
              var repeat = chaExams.storage.audiometryList.presentationList[0];
              repeat.id += "_@repeat";
              chaExams.storage.audiometryList.presentationList.push(repeat);
            } else {
              // already repeated this one - add the notes page is if is required and isn't already there
              if (
                page.dm.responseArea.notesOnGroupFailedTwice &&
                _.last(chaExams.storage.audiometryList.presentationList).id.indexOf("notes") < 0
              ) {
                chaExams.storage.audiometryList.presentationList.push(notesPage);
              }
            }
          }
        }
      }

      if (chaExams.storage.audiometryList.presentationList.length > 1 && !page.result.isSkipped) {
        // we still have presentations to go

        // results have been properly assembled in the individual audiometry page except for svantek
        examLogic.pushResults();

        startNext();

        // if (pauseDesired) {
        //   $scope.chaPresentation = undefined;
        //   $timeout(startNext, 100);
        // } else {
        //   startNext();
        // }
      } else {
        finalize(); // Done - don't push results - the normal submit method will handle that.
      }
    }

    function startNext() {
      chaExams.storage.audiometryList.presentationList.shift(); // remove first entry

      var nextPresentation = chaExams.storage.audiometryList.presentationList[0]; // grab next presentation
      setPresentation(nextPresentation, false); // set next presentation
    }

    function setPresentation(pres, first) {
      page.dm.id = pres.id;
      page.dm.title = pres.title;
      page.dm.questionMainText = pres.questionMainText;
      page.dm.questionSubText = pres.questionSubText;
      // if it's a normal audiometry exam, process it.  Otherwise, it's a message or notes page, display as is.
      if (pres.type && chaCheck.isAudiometryExam(pres.type)) {
        chaExams.state = "exam";
        page.dm.responseArea = _.extend(page.dm.responseArea, pres.responseAreaProps);
        chaExams.setup(pres.type, pres.examProperties);
      } else if (pres.id && pres.id.indexOf("notes") >= 0) {
        chaExams.state = "notes";
        chaExams.examProperties = {};
        page.dm.responseArea.autoSubmit = false;
        delete chaExams.storage.audiometryList;
        examLogic.submit = examLogic.submitDefault;
        page.dm.isSubmittable = true;
      } else if (pres.id && pres.id.indexOf("repeat") >= 0) {
        chaExams.state = "repeatGroup";
        chaExams.examProperties = {};
        page.dm.responseArea.autoSubmit = false;
      }
      // initialize result object
      page.result = results.default(page.dm);

      chaAudiometryService.reset(); // reset counters and such for the audiometry page without having to reload the controller

      if (first) {
        //|| pauseDesired) { // old method - undefine the page, let it reload, audiometry.js starts a new single page
        // do nothing - responseAreaCtrl will start it using autoBegin and audiometryCtrl will end it properly using autoSubmit
      } else {
        // run the presentation, state-to-finsih, right here.
        if (chaExams.state === "exam") {
          if (cha.state === "connected") {
            cha
              .requestStatusBeforeExam()
              .then(function() {
                if (chaExams.state === "exam") {
                  return cha.queueExam(chaExams.examType, chaExams.examProperties);
                }
              })
              .then(chaExams.wait.forReadyState)
              .then(function() {
                chaAudiometryService.examDone = true;
                if (chaExams.state !== "exam") {
                  return $q.reject({ code: 905, msg: "paused" });
                }
              })
              .then(cha.requestResults)
              .then(chaAudiometryService.processResults)
              .then(examLogic.submit) // hijacked
              .catch(function(err) {
                if (err.msg !== "repeating exam" && err.msg !== "paused") {
                  cha.errorHandler.main(err);
                }
              });
          } else {
            chaExams.state = "start";
          }
        } else {
          // do nothing - in notes or repeat message page
        }
      }
    }

    function finalize() {
      // reset storage
      delete chaExams.storage.audiometryList;

      // return submit control
      examLogic.submit = examLogic.submitDefault;
      page.dm.isSubmittable = true;
      examLogic.submit();
    }
  });
