/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";
import * as $ from "jquery";

/* jshint bitwise: false */
/* globals ChaWrap: false */

angular
  .module("cha.manual-audiometry", [])
  .directive("manualAudiometryExam", function() {
    return {
      restrict: "E",
      templateUrl:
        "scripts/components/response-areas/cha-complex-response-areas/manual-audiometry/manual-audiometry.html",
      controller: "ManualAudiometryExamCtrl"
    };
  })
  .controller("ManualAudiometryExamCtrl", function(
    $q,
    $scope,
    $timeout,
    cha,
    chaExams,
    chaResults,
    chaConstants,
    disk,
    examLogic,
    logger,
    notifications,
    page,
    results
  ) {
    var examType = page.dm.audiometryType || "HughsonWestlake"; // remove 'cha' from prefix
    chaExams.setup(examType, page.dm.responseArea.examProperties); // sets chaExams.examProperties

    $scope.panes = {
      level: true,
      masking: true,
      progression: false,
      table: true
    };
    $scope.showLevelProgression = false;
    $scope.btnState = 0;
    $scope.btnMaskingState = 0;
    $scope.masking = page.dm.responseArea.masking;
    $scope.boneConduction = page.dm.responseArea.boneConduction;
    $scope.showPresentedTones = page.dm.responseArea.showPresentedTones;
    $scope.presentationList = angular.copy(page.dm.responseArea.presentationList);
    var presentationIndex = 0;
    var defaultExamProperties = chaExams.examProperties || {};
    $scope.increaseValue = defaultExamProperties.StepSize;
    $scope.decreaseValue = defaultExamProperties.StepSize * 2;
    var defaultPresentationId = page.dm.id;
    var startTime = new Date();

    examLogic.submit = function() {
      submitResults();
    };
    page.dm.isSubmittable = true;

    // build presentation list
    if (
      angular.isUndefined(chaExams.storage.manualAudiometry) ||
      angular.isUndefined(chaExams.storage.manualAudiometry.list)
    ) {
      chaExams.storage.manualAudiometry = {
        list: {}
      };
      chaExams.storage.manualAudiometry.list.Left = angular.copy(page.dm.responseArea.presentationList);
      chaExams.storage.manualAudiometry.list.Right = angular.copy(page.dm.responseArea.presentationList);

      // build exams for left ear
      _.forEach(chaExams.storage.manualAudiometry.list.Left, function(pres) {
        var examProps = _.extend({}, defaultExamProperties, pres, {
          PresentationMax: 1
        });
        examProps.OutputChannel = "HPL0";
        if (examProps.id) {
          delete examProps.id;
        }
        pres.examProperties = examProps;
      });

      // build exames for right ear
      _.forEach(chaExams.storage.manualAudiometry.list.Right, function(pres) {
        var examProps = _.extend({}, defaultExamProperties, pres, {
          PresentationMax: 1
        });
        examProps.OutputChannel = "HPR0";
        if (examProps.id) {
          delete examProps.id;
        }
        pres.examProperties = examProps;
      });

      // bone conduction exams
      if ($scope.boneConduction) {
        // create a placeholder for Bone output
        chaExams.storage.manualAudiometry.list["Bone (Left)"] = angular.copy(page.dm.responseArea.presentationList);
        chaExams.storage.manualAudiometry.list["Bone (Right)"] = angular.copy(page.dm.responseArea.presentationList);

        // build exams for bone left - output channel is "LINEL0 NONE" for left DAC or "NONE LINEL0" for right DAC
        _.forEach(chaExams.storage.manualAudiometry.list["Bone (Left)"], function(pres) {
          var examProps = _.extend({}, defaultExamProperties, pres, {
            PresentationMax: 1
          });
          examProps.OutputChannel = "LINEL0 NONE";
          if (examProps.id) {
            delete examProps.id;
          }
          pres.examProperties = examProps;
        });

        // build exams for bone right - output channel is "LINEL0 NONE" for left DAC or "NONE LINEL0" for right DAC
        _.forEach(chaExams.storage.manualAudiometry.list["Bone (Right)"], function(pres) {
          var examProps = _.extend({}, defaultExamProperties, pres, {
            PresentationMax: 1
          });
          examProps.OutputChannel = "NONE LINEL0";
          if (examProps.id) {
            delete examProps.id;
          }
          pres.examProperties = examProps;
        });
      }
    }

    // get width of plot container
    let containerWidth = document.documentElement.clientWidth;
    $scope.plotContainerWidth = 0.8 * containerWidth;
    $scope.plotContainerWidth = $scope.plotContainerWidth > 300 ? $scope.plotContainerWidth : 300;

    // get height of device view
    let documentHeight = document.documentElement.clientHeight;
    $scope.plotContainerHeight = Math.round(documentHeight / 2.5);
    $scope.plotContainerHeight = $scope.plotContainerHeight > 300 ? $scope.plotContainerHeight : 300;

    // view fields
    $scope.data = {
      currentFrequency: undefined,
      currentLevel: undefined,
      currentMaskingLevel: undefined,
      channel: undefined, // default set below based on protocol inputs
      maskingChannel: undefined, // default set below based on protocol inputs
      levelUnits: undefined,
      minLevel: undefined,
      maxLevel: undefined,
      minMaskingLevel: undefined,
      maxMaskingLevel: undefined
    };

    // Determine if it's a threshold or pass-fail exam
    if (angular.isDefined(page.dm.responseArea.responseType)) {
      $scope.data.responseType = page.dm.responseArea.responseType;
    } else {
      $scope.data.responseType = "threshold";
    }

    // set min and max levels, handling undefined and 0 cases
    if (angular.isDefined(page.dm.responseArea.minLevel)) {
      $scope.data.minLevel = page.dm.responseArea.minLevel;
    } else {
      $scope.data.minLevel = -80;
    }
    if (angular.isDefined(page.dm.responseArea.maxLevel)) {
      $scope.data.maxLevel = page.dm.responseArea.maxLevel;
    } else {
      $scope.data.maxLevel = 100;
    }

    // set min and max masking levels, handling undefined and 0 cases
    if (angular.isDefined(page.dm.responseArea.minMaskingLevel)) {
      $scope.data.minMaskingLevel = page.dm.responseArea.minMaskingLevel;
    } else {
      $scope.data.minMaskingLevel = -80;
    }
    if (angular.isDefined(page.dm.responseArea.maxMaskingLevel)) {
      $scope.data.maxMaskingLevel = page.dm.responseArea.maxMaskingLevel;
    } else {
      $scope.data.maxMaskingLevel = 80;
    }

    // Starting channel.  Start with channel defined in exame props, or default left
    var channel = defaultExamProperties.OutputChannel || "HPL0";

    $scope.btnText = ["Play Tones", "Stop Tones"];
    if (channel.indexOf("HPR") > -1) {
      $scope.data.channel = "Right";
      $scope.data.maskingChannel = "Left";
    } else if (channel.indexOf("HPL") > -1) {
      $scope.data.channel = "Left";
      $scope.data.maskingChannel = "Right";
    } else if (channel.indexOf("NONE LINEL0") > -1) {
      $scope.data.channel = "Bone (Right)";
      $scope.data.maskingChannel = "Left";
    } else if (channel.indexOf("LINEL0") > -1) {
      channel = "LINEL0 NONE"; // make sure channel is fully specified (mostly for results consistency)
      $scope.data.channel = "Bone (Left)";
      $scope.data.maskingChannel = "Right";
    }

    // set level units
    var levelUnits =
      defaultExamProperties.LevelUnits ||
      chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].examProperties.LevelUnits;
    if (angular.isUndefined(levelUnits)) {
      logger.warn("no levelUnits defined for chaManualAudiometry, defaulting to dB HL");
      $scope.data.levelUnits = "dB HL";
    } else {
      $scope.data.levelUnits = levelUnits;
    }

    $scope.playTonesButtonClass = function() {
      let presentButtonClass;
      if ($scope.data.channel === "Right" || $scope.data.channel === "Bone (Right)") {
        presentButtonClass = ["btn btn-block btn-danger", "btn btn-block btn-danger active"];
      } else if ($scope.data.channel === "Left" || $scope.data.channel === "Bone (Left)") {
        presentButtonClass = ["btn btn-block btn-primary", "btn btn-block btn-primary active"];
      } else {
        presentButtonClass = ["btn btn-block btn-default", "btn btn-block btn-default active"];
      }
      return presentButtonClass[$scope.btnState];
    };

    $scope.playMaskingButtonClass = function() {
      let presentButtonClass;
      if ($scope.data.maskingChannel === "Right") {
        presentButtonClass = ["btn btn-block btn-danger", "btn btn-block btn-danger active"];
      } else if ($scope.data.maskingChannel === "Left") {
        presentButtonClass = ["btn btn-block btn-primary", "btn btn-block btn-primary active"];
      } else {
        presentButtonClass = ["btn btn-block btn-default", "btn btn-block btn-default active"];
      }
      return presentButtonClass[$scope.btnMaskingState];
    };

    //Check that data.currentLevel falls within the data.minLevel and
    //data.maxLevel bounds - this isn't a thorough check, as the headset may
    //be calibrated with tighter bounds at particular frequencies
    function levelBoundsCheck() {
      if ($scope.data.currentLevel > $scope.data.maxLevel) {
        $scope.data.currentLevel = $scope.data.maxLevel;
      } else if ($scope.data.currentLevel < $scope.data.minLevel) {
        $scope.data.currentLevel = $scope.data.minLevel;
      }
    }

    $scope.increase = function(val) {
      stopPresentation(); // stop presentation, if started
      $scope.data.currentLevel += val;
      levelBoundsCheck();
    };

    $scope.decrease = function(val) {
      stopPresentation(); // stop presentation, if started
      $scope.data.currentLevel -= val;
      levelBoundsCheck();
    };

    function maskingLevelBoundsCheck() {
      if ($scope.data.currentMaskingLevel > $scope.data.maxMaskingLevel) {
        $scope.data.currentMaskingLevel = $scope.data.maxMaskingLevel;
      } else if ($scope.data.currentMaskingLevel < $scope.data.minMaskingLevel) {
        $scope.data.currentMaskingLevel = $scope.data.minMaskingLevel;
      }
    }

    $scope.increaseMasking = function(val) {
      // TODO ASK: Does the presentation stop if the masking is changed?
      // stopPresentation(); // stop presentation, if started

      $scope.data.currentMaskingLevel += val;
      maskingLevelBoundsCheck();

      // update masking level if currently active
      if ($scope.btnMaskingState === 1) {
        presentMasking();
      }
    };

    $scope.decreaseMasking = function(val) {
      // TODO ASK: Does the presentation stop if the masking is changed?
      // stopPresentation(); // stop presentation, if started

      $scope.data.currentMaskingLevel -= val;
      maskingLevelBoundsCheck();

      // update masking level if currently active
      if ($scope.btnMaskingState === 1) {
        presentMasking();
      }
    };

    function setFrequencyAndLevel() {
      $scope.data.currentFrequency = chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].F;
      // switching to a new frequency.  If we already used this frequency, then there should be a threshold, or a levelprogression (use the last used level)
      if (
        $scope.data.responseType === "threshold" &&
        angular.isDefined(chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].Threshold)
      ) {
        $scope.data.currentLevel =
          chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].Threshold;
        $scope.data.currentMaskingLevel =
          chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].FinalMaskingLevel;
      } else if (
        angular.isDefined(
          chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].L &&
            chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].L.length > 0
        )
      ) {
        $scope.data.currentLevel = chaExams.storage.manualAudiometry.list[$scope.data.channel][
          presentationIndex
        ].L.slice(-1)[0];
        $scope.data.currentMaskingLevel = chaExams.storage.manualAudiometry.list[$scope.data.channel][
          presentationIndex
        ].MaskingLevel.slice(-1)[0];
      } else {
        // no previous use, look for Lstart, otherwise default to 40
        $scope.data.currentLevel =
          chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].Lstart ||
          defaultExamProperties.Lstart ||
          40; // default is 40
        levelBoundsCheck();
        //only check bounds if not setting based on a previously set threshold.
        //Not sure how you could have gotten a threshold outside of the bounds,
        //but if you did, don't want to force it to the bounds

        $scope.data.currentMaskingLevel = 40; // no other default for now
        maskingLevelBoundsCheck();
      }
    }

    $scope.chooseFrequency = function(index) {
      stopPresentation(); // stop presentation, if started
      stopMasking(); // stop masking, if started

      presentationIndex = index;
      setFrequencyAndLevel();
      updateLevelProgressionPlot();
    };

    function nextFrequency() {
      if (presentationIndex < chaExams.storage.manualAudiometry.list[$scope.data.channel].length - 1) {
        $scope.chooseFrequency(presentationIndex + 1);
      }

      // reset index to 0 and go to the next ear
      else {
        presentationIndex = 0;
        let channel_keys = Object.keys(chaExams.storage.manualAudiometry.list);
        let channel_idx = channel_keys.indexOf($scope.data.channel);
        let next_channel_idx = channel_idx === channel_keys.length - 1 ? 0 : channel_idx + 1;
        $scope.selectOutput(channel_keys[next_channel_idx]);
      }
    }

    $scope.setThreshold = function() {
      stopPresentation(); // stop presentation, if started
      stopMasking(); // stop masking, if started

      // store current level
      chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].Threshold =
        $scope.data.currentLevel;

      // store final masking level
      chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].FinalMaskingLevel =
        $scope.data.FinalMaskingLevel;

      // move on to the next frequency
      nextFrequency();
    };

    $scope.setPass = function() {
      stopPresentation(); // stop presentation, if started
      stopMasking(); // stop masking, if started

      chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].Threshold = "P";
      chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].FinalMaskingLevel =
        $scope.data.FinalMaskingLevel;

      // move on to the next frequency
      nextFrequency();
    };

    $scope.setFail = function() {
      stopPresentation(); // stop presentation, if started
      stopMasking(); // stop masking, if started

      chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].Threshold = "F";
      chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].FinalMaskingLevel =
        $scope.data.FinalMaskingLevel;

      // move on to the next frequency
      nextFrequency();
    };

    $scope.selectOutput = function(channel) {
      // return immediately if channel is not actually changed
      if ($scope.data.channel === channel) {
        return;
      }

      stopPresentation(); // stop presentation, if started
      stopMasking(); // stop masking, if started

      $scope.data.channel = channel; // set channel
      setFrequencyAndLevel();
      // presentationIndex = 0; // reset presentation number

      // toggle masking channel if it includes the set channel (i.e. Left or Bone (Left) )
      if (channel.indexOf($scope.data.maskingChannel) > -1) {
        if ($scope.data.maskingChannel === "Left") {
          $scope.data.maskingChannel = "Right";
        } else if ($scope.data.maskingChannel === "Right") {
          $scope.data.maskingChannel = "Left";
        }
      }
      updateLevelProgressionPlot();
    };

    $scope.selectMaskingChannel = function(channel) {
      // return immediately if masking channel is not actually changed
      if ($scope.data.maskingChannel === channel) {
        return;
      }

      stopMasking(); // stop masking
      stopPresentation(); // stop presentation, if started

      $scope.data.maskingChannel = channel; // set masking channel
    };

    function stopPresentation() {
      return cha.abortExams().then(function() {
        $scope.btnState = 0;
        updateLevelProgressionPlot();

        // TODO: make sure masking is not turned off by abortExams
        // restart masking if it was already engaged
        if ($scope.btnMaskingState === 1) {
          presentMasking();
        }
      });
    }

    function presentFrequency() {
      $scope.btnState = 1;

      if (!angular.isDefined(chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].L)) {
        chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].L = [];
      }
      chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].L.push($scope.data.currentLevel);
      chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].examProperties.Lstart =
        $scope.data.currentLevel;

      let examProperties =
        chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].examProperties;

      var waitFunction = function() {
        chaExams.wait
          .forReadyState()
          .then(cha.requestResults()) //try to add a step where we check if things worked out.
          .then(function() {
            if ($scope.btnState === 1) {
              $scope.btnState = 0;
              updateLevelProgressionPlot();
            }
          })
          .catch(function(err) {
            cha.errorHandler.main(err);
          });
      };

      cha
        .requestStatusBeforeExam()
        .then(function() {
          return cha.queueExam("HughsonWestlake", examProperties);
        })
        .then(waitFunction)
        .catch(function(err) {
          cha.errorHandler.main(err);
        });
    }

    $scope.toggleToneButton = function() {
      if ($scope.btnState === 1) {
        stopPresentation();
      } else {
        if ($scope.btnMaskingState === 1) {
          // If masking is enabled (based on button state), set masking level to current level displayed in UI.
          $scope.data.FinalMaskingLevel = $scope.data.currentMaskingLevel;
        } else {
          // If masking is turned off, set masking level to null.
          $scope.data.FinalMaskingLevel = NaN;
        }
        presentFrequency();
      }
    };

    function stopMasking() {
      cha.stopNoiseFeature();
      $scope.btnMaskingState = 0;
    }

    function presentMasking() {
      $scope.btnMaskingState = 1;

      // store the current masking level on the storage
      if (
        !angular.isDefined(chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].MaskingLevel)
      ) {
        chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].MaskingLevel = [];
      }
      chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].MaskingLevel.push(
        $scope.data.currentMaskingLevel
      );

      // get RetSPL to correct masking level from Effective Masking (EM) to SPL
      let RetSPL = cha.getRetSPL($scope.data.currentFrequency, $scope.data.maskingChannel);
      if (!RetSPL) {
        notifications.alert(
          `Failed to convert ${$scope.data.currentMaskingLevel} dB EM to dB SPL for Frequency ${
            $scope.data.currentFrequency
          }`
        );
        return;
      }

      // get 1/3 octave correction factor from Sections 6.3.1 and 7.4.1 of the ANSI standard S3.6
      let maskingCorrectionFactors = {
        125: 4,
        160: 4,
        200: 4,
        250: 4,
        315: 4,
        400: 4,
        500: 4,
        630: 5,
        750: 5,
        800: 5,
        1000: 6,
        1250: 6,
        1500: 6,
        1600: 6,
        2000: 6,
        2500: 6,
        3000: 6,
        3150: 6,
        4000: 5,
        5000: 5,
        6000: 5,
        6300: 5,
        8000: 5,
        9000: 5,
        10000: 5,
        11200: 5,
        12500: 5,
        14000: 5,
        16000: 5
      };

      let correctionFactor = maskingCorrectionFactors[$scope.data.currentFrequency];
      if (!correctionFactor) {
        notifications.alert(
          `Failed to convert ${$scope.data.currentMaskingLevel} dB EM to dB SPL for Frequency ${
            $scope.data.currentFrequency
          }`
        );
        return;
      }

      let channelLevel =
        $scope.data.maskingChannel === "Left"
          ? [$scope.data.currentMaskingLevel + RetSPL + correctionFactor, 0]
          : [0, $scope.data.currentMaskingLevel + RetSPL + correctionFactor]; // ignored for non-test ear

      let ear = $scope.data.maskingChannel === "Left" ? 0 : 1; // Channel to be used for the noise 0=Left, 1=Right, 2=Both

      cha
        .requestStatus()
        .then(function(status) {
          if (status.Flags & chaConstants.flags.CHA_FLAGS_NOISE_ON) {
            return cha.changeNoiseFeatureLevel(channelLevel);
          } else {
            // NOTE: Ear is not specified so that 0 level is presented to bone oscillator
            return cha.startNoiseFeature({
              Ear: ear,
              Level: channelLevel,
              BandpassCenterFrequency: $scope.data.currentFrequency,
              BandpassOctaveWidth: 3 // TODO: Bug in CHA docs - this should be 1/3 octave width
            });
          }
        })
        .catch(function(err) {
          cha.errorHandler.main(err);
        });
    }

    $scope.toggleMaskingButton = function() {
      // stopPresentation(); // stop presentation, if started

      if ($scope.btnMaskingState === 1) {
        stopMasking();
      } else {
        presentMasking();
      }
    };

    function submitResults() {
      var earSide = "";

      function submitSingleResult(pres, index) {
        var presId;
        if (angular.isDefined(pres.id)) {
          presId = earSide + "_" + pres.id;
        } else {
          presId = defaultPresentationId + earSide + "_HW" + pres.examProperties.F;
        }

        if (page.dm.responseArea.onlySubmitFrequenciesTested) {
          if (angular.isUndefined(pres.Threshold)) {
            return;
          }
        }

        page.result = results.default(page.dm);
        page.result = $.extend({}, page.result, {
          examType: examType,
          presentationId: presId,
          responseStartTime: startTime,
          chaInfo: chaExams.getChaInfo(),
          ResponseType: $scope.data.responseType,
          presentationIndex: index,
          ResultType: angular.isDefined(pres.Threshold) ? $scope.data.responseType : "Skipped",
          Threshold: angular.isDefined(pres.Threshold) ? pres.Threshold : Number.NaN,
          FinalMaskingLevel: angular.isDefined(pres.FinalMaskingLevel) ? pres.FinalMaskingLevel : Number.NaN,
          RetSPL: 0,
          Units: defaultExamProperties.LevelUnits || "dB HL",
          L: pres.L,
          response: angular.isDefined(pres.Threshold) ? pres.Threshold : Number.NaN,
          examProperties: pres.examProperties
        });
        logger.debug("CHA pushing maunal audiometry presentation result onto stack: " + angular.toJson(page.result));
        examLogic.pushResults();
      }

      earSide = "Left";
      _.forEach(chaExams.storage.manualAudiometry.list.Left, submitSingleResult);
      earSide = "Right";
      _.forEach(chaExams.storage.manualAudiometry.list.Right, submitSingleResult);

      // add bone conduction to results
      if ($scope.boneConduction) {
        earSide = "BoneLeft";
        _.forEach(chaExams.storage.manualAudiometry.list["Bone (Left)"], submitSingleResult);

        earSide = "BoneRight";
        _.forEach(chaExams.storage.manualAudiometry.list["Bone (Right)"], submitSingleResult);
      }

      page.result = {
        presentationId: "ManualAudiometry",
        responseStartTime: startTime,
        response: "complete"
      };

      // clear storage
      delete chaExams.storage.manualAudiometry;

      // push results
      examLogic.submit = examLogic.submitDefault;
      examLogic.submit();
    }

    function updateLevelProgressionPlot() {
      // skip if the user has elected not to show level progression
      if (!$scope.showPresentedTones) {
        return;
      }

      $scope.showLevelProgression = false;

      if (
        angular.isUndefined(chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].L) ||
        chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].L.length <= 0
      ) {
        return;
      }

      var tmpResult = {
        L: chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].L,
        Units: defaultExamProperties.LevelUnits || "dB HL"
      };
      var plotData = chaResults.createLevelProgressionData(tmpResult);
      plotData.maxY = 120;
      plotData.minY = -10;
      plotData.minX = 10;
      plotData.plotWidth = $scope.plotContainerWidth;
      plotData.plotHeight = $scope.plotContainerHeight;

      $scope.levelProgressionData = plotData;

      $timeout(function() {
        $scope.panes.progression = true;
        $scope.showLevelProgression = true;
      }, 0);
    }

    // TODO: this is not being used yet
    // only a stub for the future - need to update `chaResults` method
    // to create audiogram from current page's results
    function updateAudiogram() {
      $scope.showAudiogram = false;

      $scope.audiogramData = chaResults.createAudiometryResults(page.dm.responseArea.displayIds)[1];

      //
      $timeout(function() {
        $scope.panes.audiogram = true;
        $scope.showAudiogram = true;
      }, 0);
    }

    // start off in the first frequency
    $scope.chooseFrequency(presentationIndex);

    cha.errorHandler.responseArea = function(err) {
      // Display error to user with popup notifications
      logger.error("CHA - manual-audiometry error: " + JSON.stringify(err));
      $scope.chaError = true;

      if (err.msg) {
        //special error handling for exceeding maximum calibrated level.
        if (err.code === 10) {
          notifications.alert(
            "The target amplitude at this frequency is outside the calibrated bounds for this headset."
          );
          //reset button state
          $scope.btnState = 0;
          //reset the last entry on the list. No need to store something out of range.
          chaExams.storage.manualAudiometry.list[$scope.data.channel][presentationIndex].L.pop();
        } else if (err.code !== 1) {
          //ignore code 1, which is an error from trying to ask the WAHTS to do something while it's busy
          notifications.alert(err.msg);
        }
      } else {
        notifications.alert("Internal error, please restart the Manual Audiometry test");
      }
    };
  });
