/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import "../../audiometry/software-button/software-button";

/* jshint bitwise: false */
/* globals ChaWrap: false */

angular
  .module("cha.gap", ["cha.software-button"])
  .directive("gapExam", function() {
    return {
      restrict: "E",
      templateUrl: "scripts/components/response-areas/cha-complex-response-areas/gap/gap.html",
      controller: "gapExamCtrl"
    };
  })
  .controller("gapExamCtrl", function(
    $scope,
    cha,
    examLogic,
    page,
    chaExams,
    $timeout,
    logger,
    chaAudiometryService,
    notifications,
    chaResults
  ) {
    $scope.useSoftwareButton = chaExams.examProperties.UseSoftwareButton;
    $scope.startExam = function(examType, examProperties) {
      return cha
        .requestStatusBeforeExam()
        .then(switchToExamView)
        .then(startNoiseIfCalledFor)
        .then(function() {
          if (angular.isDefined(page.dm.responseArea.delay)) {
            return $timeout(function() {
              return cha.queueExam(examType, examProperties);
            }, page.dm.responseArea.delay);
          } else {
            return cha.queueExam(examType, examProperties);
          }
        })
        .catch(function(err) {
          cha.errorHandler.main(err);
        });
    };

    $scope.startGapExam = function(gapLength, noiseLevel, fullExam) {
      var examProperties = {};
      if (!$scope.gapTraining || fullExam) {
        // actually queue the exam.
        $scope.gapTraining = false; // disable the training section
        examProperties = chaExams.examProperties;
      } else {
        // queue training exam
        examProperties.UseSoftwareButton = 1;
        examProperties.AllowableGapLengths = [gapLength];
        examProperties.LNoise = noiseLevel;

        examProperties.GapLengthStartIndex = 0;
        examProperties.NReversals = 1;
        examProperties.NReversalsCalc = 1;
        examProperties.NLowestReversals = 0;
        examProperties.NPresMax = 1;
        examProperties.NHits = 1;
        examProperties.NMiss = 1;
      }
      $scope.gapState = "exam";
      chaExams.resetPage();
      chaAudiometryService.reset();
      chaAudiometryService.examDone = false;

      $scope
        .startExam(chaExams.examType, examProperties)
        .then(chaExams.wait.forReadyState)
        .then(function() {
          chaAudiometryService.examDone = true;
          // check to make sure we have not paused.  If state is still exam, request results
          if (chaExams.state === "exam") {
            cha
              .requestResults()
              .then(chaAudiometryService.processResults)
              .then(processResults)
              .then(function() {
                page.dm.isSubmittable = !$scope.gapTraining;
                if (page.dm.responseArea.autoSubmit && !$scope.gapTraining) {
                  examLogic.submit();
                }
              })
              .catch(function(err) {
                if (err.msg !== "repeating exam") {
                  cha.errorHandler.main(err);
                }
              })
              .finally(function() {
                $scope.gapState = "results";
              });
          }
        });
    };

    chaExams.setup(chaExams.complexExamType, page.dm.responseArea.examProperties);
    $scope.gapTraining = page.dm.responseArea.training;
    $scope.gapLengths = chaExams.examProperties.AllowableGapLengths;
    if (
      page.dm.responseArea.training &&
      (!$scope.gapLengths || ($scope.gapLengths && $scope.gapLengths.length === 0))
    ) {
      $scope.gapLengths = [40, 20, 12, 6, 0];
    }
    $scope.gapState = "start";
    $scope.noiseLevel = 65;
    if (!$scope.gapTraining) {
      $scope.startGapExam(undefined, undefined, true);
    }

    function processResults() {
      delete page.result.ResultType;
      delete page.result.response;
      $scope.result = page.result;

      if ($scope.gapTraining) {
        var correct = page.result.HitOrMiss === 1 ? "CORRECTLY" : "INCORRECTLY";
        notifications.alert("Gap detected " + correct);
      }
      $scope.gapResultsData = chaResults.createGapData($scope.result);
      $scope.gapState = "results";
      chaExams.state = "results";
    }

    function switchToExamView() {
      chaExams.state = "exam";

      // reset the MainText and SubText to the original page. This will happen when a test is failed and the person repeats the test
      $scope.page.dm.questionMainText = page.dm.questionMainText;
      $scope.page.dm.questionSubText = page.dm.questionSubText;
      $scope.page.dm.instructionText = page.dm.examInstructions; // custom instructions for each response area - blank if no instructions provided
    }

    function startNoiseIfCalledFor() {
      if (angular.isDefined(page.dm.responseArea.maskingNoise)) {
        cha.startNoiseFeature(page.dm.responseArea.maskingNoise);
      }
    }
  })

  .directive("gapResultsPlot", function() {
    return {
      restrict: "E",
      template: '<div id="gapResultsPlotWindow"></div>',
      controller: "GapResultsPlotCtrl",
      scope: {
        data: "="
      }
    };
  })

  .controller("GapResultsPlotCtrl", function($scope, d3Services) {
    d3Services.gapResultsPlot("#gapResultsPlotWindow", $scope.data);
  });
