/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import "./complex-response-areas.ctrl";
import "./manual-audiometry/manual-audiometry";
import "./manual-tone-generation/manual-tone-generation";
import "./audiometry-list/audiometry-list";
import "./sound-recognition/sound-recognition";
import "./playsound-array/playsound-array";
import "./gap/gap";
import "./calibration-check/calibration-check";

angular.module("tabsint.components.response-areas.cha.complex-response-areas", [
  "cha.complex-response-areas.ctrl",
  "cha.manual-audiometry",
  "cha.manual-tone-generation",
  "cha.audiometry-list",
  "cha.sound-recognition",
  "cha.playsound-array",
  "cha.gap",
  "cha.calibration-check"
]);
