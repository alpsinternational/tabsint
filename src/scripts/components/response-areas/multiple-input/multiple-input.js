/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";

angular
  .module("tabsint.components.response-areas.multiple-input", [])

  .controller("MultipleInputResponseAreaCtrl", function($scope, page) {
    $scope.list = page.dm.responseArea.inputList;
    var inputTypeForAll = page.dm.responseArea.inputTypeForAll;

    // defaults
    $scope.list.forEach(function(item) {
      if (item.inputType === angular.undefined) {
        item.inputType = inputTypeForAll || "text";
      }

      if (item.value === angular.undefined) {
        item.value = undefined;
      }
    });

    // set response to values in the list
    $scope.page.result.response = _.map($scope.list, function(item) {
      return item.value;
    });

    // Dropdown logic
    $scope.selectResponse = function(itemIdx, option) {
      $scope.page.result.response[itemIdx] = option;
    };

    function getSubmittableLogic() {
      var res = true;
      $scope.list.forEach(function(item, idx) {
        if (item.required && isUndefined(item, $scope.page.result.response[idx])) {
          res = false;
        }
      });
      $scope.page.dm.isSubmittable = res;
    }
    getSubmittableLogic();

    // submission logic
    $scope.$watch(
      "page.result.response",
      function() {
        getSubmittableLogic();
      },
      true
    );

    // custom isUndefined function to handle text
    function isUndefined(item, val) {
      if (item.inputType === "text" || item.inputType === "number") {
        return val === "" || val === null || val === undefined;
      } else {
        return angular.isUndefined(val);
      }
    }
  });
