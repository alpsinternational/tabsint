/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as $ from "jquery";

angular
  .module("tabsint.components.response-areas.free-form", [])

  .directive("canvasDirective", function CanvasDirective() {
    return {
      restrict: "A",
      link: function(scope, element) {
        var canvas = document.getElementById("canvas");
        var ctx = canvas.getContext("2d");
        canvas.width = window.innerWidth * 0.9;
        canvas.height = window.innerHeight * 0.5;

        scope.setBackground = function() {
          if (!scope.image) {
            ctx.fillStyle = "LightGray";
            ctx.fillRect(0, 0, canvas.width, canvas.height);
          }
        };

        scope.setBackground();

        var isDrawing = false;
        var lastMouse = { x: 0, y: 0 };

        element.bind("mousedown", function(event) {
          lastMouse.x = event.offsetX || event.clientX;
          lastMouse.y = event.offsetY || event.clientY;
          ctx.beginPath();
          isDrawing = true;
        });

        element.bind("mousemove", function(event) {
          if (isDrawing) {
            ctx.moveTo(lastMouse.x, lastMouse.y);
            ctx.lineTo(event.offsetX, event.offsetY);
            ctx.strokeStyle = "black";
            ctx.stroke();
            lastMouse.x = event.offsetX || event.clientX;
            lastMouse.y = event.offsetY || event.clientY;
          }
        });

        element.bind("mouseup", function(event) {
          isDrawing = false;
          scope.save();
        });

        element.bind("mouseleave", function(event) {
          isDrawing = false;
          scope.save();
        });

        element.bind("touchstart", function(event) {
          event.preventDefault();
          isDrawing = true;
          lastMouse = getTouchPos(canvas, event);
          ctx.beginPath();
          // console.log("move X:"+lastMouse.x+" Y:"+lastMouse.x);
        });

        element.bind("touchend", function(event) {
          event.preventDefault();
          isDrawing = false;
          scope.save();
        });

        element.bind("touchmove", function(event) {
          event.preventDefault();
          if (isDrawing) {
            var mousePos = getTouchPos(canvas, event);
            // console.log("move X:"+mousePos.x+" Y:"+mousePos.x);
            ctx.moveTo(lastMouse.x, lastMouse.y);
            ctx.lineTo(mousePos.x, mousePos.y);
            ctx.strokeStyle = "black";
            ctx.stroke();
            lastMouse.x = mousePos.x;
            lastMouse.y = mousePos.y;
          }
        });

        function getTouchPos(canvasDom, touchEvent) {
          var rect = canvasDom.getBoundingClientRect();
          return {
            x: touchEvent.touches[0].clientX - rect.left,
            y: touchEvent.touches[0].clientY - rect.top
          };
        }
      }
    };
  })

  .controller("FreeFormResponseAreaCtrl", function($scope, page) {
    if (
      page.dm.responseArea.image &&
      page.dm.responseArea.image.path &&
      page.dm.responseArea.image.path.split("/").pop() !== "null"
    ) {
      $scope.image = page.dm.responseArea.image;
    }
    var element = document.getElementById("canvas");
    $scope.canvas = element;

    page.dm.isSubmittable = true;
    page.result.response = "";

    $scope.clear = function() {
      // eslint-disable-next-line no-self-assign
      element.width = element.width;
      $scope.setBackground();
      $scope.save();
    };

    $scope.save = function() {
      var canvas = document.getElementById("canvas");
      page.result.response = canvas.toDataURL();
    };
  });
