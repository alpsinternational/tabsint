/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

angular
  .module("tabsint.components.response-areas.audiometry-input", [])

  .controller("AudiometryInputResponseAreaCtrl", function($scope, adminLogic, page) {
    $scope.response = page.result.response;
    page.dm.isSubmittable = true;

    $scope.response = {
      Age: undefined,
      Gender: undefined,
      "Audiometry Month": undefined,
      "Audiometry Day": undefined,
      "Audiometry Year": undefined,
      right: {},
      left: {}
    };

    $scope.patientInfoList = [];
    $scope.audiogramFrequenciesDefault = [250, 500, 1000, 2000, 3000, 4000, 6000, 8000];
    $scope.audiogramFrequencies = [];
    $scope.ageOptions = ["N/A"];
    $scope.genderOptions = ["N/A", "Male", "Female"];
    $scope.monthOptions = [
      "N/A",
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "June",
      "July",
      "Aug",
      "Sept",
      "Oct",
      "Nov",
      "Dec"
    ];
    $scope.dayOptions = ["N/A"];
    $scope.yearOptions = ["N/A"];
    $scope.thresholdOptions = ["N/A"];

    var currentDate = new Date();

    var fieldsToSkip = page.dm.responseArea.fieldsToSkip || {};
    var specifiedAudiogramFrequencies = page.dm.responseArea.audiogramFrequencies;
    var skipAge = fieldsToSkip.skipAge;
    var skipGender = fieldsToSkip.skipGender;
    var skipDate = fieldsToSkip.skipDate;
    var minAge = 18,
      maxAge = 80,
      minDay = 1,
      maxDay = 31,
      minYear = 2012,
      maxYear = currentDate.getFullYear();
    var thresholdMin = -20,
      thresholdMax = 120,
      thresholdInc = 5;

    if (specifiedAudiogramFrequencies === angular.undefined) {
      $scope.audiogramFrequencies = $scope.audiogramFrequenciesDefault;
    } else {
      $scope.audiogramFrequencies = specifiedAudiogramFrequencies;
    }

    if (!skipAge) {
      $scope.patientInfoList.push("Age");

      for (var age = minAge; age <= maxAge; age++) {
        $scope.ageOptions.push(age.toString());
      }
    }

    if (!skipGender) {
      $scope.patientInfoList.push("Gender");
    }

    if (!skipDate) {
      $scope.patientInfoList.push("Audiogram Date");

      for (var day = minDay; day <= maxDay; day++) {
        $scope.dayOptions.push(day.toString());
      }

      for (var year = minYear; year <= maxYear; year++) {
        $scope.yearOptions.push(year.toString());
      }
    }

    for (var threshold = thresholdMin; threshold <= thresholdMax; threshold += thresholdInc) {
      $scope.thresholdOptions.push(threshold.toString());
    }

    function update() {
      page.result.response = $scope.response;
    }

    update();

    // Dropdown logic
    $scope.selectResponse = function(resType, frequency, ear, option) {
      if (resType === "audio") {
        if (ear === "right") {
          $scope.response.right[frequency] = option;
        } else if (ear === "left") {
          $scope.response.left[frequency] = option;
        }
      } else {
        $scope.response[resType] = option;
      }
    };
  });
