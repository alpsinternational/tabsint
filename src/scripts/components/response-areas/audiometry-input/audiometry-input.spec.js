/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import "angular-mocks";
import "../../../app";

beforeEach(angular.mock.module("tabsint"));

var examLogic;

// beforeEach(angular.mock.inject(function(_examLogic_) {
//   examLogic = _examLogic_;
// }));

describe("Response Area: Audiometry Input", function() {
  var scope, page;

  beforeEach(
    angular.mock.inject(function($controller, $rootScope, _page_) {
      _page_.dm = {
        responseArea: {
          fieldsToSkip: {}
        }
      };
      _page_.result = {
        response: undefined
      };

      page = _page_;

      scope = $rootScope.$new(); // start a new scope

      $controller("AudiometryInputResponseAreaCtrl", {
        $scope: scope
      });
    })
  );

  it("should have default options defined", function() {
    expect(scope.audiogramFrequenciesDefault).toEqual([250, 500, 1000, 2000, 3000, 4000, 6000, 8000]);
    expect(scope.genderOptions).toEqual(["N/A", "Male", "Female"]);
    expect(scope.monthOptions).toEqual([
      "N/A",
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "June",
      "July",
      "Aug",
      "Sept",
      "Oct",
      "Nov",
      "Dec"
    ]);
  });

  it("should have inputs propagate to the response", function() {
    var audio_response_1 = 15,
      audio_response_2 = -10,
      audio_response_3 = 100;
    var freq_1 = 250,
      freq_2 = 1000,
      freq_3 = 8000;
    var age = 32;
    var gender = "Female";
    var month = "Feb";
    var day = 15;
    var year = 2014;

    scope.selectResponse("audio", freq_1, "right", audio_response_1);
    scope.selectResponse("audio", freq_2, "left", audio_response_2);
    scope.selectResponse("audio", freq_3, "right", audio_response_3);
    scope.selectResponse("Age", null, null, age);
    scope.selectResponse("Gender", null, null, gender);
    scope.selectResponse("Audiometry Month", null, null, month);
    scope.selectResponse("Audiometry Day", null, null, day);
    scope.selectResponse("Audiometry Year", null, null, year);

    expect(scope.response.right[freq_1]).toEqual(audio_response_1);
    expect(scope.response.left[freq_2]).toEqual(audio_response_2);
    expect(scope.response.right[freq_3]).toEqual(audio_response_3);
    expect(scope.response.Age).toEqual(age);
    expect(scope.response.Gender).toEqual(gender);
    expect(scope.response["Audiometry Month"]).toEqual(month);
    expect(scope.response["Audiometry Day"]).toEqual(day);
    expect(scope.response["Audiometry Year"]).toEqual(year);
  });
});
