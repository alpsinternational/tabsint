/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";

angular
  .module("tabsint.components.partial-result", [])

  .factory("partialResult", function($uibModal, logger, disk) {
    var partialResult = {};

    /**
     * Check to see if there are results and responses on `disk.currentResults` object
     * Prompt the user to save if results are found
     */
    partialResult.check = function() {
      // if results and responses exist on disk.currentResults, present modal
      if (
        disk.currentResults &&
        disk.currentResults.testResults &&
        disk.currentResults.testResults.responses &&
        disk.currentResults.testResults.responses.length > 0
      ) {
        logger.warn("Found temporary results stored on disk during startup");

        // Modal Controller
        var PartialResultCtrl = function(
          $scope,
          $uibModalInstance,
          logger,
          disk,
          results,
          notifications,
          gettextCatalog
        ) {
          $scope.partialResult = disk.currentResults;

          $scope.save = function() {
            logger.info("User opted to save stored partial results found on disk during startup.");

            // tabulate as done in examLogic finalize
            disk.currentResults.nCorrect = 0;
            disk.currentResults.nIncorrect = 0;
            disk.currentResults.nResponses = 0;

            for (var i = 0; i < disk.currentResults.testResults.responses.length; i++) {
              var response = disk.currentResults.testResults.responses[i];
              disk.currentResults.nResponses += 1;

              if (typeof response.correct === "string") {
                if (_.includes(angular.fromJson(response.correct), false)) {
                  disk.currentResults.nIncorrect += 1;
                } else {
                  disk.currentResults.nCorrect += 1;
                }
              } else if (response.correct === true) {
                disk.currentResults.nCorrect += 1;
              } else if (response.correct === false) {
                disk.currentResults.nIncorrect += 1;
              }
            }

            // set flag telling analysis engines this was a salvaged test
            disk.currentResults.testResults.storedResultsFoundOnStartup = true;
            results.save(disk.currentResults);
            disk.currentResults = undefined;

            $uibModalInstance.close();
          };

          $scope.discard = function() {
            notifications.confirm(
              gettextCatalog.getString(
                "Are you sure you want to delete this partial exam? This action is irreversible."
              ),
              function(buttonIndex) {
                if (buttonIndex === 1) {
                  logger.info("User opted to discard stored partial results found on disk during startup.");
                  disk.currentResults = undefined;
                  $uibModalInstance.close();
                }
              },
              gettextCatalog.getString("Confirm Delete Partial Result"),
              [gettextCatalog.getString("OK"), gettextCatalog.getString("Cancel")]
            );
          };
        };

        var modalInstance = $uibModal.open({
          templateUrl: "scripts/components/partial-result/partial-result.html",
          controller: PartialResultCtrl,
          backdrop: "static"
        });
      } else {
        // remove any remnant of previously set temporary results
        disk.currentResults = undefined;
      }
    };

    return partialResult;
  });
