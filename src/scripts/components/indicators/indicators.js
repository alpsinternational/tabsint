/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

angular
  .module("tabsint.components.indicators", [])

  .directive("indicators", function() {
    return {
      templateUrl: "scripts/components/indicators/indicators.html",
      controller: function($scope, networkModel, bluetoothStatus, plugins, svantek) {
        $scope.networkModel = networkModel;
        $scope.bluetoothStatus = bluetoothStatus;
        $scope.plugins = plugins;
        $scope.svantek = svantek;
      }
    };
  });
