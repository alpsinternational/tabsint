/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

angular
  .module("tabsint.components.results-warnings", [])

  .directive("resultsWarnings", function() {
    return {
      templateUrl: "scripts/components/results-warnings/results-warnings.html",
      controller: function($scope, disk, results, sqLite) {
        $scope.disk = disk;
        $scope.results = results;
        $scope.sqLite = sqLite;
      }
    };
  });
