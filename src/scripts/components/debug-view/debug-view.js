/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

angular
  .module("tabsint.components.debug-view", [])

  .directive("debugView", function() {
    return {
      templateUrl: "scripts/components/debug-view/debug-view.html",
      controller: function($scope, disk, examLogic, page, results) {
        // show
        $scope.debugMode = disk.debugMode;

        // handle open/closed
        $scope.isCollapsed = true;

        // data from models
        $scope.state = examLogic.dm.state;
        $scope.page = page;
        $scope.results = results;
      },
      scope: {}
    };
  });
