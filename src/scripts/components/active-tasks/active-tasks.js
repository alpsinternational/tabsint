/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

angular
  .module("tabsint.components.active-tasks", [])

  .directive("activeTasks", function() {
    return {
      templateUrl: "scripts/components/active-tasks/active-tasks.html",
      controller: function($scope, tasks, file) {
        $scope.tasks = tasks;
        $scope.file = file;
      }
    };
  });
