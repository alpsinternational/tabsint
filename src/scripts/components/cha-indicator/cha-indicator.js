/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

angular
  .module("tabsint.components.cha.indicator", [])
  .directive("chaIndicator", function() {
    return {
      restrict: "E",
      templateUrl: "scripts/components/cha-indicator/cha-indicator.html",
      controller: "ChaIndicatorCtrl",
      scope: {}
    };
  })
  .controller("ChaIndicatorCtrl", function($scope, cha) {
    var self = this;
    self.cha = cha;
    $scope.cha = cha;
    $scope.chaInfoURL = "scripts/components/cha-info/cha-info.template.html";
  });
