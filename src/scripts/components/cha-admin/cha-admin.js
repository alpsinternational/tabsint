/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";

/* globals ChaWrap: false */

angular
  .module("tabsint.components.cha.admin", ["tabsint.components.cha.info"])

  .directive("chaAdmin", function() {
    return {
      restrict: "E",
      templateUrl: "scripts/components/cha-admin/cha-admin.html",
      controller: "ChaAdminCtrl",
      scope: {}
    };
  })
  .controller("ChaAdminCtrl", function(
    $scope,
    cha,
    chaExams,
    disk,
    notifications,
    tasks,
    $q,
    logger,
    gettextCatalog,
    bluetoothStatus,
    media,
    $sce,
    chaMedia,
    adminLogic,
    chaConstants
  ) {
    $scope.cha = cha;
    $scope.chaExams = chaExams;
    $scope.chaTasks = chaConstants.chaTasks;
    $scope.disk = disk;
    $scope.tasks = tasks;
    $scope.panes = adminLogic.panes;
    $scope.chaExamInProgress = false;
    $scope.bluetoothStatus = bluetoothStatus;
    var pollInterval;

    $scope.bluetoothTypePopover = $sce.trustAsHtml(
      gettextCatalog.getString(
        "Set the bluetooth type for connecting to the WAHTS. <br><br><b>Bluetooth 2.0</b> is used for the handheld WAHTS <br><b>Bluetooth 3.0</b> is used for the Creare Wireless Headset"
      )
    );
    $scope.disableAudioStreamingPopover = $sce.trustAsHtml(
      gettextCatalog.getString(
        "This option will disable audio streaming. Audio streaming should only be disabled in admin mode for debugging, and may cause issues with some protocols."
      )
    );

    $scope.ignoreFirmwareUpdatesPopover = $sce.trustAsHtml(
      gettextCatalog.getString(
        "This option will ignore firmware update messages when connecting to the WAHTS. Each version of TabSINT supports a specific version of the WAHTS firmware. If this option is not checked, TabSINT will pop a notification if the current version of WAHTS firmware is not supported by TabSINT."
      )
    );
    ``;

    $scope.firmwarePopover = $sce.trustAsHtml(
      gettextCatalog.getString(
        "This version of the WAHTS Firmware is built into TabSINT and can be updated to the WAHTS wirelessly. <br><br>TabSINT will work best if the WAHTS is updated to this version of the firmware."
      )
    );
    $scope.headsetMediaDownloadPopover = $sce.trustAsHtml(
      gettextCatalog.getString(
        "TabSINT can update the media stored on the WAHTS.  Use this panel to download/update WAHTS media from Gitlab.\n\nSee the <b>Gitlab Configuration</b> pane under the <i>Configuration</i> tab to specify the Gitlab host, group and other repository parameters."
      )
    );
    $scope.gitlabAddMediaPopover = $sce.trustAsHtml(
      gettextCatalog.getString(
        "Type in the name of media repository located on the host in the group defined in the <b>Gitlab Configuration</b> pane under the <i>Configuration</i> tab."
      )
    );
    $scope.gitlabAddMediaVersionPopover = $sce.trustAsHtml(
      gettextCatalog.getString(
        "<strong>OPTIONAL:</strong> Type in the repository tag for the version of the repository you would like to download. Leave blank to download the latest tag/commit from the repository."
      )
    );
    $scope.headsetMediaTablePopover = $sce.trustAsHtml(
      gettextCatalog.getString(
        "Table of WAHTS media repositories currently stored in TabSINT.  Select a repo to update, delete, or sync to the WAHTS. <b>NOTE</b> Transmitting media to the headset can take hours.  Make sure the headset and tablet are plugged in."
      )
    );

    $scope.addCreareMediaRepoPopover = $sce.trustAsHtml(
      gettextCatalog.getString(
        "Select this box to download the standard CREARE media on the headset.  Syncing this repo to the headset could change the media used in all standard Creare CHA exams.  Only download and sync this repo if you know what you are doing!"
      )
    );

    $scope.chaMedia = chaMedia;

    $scope.batteryIndicatorWidth = 100;
    $scope.battery = {
      status: undefined,
      level: undefined
    };
    $scope.battery.status = function() {
      if (cha.myCha.vBattery > 3.6) {
        return "Good";
      } else if (cha.myCha.vBattery > 3.0) {
        return "Satisfactory";
      } else {
        return "Low";
      }
    };
    $scope.battery.level = function() {
      var full = $scope.batteryIndicatorWidth - 8;
      if (cha.myCha.vBattery > 3.6) {
        return full;
      } else if (cha.myCha.vBattery > 3.0) {
        return 50;
      } else {
        return 10;
      }
    };

    $scope.updateCHAFirmware = function() {
      notifications.confirm(
        "Are you sure you want to update CHA firmware?",
        function(buttonIndex) {
          if (buttonIndex === 1) {
            cha.updateFirmware();
          }
        },
        "Confirm Firmware Update",
        ["OK", "Cancel"]
      );
    };

    $scope.bluetoothTypes = {
      BLUETOOTH: "Bluetooth 2.0",
      BLUETOOTH_LE: "Bluetooth 3.0",
      USB: "USB Host"
    };

    $scope.getBluetoothType = function() {
      return cha.getBluetoothType();
    };

    $scope.changeBluetoothType = function(type) {
      cha.changeBluetoothType(type);
    };

    $scope.removeCha = function() {
      if (_.includes(["discovery", "autoDiscovery", "connecting"], cha.state)) {
        logger.error("Attempting to disconnect during discovery, autodiscovery, or connecting cha states");
        notifications.alert("The cha is currently running the connection process. Please wait until this has finished");
      } else {
        cha.disconnect();
        disk.cha.myCha = "";
      }
    };

    function pollExam() {
      cha
        .requestStatus()
        .then(function(status) {
          if (status.State === 2) {
            // still running
          } else if (status.State !== 1) {
            return $q.reject({ code: 66, msg: "Cha is in an unknown state" });
          } else if (status.State === 1) {
            // done
            $scope.chaExamInProgress = false;
            clearInterval(pollInterval);
          }
        })
        .catch(function(err) {
          $scope.chaExamInProgress = false;
          clearInterval(pollInterval);
          cha.errorHandler.main(err);
        });
    }

    $scope.stopExam = function() {
      $scope.chaExamInProgress = false;
      clearInterval(pollInterval);
      try {
        cha.setSoftwareButtonState(1);
      } catch (e) {
        logger.warn("CHA Error aborting exams manually: " + e);
      }
    };

    // Start exam, pass exam properties to cha
    $scope.startExam = function(examType, examProperties) {
      try {
        cha.abortExams();
      } catch (e) {
        logger.warn("CHA Error aborting exams when resetExam() executed: " + e);
      }

      $scope.chaExamInProgress = true;
      return cha
        .requestStatus()
        .then(function(status) {
          status.vBattery = Math.round(status.vBattery * 100) / 100;
          logger.debug("CHA - Status before queueing exam: " + angular.toJson(status));
          if (status.State === 2) {
            logger.warn("CHA exam is still running while user queues an exam. Aborting exams...");
            return cha.abortExams();
          } else if (status.State !== 1) {
            return $q.reject({ code: 66, msg: "Cha is in an unknown state" });
          }
        })
        .then(function() {
          return cha.queueExam(examType, examProperties);
        })
        .then(function() {
          pollInterval = setInterval(pollExam, 500);
        })
        .catch(function(err) {
          $scope.chaExamInProgress = false;
          clearInterval(pollInterval);
          cha.errorHandler.main(err);
        });
    };

    $scope.play1kHzTone = function() {
      media.stopAudio();
      media.playAudio([{ src: "/android_asset/www/res/wavs/word01_norm.wav", vol: 1.0 }]);
    };

    $scope.playCompAudio = function() {
      media.stopAudio();
      media.playAudio([{ src: "/android_asset/www/res/wavs/CompAudioTest.wav", vol: 1.0 }]);
    };

    $scope.playCompAudioLinear = function() {
      media.stopAudio();
      media.playAudio([
        {
          src: "/android_asset/www/res/wavs/CompAudioTest_16bit_48kHz_MCR.wav",
          vol: 1.0
        }
      ]);
    };

    $scope.stopAudio = function() {
      media.stopAudio();
    };

    $scope.noise = {
      type: "white",
      level: 55,
      typeList: ["white", "pink"]
    };

    $scope.changeNoiseType = function(newType) {
      $scope.noise.type = newType;
    };

    $scope.noiseOn = function() {
      var ne = {
        Type: $scope.noise.type,
        Level: [$scope.noise.level, $scope.noise.level]
      };
      cha.startNoiseFeature(ne);
    };

    $scope.noiseOff = function() {
      cha.stopNoiseFeature();
    };

    $scope.srint = {
      level: 55,
      enabled: false
    };

    var srintSounds = [
      "C:USER/SRIN/Aircraft/Jet/A-J-0001.wav",
      "C:USER/SRIN/Aircraft/Jet/A-J-0002.wav",
      "C:USER/SRIN/Aircraft/Jet/A-J-0003.wav",
      "C:USER/SRIN/Aircraft/Jet/A-J-0004.wav",
      "C:USER/SRIN/Aircraft/Jet/A-J-0005.wav",
      "C:USER/SRIN/Aircraft/Rotor/A-R-0001.wav",
      "C:USER/SRIN/Aircraft/Rotor/A-R-0002.wav",
      "C:USER/SRIN/Aircraft/Rotor/A-R-0003.wav",
      "C:USER/SRIN/Aircraft/Rotor/A-R-0004.wav",
      "C:USER/SRIN/Aircraft/Rotor/A-R-0005.wav",
      "C:USER/SRIN/Crowd/Positive/C-P-0001.wav",
      "C:USER/SRIN/Crowd/Positive/C-P-0002.wav",
      "C:USER/SRIN/Crowd/Positive/C-P-0003.wav",
      "C:USER/SRIN/Crowd/Positive/C-P-0004.wav",
      "C:USER/SRIN/Crowd/Positive/C-P-0005.wav",
      "C:USER/SRIN/Crowd/Negative/C-N-0001.wav",
      "C:USER/SRIN/Crowd/Negative/C-N-0002.wav",
      "C:USER/SRIN/Crowd/Negative/C-N-0003.wav",
      "C:USER/SRIN/Crowd/Negative/C-N-0004.wav",
      "C:USER/SRIN/Crowd/Negative/C-N-0005.wav",
      "C:USER/SRIN/Crowd/Negative/C-N-0006.wav",
      "C:USER/SRIN/Crowd/Negative/C-N-0007.wav",
      "C:USER/SRIN/Crowd/Negative/C-N-0008.wav",
      "C:USER/SRIN/Crowd/Negative/C-N-0009.wav",
      "C:USER/SRIN/Crowd/Negative/C-N-0010.wav",
      "C:USER/SRIN/Crowd/Negative/C-N-0011.wav",
      "C:USER/SRIN/Crowd/Negative/C-N-0012.wav",
      "C:USER/SRIN/Language/English/L-E-0001.wav",
      "C:USER/SRIN/Language/English/L-E-0002.wav",
      "C:USER/SRIN/Language/English/L-E-0003.wav",
      "C:USER/SRIN/Language/English/L-E-0004.wav",
      "C:USER/SRIN/Language/English/L-E-0005.wav",
      "C:USER/SRIN/Language/English/L-E-0006.wav",
      "C:USER/SRIN/Language/English/L-E-0007.wav",
      "C:USER/SRIN/Language/English/L-E-0008.wav",
      "C:USER/SRIN/Language/English/L-E-0009.wav",
      "C:USER/SRIN/Language/English/L-E-0010.wav",
      "C:USER/SRIN/Language/English/L-E-0011.wav",
      "C:USER/SRIN/Language/Other/L-O-0001.wav",
      "C:USER/SRIN/Language/Other/L-O-0002.wav",
      "C:USER/SRIN/Language/Other/L-O-0003.wav",
      "C:USER/SRIN/Language/Other/L-O-0004.wav",
      "C:USER/SRIN/Language/Other/L-O-0005.wav",
      "C:USER/SRIN/Language/Other/L-O-0006.wav",
      "C:USER/SRIN/Language/Other/L-O-0007.wav",
      "C:USER/SRIN/Language/Other/L-O-0008.wav",
      "C:USER/SRIN/Language/Other/L-O-0009.wav",
      "C:USER/SRIN/Language/Other/L-O-0010.wav",
      "C:USER/SRIN/Language/Other/L-O-0011.wav",
      "C:USER/SRIN/Language/Other/L-O-0012.wav",
      "C:USER/SRIN/Language/Other/L-O-0013.wav",
      "C:USER/SRIN/Language/Other/L-O-0014.wav",
      "C:USER/SRIN/Language/Other/L-O-0015.wav",
      "C:USER/SRIN/Language/Other/L-O-0016.wav",
      "C:USER/SRIN/Language/Other/L-O-0017.wav",
      "C:USER/SRIN/Language/Other/L-O-0018.wav",
      "C:USER/SRIN/Footsteps/Running/F-R-0001.wav",
      "C:USER/SRIN/Footsteps/Running/F-R-0002.wav",
      "C:USER/SRIN/Footsteps/Running/F-R-0003.wav",
      "C:USER/SRIN/Footsteps/Running/F-R-0004.wav",
      "C:USER/SRIN/Footsteps/Walking/F-W-0001.wav",
      "C:USER/SRIN/Footsteps/Walking/F-W-0002.wav",
      "C:USER/SRIN/Footsteps/Walking/F-W-0003.wav",
      "C:USER/SRIN/Footsteps/Walking/F-W-0004.wav",
      "C:USER/SRIN/Footsteps/Walking/F-W-0005.wav",
      "C:USER/SRIN/Gunfire/Indoor/G-I-0001.wav",
      "C:USER/SRIN/Gunfire/Indoor/G-I-0002.wav",
      "C:USER/SRIN/Gunfire/Indoor/G-I-0003.wav",
      "C:USER/SRIN/Gunfire/Outdoor/G-O-0001.wav",
      "C:USER/SRIN/Gunfire/Outdoor/G-O-0002.wav",
      "C:USER/SRIN/Gunfire/Outdoor/G-O-0003.wav",
      "C:USER/SRIN/Gunfire/Outdoor/G-O-0004.wav"
    ];

    $scope.playSRINT = function() {
      $scope.srint.enabled = true;

      function playWav(chaWavFile) {
        return cha
          .requestStatus()
          .then(function(status) {
            status.vBattery = Math.round(status.vBattery * 100) / 100;
            // logger.debug('CHA - Status before queueing exam: ' + angular.toJson(status));
            if (status.State === 2) {
              logger.warn("CHA exam is still running while user queues an exam. Aborting exams...");
              return cha.abortExams();
            } else if (status.State !== 1) {
              return $q.reject({ code: 66, msg: "Cha is in an unknown state" });
            }
          })
          .then(function() {
            function processWav(wav) {
              // assume files that don't start with C: are in the C:USER/ directory
              if (!wav.path.startsWith("C:")) {
                wav.path = "C:USER/" + wav.path;
              }

              if (!wav.Leq) {
                // Leq default
                wav.Leq = [72, 72, 0, 0];
              } else if (wav.Leq.length === 2) {
                // handle 2 single inputs
                wav.Leq.concat(0, 0);
              }
              return wav;
            }

            var wav = processWav(chaWavFile);
            var pse = {
              UseMetaRMS: wav.UseMetaRMS || false,
              SoundFileName: wav.path,
              Leq: wav.Leq
            };

            return cha.queueExam("PlaySound", pse);
          })
          .then(chaExams.wait.forReadyState)
          .then(function() {
            console.log("-- cha SRINT  play sound(s) done");
          })
          .catch(function(e) {
            console.log("-- cha play sound on pageStart failed with error: " + JSON.stringify(e));
          });
      }

      var tmpSoundList = srintSounds.slice(0);

      function playNext() {
        var nextSound = tmpSoundList.splice(0, 1)[0];
        playWav({
          path: nextSound,
          Leq: [$scope.srint.level, $scope.srint.level]
        }).then(function() {
          if ($scope.srint.enabled && tmpSoundList.length > 0) {
            playNext();
          }
        });
      }
      playNext();
    };

    $scope.stopSRINT = function() {
      $scope.srint.enabled = false;
      cha.abortExams();
    };

    $scope.toggleHeadsetMedia = function() {
      if (disk.cha.enableHeadsetMedia) {
        cha.getMediaVersions();
      }
    };

    $scope.toggleStreaming = function() {
      if (!disk.disableAudioStreaming) {
        cha.disconnect();
        notifications.alert("Please reconnect to the WAHTS to complete enabling audio streaming.");
      }
    };
  });
