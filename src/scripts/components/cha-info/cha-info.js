/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

angular.module("tabsint.components.cha.info", []).component("chaInfo", {
  templateUrl: "scripts/components/cha-info/cha-info.template.html",
  controller: [
    "$scope",
    "cha",
    function ChaInfoController($scope, cha) {
      var self = this;
      self.cha = cha;
      $scope.cha = cha;
    }
  ]
});
