/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import "angular-mocks";
import "../../../app";

describe("cha-info test", function() {
  // Load the module that contains the `chaInfo` component before each test
  beforeEach(angular.mock.module("tabsint.components.cha.info"));

  describe("chaInfo component test", function() {
    //TODO: this should inject mock cha...
    var ctrl;
    var cha = {
      name: "A cha",
      info: {
        id: {
          serialNumber: "232352f2f",
          description: "description",
          buildDateTime: "03/22/2017"
        },
        vBattery: 3.7,
        batteryIndicatorWidth: 92
      }
    };

    beforeEach(
      angular.mock.inject(function($componentController) {
        ctrl = $componentController("chaInfo");
        //TODO: need to connect to cha here, I guess, or inject a mock cha?
      })
    );

    it("should have cha injected properly", function() {
      expect(true).toBeTruthy();
      expect(cha).toBeDefined();
      // expect(ctrl.cha.myCha.name).toBeDefined();
      expect(cha.myCha.id.serialNumber).toBeDefined();
      expect(cha.myCha.id.description).toEqual("description");
      // expect(ctrl.cha.myCha.name).toEqual('FAKE CHA');
    });
  });
});
