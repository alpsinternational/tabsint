/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as LZString from "lz-string";

angular
  .module("tabsint.routes.welcome", [])

  .directive("welcomeView", function() {
    return {
      restrict: "E",
      templateUrl: "scripts/routes/welcome/welcome.html",
      controller: "WelcomeCtrl",
      scope: {}
    };
  })

  .controller("WelcomeCtrl", function(
    $q,
    $scope,
    $uibModal,
    adminLogic,
    app,
    autoConfig,
    config,
    disk,
    examLogic,
    gettextCatalog,
    logger,
    notifications,
    router
  ) {
    $scope.app = app;
    $scope.disk = disk;
    $scope.router = router;
    $scope.config = config;

    $scope.switchToExamView = function() {
      examLogic.switchToExamView();
    };

    $scope.switchToAdminView = function() {
      adminLogic.switchToAdminView();
    };

    /**
     * Scan Config Qr code and load it into the config object
     */
    $scope.scanQrCodeandAutoConfig = function() {
      function scanQrCode() {
        var deferred = $q.defer();
        var scanner;
        // check that QR code scanner is enabled
        try {
          scanner = cordova.plugins.barcodeScanner;
        } catch (e) {
          logger.error("Could not load barcode scanner: " + angular.toJson(e, true));
          deferred.reject("Could not load barcode scanner with error: " + e);
        }
        scanner.scan(
          function(result) {
            let decompressed;

            // try parsing with JSON first for backwards compatibility
            // if this fails, parse with LZString
            try {
              let c = JSON.parse(result.text);
              if (typeof c !== "object") {
                throw "Failed to parse text to object";
              }
              decompressed = result.text;
            } catch (e) {
              decompressed = LZString.decompressFromBase64(result.text);
            }

            logger.debug("QR Scan: " + decompressed);

            $scope.$apply(function() {
              if (result.cancelled) {
                deferred.reject("Scanning cancelled");
              }

              // confirm we can read object
              try {
                let c = JSON.parse(decompressed);
                if (typeof c !== "object") {
                  deferred.reject("Failed to read configuration");
                }
              } catch (e) {
                deferred.reject("Failed to parse configuration text");
              }

              deferred.resolve(decompressed);
            });
          },
          function(result) {
            $scope.$apply(function() {
              deferred.reject("Scanner failed with error: " + angular.toJson(result, true));
            });
          },
          {
            orientation: "landscape",
            formats: "QR_CODE, PDF_417, CODE_128", // default: all but PDF_417 and RSS_EXPANDED
            disableSuccessBeep: true
          }
        );
        return deferred.promise;
      }

      return scanQrCode()
        .catch(function(e) {
          if (e !== "Scanning cancelled") {
            notifications.alert(
              gettextCatalog.getString("TabSINT failed to read configuration code error: ") + angular.toJson(e, true)
            );
          }

          return $q.reject();
        })
        .then(function(configObject) {
          return config.load(configObject, true);
        }) // Load the configuration Qr Code into the config object
        .then(autoConfig.load); // Act on the config object: apply admin settings, load protocol and exam view
    };

    var DisclaimerInstanceCtrl = function($scope, $uibModalInstance, disk) {
      $scope.ok = function() {
        $uibModalInstance.close();
        disk.init = false;
      };
    };

    app.ready().then(function() {
      if (disk.init) {
        // only run when the app loads for the first time (when disk.init is true)
        $uibModal.open({
          templateUrl: "scripts/routes/welcome/disclaimer.html",
          controller: DisclaimerInstanceCtrl //is there a reason not to use this simplistic logic?
        });
      }
    });
  });
