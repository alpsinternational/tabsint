/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";

angular
  .module("tabsint.routes.admin.results", [])

  .directive("resultsView", function() {
    return {
      restrict: "E",
      controller: "ResultsCtrl",
      templateUrl: "scripts/routes/admin/results/results.html",
      scope: {}
    };
  })

  .controller("ResultsCtrl", function(
    $scope,
    logger,
    disk,
    results,
    notifications,
    localServer,
    tasks,
    $uibModal,
    networkModel,
    gettextCatalog
  ) {
    $scope.disk = disk;
    $scope.localServer = localServer;
    $scope.tasks = tasks;
    $scope.uploadLogExpanded = false;
    $scope.results = results;

    // Update queued results on Admin View load
    results.getResultsForResultsView();

    $scope.viewResult = function(ind) {
      var modal = $uibModal.open({
        templateUrl: "scripts/routes/admin/results/result.modal.html",
        controller: "ResultViewCtrl",
        resolve: {
          ind: function() {
            return ind;
          }
        }
      });
    };

    // only app developer
    $scope.deleteAll = function() {
      notifications.confirm(
        gettextCatalog.getString("Are you sure you want to delete all result files? These results cannot be restored."),
        function(buttonIndex) {
          if (buttonIndex === 1) {
            results.deleteAll().finally(results.getResultsForResultsView);
          }
        }
      );
    };
  })

  .controller("ResultViewCtrl", function(
    $scope,
    $uibModalInstance,
    $q,
    results,
    logger,
    disk,
    sqLite,
    notifications,
    tasks,
    ind,
    gettextCatalog
  ) {
    $scope.idx = ind;
    $scope.results = results;
    $scope.tasks = tasks;
    $scope.disk = disk; // HG, so result.modal.html access disk variables

    // get result from sqLite db on Single Result View load
    results.getResultsForResultsView(ind).then(function() {
      var q = $q.defer();
      $scope.uploadable = function() {
        if ($scope.results.singleResultView) {
          return _.includes(["tabsintServer", "gitlab"], $scope.results.singleResultView.testResults.protocol.server);
        } else {
          return false;
        }
      };
      q.resolve();
      return q.promise;
    });

    $scope.close = function() {
      $uibModalInstance.close();
    };

    $scope.delete = function() {
      notifications.confirm(
        gettextCatalog.getString("Are you sure you want to delete this exam? This action is irreversible."),
        function(buttonIndex) {
          if (buttonIndex === 1) {
            results.delete($scope.results.singleResultView.testDateTime);
            $scope.close();
          }
        },
        "Confirm Delete Result",
        ["OK", "Cancel"]
      );
    };

    $scope.upload = function() {
      results.upload($scope.idx);
      $scope.close();
    };

    $scope.export = function() {
      results.export($scope.idx);
      $scope.close();
    };
  });
