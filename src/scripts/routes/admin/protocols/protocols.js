/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";

angular
  .module("tabsint.routes.admin.protocols", [])

  .directive("protocolsView", function() {
    return {
      restrict: "E",
      controller: "ProtocolsCtrl",
      templateUrl: "scripts/routes/admin/protocols/protocols.html",
      scope: {}
    };
  })

  .controller("ProtocolsCtrl", function(
    $q,
    $sce,
    $scope,
    $timeout,
    adminLogic,
    app,
    autoConfig,
    config,
    devices,
    disk,
    examLogic,
    gettextCatalog,
    gitlab,
    json,
    localServer,
    logger,
    notifications,
    paths,
    pm,
    protocol,
    tabsintServer,
    tasks
  ) {
    $scope.protocol = protocol;
    $scope.disk = disk;
    $scope.selected = disk.protocol.path ? disk.protocol : undefined; // initiate to disk protocol
    $scope.devices = devices;
    $scope.app = app;
    $scope.panes = adminLogic.panes;

    // Gitlab Server Controls
    $scope.gitlab = gitlab;
    $scope.gitlabProtocol = {}; // temp input storage

    // TabSINT Server Controls
    $scope.tabsintServer = tabsintServer;
    $scope.newSite = {
      name: undefined
    };

    // popovers
    $scope.protocolTablePopover = $sce.trustAsHtml(
      "The table below shows a list of the available test protocols within TabSINT. You can select a protocol by pressing on the table row, then <b>load</b>, <b>update</b>, or <b>delete</b> the protocol using the buttons below. <br><br>" +
        "Protocols can be added from each of the servers listed on the <i>Configuration</i> page using the pane below this one. The input area will change depending on the server selected."
    );
    $scope.mediaTablePopover = $sce.trustAsHtml(
      "This table shows a list of the downloaded media repositories. " +
        "These repositories can be referenced by any protocols using the <code>mediaRepository</code> key in the top level of the protocol. <br><br>" +
        "Media repositories will be downloaded from the Gitlab Server defined in the <b>Gitlab Configuration</b> pane under the <i>Configuration</i> tab."
    );
    $scope.mediaAddPopover = $sce.trustAsHtml(
      "Type in the name of a repository to use as a common media repository. <br><br>" +
        "The repository must be located on the host in the group defined in the <b>Gitlab Configuration</b> pane under the <i>Configuration</i> tab."
    );

    $scope.serverPopover = $sce.trustAsHtml(
      gettextCatalog.getString(
        "Choose the data store to use as the protocol source and results output. <br><br>Additional configuration for the <b>TabSINT Server</b> and <b>Gitlab</b> will become active below this box when a server is selected"
      )
    );
    $scope.serverDefaultPopover = $sce.trustAsHtml(
      gettextCatalog.getString(
        "Reset all configuration values to the defaults set in the build configuaration file. This file can only be edited when TabSINT is built from source code."
      )
    );

    $scope.gitlabAddPopover = $sce.trustAsHtml(
      gettextCatalog.getString(
        "Type in the name of the protocol repository located on the host and group defined in the <b>Advanced Gitlab Settings</b>"
      )
    );
    $scope.gitlabAddVersionPopover = $sce.trustAsHtml(
      "<strong>OPTIONAL:</strong> Type in the repository tag for the version of the repository you would like to download. Leave blank to download the latest tag/commit from the repository."
    );
    $scope.gitlabHostPopover = $sce.trustAsHtml(
      gettextCatalog.getString(
        'Hostname of the gitlab server instance you are running. Generally this will be "https://gitlab.com/"'
      )
    );
    $scope.gitlabTokenPopover = $sce.trustAsHtml(
      gettextCatalog.getString(
        "The secret token used to access your gitlab repositories. See the user guide for more information about finding the Token."
      )
    );
    $scope.gitlabNamespacePopover = $sce.trustAsHtml(
      gettextCatalog.getString("The group where protocol, media, and result repositories are stored.")
    );
    $scope.gitlabUseTagsPopover = $sce.trustAsHtml(
      gettextCatalog.getString(
        "By default, TabSINT will track changes to protocol files based on the <b>tags</b> to a repository.<br><br>" +
          "Uncheck this box if you would only like to download changes that are associated with repository <b>commits</b>."
      )
    );
    $scope.gitlabUseSeperateResultsRepoPopover = $sce.trustAsHtml(
      gettextCatalog.getString(
        "Select this option to choose a different gitlab group or repository for results upload.  <br><br>By default, results are uploaded to a <code>results</code> repository in the same group that contains the protocol."
      )
    );
    $scope.gitlabResultsGroupPopover = $sce.trustAsHtml(
      gettextCatalog.getString(
        "Type the group that contains the <b>Results Repository</b> specified below. <br><br> <i>Note: This group must use the same <b>Host</b> and <b>Token</b> above.</i>"
      )
    );
    $scope.gitlabResultsRepoPopover = $sce.trustAsHtml(
      gettextCatalog.getString(
        "Type the name of the repository where results will be uploaded. To avoid errors, please create the repository before trying to upload results to it.<br><br> <i>Note: This repository must use the same <b>Host</b> and <b>Token</b> above.</i>"
      )
    );
    $scope.downloadCreareProtocolsPopover = $sce.trustAsHtml(
      "Select this option to download standard protocols from Creare.  Results will still go to the gitlab host, group, and repository defined in <b>Gitlab Configuration</b> on the <i>Configuration</i> tab.  When this option is not selected, protocols are downloaded from the host and group defined in <b>Gitlab Configuration</b> on the <i>Configuration</i> tab."
    );

    $scope.localAddPopover = $sce.trustAsHtml(
      "Press <b>Add</b> to select a protocol directory from the SD card of the tablet"
    );
    $scope.validatePopover = $sce.trustAsHtml(
      "Validate protocols against the <b>Protocol Schema</b> before loading into the application. <br><br> The protocol schema defines the allowable inputs for use in protocols."
    );

    $scope.tabsintAddPopover = $sce.trustAsHtml(
      "Type in the <b>Site Name</b> to download from the TabSINT Server defined in the <b>TabSINT Server Configuration</b> advanced settings below."
    );
    $scope.tabsintUrlPopover = $sce.trustAsHtml(
      gettextCatalog.getString(
        'Host URL of the TabSINT server. Generally this will be "https://hffd.crearecomputing.com/"'
      )
    );
    $scope.tabsintUsernamePopover = $sce.trustAsHtml(
      gettextCatalog.getString("The username used to access the TabSINT server")
    );
    $scope.tabsintPasswordPopover = $sce.trustAsHtml(
      gettextCatalog.getString("The password used to access the TabSINT server")
    );
    $scope.serverAuthorizePopover = $sce.trustAsHtml(
      gettextCatalog.getString(
        "If TabSINT Server configuration values are not valid, TabSINT will not be able to download protocols or upload results. Tap <strong>Validate Now</strong> to ensure configuration parameters are valid. <br><br> Validation may take up to 15 seconds."
      )
    );

    $scope.select = function(p) {
      $scope.selected = p;
    };

    $scope.changeServer = function(s) {
      protocol.changeSource(s);
    };

    /**
     * Load a protocol.
     * If the protocol is 'de
     * @return {[type]} [description]
     */
    $scope.load = function() {
      if (!$scope.selected) {
        return;
      }

      // if no protocol is available, load it
      if (!pm.root) {
        loadAndReset();
      } else {
        // otherwise, notify user appropriately and loadAndReset()
        var msg =
          gettextCatalog.getString("Switch to protocol ") +
          $scope.selected.name +
          gettextCatalog.getString(" and reset the current test? The current test results will be deleted");
        if (protocol.isRoot($scope.selected)) {
          msg =
            gettextCatalog.getString("Reload protocol ") +
            $scope.selected.name +
            gettextCatalog.getString(" and reset the current test? The current test will be reset");
        }

        notifications.confirm(msg, function(buttonIndex) {
          if (buttonIndex === 1) {
            loadAndReset();
          }
        });
      }

      // repeated function
      function loadAndReset() {
        tasks
          .register("updating", "Loading Protocol...")
          .then(function() {
            return protocol.load($scope.selected, disk.validateProtocols, true);
          })
          .then(function() {
            if (protocol.isRoot($scope.selected)) {
              examLogic.reset();
            }
          })
          .finally(function() {
            tasks.deregister("updating");
          });
      }
    };

    /**
     * Delete a protocol
     */
    $scope.delete = function() {
      if (!$scope.selected) {
        return;
      }

      notifications.confirm(
        gettextCatalog.getString("Delete protocol ") +
          $scope.selected.name +
          gettextCatalog.getString(" and remove protocol files from disk?"),
        function(buttonIndex) {
          if (buttonIndex === 1) {
            protocol.delete($scope.selected);
            $scope.selected = undefined;
          }
        }
      );
    };

    /**
     * Update a protocol.
     * Once a protocol has been updated, it will attempt to update a common media repository, if specified.
     * If the protocol is currently active, it will reload the protocol and reset the test.
     */
    $scope.update = function() {
      if (!$scope.selected) {
        return;
      }

      // check to see if task is already running (shouldn't even be able to get to this state)
      if (tasks.isOngoing("updating")) {
        logger.warn("Task still in progress...");
        notifications.alert(gettextCatalog.getString("Task still in progress..."));
        return;
      }

      if (protocol.isRoot($scope.selected)) {
        notifications.confirm(
          gettextCatalog.getString("Update protocol ") +
            $scope.selected.name +
            "? " +
            gettextCatalog.getString("The current test will be reset"),
          function(buttonIndex) {
            if (buttonIndex === 1) {
              tasks.register("updating", "Updating Protocol");
              updateProtocol()
                .then(function() {
                  return protocol.load($scope.selected, disk.validateProtocols, false);
                })
                .then(examLogic.reset)
                .finally(function() {
                  tasks.deregister("updating");
                });
            }
          }
        );
      } else {
        notifications.confirm(gettextCatalog.getString("Update protocol ") + $scope.selected.name + "?", function(
          buttonIndex
        ) {
          if (buttonIndex === 1) {
            tasks.register("updating", "Updating Protocol");
            updateProtocol().finally(function() {
              tasks.deregister("updating");
            });
          }
        });
      }

      function updateProtocol() {
        if ($scope.selected.server === "tabsintServer") {
          return tabsintServer.updateConfiguration($scope.selected.siteName).then(function() {
            return autoConfig.checkMedia($scope.selected);
          });
        } else if ($scope.selected.server === "gitlab") {
          return gitlab
            .pull($scope.selected.repo)
            .then(function(repo) {
              // update protocols on disk
              var pidx = _.findIndex(disk.protocols, {
                path: paths.data(paths.gitlab(repo))
              });
              if (pidx !== -1) {
                disk.protocols[pidx] = gitlab.defineProtocol(repo);
                return autoConfig.checkMedia($scope.selected);
              } else {
                logger.error("Protocol path was not found in disk.protocols");
                return $q.reject();
              }
            })
            .catch(function(e) {
              if (e && e.msg) {
                notifications.alert(e.msg);
              } else {
                logger.error(
                  `Unknown failure while pulling gitlab protocol ${$scope.selected.repo.host} ${
                    $scope.selected.repo.group
                  } ${$scope.selected.repo.name} ${$scope.selected.repo.token} with error: ${angular.toJson(e)}`
                );
                notifications.alert(
                  gettextCatalog.getString(
                    "TabSINT encountered an issue while updating the repository. Please verify the repository location and version and upload the application logs if the issue persists."
                  )
                );
              }
            });
        }
      }
    };

    /**
     * Sets the class for the protocol entry in the table
     */
    $scope.pclass = function(p) {
      if (protocol.isRoot(p)) {
        return "active-row";
      } else if (_.isEqual($scope.selected, p)) {
        return "table-selected";
      } else {
        return "";
      }
    };

    /**
     * Sets the class for the media entry in the table
     */
    $scope.mclass = function(m) {
      if (pm.root && pm.root.commonMediaRepository && pm.root.commonMediaRepository === m.name) {
        return "active-row";
      } else {
        return "";
      }
    };

    /**
     * Add protocols from server set on `disk.server`
     */
    $scope.add = function() {
      return autoConfig
        .add()
        .then(function() {
          $timeout(function() {
            $scope.newSite.name = undefined;
            // reset the temporary input
            $scope.gitlabProtocol = {};
          }, 0);
        })
        .catch(function(e) {
          $q.reject(e);
        });
    };

    /**
     * Update media repository specifically
     * @param  {object} mediaObject - media object saved on disk
     */
    $scope.updateMedia = function(mediaObject) {
      if (pm.root && pm.root.commonMediaRepository && pm.root.commonMediaRepository === mediaObject.name) {
        notifications.confirm(
          gettextCatalog.getString('This media repository "') +
            mediaObject.name +
            gettextCatalog.getString(
              '" is used by the current protocol.  TabSINT will reset the protocol once the media repository has been updated.'
            ),
          function(buttonIndex) {
            if (buttonIndex === 1) {
              tasks.register("update media", "Updating media repository");

              autoConfig
                .pullMedia(mediaObject)
                .then(protocol.load) // RE-LOAD Current Protocol.  The meta for the current protocol is stored on disk.protocol
                .then(examLogic.reset)
                .finally(function() {
                  tasks.deregister("update media");
                });
            }
          },
          "Update Media",
          ["Continue", "Cancel"]
        );
      } else {
        notifications.confirm(
          gettextCatalog.getString("Update media repository ") + mediaObject.name + "?",
          function(buttonIndex) {
            if (buttonIndex === 1) {
              tasks.register("update media", "Updating media repository");
              autoConfig.pullMedia(mediaObject).finally(function() {
                tasks.deregister("update media");
              });
            }
          },
          "Update Media",
          ["Continue", "Cancel"]
        );
      }
    };

    /**
     * Delete a media repository
     */
    $scope.deleteMedia = function(m) {
      // check if the media repo to delete is required by the current protocol
      if (pm.root && pm.root.commonMediaRepository && pm.root.commonMediaRepository === m.name) {
        notifications.confirm(
          gettextCatalog.getString('This media repository "') +
            m.name +
            gettextCatalog.getString(
              '" is in use by the current protocol. Would you still like to remove the repository?'
            ) +
            " \n \n" +
            gettextCatalog.getString("TabSINT will reset the protocol once the media repository has been deleted."),
          function(buttonIndex) {
            if (buttonIndex === 1) {
              protocol.deleteMedia(m);
              protocol.load().then(examLogic.reset);
            }
          },
          "Delete Media",
          ["Continue", "Cancel"]
        );
      } else {
        notifications.confirm(
          gettextCatalog.getString("Delete media repository ") +
            m.name +
            gettextCatalog.getString(" and remove media files from disk?"),
          function(buttonIndex) {
            if (buttonIndex === 1) {
              protocol.deleteMedia(m);
            }
          },
          gettextCatalog.getString("Delete Media"),
          [gettextCatalog.getString("Continue"), gettextCatalog.getString("Cancel")]
        );
      }
    };
  });
