/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";

angular
  .module("tabsint.routes.admin.config", [])

  .directive("configView", function() {
    return {
      restrict: "E",
      controller: "ConfigCtrl",
      templateUrl: "scripts/routes/admin/config/config.html",
      scope: {}
    };
  })

  .controller("ConfigCtrl", function(
    $cordovaFileTransfer,
    $http,
    $interval,
    $q,
    $sce,
    $scope,
    adminLogic,
    app,
    autoConfig,
    changePin,
    changeLog,
    config,
    devices,
    disk,
    gain,
    gettextCatalog,
    gitlab,
    localServer,
    logExport,
    logger,
    media,
    networkModel,
    notifications,
    paths,
    plugins,
    pm,
    protocol,
    results,
    remote,
    tabsintNative,
    tabsintServer,
    tasks,
    version,
    permissions
  ) {
    $scope.pm = pm;
    $scope.plugins = plugins;
    $scope.devices = devices;
    $scope.adminLogic = adminLogic;
    $scope.disk = disk;
    $scope.config = config;
    $scope.gitlab = gitlab;
    $scope.tabsintServer = tabsintServer;
    $scope.localServer = localServer;
    $scope.tasks = tasks;
    $scope.logExport = logExport;
    $scope.remote = remote;
    $scope.version = version;
    $scope.networkModel = networkModel;
    $scope.changePin = changePin;
    $scope.changeLog = changeLog;
    $scope.gain = gain;

    /**
     * View releases urls
     */
    $scope.viewReleases = function(loc) {
      if (loc === "Google") {
        remote.link("https://play.google.com/store/apps/details?id=com.creare.skhr.tabsint");
      } else if (loc === "Gitlab") {
        remote.link("https://gitlab.com/creare-com/tabsint/-/releases");
      } else {
        remote.link(loc);
      }
    };

    // Popover text
    $scope.adminPopover = $sce.trustAsHtml(
      gettextCatalog.getString(
        "Includes additional configuration options, displays expandable <b>debug</b> menus showing program state at the bottom of exam pages, and suppresses Admin Password prompts"
      )
    );
    $scope.externalControlPopover = $sce.trustAsHtml(
      gettextCatalog.getString("Enable control of protocol flow using external tools like Matlab.")
    );
    $scope.disableLogsPopover = $sce.trustAsHtml(
      gettextCatalog.getString(
        "Disable log messages from being stored and uploaded. <br><br>Logs are useful for investigating software bugs, but may introduce privacy concerns. Disable logging anytime sensitive data is being collected."
      )
    );
    $scope.setMaxLogRowsPopover = $sce.trustAsHtml(
      gettextCatalog.getString(
        "Set the maximum number of log records to be saved. This will prevent the logs from consuming too much memory."
      )
    );
    $scope.disableVolumePopover = $sce.trustAsHtml(
      gettextCatalog.getString(
        "This option will disable TabSINT from setting the volume to 100% on every page. This feature is essential to the functionality of TabSINT while playing calibrated audio through the speaker.<br><br>" +
          "Check this box if you would like to set the volume of the app manually using the buttons on the side of the device. <br><br>In almost all cases, this box should remain unchecked."
      )
    );
    $scope.adminSkipModeControlPopover = $sce.trustAsHtml(
      gettextCatalog.getString(
        "This option will enable skipping on every page of an Exam. This feature is should only be enabled while developing or debugging, and may cause issues with some protocols."
      )
    );
    $scope.languagePopover = $sce.trustAsHtml(
      gettextCatalog.getString(
        "Select preferred language for the application. This language will be used where supported. Otherwise, English will be used. Note this cannot change any text configured in protocols."
      )
    );

    $scope.requireEncryptedResultsPopover = $sce.trustAsHtml(
      gettextCatalog.getString(
        "This option requires that the protocol contains a public RSA key for encrypting output results."
      )
    );
    $scope.recordTestLocationPopover = $sce.trustAsHtml(
      gettextCatalog.getString(
        "This option will record the test location (latitude, longitude) in the results. The test location is only recorded when the test first starts." +
          "<br><br>TabSINT will request permission to access the device location when this option is enabled. The device's location services must be turned for the location to be successfully stored."
      )
    );

    $scope.qrCodePopover = $sce.trustAsHtml(
      gettextCatalog.getString(
        "Generate a QR Code containing all the current configuration settings. Select the local directory to save the QR Code for future use."
      )
    );

    $scope.headsetPopover = $sce.trustAsHtml(
      gettextCatalog.getString(
        "Select the deafult headset used to adminster hearing tests. " +
          "This selection is overridden by the <code>headset</code> parameter in protocols. <br><br> If the protocol does not specify a <code>headset</code>, " +
          "this value must match the value in the protocol's <code>calibration.json</code> file."
      )
    );
    $scope.tabletPopover = $sce.trustAsHtml(
      gettextCatalog.getString(
        "The model of the mobile device being used to run TabSINT. <br><br> If you are using audio files calibrated for a specific tablet model, " +
          "this value must match the value in <code>calibration.json</code>."
      )
    );

    $scope.interAppNamePopover = $sce.trustAsHtml("Package name of other app.  example:  com.companyname.appname");
    $scope.interAppMessagePopover = $sce.trustAsHtml("Message to send");
    $scope.interAppDataPopover = $sce.trustAsHtml("Data to send");
    $scope.interAppSendPopover = $sce.trustAsHtml("Send message and data to app specified.");
    $scope.interAppLastMessagePopover = $sce.trustAsHtml("Last message and data received from another app");

    $scope.adminPinPopover = $sce.trustAsHtml(
      gettextCatalog.getString(
        "Change the Admin PIN to any numerical value.  This PIN is required to switch to Admin View and to reset exams when Admin Mode is off."
      )
    );
    $scope.gainPopover = $sce.trustAsHtml(
      gettextCatalog.getString(
        "Apply a special gain in dB to the audio level output through TabSINT. " +
          "This can be used to calibrate the audio jack output to a specified level for all audio played through the tablet."
      )
    );

    $scope.automaticallyOutputResultsPopover = $sce.trustAsHtml(
      "Automatically upload or export the result when a test is finished. The result will be uploaded or exported on the <b>Exam Complete</b> page. <br><br> Once the result is uploaded to a server or exported to a local file, it will be removed from TabSINT."
    );

    $scope.resetConfigurationPopover = $sce.trustAsHtml(
      "Reset TabSINT configuration to the default configuration. All manual changes will be removed."
    );

    // log messages
    $scope.logs = {
      count: logger.getCount,
      show: false,
      nLogs: 50,
      disp: []
    };

    // method to show logs
    $scope.displayLogs = function() {
      // get logs from sqlite if the logs.disp array is less than the count
      if ($scope.logs.disp.length !== logger.getCount.logs) {
        logger.getLogs().then(function(ret) {
          $scope.logs.disp = ret.reverse(); // reverse order of logs so its easier to read
        });
      }

      // toggle the well
      $scope.logs.show = !$scope.logs.show;
    };

    // Object with language name available for choosing
    $scope.languages = {
      en: gettextCatalog.getString("English"),
      ja: gettextCatalog.getString("Japanese"),
      fr: gettextCatalog.getString("French"),
      sp: gettextCatalog.getString("Spanish")
    };

    $scope.changeLanguage = config.changeLanguage;

    $scope.changeHeadset = config.changeHeadset;

    $scope.changeResultsMode = config.changeResultsMode;

    // Object with headset and Display name available for choosing - must correspond with calibration names (see protocol_schema/headset)
    $scope.headsets = {
      None: "None",
      HDA200: "HDA200",
      VicFirth: "VicFirth",
      VicFirthS2: "VicFirthS2",
      WAHTS: "WAHTS",
      EPHD1: "EPHD1",
      Audiometer: "Audiometer"
    };

    $scope.resultsModeOptions = {
      0: "Upload/Export",
      1: "Upload Only",
      2: "Export Only"
    };

    // Add plugin headsets
    _.forEach(plugins.elems.headsets, function(headset) {
      $scope.headsets[headset.id] = headset.display;
    });

    $scope.play1kHz94dB = function() {
      media.stopAudio();
      var wavfile = {
        path: "/android_asset/www/res/wavs/1kHz_cal_tone.wav",
        playbackMethod: "arbitrary",
        targetSPL: 94
      };

      // A seperate calibration is needed for each distinct tablet/headset
      // combination. Each calibration must have:
      //      {'wavRMSZ': 0.70231, 'RMSZ': 0.70231, 'scaleFactor': X.XXXXX};
      // The RMSs always have these values - a function of the wav file.
      // The scaleFactor is hardware dependent and can be found in the
      // tablet_headset-audio_profile.json file.
      if (disk.headset === "VicFirth") {
        wavfile.cal = { wavRMSZ: 0.70231, RMSZ: 0.70231, scaleFactor: 0.60027 };
      } else if (disk.headset === "VicFirthS2") {
        wavfile.cal = {
          wavRMSZ: 0.70231,
          RMSZ: 0.70231,
          scaleFactor: 0.2330724734026138
        };
      } else if (disk.headset === "HDA200") {
        wavfile.cal = { wavRMSZ: 0.70231, RMSZ: 0.70231, scaleFactor: 0.2774 };
      } else if (disk.headset === "Creare Headset" || disk.headset === "WAHTS") {
        wavfile.cal = {
          wavRMSZ: 0.70231,
          RMSZ: 0.70231,
          scaleFactor: 0.13519823071552697
        };
      } else if (disk.headset === "EPHD1") {
        wavfile.cal = {
          wavRMSZ: 0.70231,
          RMSZ: 0.70231,
          scaleFactor: 0.09404459105486002
        };
      } else if (disk.headset === "Audiometer") {
        wavfile.cal = { wavRMSZ: 1, RMSZ: 1, scaleFactor: 1 };
      } else {
        // default to VicFirth
        notifications.alert(gettextCatalog.getString("No calibrated sound available for this headset."));
        return;
      }

      media.playWavCalTone(wavfile);
    };

    $scope.playCompAudio = function() {
      media.stopAudio();
      var startDelayTime = 0;
      media.playAudio([{ src: "/android_asset/www/res/wavs/CompAudioTest.wav", vol: 1.0 }], startDelayTime);
    };

    $scope.playCompAudioLinear = function() {
      media.stopAudio();
      var startDelayTime = 0;
      media.playAudio(
        [
          {
            src: "/android_asset/www/res/wavs/CompAudioTestLinear.wav",
            vol: 1.0
          }
        ],
        startDelayTime
      );
    };

    $scope.toggleAutoUpload = function() {
      if (disk.autoUpload) {
        if (!networkModel.status) {
          notifications.alert(
            gettextCatalog.getString(
              "The tablet is not currently online. Results will auto-upload when an internet connection is established."
            )
          );
        } else {
          notifications.alert(
            gettextCatalog.getString(
              "Auto-upload Enabled. Results will auto-upload at the end of each exam and/or whenever a wifi or cellular internet connection is available."
            )
          );
          results.upload();
        }
      } else {
        notifications.alert(
          gettextCatalog.getString(
            "Auto-upload Disabled.  Please manually upload results often to ensure that data is not lost."
          )
        );
      }

      logger.info("Auto upload changed to: " + disk.autoUpload);
    };

    $scope.requestLocationPermission = function() {
      if (disk.recordTestLocation) {
        permissions.requestLocationPermission();
      } else {
        // reset location
        disk.tabletLocation = {
          latitude: undefined,
          longitude: undefined,
          accuracy: undefined
        };
      }
    };

    $scope.resetConfig = function() {
      notifications.confirm(
        gettextCatalog.getString("Reset configuration to defaults?"),
        function(buttonIndex) {
          if (buttonIndex === 1) {
            // pass an empty stringified object to overwrite any disk.qrCode, true to reset disk
            config.load("{}", true);
          } else if (buttonIndex === 2) {
            logger.info("Reset configuration Canceled");
          }
        },
        gettextCatalog.getString("Confirm Selection"),
        [gettextCatalog.getString("OK"), gettextCatalog.getString("Cancel")]
      );
    };
  });
