/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

angular
  .module("tabsint.routes.tabsint-content", [])

  .directive("tabsintContent", function() {
    return {
      templateUrl: "scripts/routes/tabsint-content/tabsint-content.html",
      controller: "TabsintCtrl",
      scope: {}
    };
  })

  .controller("TabsintCtrl", function($scope, router) {
    $scope.router = router;
    $scope.text = "hello";
  });
