"use strict";

import * as angular from "angular";
import "angular-mocks";
import "./app";

beforeEach(function() {
  localStorage.clear();
});

describe("App test", function() {
  // load the controller's module
  beforeEach(angular.mock.module("tabsint"));

  beforeEach(
    angular.mock.inject(function(_$httpBackend_) {
      _$httpBackend_.whenGET("res/translations/translations.json").respond({});
    })
  );

  var TabsintCtrl, scope, app;

  // Initialize the controller and a mock scope
  beforeEach(
    angular.mock.inject(function($controller, $rootScope) {
      $rootScope.$digest(); // necessary to get app.run to fire all the way through

      //load new scope and then load controller
      scope = $rootScope.$new();
      $controller("TabsintCtrl", {
        $scope: scope
      });
    })
  );

  it("should have the router on its scope", function() {
    expect(scope.router).toBeDefined();
  });

  it("should be on the welcome page", function() {
    expect(scope.router.page).toEqual("WELCOME");
  });
});
