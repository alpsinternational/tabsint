/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import "angular-mocks";
import "../../app";

describe("Logger", function() {
  var app, disk, $rootScope, $httpBackend, config, $timeout, logger, networkModel, logExport;

  beforeEach(angular.mock.module("tabsint"));

  beforeEach(
    angular.mock.inject(function(
      _app_,
      _$httpBackend_,
      _$rootScope_,
      _logger_,
      _config_,
      _$timeout_,
      _disk_,
      _networkModel_,
      _logExport_
    ) {
      app = _app_;
      config = _config_;
      $rootScope = _$rootScope_;
      logger = _logger_;
      $httpBackend = _$httpBackend_;
      $timeout = _$timeout_;
      disk = _disk_;
      networkModel = _networkModel_;
      logExport = _logExport_;
      _$httpBackend_.whenGET("res/translations/translations.json").respond(200, "a string");
    })
  );

  // necessary to do to load
  beforeEach(function() {
    $rootScope.$digest();
    disk.disableLogs = false;
  });

  function expectLog(success) {
    $httpBackend.whenPOST("https://logs.tabsint.org/tabsint/logs").respond(function(method, url, data) {
      if (success) {
        return [200, ""];
      } else {
        return [422, ""];
      }
    });
  }

  it("should add logging messages to the logged messages structure", function(done) {
    var msg = "This is a UniQUe test message.",
      objMsg = {
        msg: "This is another message that doesn't exist anywhere else.."
      };
    var ret;
    logger
      .clearLogs()
      .then(function() {
        logger.debug(msg);
      })
      .then(function() {
        logger.debug(objMsg);
      })
      .then(logger.count)
      .then(logger.getLogs)
      .then(function(returned) {
        ret = returned;
        expect(ret.length).toBeGreaterThan(1);
        expect(ret[0].data).toEqual(msg);
        expect(ret[1].data).toEqual(angular.toJson(objMsg));
      })
      .catch(function() {
        expect(false).toBeTruthy();
      });

    $rootScope.$digest();
    done();
  });

  it("should upload them when requested and clear the log.", function() {
    expectLog(true);

    var oldLogCount;

    logger.debug("help! I'm stuck in unit test land!");
    logger.debug("help! I'm also stuck in unit test land!");
    logger.debug("help! I'm still stuck in unit test land!");
    logger.debug("help! why is this still breaking");
    logger.debug("help! I'm still stuck in unit test land!");
    logger.debug("help! I'm still stuck in unit test land!");
    logger.debug("help! I'm still stuck in unit test land!");
    logger.debug("help! I'm still stuck in unit test land!");
    logger.debug("help! I'm still stuck in unit test land!");

    logger.count().then(function(ret) {
      console.log("old log count:" + ret);
      oldLogCount = ret;
    });
    $rootScope.$digest();
    networkModel.status = true;

    logExport.upload();
    $rootScope.$digest();

    $httpBackend.flush();

    var newLogCount;

    logger.count().then(function(ret) {
      newLogCount = ret;
      console.log("new: " + newLogCount + ", old: " + oldLogCount);
      expect(newLogCount).not.toEqual(oldLogCount);
    });
    $rootScope.$digest();

    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  it("should keep queued results when upload fails", function() {
    expectLog(false);

    var oldLogCount;
    logger.count().then(function(ret) {
      console.log("old log count:" + ret);
      oldLogCount = ret;
    });
    networkModel.status = true;

    logExport.upload();
    $rootScope.$digest();

    $httpBackend.flush();

    logger.count().then(function(ret) {
      var newLogCount = ret;
      console.log("new: " + newLogCount + ", old: " + oldLogCount);
      expect(newLogCount).toEqual(oldLogCount + 6);
    });

    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });
});
