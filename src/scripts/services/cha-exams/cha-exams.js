/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

/* jshint bitwise: false */
/* globals ChaWrap: false */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";

angular
  .module("tabsint.services.cha.exams", [])
  .factory("chaExams", function($q, $timeout, page, cha, chaResults, logger, media, notifications) {
    var api = {
      polling: undefined,
      cancelPolling: undefined,
      setup: undefined,
      wait: undefined,
      reset: undefined,
      getChaInfo: undefined,
      state: undefined,
      storage: {},
      clearStorage: undefined,
      examType: undefined,
      complexExamType: undefined,
      examProperties: undefined,
      runTOBifCalledFor: {}
    };

    // The storage field provides storage for the current state in a complex exam, such as audiometry-list
    // The storage field is cleared using clearStorage on certain events using the plugins onEvent api
    api.clearStorage = function() {
      api.storage = {};
    };

    var exams = {
      HughsonWestlake: {},
      HughsonWestlakeFrequency: {},
      BekesyLike: {},
      BekesyFrequency: {},
      BHAFT: {},
      BekesyMLD: {},
      ThreeDigit: {},
      MLD: {},
      HINT: {},
      CRM: {},
      ThirdOctaveBands: {},
      ToneGeneration: {},
      DPOAE: {},
      TAT: {},
      GAP: {},
      CalibrationCheck: {},
      TRT: {},
      PlaysoundArray: {},
      MaskedThreshold: {}
    };

    // Exam Reset Functions
    api.reset = function() {
      api.examType = undefined;
      api.examProperties = {};
      api.complexExamType = undefined;
      return api.resetPage();
    };

    api.resetPage = function() {
      if (angular.isDefined(page.dm)) {
        page.dm.hideProgressbar = true;
      }

      api.cancelPolling();
      return cha
        .abortExams()
        .then(cha.stopNoiseFeature)
        .then(cha.disconnectA2DP);
    };

    // Setup Functions
    api.setup = function(examType, examProp) {
      api.examType = examType;
      var properties = examProp || {};

      // must convert to an examProperties object that can be sent to the CHA
      if (exams[api.examType].hasOwnProperty("setup")) {
        api.examProperties = exams[api.examType].setup(properties) || {};
      } else {
        api.examProperties = properties;
      }
    };

    exams.ThirdOctaveBands = {
      setup: function(examProp) {
        if (page.dm.responseArea.measureBothEars || angular.isUndefined(examProp.InputChannel)) {
          // go to default left input channel
          examProp.InputChannel = "SMICR0";
        }
        return examProp;
      }
    };

    exams.DPOAE = {
      setup: function(examProp) {
        if (examProp.F2 && !examProp.F1) {
          examProp.F1 = examProp.F2 / 1.2; // F2 = 1.2*F1
        }
        return examProp;
      }
    };

    exams.MLD = {
      setup: function(examProp) {
        if (angular.isUndefined(examProp.UseSoftwareButton)) {
          examProp.UseSoftwareButton = true;
        }
        return examProp;
      }
    };

    exams.GAP = {
      setup: function(examProp) {
        if (angular.isUndefined(examProp.UseSoftwareButton)) {
          examProp.UseSoftwareButton = 1;
        } else {
          examProp.UseSoftwareButton = examProp.UseSoftwareButton ? 1 : 0;
        }
        if (angular.isUndefined(examProp.Channel)) {
          examProp.Channel = 2;
        }
        return examProp;
      }
    };

    exams.CalibrationCheck = {
      setup: function(examProperties) {
        examProperties.Tol_cal = 10;
        return examProperties;
      }
    };

    exams.TAT = {
      setup: function(examProp) {
        if (angular.isDefined(examProp.Ear)) {
          var ear = String(examProp.Ear);
          if (ear.toLowerCase() === "left") {
            examProp.Ear = 0;
          } else if (ear.toLowerCase() === "right") {
            examProp.Ear = 1;
          } else if (ear.toLowerCase() === "both") {
            examProp.Ear = 2;
          } else {
            examProp.Ear = 0; // default to Left
          }
        } else {
          examProp.Ear = 0; //default to Left
        }
        return examProp;
      }
    };

    exams.TRT = {
      setup: function(examProp) {
        examProp.LevelUnits = angular.isDefined(examProp.LevelUnits) ? examProp.LevelUnits : "dB HL";

        examProp.ThresholdLevels = [];
        examProp.Ears = [];
        examProp.Frequencies = [];
        examProp.Thresholds.forEach(function(item) {
          examProp.ThresholdLevels.push(item.ThresholdLevel);
          examProp.Ears.push(item.Ear);
          examProp.Frequencies.push(item.Frequency);
        });
        examProp.Ears = examProp.Ears.map(function(ear) {
          return ear === "Right" ? 1 : 0;
        });

        examProp.Thresholds = undefined;

        return examProp;
      }
    };

    exams.MaskedThreshold = {
      setup: function(examProp) {
        return examProp;
      }
    };

    exams.HughsonWestlake = {
      setup: function(examProp) {
        // Dynamic Start Level
        if (examProp.DynamicStartLevel) {
          examProp.Lstart = calculateDynamicStartLevel(examProp.DynamicStartLevel, examProp.Lstart);
          delete examProp.DynamicStartLevel;
        }

        // Relative Frequency
        if (examProp.RelativeF) {
          examProp.F = calculateRelativeF(examProp.F, examProp.RelativeF);
          delete examProp.RelativeF;
        }

        if (angular.isUndefined(examProp.StepSize)) {
          examProp.StepSize = 5;
        }

        return examProp;

        function calculateDynamicStartLevel(dynStartLevel, Lstart) {
          var base, offset, ret;
          var idList = dynStartLevel.baseIdList || ["HW1000"];
          var resultsList = chaResults.getPastResults(idList, ["HughsonWestlake"]);

          for (var i = resultsList.length - 1; i >= 0; i--) {
            if (resultsList[i].ResultType === "Threshold") {
              base = resultsList[i].Threshold;
              break;
            }
          }

          if (angular.isUndefined(base)) {
            ret = angular.isDefined(Lstart) ? Lstart : 40;
          } else {
            offset = angular.isDefined(dynStartLevel.offset) ? dynStartLevel.offset : 15;
            Lstart = angular.isDefined(Lstart) ? Lstart : 40;
            ret = Math.max(Lstart, base + offset);
          }

          //console.log('newLstart: '+ret+', from offset: '+offset+', base: '+base);

          return ret;
        }

        function calculateRelativeF(F, rel) {
          var LUT, i;
          if (typeof rel[1] !== "number" || typeof rel[2] !== "number") {
            logger.warn(
              "Numerator or Denominator in relative frequency conditions are not specified as numbers, returning un-modified HAF"
            );
            return F;
          }
          if (angular.isDefined(rel[3])) {
            // protocol defined a look up table - use that
            if (page.dm.lookUpTables) {
              _.forEach(page.dm.lookUpTables, function(lut) {
                if (lut.name === rel[3]) {
                  LUT = lut.table;
                }
              });
            }

            if (angular.isUndefined(LUT)) {
              return F;
            }
            var lutInd = LUT.indexOf(F);

            if (rel[0] === "below") {
              if (lutInd < 0) {
                for (i = 0; i < LUT.length; i++) {
                  if (LUT[i] > F) {
                    lutInd = i;
                    break;
                  }
                }
              }
              if (lutInd - rel[1] >= 0) {
                return LUT[lutInd - rel[1]];
              } else {
                logger.warn(
                  "CHA calculateRelativeF reached lowest value in look up table.  HAF = " +
                    F +
                    ", index = " +
                    lutInd +
                    ", step = " +
                    rel[1]
                );
                return LUT[0];
              }
            } else if (rel[0] === "above") {
              if (lutInd < 0) {
                for (i = LUT.length; i >= 0; i--) {
                  if (LUT[i] < F) {
                    lutInd = i;
                    break;
                  }
                }
              }
              if (lutInd + rel[1] < LUT.length) {
                return LUT[lutInd + rel[1]];
              } else {
                logger.warn(
                  "CHA calculateRelativeF reached highest value in look up table.  HAF = " +
                    F +
                    ", index = " +
                    lutInd +
                    ", step = " +
                    rel[1]
                );
                return LUT[LUT.length - 1];
              }
            }
          } else {
            // no lookup table provided, use math
            if (rel[0] === "below") {
              return Math.round(F * Math.pow(2, -rel[1] / rel[2]));
            } else if (rel[0] === "above") {
              return Math.round(F * Math.pow(2, rel[1] / rel[2]));
            }
          }
        }
      }
    };

    api.runTOBifCalledFor = function() {
      var deferred = $q.defer();
      var measure = page.dm.responseArea.measureBackground;
      if (measure !== angular.undefined && measure === "ThirdOctaveBands") {
        logger.debug("Measuring background using " + measure);
        var tmpMainText = page.dm.questionMainText;
        var tmpSubText = page.dm.questionSubText;
        var tmpInstText = page.dm.instructionText;
        page.dm.questionMainText = "Measuring Background Noise";
        page.dm.questionSubText = "Please sit quietly and do not move";
        page.dm.instructionText =
          "This test measures the background noise levels using third-octave bands.  Wait quietly for measurement to complete.";
        return cha
          .requestStatusBeforeExam()
          .then(function() {
            return cha.queueExam("ThirdOctaveBands", {});
          })
          .then(api.wait.forReadyState)
          .then(cha.requestResults)
          .then(function(results) {
            chaResults.addTOBResults(results);
            page.dm.questionMainText = tmpMainText;
            page.dm.questionSubText = tmpSubText;
            page.dm.instructionText = tmpInstText;
          });
      } else {
        //ThirdOctaveBands not called for
        deferred.resolve();
        return deferred.promise;
      }
    };

    api.getChaInfo = function() {
      if (cha.myCha.id) {
        return {
          serialNumber: cha.myCha.id.serialNumber,
          buildDateTime: cha.myCha.id.buildDateTime,
          probeId: cha.myCha.probeId,
          vBattery: cha.myCha.vBattery
        };
      } else {
        logger.warn(`Requesting CHA info, but no cha is connected`);
        return {};
      }
    };

    api.wait = {
      forReadyState: function(initialDelay, intervalDelay) {
        var deferredExam = $q.defer();

        if (angular.isUndefined(initialDelay)) {
          initialDelay = 500;
        }
        if (angular.isUndefined(intervalDelay)) {
          intervalDelay = 500;
        }
        logger.debug(
          "CHA - Beginning to poll status every " + intervalDelay + " ms, after a delay of " + initialDelay + " ms."
        );

        api.cancelPolling();
        $timeout(function() {
          api.polling = setInterval(function() {
            return cha.requestStatus().then(
              function(status) {
                if (status.State === 1) {
                  api.cancelPolling();
                  deferredExam.resolve();
                }
              },
              function(err) {
                clearInterval(api.polling);
                deferredExam.reject(err);
              }
            );
          }, intervalDelay);
        }, initialDelay);

        return deferredExam.promise;
      },

      forStreamingState: function(initialDelay, intervalDelay, deferredA2DP) {
        var deferredExam = $q.defer();

        if (angular.isUndefined(initialDelay)) {
          initialDelay = 500;
        }
        if (angular.isUndefined(intervalDelay)) {
          intervalDelay = 500;
        }
        logger.debug(
          "CHA - Beginning to poll status every " + intervalDelay + " ms, after a delay of " + initialDelay + " ms."
        );

        api.cancelPolling();
        $timeout(function() {
          api.polling = setInterval(function() {
            return cha
              .requestStatus()
              .then(
                function(status) {
                  if (status.State === 1) {
                    api.cancelPolling();
                    deferredExam.resolve();
                  }
                },
                function(err) {
                  clearInterval(api.polling);
                  deferredExam.reject(err);
                }
              )
              .then(cha.checkA2DPConnection, function(err) {
                clearInterval(api.polling);
                deferredExam.reject(err);
              })
              .then(function() {
                // resolve any input promises when the A2DP connection is confirmed
                if (deferredA2DP) {
                  deferredA2DP.resolve();
                }
              })
              .catch(function(err) {
                api.cancelPolling();
                deferredExam.reject(err);
              });
          }, intervalDelay);
        }, initialDelay);

        return deferredExam.promise;
      }
    };

    api.cancelPolling = function() {
      try {
        clearInterval(api.polling);
      } catch (e) {
        logger.debug("CHA - Failed to clear results polling in chaExams with error: " + JSON.stringify(e));
      }
    };

    /**
     * PlaySound Exam
     * @param  {array} chaWavFiles - Array of string or chaWavFiles objects (see *chaWavFiles* in `page.json` schema) describing sounds to play on the SD card of the WAHTS
     * @return {promise}           - promise to queue a PlaySound exam
     */
    api.playSound = function(chaWavFiles) {
      if (chaWavFiles && chaWavFiles.length > 0) {
        return (
          cha
            .requestStatus()
            .then(function(status) {
              status.vBattery = Math.round(status.vBattery * 100) / 100;
              logger.debug("CHA - Status before queueing exam: " + angular.toJson(status));
              if (status.State === 2) {
                logger.warn("CHA exam is still running while user queues an exam. Aborting exams...");
                return cha.abortExams();
              } else if (status.State !== 1) {
                return $q.reject({
                  code: 66,
                  msg: "Cha is in an unknown state"
                });
              }
            })
            .then(function() {
              function processWav(wav) {
                // allow each member of chaWavFiles input to be a string path
                if (typeof wav === "string") {
                  wav = { SoundFileName: angular.copy(wav) };

                  // this should always be caught by schema validation, but adding error handling in case someone gets here
                } else if (typeof wav !== "object") {
                  notifications.alert(
                    'The "chaWavFiles" field of this page is invalid. Please validate this protocol against the protocol schema.'
                  );
                  logger.error("PlaySound wav file definition must be an object or a string: " + JSON.stringify(wav));
                  return;
                }

                // set `path` to `SoundFileName`
                if (angular.isDefined(wav.path)) {
                  wav.SoundFileName = wav.path;
                } else {
                  wav.SoundFileName = ""; // this will throw an error on the CHA that the wav file cannot be found
                }

                // assume files that don't start with C: are in the C:USER/ directory
                if (!wav.SoundFileName.startsWith("C:")) {
                  wav.SoundFileName = "C:USER/" + wav.SoundFileName;
                }

                // Leq default
                if (!wav.Leq) {
                  // Leq default
                  wav.Leq = [72, 72, 0, 0];
                } else if (wav.Leq.length === 2) {
                  // handle 2 single inputs
                  wav.Leq.concat(0, 0);
                }

                // UseMetaRMS default
                wav.UseMetaRMS = angular.isDefined(wav.UseMetaRMS)
                  ? wav.UseMetaRMS
                  : angular.isDefined(wav.useMetaRMS)
                  ? wav.useMetaRMS
                  : false;

                return wav;
              }

              // construct playSound exam object
              var wav = processWav(chaWavFiles[0]);

              // if processWav fails, reject
              if (!wav) {
                return $q.reject();
              }

              var pse = {
                UseMetaRMS: wav.UseMetaRMS, // old way - need to make sure nobody depends on this: wav.path.startsWith('C:USER/'),
                SoundFileName: wav.SoundFileName,
                Leq: wav.Leq
              };

              if (chaWavFiles.length === 2) {
                var wav2 = processWav(chaWavFiles[1]);

                // if processWav2 fails, reject
                if (!wav2) {
                  return $q.reject();
                }

                pse.SecondSoundFileName = wav2.SoundFileName;
                pse.SecondLeq = wav2.Leq;
                // pse.SecondFileDelay = wav2.SecondFileDelay || 0;
              }

              return cha.queueExam("PlaySound", pse);
            })
            .then(cha.requestResults) // we request results right away to make sure there is not an error with the playing of the wav file
            // .then(function(results) {  // we could act on the results in the future
            // })
            .catch(function(e) {
              logger.error("CHA - playWavs failed with error: " + JSON.stringify(e));
            })
        );
      }
    };

    api.startTalkThrough = function() {
      var deferredA2DP = $q.defer();
      logger.debug("CHA - starting talkThrough");

      media.stopAudio();

      cha
        .requestStatus()
        .then(function(status) {
          logger.debug("CHA - Status before queueing talkThrough: " + angular.toJson(status));
          if (status.State === 2) {
            logger.warn("CHA - CHA exam is still running while user starts talkThrough. Aborting exams...");
            return cha.abortExams();
          } else if (status.State !== 1) {
            return $q.reject({ code: 66, msg: "Cha is in an unknown state" });
          }
        })
        .then(cha.connectA2DP)
        .then(() => new Promise(resolve => setTimeout(resolve, 1000)))

        .then(function() {
          // TalkThrough times out after 5 seconds so CHA doesn't get stuck in headset mode if connection dies.
          return cha.queueExam("TalkThrough", {});
        })
        .then(cha.requestStatus)
        .then(function(status) {
          let statusPromise = $q.defer();
          logger.debug("CHA - Status after queuing talkThrough: " + angular.toJson(status));
          if (status.State !== 2) {
            logger.error("CHA - TalkThrough exam was not started");
            statusPromise.reject({ code: -1, message: "Check protocol" });
          } else {
            statusPromise.resolve();
          }
          return statusPromise.promise;
        })
        .then(function() {
          return api.wait.forStreamingState(500, 1000, deferredA2DP); // need to poll, to keep the TalkThrough alive.
        })
        .then(function() {
          // this shouldn't happen - it should be killed on pageEnd
          logger.debug("CHA - TalkThrough interval ended");
          //cha.abortExams(); // TODO unnecessary?
        })
        .catch(function(err) {
          var msg = "";
          logger.error("CHA - TalkThrough failed with error: " + JSON.stringify(err));
          if (err.code && err.code === 64) {
            msg =
              "Connection with the wireless headset was lost while streaming audio.  " +
              "The audio may start playing through the tablet speakers.  " +
              "Please hand the tablet to an administrator.";
          } else if (err.code && (err.code === 554 || err.code === 557)) {
            msg =
              "The tablet is not set up to stream to the headset. Please hand the tablet to an administrator. \n\nTo set up the streaming connection, try reconnecting to the headset.";
          } else if (err.code && (err.code === 558 || err.code === 555 || err.code === 560)) {
            msg = "The tablet failed to set up a streaming connection. Please hand the tablet to an administrator.";
          } else {
            msg =
              "The tablet was unable to initialize streaming to the WAHTS.  Please hand the tablet to an administrator.";
            deferredA2DP.reject("CHA Streaming not Ready");
          }
          media.stopAudio();
          cha.abortExams();
          notifications.alert(msg);
        });

      return deferredA2DP.promise;
    };

    api.stopTalkThrough = function() {
      logger.debug("CHA - stopping talkThrough");
      media.stopAudio();
      api.reset();
    };

    return api;
  });
