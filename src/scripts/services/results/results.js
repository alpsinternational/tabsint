/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";

angular
  .module("tabsint.services.results", [])

  .factory("results", function(
    $cordovaFile,
    $q,
    app,
    config,
    csv,
    devices,
    disk,
    cha,
    encryptResults,
    file,
    gettextCatalog,
    gitlab,
    json,
    logExport,
    logger,
    localServer,
    networkModel,
    notifications,
    paths,
    pm,
    sqLite,
    tabletLocation,
    tabsintServer,
    tasks,
    version
  ) {
    var results = {};

    results.dm = {
      queuedResultsWarnLength: 30,
      uploadSummaryLength: 30
    };

    // container to hold the current exam results
    results.current = undefined;
    // container to hold results for 'Completed Tests' results view
    results.completedTests = undefined;
    // container to hold result for the single result view
    results.singleResultView = undefined;

    results.default = function(pg) {
      return {
        presentationId: pg.id,
        response: undefined,
        correct: undefined,
        isSkipped: false,
        changedFields: pg.changedFields,
        responseArea: pg.responseArea ? pg.responseArea.type : undefined,
        page: {
          wavfiles: pg.wavfiles,
          chaWavFiles: pg.chaWavFiles,
          image: pg.image,
          video: pg.video,
          responseArea: pg.responseArea
        }
      };
    };

    /**
     * Generate a standardized filename across results
     * @param  {object} [result] - result object. If no result object provided, then it will use the current datetime
     * @param  {object} [suffix] - optional suffix to append
     * @return {string}        string filename
     */
    results.filename = function(result, suffix) {
      var dateTime, filename;

      dateTime = getDateString(result);

      // if resultFilename defined, use as prefix, otherwise use the device uuid
      if (result.resultFilename) {
        filename = result.resultFilename + "." + dateTime;
      } else {
        filename = devices.shortUUID + "." + dateTime;
      }

      // add any suffix asked for
      if (suffix) {
        filename = filename + suffix;
      }

      return filename;
    };

    results.create = function() {
      results.current = {
        // Age, gender, and audiometryResults are present in schema, but may not be in use.
        // Adding here for now so results match schema.
        age: undefined,
        gender: undefined,
        audiometryResults: undefined,

        // These are initialized to 0 in exam.finalize. May as well initialize here to 0
        // and update when exam is finalized
        nCorrect: 0,
        nIncorrect: 0,
        nResponses: 0,

        publicKey: pm.root.publicKey,
        siteId: disk.protocol.siteId || null,
        protocolId: disk.protocol.id || null,
        protocolName: disk.protocol.name,
        protocolHash: disk.protocol.hash,
        qrString: undefined,
        testDateTime: new Date().toJSON(),
        elapsedTime: undefined,
        subjectId: undefined,
        // Schema expects this to be here, not nested in testResults
        buildName: config.build,

        testResults: {
          protocol: angular.copy(disk.protocol),
          responses: [],
          softwareVersion: {
            version: version.dm.tabsint,
            date: version.dm.date,
            rev: version.dm.rev
          },
          cordovaPlugins: config.cordovaPlugins,
          tabsintPlugins: config.tabsintPlugins,
          platform: devices.platform,
          network: null, // this gets set when uploaded, if uploaded
          tabletUUID: devices.UUID,
          tabletModel: devices.model,
          tabletLocation: disk.tabletLocation,
          partialResults: undefined,
          headset: pm.root.headset || "None",
          calibrationVersion: {
            audioProfileVersion: pm.root._audioProfileVersion,
            calibrationPySVNRevision: pm.root._calibrationPySVNRevision,
            calibrationPyManualReleaseDate: pm.root._calibrationPyManualReleaseDate
          },
          isAdminMode: disk.debugMode,
          chaMediaVersion: cha.myCha && cha.myCha.chaMediaVersion ? cha.myCha.chaMediaVersion : undefined,
          chaProtectedMediaVersion:
            cha.myCha && cha.myCha.chaProtectedMediaVersion ? cha.myCha.chaProtectedMediaVersion : undefined,
          chaCalibrationDate:
            cha.myCha && cha.myCha.id && cha.myCha.id.chaCalibrationDate ? cha.myCha.id.chaCalibrationDate : undefined
        }
      };
    };

    /**
     * Method to get and return device parameters
     */
    results.getParams = function(devices) {
      return {
        build: devices.build,
        siteId: devices.siteId,
        uuid: devices.UUID,
        version: devices.version,
        platform: devices.platform,
        model: devices.model,
        os: devices.os,
        other: devices.other
      };
    };

    /**
     * Method to count results in the SQLite database
     */
    results.count = function() {
      return sqLite.count("results");
    };

    /**
     * Convience method to getDateString
     * @param  {[type]} result [description]
     * @return {[type]}        [description]
     */
    function getDateString(result) {
      // set dateString to testDateTime if result is passed, otherwise set to the current dateTime
      var dateString;
      if (result.testDateTime) {
        dateString = result.testDateTime
          .replace(":", "-")
          .replace(":", "-")
          .split(".")[0];
      } else {
        dateString = new Date()
          .toJSON()
          .replace(":", "-")
          .replace(":", "-")
          .split(".")[0];
      }
      return dateString;
    }

    /**
     * Convience method to write out results field depending on encryption
     * @param  {object} result       [description]
     * @param  {string} filename     [description]
     * @param  {string} dir [description]
     * @param  {string} resultString [description]
     * @return {Promise<>}              [description]
     */
    function writeOutResultsFile(result, filename, dir, resultString) {
      // encrypted
      if (result.publicKey) {
        var filenameResult = filename + ".enc";
        var filenameKey = filename + ".key.enc";

        // encrypt results
        var enc = encryptResults.encryptUploadExport(getDateString(result), result.publicKey, resultString);

        return $cordovaFile
          .writeFile(dir, filenameResult, enc[0])
          .then(function() {
            return $cordovaFile.writeFile(dir, filenameKey, enc[1]);
          })
          .then(function() {
            return filenameResult;
          });
      } else if (disk.requireEncryptedResults) {
        logger.error("Encrypted results required but no public key is defined");
        return $q.reject("Public encryption key must be defined in protocol to output encrypted results");
      } else {
        // not encrypted
        return $cordovaFile.writeFile(dir, filename, resultString).then(function() {
          return filename;
        });
      }
    }

    /**
     * Method to save a result in preperation for export/upload
     * Update of adminLogic.act.pushResult
     * @param  {object} result - raw result object
     * @return {promise}        [description]
     */
    results.save = function(result) {
      var q = $q.defer();

      logger.info("Saving result: " + angular.toJson(result));

      var dateString = getDateString(result);
      var type = "result";

      return encryptResults
        .encryptSQLite(dateString, angular.toJson(result))
        .then(function(encryptedAESResult) {
          // save encrypted result in results sqLite db
          return sqLite.store("results", dateString, type, encryptedAESResult, results.getParams(devices));
        })
        .then(function() {
          return results.backup(result);
        })
        .then(results.count)
        .then(function() {
          // export/upload the result
          var idx = sqLite.numLogs.results;
          if (disk.autoUpload) {
            if (disk.server === "localServer") {
              return results.export(idx - 1);
            } else {
              return results.upload(idx - 1);
            }
          }
        })
        .finally(function() {
          if (disk.exportCSV) {
            return results.exportCSV(result);
          }

          // alert if too many results are stored locally
          if (sqLite.numLogs.results >= results.dm.queuedResultsWarnLength && config.build !== "heartrack") {
            return results.resultsAlert();
          }

          // upload logs
          if (!disk.disableLogs && app.tablet) {
            return logExport.export();
          }
        });
    };

    /**
     * Back up result object to disk. Backups will be saved in .tabsint-results-backup
     * @param {object} result - result object to backup to file on sd card
     */
    results.backup = function(result) {
      var filename, dir;

      filename = results.filename(result) + ".json";
      dir = ".tabsint-results-backup/" + result.protocolName;

      return file
        .createDirRecursively(paths.public(""), dir)
        .then(function() {
          return writeOutResultsFile(result, filename, paths.public(dir), angular.toJson(result));
        })
        .then(
          function(updatedFilename) {
            logger.info("Successfully exported backup result to file: " + updatedFilename);
          },
          function(e) {
            logger.error("Failed to export backup result to file with error: " + angular.toJson(e));
          }
        );
    };

    /**
     * Pop alert that too many results are being queued.
     * Allow the user to suppress for one day
     */
    results.resultsAlert = function() {
      if (!disk.suppressAlerts) {
        logger.warn("" + sqLite.numLogs.results + gettextCatalog.getString(" have accumulated on the tablet"));
        notifications.confirm(
          sqLite.numLogs.results +
            gettextCatalog.getString(
              " results are saved on the tablet. Please upload or export and remove results from the tablet to avoid losing data"
            ),
          function(btnIdx) {
            if (btnIdx === 2) {
              results.suppressAlerts();
            }
          },
          "Warning",
          ["OK", "Suppress Warnings for 1 Day"]
        );
      }
    };

    /**
     * Suppress queued results warning for 1 day
     */
    results.suppressAlerts = function() {
      logger.info("Too many results: Suppress turned on");
      disk.suppressAlerts = true;
      setTimeout(function() {
        disk.suppressAlerts = false;
        logger.info("Too many results: Suppress turned back off by timeout");
      }, 1000 * 60 * 60 * 24);
    };

    /**
     * Method to upload result(s) to an uploadable server type.
     * Errors during the upload process are handled within this method, so rejected promises will be empty.
     * @param {number} [idx] = 0 - index of result to upload
     * @return {promise} Promise to upload single result.
     */
    results.upload = function(idx) {
      var r = [];
      var errors = [],
        success = [];

      if (idx > sqLite.numLogs.results) {
        logger.error("Index " + idx + " is outside of results length: " + sqLite.numLogs.results);
        notifications.alert(
          gettextCatalog.getString("Failed to upload result at index: ") +
            idx +
            ". " +
            gettextCatalog.getString(
              "Index is out of range of queued results. Please upload your logs if the issue persists."
            )
        );
        return $q.reject();
      }

      // write the file, update summary and delete results for each result uploaded
      function directoryHandling(r) {
        // only upload result if it was sourced from the right place
        if (_.includes(["tabsintServer", "gitlab"], r.testResults.protocol.server)) {
          // tabsintServer
          if (r.testResults.protocol.server === "tabsintServer") {
            // If Hear Track, check if result is already on the server
            let resultAlreadyUploaded = false;
            if (config.build === "heartrack") {
              disk.uploadSummary.forEach(function(item) {
                if (r.testDateTime.localeCompare(item.testDateTime) === 0) {
                  resultAlreadyUploaded = true;
                }
              });
            }

            delete r.publicKey; // remove the public key before uploading - this will break our sqlite server
            logger.debug("Uploading test started " + r.testDateTime + " to tabsintServer");
            tasks.register(
              "upload results",
              gettextCatalog.getString("Uploading test started ") +
                r.testDateTime.replace("T", " ").replace("Z", "") +
                gettextCatalog.getString(" to TabSINT Server")
            );
            if (config.build === "heartrack" && resultAlreadyUploaded) {
              logger.info(
                "The result with testDateTime " +
                  r.testDateTime +
                  "cannot be uploaded to the tabsintServer because it is already uploaded."
              );
              return $q.resolve();
            } else {
              return tabsintServer.submitResults(r).then(
                function() {
                  // add results to success list
                  success.push(results.filename(r));

                  // updatesummary
                  return results.updateSummary(r).then(function() {
                    if (config.build !== "heartrack") {
                      return results.delete(r.testDateTime);
                    } else {
                      return $q.resolve();
                    }
                  });
                },
                function(e) {
                  logger.error("Failure while uploading result: " + angular.toJson(r)); // put the whole result in the log at this point
                  // pushing error
                  errors.push({ filename: r.testDateTime, error: e });
                  return $q.reject();
                }
              );
            }

            // gitlab
          }

          // gitlab server
          else if (r.testResults.protocol.server === "gitlab") {
            var host, group, token, repositoryName, toPath, filename;

            host = disk.servers.gitlab.host; // host is not yet configurable
            token = disk.servers.gitlab.token; // token is not yet configurable
            toPath = r.testResults.protocol.name + "/"; // root path in gitlab repository
            filename = results.filename(r) + ".json"; // base filename

            // allow user to configure group and repository
            if (disk.gitlab.useSeparateResultsRepo) {
              group = disk.servers.gitlab.resultsGroup;
              repositoryName = disk.servers.gitlab.resultsRepo;
            } else {
              group = disk.servers.gitlab.group;
              repositoryName = "results";
            }

            logger.debug(
              `Uploading test started ${r.testDateTime} to gitlab server with testDateTime: ${angular.toJson(
                r.testDateTime
              )}`
            );
            tasks.register(
              "upload results",
              `Uploading test started ${r.testDateTime
                .replace("T", " ")
                .replace("Z", "")} to gitlab repository ${group} / ${repositoryName}`
            );

            return $q
              .resolve() // start the promise chain
              .then(function() {
                // encrypt results
                if (r.publicKey) {
                  var filenameResult = filename + ".enc";
                  var filenameKey = filename + ".key.enc";
                  var enc = encryptResults.encryptUploadExport(getDateString(r), r.publicKey, angular.toJson(r));

                  return gitlab
                    .commit(
                      host,
                      group,
                      repositoryName,
                      token,
                      enc[0],
                      toPath + filenameResult,
                      `Pushing encrypted result from tablet ${devices.shortUUID} for protocol ${
                        r.testResults.protocol.name
                      }`
                    )
                    .then(function() {
                      return gitlab.commit(
                        host,
                        group,
                        repositoryName,
                        token,
                        enc[1],
                        toPath + filenameKey,
                        `Pushing encryption key from tablet ${devices.shortUUID} for protocol ${
                          r.testResults.protocol.name
                        }`
                      );
                    });
                } else if (disk.requireEncryptedResults) {
                  logger.error("Encrypted results required but no public key is defined");
                  return $q.reject("Public encryption key must be defined in protocol to output encrypted results");
                } else {
                  return gitlab.commit(
                    host,
                    group,
                    repositoryName,
                    token,
                    angular.toJson(r),
                    toPath + filename,
                    `Pushing result from tablet ${devices.shortUUID} for protocol ${r.testResults.protocol.name}`
                  );
                }
              })
              .then(
                function() {
                  // add results to success list
                  success.push(results.filename(r));

                  // update summary
                  return results.updateSummary(r).then(function() {
                    if (config.build !== "heartrack") {
                      return results.delete(r.testDateTime);
                    } else {
                      return $q.resolve();
                    }
                  });
                },
                function(e) {
                  logger.error("Failure while uploading result: " + angular.toJson(r)); // put the whole result in the log at this point

                  // add error to error list
                  let error = e && e.msg ? e.msg : e;
                  errors.push({ filename: results.filename(r), error: error });

                  return $q.reject();
                }
              );
          }
        } else {
          errors.push({
            filename: results.filename(r),
            error: gettextCatalog.getString(
              "The protocol used to generate this result was loaded locally from the tablet, so the result cannot be uploaded"
            )
          });
          return $q.reject("local protocols cannot be uploaded");
        }
      }

      function uploadResult(resultFromSQLite) {
        var q = $q.defer();
        var promise = q.promise;

        // check network status, set
        if (!networkModel.status) {
          logger.info("Offline while trying to upload a result");
          notifications.alert(
            gettextCatalog.getString("The device is having difficulty communicating with the network")
          );
          return $q.reject("offline");
        }

        _.forEach(resultFromSQLite, function(res, i) {
          // if idx is defined, only upload this result. Otherwise upload all results
          if (angular.isDefined(idx)) {
            if (i === idx) {
              promise = promise
                .then(function() {
                  return encryptResults.decrypt(res.date, res.data);
                })
                .then(function(dec) {
                  r = JSON.parse(dec);
                  r.testResults.network = networkModel.type; // put network type for upload on the results structure
                  return directoryHandling(r);
                });
            }
          } else {
            promise = promise
              .then(function() {
                return encryptResults.decrypt(res.date, res.data);
              })
              .then(function(dec) {
                r[i] = JSON.parse(dec);
                r[i].testResults.network = networkModel.type; // put network type for upload on the results structure
                return directoryHandling(r[i]);
              })
              .catch(function() {
                return $q.resolve();
              });
          }
        });

        // after exporting one result of many results, show notification
        promise = promise.finally(function() {
          if (errors.length > 0) {
            let error_string = errors.map(function(value, idx) {
              return value.filename + ": " + value.error + "\n";
            });
            notifications.alert(
              gettextCatalog.getString("One or more results were not uploaded due to errors: " + "\n\n" + error_string)
            );
          } else {
            let success_string = success.map(function(value, idx) {
              return value + "\n";
            });
            notifications.alert(gettextCatalog.getString("Successfully uploaded results: ") + "\n\n" + success_string);
          }
        });

        q.resolve();
        return promise;
      }

      // main promise chain to upload result(s)
      return sqLite
        .count("results")
        .then(sqLite.prepareForUpload("results"))
        .then(function() {
          return sqLite.get("results"); // return all results in sqLite db and pass to uploadResults function
        })
        .then(uploadResult)
        .catch(function(e) {
          logger.error("The tablet encountered an issue while uploading result: " + angular.toJson(e));
          $q.reject(e);
        })
        .finally(function() {
          return tasks.deregister("upload results");
        });
    };

    /**
     * Method to update the upload summary for uploadable results
     * @param  {object} r - result object
     */
    results.updateSummary = function(r) {
      var deferred = $q.defer();
      var meta = {
        siteId: r.siteId,
        protocolName: r.protocolName,
        testDateTime: r.testDateTime,
        nResponses: r.nResponses,
        source: r.testResults.protocol.server,
        output: r.exportLocation || r.testResults.protocol.server,
        uploadedOn: new Date().toJSON()
      };
      var len = disk.uploadSummary.unshift(meta);
      logger.info("Result summary: " + angular.toJson(meta));

      // remove the last element if the summary is beyond the prescribed length
      if (len > results.dm.uploadSummaryLength) {
        disk.uploadSummary.splice(-1, 1);
      }
      deferred.resolve();
      return deferred.promise;
    };

    results.exportCSV = function(result) {
      var base, extension, filenameCSV, filenameFlatHeaderCSV, filenameBroadHeaderCSV, filenameKey;

      base = results.filename(result);
      filenameCSV = base + "_csv.csv";
      filenameFlatHeaderCSV = base + "_csv_FLAT_HEADER.csv";
      filenameBroadHeaderCSV = base + "_csv_BROAD_HEADER.csv";

      var dir;
      if (disk.servers.localServer.resultsDir) {
        dir = disk.servers.localServer.resultsDir;
      } else {
        dir = "tabsint-results";
      }

      // append the protocol name
      dir = dir + "/" + result.protocolName;

      return file
        .createDirRecursively(paths.public(""), dir)
        .then(function() {
          return csv.generateFlatCSV(result).then(function(csvResultString) {
            return writeOutResultsFile(result, filenameCSV, paths.public(dir), csvResultString);
          });
        })
        .then(function() {
          return csv.generateFlatHeaderCSV(result).then(function(csvResultString) {
            return writeOutResultsFile(result, filenameFlatHeaderCSV, paths.public(dir), csvResultString);
          });
        })
        .then(function() {
          return csv.generateBroadHeaderCSV(result).then(function(csvResultString) {
            return writeOutResultsFile(result, filenameBroadHeaderCSV, paths.public(dir), csvResultString);
          });
        })
        .catch(function(e) {
          logger.error("Failed to export results to csv file with error: " + angular.toJson(e));
          notifications.alert(
            gettextCatalog.getString("Failed to export results to csv file. Please file an issue at") +
              " https://gitlab.com/creare-com/tabsint"
          );
          return $q.reject("Failed to write result with error: " + e);
        });
    };

    /**
     * Method to export results to local file
     * @param {number} [idx] - index of result to export. If undefined, will export all
     * @return {promise} promise to export result
     */
    results.export = function(idx) {
      var r = [];
      var filename = [];
      var dir = [];
      var errors = [],
        success = [];
      tasks.register("export result", "Exporting results");

      if (idx > sqLite.numLogs.results) {
        logger.error("Index " + idx + " is outside of results length: " + sqLite.numLogs.results);
        notifications.alert(
          gettextCatalog.getString("Failed to export result at index: ") +
            idx +
            ". " +
            gettextCatalog.getString(
              "Index is out of range of queued results. Please upload your logs if the issue persists."
            )
        );
        return $q.reject();
      }

      // write the file, update summary and delete results for each result exported
      function directoryHandling(r, filename, dir) {
        return file
          .createDirRecursively(paths.public(""), dir)
          .then(function() {
            return writeOutResultsFile(r, filename, paths.public(dir), angular.toJson(r));
          })
          .then(
            function(updatedFilename) {
              r.exportLocation = dir + "/" + updatedFilename;
              logger.info("Successfully exported results to file: " + updatedFilename);

              // add result to success list
              success.push(updatedFilename);

              // update summary
              return results.updateSummary(r).then(function() {
                if (config.build !== "heartrack") {
                  return results.delete(r.testDateTime);
                } else {
                  return $q.resolve();
                }
              });
            },
            function(e) {
              logger.error("Failed to export results to file with error: " + angular.toJson(e));
              errors.push({ filename: filename, error: angular.toJson(e) });
              return $q.reject(e);
            }
          );
      }

      function exportResult(resultsFromSQLite) {
        var q = $q.defer();
        var promise = q.promise;

        _.forEach(resultsFromSQLite, function(res, i) {
          // if idx is defined, only export this result. Otherwise export all results
          if (angular.isDefined(idx)) {
            if (i === idx) {
              promise = promise
                .then(function() {
                  return encryptResults.decrypt(res.date, res.data);
                })
                .then(function(dec) {
                  r = JSON.parse(dec);
                  filename = results.filename(r) + ".json";

                  // select directory
                  if (disk.servers.localServer.resultsDir) {
                    dir = disk.servers.localServer.resultsDir;
                  } else {
                    dir = "tabsint-results";
                  }

                  logger.debug("Exporting test started " + r.testDateTime);
                  tasks.register(
                    "export result",
                    "Exporting test started " +
                      r.testDateTime.replace("T", " ").replace("Z", "") +
                      ' to the "' +
                      dir +
                      '" directory'
                  );

                  // append protocol name into path
                  dir = dir + "/" + r.protocolName;
                  return directoryHandling(r, filename, dir);
                });
              // pass along errors (don't catch)
            }
          } else {
            promise = promise
              .then(function() {
                return encryptResults.decrypt(res.date, res.data);
              })
              .then(function(dec) {
                r[i] = JSON.parse(dec);
                filename[i] = results.filename(r[i]) + ".json";

                // select directory
                if (disk.servers.localServer.resultsDir) {
                  dir[i] = disk.servers.localServer.resultsDir;
                } else {
                  dir[i] = "tabsint-results";
                }

                logger.debug("Exporting test started " + r[i].testDateTime);
                tasks.register(
                  "export result",
                  gettextCatalog.getString("Exporting test started ") +
                    r[i].testDateTime.replace("T", " ").replace("Z", "") +
                    ' to the "' +
                    dir[i] +
                    '" directory'
                );

                // append protocol name into path
                dir[i] = dir[i] + "/" + r[i].protocolName;
                return directoryHandling(r[i], filename[i], dir[i]);
              })
              .catch(function() {
                // catching rejection to continue uploading other results
                return $q.resolve();
              });
          }
        });

        // after exporting one result of many results, show notification
        promise = promise.finally(function() {
          if (errors.length > 0) {
            let error_string = errors.map(function(value, idx) {
              return value.filename + ": " + value.error + "\n";
            });
            notifications.alert(
              gettextCatalog.getString("One or more results were not exported due to errors: " + "\n\n" + error_string)
            );
          } else {
            let success_string = success.map(function(value, idx) {
              return value + "\n";
            });
            notifications.alert(
              gettextCatalog.getString("Successfully exported results in directory: ") + dir + "\n\n" + success_string
            );
          }
        });

        q.resolve();
        return promise;
      }

      // main promise chain to export result(s)
      return sqLite
        .count("results")
        .then(sqLite.prepareForUpload("results"))
        .then(function() {
          return sqLite.get("results"); // return all results in sqLite db and pass to exportResult function
        })
        .then(exportResult)
        .catch(function(e) {
          logger.error("The tablet encountered an issue while exporting result: " + angular.toJson(e));
          $q.reject(e);
        })
        .finally(function() {
          return tasks.deregister("export result");
        });
    };

    /**
     * Delete a result from the queued Results
     * @param  {object} result object to delete
     */
    results.delete = function(date) {
      try {
        date = date
          .replace(":", "-")
          .replace(":", "-")
          .split(".")[0];
        return sqLite.deleteResult(date).then(results.getResultsForResultsView);
      } catch (e) {
        logger.error("Failed to delete result at index: " + date + " with error: " + angular.toJson(e));
        notifications.alert(
          gettextCatalog.getString("Failed to remove result. Please file an issue at") +
            " https://gitlab.com/creare-com/tabsint"
        );
      }
    };

    /**
     * Delete all results from the queued Results
     */
    results.deleteAll = function() {
      logger.info("Deleting all queued results");
      return sqLite.drop("results").then(results.getResultsForResultsView);
    };

    /**
     * Delete all backup results
     */
    results.removeBackupResults = function() {
      logger.info("Deleting all backup results");
      return $cordovaFile.removeRecursively(paths.public(""), ".tabsint-results-backup").then(function() {
        logger.info("Successfully deleted all backup result files");
      });
    };

    /**
     * Used by results-warnings component
     * @return {[type]} [description]
     */
    results.checkStoredResults = function() {
      var ageLimit = 1000 * 3600 * 24; // 1 day, in milliseconds
      var oldExamsStoredLocally = false;
      var now = Date.parse(new Date());
      for (var i = 0; i < sqLite.numLogs.results; i++) {
        var d = Date.parse(sqLite.get("results").date);
        if (now - d > ageLimit) {
          oldExamsStoredLocally = true;
        }
      }
      return oldExamsStoredLocally;
    };

    /**
     * Method to refresh completed tests from sqLite results table for Result(s) View
     * @param  {int} ind - integer (optional)
     * @return {promise} Promise to refresh one or all result(s).
     */
    results.getResultsForResultsView = function(ind) {
      function getResults(resultsFromSQLite) {
        var q = $q.defer();
        var promise = q.promise;

        // reset resultsView
        results.completedTests = [];

        _.forEach(resultsFromSQLite, function(res, idx) {
          // list all the completed tests still in the sqLite db
          promise = promise
            .then(function() {
              return encryptResults.decrypt(res.date, res.data);
            })
            .then(function(dec) {
              results.completedTests.push(JSON.parse(dec));

              // if ind is defined, get one result for the single result viewer
              if (angular.isDefined(ind) && idx === ind) {
                results.singleResultView = results.completedTests[idx];
              }

              return q.resolve();
            })
            .catch(function() {
              // catching rejection to continue getting other results
              return $q.resolve();
            });
        });

        q.resolve();
        return q.promise;
      }

      // if there are results in the sqLite table, refresh them
      if (angular.isDefined(sqLite.numLogs.results) && sqLite.numLogs.results > 0) {
        return sqLite
          .count("results")
          .then(function() {
            return sqLite.prepareForUpload("results");
          })
          .then(function() {
            return sqLite.get("results"); // return all results in sqLite db and pass to getResult function
          })
          .then(getResults)
          .catch(function(e) {
            logger.error("The tablet encountered an issue while getting results: " + angular.toJson(e));
          });

        // if there are no results in the sqLite table, reset completed tests
      } else {
        results.completedTests = undefined;
        return $q.resolve();
      }
    };

    return results;
  });
