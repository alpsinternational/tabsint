/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

// jshint ignore: start

"use strict";

import * as angular from "angular";
import * as _ from "lodash";
import "angular-mocks";
import "../../app";

beforeEach(angular.mock.module("tabsint"));

describe("Results Service", function() {
  var results, disk, file, sqLite, config, json, paths, pm, networkModel, adminLogic, $scope;

  beforeEach(
    angular.mock.inject(function(
      $injector,
      _results_,
      _disk_,
      _file_,
      _sqLite_,
      _config_,
      _json_,
      _paths_,
      _pm_,
      _networkModel_,
      _adminLogic_,
      _$httpBackend_
    ) {
      results = _results_;
      disk = _disk_;
      file = _file_;
      sqLite = _sqLite_;
      config = _config_;
      json = _json_;
      paths = _paths_;
      pm = _pm_;
      networkModel = _networkModel_;
      adminLogic = _adminLogic_;
      $scope = $injector.get("$rootScope");
      _$httpBackend_.whenGET("res/translations/translations.json").respond(200, "a string");
    })
  );

  function fakeResult(siteId, date) {
    return {
      siteId: siteId,
      testResults: { fake: "fake result" },
      testDateTime: date
    };
  }

  describe("results.create", function() {
    it("results properties should match schema properties 1:1", function() {
      // results create method looks for pm.root.publicKey, add structure here
      pm.root = { publicKey: "" };
      var jsonSchema = json.load(paths.www("scripts/services/results/results_schema.json"));
      var schema_keys = Object.keys(jsonSchema.properties).sort();
      results.create();
      expect(Object.keys(results.current).sort()).toEqual(schema_keys.sort());
      expect(Object.keys(results.current.testResults).sort()).toEqual(
        Object.keys(jsonSchema.properties.testResults.properties).sort()
      );
    });
  });

  describe("results.upload", function() {
    var r1 = fakeResult(1, new Date().toJSON());
    it("Test that upload calls sqLite.prepareForUpload", function() {
      // results create method looks for pm.root.publicKey, add structure here
      spyOn(sqLite, "prepareForUpload");
      results.upload(0);
      expect(sqLite.prepareForUpload).toHaveBeenCalled();
    });
    // it("Test that upload results w/ index out of scope fails", function() {
    //   // results create method looks for pm.root.publicKey, add structure here
    //   response = results.upload(1);

    // });
  });

  describe("results.save", function() {
    var r1 = fakeResult(1, new Date().toJSON());
    var r2 = fakeResult(2, new Date("2015-07-01T00:00:00").toJSON()); // results are stored by date, so r1 and r2 cannot have the same date
    it("should properly push 1 result to the queue", function() {
      disk.server = "localServer"; // do this so that results do not get uploaded
      results
        .save(r1)
        .then(results.getResultsForResultsView)
        .finally(function() {
          expect(_.includes(JSON.stringify(results.completedTests), JSON.stringify(r1))).toBeTruthy();
          expect(_.includes(JSON.stringify(results.completedTests), JSON.stringify(r2))).toBeFalsy();
        });
      $scope.$apply();
    });

    it("should properly push 2 results to the queue", function() {
      results
        .save(r1)
        .then(function() {
          return results.save(r2);
        })
        .then(function() {
          expect(sqLite.numLogs.results).toEqual(2);
        })
        .catch(function() {
          expect(false).toBeTruthy();
        });
      $scope.$apply();
    });

    it("should properly delete first result from the queue", function() {
      results
        .save(r1)
        .then(function() {
          return results.save(r2);
        })
        .then(function() {
          return results.delete(r1.testDateTime);
        })
        .then(results.getResultsForResultsView)
        .finally(function() {
          expect(_.includes(JSON.stringify(results.completedTests), JSON.stringify(r1))).toBeFalsy();
          expect(_.includes(JSON.stringify(results.completedTests), JSON.stringify(r2))).toBeTruthy();
        });
      $scope.$apply();
    });

    it("should properly delete second result from the queue", function() {
      results
        .save(r1)
        .then(function() {
          return results.save(r2);
        })
        .then(function() {
          return results.delete(r1.testDateTime);
        })
        .then(function() {
          return results.delete(r2.testDateTime);
        })
        .then(results.getResultsForResultsView)
        .finally(function() {
          expect(_.includes(JSON.stringify(results.completedTests), JSON.stringify(r1))).toBeFalsy();
          expect(_.includes(JSON.stringify(results.completedTests), JSON.stringify(r2))).toBeFalsy();
        });
      $scope.$apply();
    });

    it("should backup results every time", function() {});

    it("should export results if its a localServer", function() {});

    it("should upload results if its a gitlab server or tabsintServer", function() {});

    it("should show alert if too many results are queued", function() {});
  });

  xdescribe("results.upload", function() {
    // this was copied over from admin.spec.js and should be adapted to fit here

    describe("Upload", function() {
      function expectPost(val) {
        $httpBackend
          .expectPOST(config.tabsintServer.url + "InsertTest")
          .respond(200, '{"success":' + val + ',"status":"created","testid":10}');
      }

      var logSuccess;

      function expectLog() {
        $httpBackend.whenPOST(config.tabsintServer.url + "UploadLogJSON").respond(function(method, url, data) {
          var val = false;
          if (logSuccess.length > 0) {
            val = logSuccess.splice(0, 1)[0];
          }
          if (val) {
            console.log("respond");
            data = JSON.parse(data);
            var acceptedLogs = [];
            data.logs.forEach(function(item) {
              acceptedLogs.push(item.msgID);
            });
            return [200, '{"success":"true","acceptedLogs":' + angular.toJson(acceptedLogs) + "}"];
          } else {
            return [200, '{"success":"false"}'];
          }
        });
      }

      beforeEach(function() {
        expectLog();
      });

      function expectGet(num) {
        $httpBackend
          .expectGET(config.tabsintServer.url + "countTests")
          .respond(200, '{"success":true,"status":"counted","count":"' + num + '"}');
      }

      function fakeResult(siteId) {
        return {
          siteId: siteId,
          testResults: { fake: "fake result" },
          testDateTime: new Date().toJSON()
        };
      }

      beforeEach(function() {
        sqLite.store(
          "results",
          new Date()
            .toJSON()
            .replace(":", "-")
            .replace(":", "-")
            .split(".")[0],
          "result",
          fakeResult(1),
          {
            build: null,
            siteId: null,
            uuid: null,
            version: null,
            platform: null,
            model: null,
            os: null,
            other: null
          }
        );
        disk.autoUpload = false;
        networkModel.status = true;
        spyOn(notifications, "alert");
        spyOn(notifications, "confirm");
      });

      afterEach(function() {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      it("should upload a single result properly", function() {
        adminLogic.act.uploadResults(1);
        expectGet(1);
        expectPost("true");
        expectGet(2);
        // expectReportTrigger();
        logSuccess = [true];
        $httpBackend.flush();
        expect(sqLite.numLogs.results).toEqual(2);
      });

      it("should fail gracefully in the single result case if success==false", function() {
        adminLogic.act.uploadResults();
        expectGet(1);
        expectPost("false");
        logSuccess = [true];
        $httpBackend.flush();
        expect(sqLite.numLogs.results).toEqual(3);
        expect(notifications.alert).toHaveBeenCalled();
      });

      it("should fail gracefully in the single result case if testresults not incremented", function() {
        adminLogic.act.uploadResults();
        expectGet(1);
        expectPost("true");
        expectGet(1);
        logSuccess = [true];
        $httpBackend.flush();
        expect(sqLite.numLogs.results).toEqual(3);
        expect(notifications.alert).toHaveBeenCalled();
      });
    });

    describe("Auto-uploader ", function() {
      function expectPost(val) {
        $httpBackend
          .expectPOST(config.tabsintServer.url + "InsertTest")
          .respond(200, '{"success":' + val + ',"status":"created","testid":10}');
      }

      function expectGet(num) {
        $httpBackend
          .expectGET(config.tabsintServer.url + "countTests")
          .respond(200, '{"success":true,"status":"counted","count":"' + num + '"}');
      }

      // must call this after each expectPost now - auto-uploading logs once after uploading result(s)
      var logSuccess;

      function expectLog() {
        $httpBackend.whenPOST(config.tabsintServer.url + "UploadLogJSON").respond(function(method, url, data) {
          var val = false;
          if (logSuccess.length > 0) {
            val = logSuccess.splice(0, 1)[0];
          }
          if (val) {
            console.log("respond");
            data = JSON.parse(data);
            var acceptedLogs = [];
            data.logs.forEach(function(item) {
              acceptedLogs.push(item.msgID);
            });
            return [200, '{"success":"true","acceptedLogs":' + angular.toJson(acceptedLogs) + "}"];
          } else {
            return [200, '{"success":"false"}'];
          }
        });
      }

      beforeEach(function() {
        expectLog();
      });

      beforeEach(function() {
        networkModel.status = true;
        disk.autoUpload = false;
        sqLite.drop("results");
        disk.suppressAlerts = false;
        spyOn(notifications, "alert");
        spyOn(notifications, "confirm");
        config.load();
        disk.site.siteName = "defaultName";
        disk.site.siteId = 7;
      });

      afterEach(function() {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
      });

      it("should alert if results are piling up and auto-upload is off", function() {
        if (disk.server === "tabsintServer") {
          expect(sqLite.numLogs.results).toEqual(0);
          var r1;
          for (var i = 0; i < results.queuedResultsWarnLength; i++) {
            r1 = "c" + i;
            results.save(r1);
          }
          expect(sqLite.numLogs.results).toEqual(results.queuedResultsWarnLength);
          expect(notifications.confirm).toHaveBeenCalled();
        }
      });

      it("should not alert if results are piling up and auto-upload is off, but suppressAlerts is enabled", function() {
        if (disk.server === "tabsintServer") {
          disk.suppressAlerts = true;

          expect(sqLite.numLogs.results).toEqual(0);
          var r1;
          for (var i = 0; i < results.queuedResultsWarnLength; i++) {
            r1 = "c" + i;
            results.save(r1);
          }
          expect(sqLite.numLogs.results).toEqual(results.queuedResultsWarnLength);
          expect(notifications.confirm).not.toHaveBeenCalled();
        }
      });

      it("should alert when user toggles auto-upload on but device is not online", function() {
        if (disk.server === "tabsintServer") {
          networkModel.status = false;

          disk.autoUpload = true;
          adminLogic.act.changedAutoUpload();

          expect(disk.autoUpload).toBeTruthy();
          expect(notifications.alert).toHaveBeenCalled();
        }
      });

      it("should successfully upload when user toggles auto-upload on and device is online", function() {
        if (disk.server === "tabsintServer") {
          var r1 = { stuff: "b" };
          results.save(r1);
          expect(sqLite.numLogs.results).toEqual(1);

          expectGet(1);
          expectPost("true");
          expectGet(2);
          // expectReportTrigger();
          logSuccess = [true];
          disk.autoUpload = true;
          adminLogic.act.changedAutoUpload();

          expect(disk.autoUpload).toBeTruthy();
          $httpBackend.flush();

          expect(sqLite.numLogs.results).toEqual(0);
        }
      });

      it("should alert when it fails to upload when user toggles auto-upload on and device is online", function() {
        if (disk.server === "tabsintServer") {
          var r1 = { stuff: "b" };
          results.save(r1);
          expect(sqLite.numLogs.results).toEqual(1);

          disk.autoUpload = true;
          adminLogic.act.changedAutoUpload();
          expect(disk.autoUpload).toBeTruthy();

          expectGet(1);
          expectPost("false");
          logSuccess = [true];
          $httpBackend.flush();

          expect(sqLite.numLogs.results).toEqual(1);
          expect(notifications.alert).toHaveBeenCalled();
        }
      });
    });
  });
});
