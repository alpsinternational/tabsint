/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import "./checksum/checksum";
import "./json/json";
import "./disk/disk";
import "./encrypt-results/encrypt-results";
import "./paths/paths";
import "./router/router";
import "./cordova/cordova";
import "./plugins/plugins";
import "./gitlab/gitlab";
import "./protocol/protocol";
import "./logger/logger";
import "./logger/log-export";
import "./results/results";
import "./config/config";
import "./auto-config/auto-config";
import "./local-server/local-server";
import "./tabsint-server/tabsint-server";
import "./admin/admin";
import "./remote/remote";
import "./version/version";
import "./tasks/tasks";
import "./exam/exam";
import "./page/page";
import "./csv/csv";
import "./advanced-protocol/advanced-protocol";
import "./file-chooser/file-chooser";
import "./subject-history/subject-history";
import "./cha/cha";
import "./cha-choose/cha-choose";
import "./cha-exams/cha-exams";
import "./cha-check/cha-check";
import "./cha-media/cha-media";
import "./cha-results/cha-results";
import "./cha-plot/cha-plot";
import "./cha-mock/mockChaWrap";
import "./cha/cha-constants";
import "./svantek/svantek";
import "./flic/flic";
import "./external-control-mode/external-control-mode";
import "./gain/gain";

angular.module("tabsint.services", [
  "tabsint.services.checksum",
  "tabsint.services.encrypt-results",
  "tabsint.services.local-server",
  "tabsint.services.tabsint-server",
  "tabsint.services.disk",
  "tabsint.services.paths",
  "tabsint.services.router",
  "tabsint.services.cordova",
  "tabsint.services.plugins",
  "tabsint.services.gitlab",
  "tabsint.services.protocol",
  "tabsint.services.logger",
  "tabsint.services.log-export",
  "tabsint.services.results",
  "tabsint.services.config",
  "tabsint.services.auto-config",
  "tabsint.services.admin",
  "tabsint.services.remote",
  "tabsint.services.version",
  "tabsint.services.tasks",
  "tabsint.services.exam",
  "tabsint.services.page",
  "tabsint.services.advanced-protocol",
  "tabsint.services.file-chooser",
  "tabsint.services.subject-history",
  "tabsint.services.json",
  "tabsint.services.csv",

  "tabsint.services.cha",
  "tabsint.services.cha.cha-choose",
  "tabsint.services.cha.exams",
  "tabsint.services.cha.check",
  "tabsint.services.cha.media",
  "tabsint.services.cha.results",
  "tabsint.services.cha.plot",
  "tabsint.services.cha.mockChaWrap",
  "tabsint.services.cha.constants",
  "tabsint.services.svantek",
  "tabsint.services.flic",
  "tabsint.services.external-control-mode",
  "tabsint.services.gain"
]);
