/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

angular
  .module("tabsint.services.csv", [])

  .factory("csv", function() {
    var api = {};

    api.generateBroadHeaderCSV = function(dataIn) {
      var csvFieldName = "";
      var csvValue = "";

      var dataInCopy = angular.copy(dataIn);
      delete dataInCopy.testResults.responses;

      function flattenHeader(data) {
        function recurse(cur, prop) {
          if (Object(cur) !== cur) {
            csvFieldName += prop + ",";
            csvValue += JSON.stringify(cur) + ",";
          } else if (Array.isArray(cur)) {
            for (var i = 0, l = cur.length; i < l; i++) {
              recurse(cur[i], prop + "[" + i + "]");
            }
            if (l === 0) {
              csvFieldName += prop + ",";
              csvValue += "[empty]" + ",";
            } else {
              csvFieldName += prop + ",";
              csvValue += "[]" + ",";
            }
          } else if (!Array.isArray(cur)) {
            var isEmpty = true;
            for (var p in cur) {
              isEmpty = false;
              recurse(cur[p], prop ? prop + "." + p : p);
            }
            if (isEmpty && prop) {
              csvFieldName += prop + ",";
              csvValue += "{empty}" + ",";
            } else {
              csvFieldName += prop + ",";
              csvValue += "{}" + ",";
            }
          }
        }
        recurse(data, "");
      }

      flattenHeader(dataInCopy);
      var csv = csvFieldName + "\r\n" + csvValue + "\r\n";

      csv += "\r\n SUBJECT RESPONSES \r\n";
      csv += "Presentation Id, Response \r\n";

      dataIn.testResults.responses.forEach(function(item) {
        csv += item.presentationId + ",";
        csv += JSON.stringify(item.response) + "\r\n";
      });

      // console.log('Generated BroadHeader CSV:');
      // console.log(csv);
      return csv;
    };

    api.generateFlatCSV = function(dataIn) {
      function flatten(data) {
        var result = {};

        function recurse(cur, prop) {
          if (Object(cur) !== cur) {
            result[prop] = cur;
          } else if (Array.isArray(cur)) {
            for (var i = 0, l = cur.length; i < l; i++) {
              recurse(cur[i], prop + "[" + i + "]");
            }
            if (l === 0) {
              result[prop] = [];
            }
          } else {
            var isEmpty = true;
            for (var p in cur) {
              isEmpty = false;
              recurse(cur[p], prop ? prop + "." + p : p);
            }
            if (isEmpty && prop) {
              result[prop] = {};
            }
          }
        }
        recurse(data, "");
        return result;
      }

      function flatJSONtoCSV(data) {
        var fields = Object.keys(data);

        var csv = "";
        fields.forEach(function(item, idx) {
          csv += item + "," + JSON.stringify(data[item]);
          csv += "\r\n";
        });

        // console.log('Generated Flat CSV:');
        // console.log(csv);
        return csv;
      }

      var flattenedData = flatten(dataIn);
      return flatJSONtoCSV(flattenedData);
    };

    api.generateFlatHeaderCSV = function(dataIn) {
      var dataInCopy = angular.copy(dataIn);
      delete dataInCopy.testResults.responses;
      var csv = api.generateFlatCSV(dataInCopy);

      csv += "\r\n SUBJECT RESPONSES \r\n";
      csv += "Presentation Id, Response \r\n";

      dataIn.testResults.responses.forEach(function(item) {
        csv += item.presentationId + ",";
        csv += JSON.stringify(item.response) + "\r\n";
      });
      // console.log('Generated FlatHeader CSV:');
      // console.log(csv);

      return csv;
    };

    return api;
  });
