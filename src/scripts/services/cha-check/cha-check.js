/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";

angular.module("tabsint.services.cha.check", []).factory("chaCheck", function() {
  return {
    isAudiometryExam: function(exam) {
      return _.includes(
        ["BekesyLike", "BekesyFrequency", "HughsonWestlake", "HughsonWestlakeFrequency", "BHAFT", "BekesyMLD"],
        exam
      );
    },
    isBekesyExam: function(exam) {
      return _.includes(["BekesyLike", "BekesyFrequency", "BHAFT", "BekesyMLD"], exam);
    },
    isFrequencyExam: function(exam) {
      return _.includes(["BekesyFrequency", "HughsonWestlakeFrequency"], exam);
    },
    isLevelExam: function(exam) {
      return _.includes(["BekesyLike", "HughsonWestlake", "BekesyMLD"], exam);
    },
    isHughsonWestlakeExam: function(exam) {
      return _.includes(["HughsonWestlake", "HughsonWestlakeFrequency"], exam);
    },
    isThirdOctaveBandsExam: function(exam) {
      return _.includes(["ThirdOctaveBands"], exam);
    },
    isToneGenerationExam: function(exam) {
      return _.includes(["ToneGeneration"], exam);
    },
    isDPOAEExam: function(exam) {
      return _.includes(["DPOAE"], exam);
    }
  };
});
