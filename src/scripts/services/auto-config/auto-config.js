/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";

angular
  .module("tabsint.services.auto-config", [])
  .factory("autoConfig", function(
    $q,
    config,
    disk,
    examLogic,
    gain,
    gettextCatalog,
    gitlab,
    json,
    localServer,
    logger,
    notifications,
    paths,
    protocol,
    tabsintServer,
    tasks
  ) {
    var api = {
      load: undefined,
      scanQrCode: undefined,
      loadProtocol: undefined,
      loadExamView: undefined,
      add: undefined,
      checkMedia: undefined,
      pullMedia: undefined
    };

    /**
     * Auto-configure tabsint and start protocol on QR Config Code scan
     * @returns {promise}
     */
    api.load = function() {
      tasks.register("autoconfig", "Loading Protocol");
      api.loadProtocol().finally(function() {
        tasks.deregister("autoconfig");
      });
    };

    /**
     * Load protocol based on Qr Code scan protocol object.
     */
    api.loadProtocol = function() {
      if (config.server === "tabsintServer") {
        disk.servers.tabsintServer.url = config.tabsintServer.url;
        disk.servers.tabsintServer.username = config.tabsintServer.username;
        disk.servers.tabsintServer.password = config.tabsintServer.password;
        disk.servers.tabsintServer.site = config.tabsintServer.site;

        tabsintServer
          .reauthorize()
          .then(function() {
            if (tabsintServer.dm.authorized === false) {
              notifications.alert("TabSINT server authentication failed. Please verify credentials and try again.");
              logger.error(
                "TabSINT server authentication failed while auto-configuring TabSINT and loading protocol. "
              );
              return $q.reject();
            }
          })
          .catch(function(e) {
            logger.error("Failure while authentication TabSINT server.");
          });
      } else if (config.server === "gitlab") {
        disk.servers.gitlab.repository = config.gitlab.repository;
        disk.servers.gitlab.version = config.gitlab.version;
        disk.servers.gitlab.host = config.gitlab.host;
        disk.servers.gitlab.token = config.gitlab.token;
        disk.servers.gitlab.group = config.gitlab.group;
        disk.gitlab.useTagsOnly = config.gitlab.onlyTrackTags;
        disk.servers.gitlab.resultsGroup = config.gitlab.resultsGroup;
        disk.servers.gitlab.resultsRepo = config.gitlab.resultsRepo;
      }

      return api
        .add()
        .then(function(gProtocol) {
          return protocol.load(gProtocol, true, false);
        })
        .then(function() {
          return examLogic.reset();
        })
        .then(function() {
          return examLogic.switchToExamView();
        })
        .catch(function(e) {
          $q.reject("Failed to load protocol and switch to exam view. " + e);
        });
    };

    /**
     * Add protocols from server set on `disk.server`
     */
    api.add = function() {
      if (disk.server === "tabsintServer") {
        // if no site is defined, skip auto-configuration and reject to skip protocol loading
        if (!disk.servers.tabsintServer.site) {
          logger.warn("no site defined during auto-configuration");
          return $q.reject();
        }

        return tabsintServer
          .updateConfiguration(disk.servers.tabsintServer.site) // run site configuration update.
          .then(function(newProtocol) {
            api.checkMedia(_.last(disk.protocols));
            return newProtocol;
          });
      } else if (disk.server === "gitlab") {
        // if no repository is defined, skip auto-configuration, and reject to skip protocol loading
        if (!disk.servers.gitlab.repository) {
          logger.warn("no repository defined during auto-configuration");
          return $q.reject();
        }

        var host, group, token;
        host = disk.servers.gitlab.host;
        group = disk.servers.gitlab.group;
        token = disk.servers.gitlab.token;

        return gitlab
          .add(host, group, disk.servers.gitlab.repository, token, disk.servers.gitlab.version, "protocol")
          .then(function(repo) {
            // store on disk in the correct format
            var gProtocol = gitlab.defineProtocol(repo);
            protocol.store(gProtocol);

            // check for media repositories
            api.checkMedia(_.last(disk.protocols));

            // show user success message
            notifications.alert(
              `${gettextCatalog.getString("Successfully added protocol:")} ${repo.path_with_namespace}`
            );
            return gProtocol;
          })
          .catch(function(e) {
            if (e && e.msg) {
              notifications.alert(e.msg);
            } else {
              logger.error(
                `Unknown failure while adding gitlab protocol ${host} ${group} ${
                  disk.servers.gitlab.repository
                } ${token} ${disk.servers.gitlab.version} with error: ${angular.toJson(e)}`
              );
              notifications.alert(
                gettextCatalog.getString(
                  "TabSINT encountered an issue while accessing gitlab. Please verify the wifi connection and gitlab settings. Please upload your logs if the issue persists."
                )
              );
            }
          });
      } else if (disk.server === "localServer") {
        return localServer.addProtocol();
      }
    };

    /**
     * Local function to check protocol and update media
     * Check current protocol, if it uses a common media repo, update if loaded/download if not loaded
     */
    api.checkMedia = function(meta) {
      var deferred = $q.defer();
      var p = json.load(meta.path + "protocol.json");

      // if the protocol uses a common media repo, we need to ensure the repo is loaded and up to date
      if (p && p.commonMediaRepository) {
        var midx = _.findIndex(disk.mediaRepos, {
          name: p.commonMediaRepository
        });

        // Protocol uses a common repo, which is currently checked out.  Check for updates for the common media repo
        if (midx > -1) {
          var mediaObject = disk.mediaRepos[midx];
          logger.debug(
            `This protocol uses the common media repository ${
              mediaObject.name
            }. Checking for updates for the media repository now.`
          );
          return api.pullMedia(mediaObject);
        }

        // Protocol uses a common repo, but the common repo is not checked out - try now.
        else if (midx === -1) {
          logger.debug(
            `This protocol uses a common media repository ${
              p.commonMediaRepository
            }, but the repository is not downloaded. Attempting to download now.`
          );

          // select location for protocols
          var host, group, token;
          host = disk.servers.gitlab.host;
          group = disk.servers.gitlab.group;
          token = disk.servers.gitlab.token;

          // download the latest version of the media repository
          return gitlab
            .add(host, group, p.commonMediaRepository, token, undefined, "media")
            .then(function(repo) {
              api.storeMedia(gitlab.defineProtocol(repo));

              // show user success message
              notifications.alert(
                `${gettextCatalog.getString("Successfully added media:")} ${repo.path_with_namespace}`
              );
            })
            .catch(function(e) {
              if (e && e.msg) {
                notifications.alert(e.msg);
              } else {
                logger.error(
                  `Unknown failure while adding gitlab media ${host} ${group} ${
                    p.commonMediaRepository
                  } ${token} with error: ${angular.toJson(e)}`
                );
                notifications.alert(
                  gettextCatalog.getString(
                    "TabSINT encountered an issue while accessing gitlab. Please verify the wifi connection and gitlab settings. Please upload your logs if the issue persists."
                  )
                );
              }
            });
        }
      } else {
        deferred.resolve();
      }
      return deferred.promise;
    };

    /**
     * Convienence function to pull media via gitlab and update disk media repo object
     * @param  {object} mediaObject - media object to pull
     */
    api.pullMedia = function(mediaObject) {
      return gitlab
        .pull(mediaObject.repo)
        .then(function(repo) {
          var pidx = _.findIndex(disk.mediaRepos, {
            path: paths.data(paths.gitlab(repo))
          });
          if (pidx !== -1) {
            disk.mediaRepos[pidx] = gitlab.defineProtocol(repo);
          }
        })
        .catch(function(e) {
          if (e && e.msg) {
            notifications.alert(e.msg);
          } else {
            logger.error(
              `Unknown failure while pulling gitlab protocol ${mediaObject.repo.host} ${mediaObject.repo.group} ${
                mediaObject.repo.name
              } ${mediaObject.repo.token} with error: ${angular.toJson(e)}`
            );
            notifications.alert(
              gettextCatalog.getString(
                "TabSINT encountered an issue while updating the media repository.  Please verify the media location. Please upload your logs if the issue persists."
              )
            );
          }
        });
    };

    return api;
  });
