/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

/* jshint bitwise: false */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";

angular
  .module("tabsint.services.cha.media", [])
  .factory("chaMedia", function(
    $q,
    disk,
    logger,
    gitlab,
    tasks,
    notifications,
    $cordovaFile,
    paths,
    app,
    cha,
    file,
    checksum
  ) {
    var api = {
      selected: undefined,
      model: {},
      select: undefined,
      mclass: undefined,
      syncRepoToCha: undefined,
      cancelMediaSync: undefined,
      addRepo: undefined,
      updateRepo: undefined,
      deleteRepo: undefined,
      isAdmin: false,
      syncActive: false
    };

    var protectedRepo = {
      group: "cha-media",
      name: "protected-media",
      host: "https://gitlab.com/",
      token: "YVUR76jq9Ga9xPFNgzfP"
    };

    var fileListEqualOnCha, fileListDeleteFromCha, fileListTransferToCha;

    if (!disk.cha) {
      disk.cha = {};
    }

    if (!disk.cha.mediaRepos) {
      disk.cha.mediaRepos = [];
    }

    // Select a specific media repo, from disk.cha.mediaRepos, to sync to cha
    api.select = function(m) {
      if (api.selected === m) {
        api.selected = undefined;
      } else {
        api.selected = m;
      }
    };

    api.mclass = function(m) {
      if (_.isEqual(api.selected, m)) {
        return "table-selected";
      } else {
        return "";
      }
    };

    // mediaSync is a very async process, so it can be pretty difficult to
    // cancel immediately.  api.syncActive is checked at all steps to abort
    // processes as necessary
    api.cancelMediaSync = function() {
      logger.debug("Cancelling Cha Media Repo Sync");
      tasks.register("cancel-media-sync", "Cancelling CHA Media Sync.");
      return cha
        .cancelFileOperation()
        .catch(function(e) {
          logger.debug("CHA - Error caught when cancelling CHA file operation" + JSON.stringify(e));
        })
        .finally(function() {
          api.syncActive = false; // this brake gets checked all over - use it to kill async processes
          tasks.deregister("cancel-media-sync");
        });
    };

    function delay(t, v) {
      return new Promise(function(resolve) {
        setTimeout(resolve.bind(null, v), t);
      });
    }

    api.syncRepoToCha = function() {
      if (!api.selected || !api.selected.path) {
        var msg = "Cannot sync repo to CHA.  No repo selected.";
        logger.warn(msg);
        notifications.alert(msg);
        return;
      }

      logger.info(
        "Syncing CHA Media Repo " +
          api.selected.group +
          ":" +
          api.selected.name +
          " to headset with adminMode " +
          api.selected.admin
      );
      tasks.register(
        "sync-CHA-media-repo",
        "Syncing CHA Media Repo.  Comparing Media on CHA with Selected Repository.  This comparison may take a few minutes."
      );
      api.syncActive = true; // release the brake
      var repoPath = api.selected.path;
      var isAdmin = api.selected.admin;
      // these arrays will get populated during the sync comparison, then used afterward for the actual deleting and transferring
      var transferList = [],
        equalDirList = [],
        equalFileList = [],
        deleteList = [],
        deleteDirectoryList = [],
        deleteDirectoryCrcList = [];

      /**
       * Recursively iterate through all files and folders in the repo path.
       * Compare to files and folders on cha.
       * Delete files and folders on cha that aren't in the repo path (Except the 'user' directory and any folders immediately in that directory.)
       * Create any directories that aren't yet on the cha.
       * Transfer any files in the repo path that aren't on the cha.
       */
      function recurse(currentPath) {
        console.log("--recursing.  current path: " + currentPath);
        if (!api.syncActive) {
          logger.debug(
            "CHA media sync breaking recursion chain because sync has been cancelled, probably by the user."
          );
          return $q.reject("Sync Cancelled.");
        }
        var chaDirectories = [],
          chaDirCrcs = [],
          chaCrcs = [];
        var tabletDirectories = [];
        var chaDirectoryName = currentPath.replace(repoPath, ""); // remove the tablet portion of the nativeURL
        chaDirectoryName = chaDirectoryName.toUpperCase(); // transition to upper case so everything matches the cha directory structure
        /* we have to deal with this really strange behavior where some cha api's
         * accept and return absolute paths (root or C:) and others work at the relative path level of C:USER */
        var chaDirectoryNameRoot = chaDirectoryName;
        if (!isAdmin) {
          chaDirectoryNameRoot = "USER/" + chaDirectoryName;
        }
        console.log("--requestingcrc for path: " + chaDirectoryNameRoot);

        return cha
          .getDirectoryCrc(chaDirectoryNameRoot) // get crcs for cha files and dirs in the current recurse directory
          .then(function(returnedObj) {
            // if anything found, sort into files and directories.  If nothing found, mkdir
            if (returnedObj.success && returnedObj.result.length > 0) {
              chaCrcs = returnedObj.result.filter(function(entry) {
                return angular.isDefined(entry.Attributes) && !(entry.Attributes & 16);
              });
              chaDirCrcs = returnedObj.result.filter(function(entry) {
                return angular.isDefined(entry.Attributes) && entry.Attributes & 16;
              });
              console.log("chaCrcs: " + JSON.stringify(chaCrcs));
              console.log("chaDirCrcs: " + JSON.stringify(chaDirCrcs));
            } else {
              return cha.makeDirectoryRoot(chaDirectoryNameRoot); // make the directory
            }
          })
          .then(function() {
            /* Any directories on the CHA that aren't in the media repo should be removed.
             * Caveat - not at the USER level - folks may have two different repos there
             * Caveat - not at the root level or for any protected media - we
             * can't delete non-empty directories, and we can't delete files in
             * directory if we only know it's CRC, because we'd need the path
             */
            if (!isAdmin || chaDirectoryName.indexOf("user") >= 0) {
              return cha.getDirectory(chaDirectoryName).then(function(returnedObj) {
                if (returnedObj.success && returnedObj.result.length > 0) {
                  chaDirectories = returnedObj.result.filter(function(entry) {
                    return angular.isDefined(entry.Attributes) && entry.Attributes & 16;
                  });
                } else {
                  chaDirectories = [];
                }
                console.log("cha directories: " + JSON.stringify(chaDirectories));
              });
            }
          })
          .then(function() {
            // get tablet-based media repo files in this directory
            return file.getEntries(currentPath);
          })
          .then(function(entries) {
            var chain = $q.when(); // start a promise chain to run each async task sequentially and find out when they are all done.
            entries.forEach(function(entry) {
              // iterate over media entries
              chain = chain.then(function() {
                console.log("currentPath: " + currentPath);
                var chaTarget = chaDirectoryNameRoot + entry.name; // this is why we needed the root definition
                if (entry.isDirectory) {
                  var dirCrc = checksum.crc32ofString(entry.name);
                  tabletDirectories.push(entry.nativeURL); // save this for recursion later

                  // is the directory already on the cha?
                  var tmpIndCrc = chaDirCrcs.findIndex(function(chaDirCrc) {
                    return chaDirCrc.Path === dirCrc;
                  });
                  if (tmpIndCrc >= 0) {
                    equalDirList.push(chaTarget); // for debugging
                    // remove from the directories list.
                    chaDirCrcs.splice(tmpIndCrc, 1); // any remaining should be deleted if deleting non-empty directories by crc ever becomes possible

                    // messy, but have to build a human-readable path list separately - can't recurse into a dir to delete files using the crc because we don't know the actual name!
                    var tmpIndDir = chaDirectories.findIndex(function(chaDir) {
                      return chaDir.Path === entry.name.toUpperCase();
                    });
                    if (tmpIndDir >= 0) {
                      chaDirectories.splice(tmpIndDir, 1);
                    }
                  } else {
                    return cha.makeDirectoryRoot(chaTarget); // it's not on the cha. create this directory - fine if it already exists.
                  }
                } else if (entry.isFile) {
                  var fileSize, fileCrc; // need these at this closure level - can only pass one variable via the promise chain
                  return checksum
                    .crc32(entry.fullPath) // it's a file - get crc, compare with chaCrcs list
                    .then(function(crc) {
                      fileCrc = crc; // store for use later
                    })
                    .then(function() {
                      // get file size.  For transfer time calculations.
                      // return entry.file(function(theFile) fileSize=theFile.fileSize)  doesn't properly hold the promise chain until fileSize is defined.
                      // Have to create a promise manually.
                      var deferred = $q.defer();
                      entry.file(
                        function(theFile) {
                          fileSize = theFile.size;
                          deferred.resolve();
                        },
                        function(err) {
                          fileSize = 0;
                          deferred.resolve();
                        } // something went wrong, but don't let this break functionality - a transfer time estimate is just a nice-to-have.
                      );
                      return deferred.promise;
                    })
                    .then(function() {
                      // Find out if this file is on the cha already
                      var tmpInd = chaCrcs.findIndex(function(chaCrc) {
                        return chaCrc.Path === fileCrc;
                      });
                      if (tmpInd >= 0) {
                        equalFileList.push(chaTarget); // for debugging
                        chaCrcs.splice(tmpInd, 1); // remove from the chaCrcs list.  any remaining after this entries.forEach loop should be deleted.
                      } else {
                        transferList.push({
                          targetURL: chaTarget,
                          nativeURL: entry.nativeURL,
                          fileSize: fileSize
                        }); // it's not on the cha - add to transfer list
                      }
                    });
                }
              });
            });
            return chain;
          })
          .then(function() {
            // Build delete action lists - any remaining crcs/dirCrcs/directories should be deleted
            var userIndex = chaDirectoryNameRoot.toUpperCase().indexOf("USER");
            var userString = "USER/";
            if (userIndex >= 0 && userIndex >= chaDirectoryNameRoot.length - userString.length) {
              logger.debug("CHA media sync: not deleting - this is a user directory: " + chaDirectoryNameRoot);
              // don't delete files here - they are in the main user directory - things like mediaver.txt and cha_prog.dat, plus users can have more than one media repo, and they'd all go in USER!
            } else {
              chaCrcs.forEach(function(chaCrc) {
                // all remaining chaCrcs did not match a file in the tablet-based repo.  delete these files
                deleteList.push(chaDirectoryNameRoot + chaCrc.Path);
              });
              chaDirCrcs.forEach(function(chaDirCrc) {
                // all remaining chaDirCrcs did not match a dir in the tablet-based repo.  delete these dirs (Not currently possible, but a nice-to-have)
                deleteDirectoryCrcList.push(chaDirectoryNameRoot + chaDirCrc.Path);
              });
              chaDirectories.forEach(function(chaDir) {
                // all remaining chaDirs did not match a dir in the tablet-based repo.  delete these dirs
                deleteDirectoryList.push(chaDirectoryName + chaDir.Path);
              });
            }
          })
          .then(function() {
            // recurse using the saved tabletDirectories
            var chain = $q.when();
            tabletDirectories.forEach(function(recurseDir) {
              chain = chain.then(function() {
                return recurse(recurseDir);
              });
            });
            return chain;
          });
      }

      return cha
        .abortExams()
        .then(function() {
          return recurse(repoPath);
        })
        .then(function() {
          logger.info("Cha Media Repo File Sync Ready for File Operations!");
          logger.info("Files to Delete from CHA:");
          logger.info(JSON.stringify(deleteList));
          logger.info("Directories to Delete from CHA:");
          logger.info(JSON.stringify(deleteDirectoryList));
          logger.info("DirectorieCrcs to Delete from CHA:");
          logger.info(JSON.stringify(deleteDirectoryCrcList));
          logger.info("Files to transfer to CHA:");
          logger.info(JSON.stringify(transferList));
          logger.info("Files equivalent on CHA:");
          logger.info(JSON.stringify(equalFileList));
          logger.info("Directories equivalent on CHA:");
          logger.info(JSON.stringify(equalDirList));
        })
        .then(function() {
          // Delete all unused files
          tasks.register("sync-CHA-media-repo", "Syncing CHA Media Repo.  Deleting unused files from CHA.");
          var deferred = $q.defer();

          // recursive (could use chains instead...)
          function deleteNext() {
            if (!api.syncActive) {
              logger.debug(
                "CHA media sync breaking recursion chain because sync has been cancelled, probably by the user."
              );
              deferred.reject("Sync Cancelled.");
              return;
            }
            if (deleteList.length > 0) {
              var fileToDelete = deleteList.shift(); // pull first - the cha returns crc's in order, so this may save a bunch of time finding each file to delete by crc
              cha.deleteFileCrc(fileToDelete).then(deleteNext);
            } else {
              deferred.resolve();
            }
          }

          deleteNext(); // start the recursion
          return deferred.promise;
        })
        .then(function() {
          // delete all unused directories - this has to be done recursively, since we cannot delete non-empty dirs
          tasks.register("sync-CHA-media-repo", "Syncing CHA Media Repo.  Deleting unused folders from CHA.");
          var chain = $q.when();
          deleteDirectoryList.forEach(function(dirToDelete) {
            chain = chain.then(function() {
              return cha.deleteDirectory(dirToDelete); // this function first recursively empties the dir
            });
          });
          return chain.then(function() {
            console.log("All directories in the delete list have been removed from the cha.");
          });
        })
        .then(function() {
          logger.info("Checking if file system is ready.");
          return file.ready();
        })
        .then(function() {
          api.syncActive = false;
          tasks.register("sync-CHA-media-repo", "Syncing CHA Media Repo.");
          if (isAdmin) {
            var data = api.selected.hash;
            var path = "/";
            return file.createFile(path, "PROTECTEDMEDIAVERSION.txt", data);
          } else {
            return $q.resolve();
          }
        })
        .then(function() {
          var deferred = $q.defer();
          if (isAdmin) {
            logger.info("Getting the protected media version file from tablet...");
            file.localFS.getFile(
              "/PROTECTEDMEDIAVERSION.txt",
              {
                create: false,
                exclusive: false
              },
              function(fileEntry) {
                deferred.resolve(fileEntry);
              },
              function(e) {
                logger.error(e);
                deferred.reject();
              }
            );
          } else {
            deferred.resolve();
          }
          return deferred.promise;
        })
        .then(function(fileEntry) {
          if (isAdmin && fileEntry) {
            var nativeURL = fileEntry.toURL();
            logger.info("Got a file entry, pushing to transfer list. NativeURL = " + nativeURL);
            var fileSize = null;
            transferList.push({
              targetURL: "USER/PROTECTEDMEDIAVERSION.txt",
              nativeURL: nativeURL,
              fileSize: fileSize
            });
          }
          return $q.resolve();
        })
        .then(function() {
          return cha.transferFiles(transferList);
        })
        .then(function() {
          logger.log("Done syncing media repo to CHA.");
          tasks.register("sync-CHA-media-repo", "Done syncing media repo to CHA.");
          return $q.resolve();
        })
        .then(function() {
          return delay(1500);
        })
        .catch(function(e) {
          logger.log("CHA media sync stopped with reason: " + JSON.stringify(e));
        })
        .finally(function() {
          notifications.alert("Done syncing headset media, press OK to continue.");
          tasks.deregister("sync-CHA-media-repo");
        });
    };

    // The below gitlab utilities are very similar to those used in gitlab.js
    // but the requirements were different enough that it was easier to duplicate
    // than shoehorn all functionality for both needs into one service...
    api.addRepo = function() {
      var host,
        group,
        token,
        name,
        version,
        type = "CHA media";
      if (api.isAdmin) {
        host = protectedRepo.host;
        group = protectedRepo.group;
        token = protectedRepo.token;
        name = api.model.name; // this could also be protectedRepo.name
        version = api.model.version;
      } else {
        host = disk.servers.gitlab.host;
        group = disk.servers.gitlab.group;
        token = disk.servers.gitlab.token;
        name = api.model.name;
        version = api.model.version;
      }

      gitlab
        .add(host, group, name, token, version, "CHA media")
        .then(function(repo) {
          // define and store on disk
          var web_url = repo.web_url;
          repo = gitlab.defineProtocol(repo); // overwrites
          if (api.isAdmin) {
            repo.admin = true;
            api.isAdmin = false;
          } else if (web_url === protectedRepo.host + protectedRepo.group + "/" + protectedRepo.name) {
            repo.admin = true;
          }
          if (repo.admin) {
            logger.debug(
              "Adding protected-media repo: repo.admin = true in order to sync to protected portion of WAHTS storage."
            );
          }

          var dums = _.filter(disk.cha.mediaRepos, {
            name: repo.name,
            path: repo.path,
            date: repo.date
          });

          // if the media does not already exist, then push it onto the stack
          if (dums.length === 0) {
            disk.cha.mediaRepos.push(repo);
          } else {
            logger.error("Media meta data already in cha mediaRepos");
            notifications.alert(
              "A matching version of the CHA media repo you just tried to add already exists in the stored CHA media repos!  See the Headset Media section to find it."
            );
          }

          // reset the temporary input
          api.model = {};
        })
        .catch(function(reason) {
          logger.error("Adding gitlab repo returns: " + JSON.stringify(reason));
          notifications.alert("There was a problem downloading the media from gitlab. Error: " + reason.msg);
        });
    };

    api.updateRepo = function() {
      var deferred = $q.defer();
      if (!api.selected) {
        return;
      }

      notifications.confirm("Update CHA media repo " + api.selected.name + "?", function(buttonIndex) {
        if (buttonIndex === 1) {
          tasks.register("updating", "Updating CHA Media");
          updateMedia().finally(function() {
            tasks.deregister("updating");
            deferred.resolve();
          });
        } else {
          deferred.resolve();
        }
      });

      function updateMedia() {
        return gitlab.pull(api.selected.repo).then(function(repo) {
          // update media on disk
          // Hack to keep track of the admin flag (for creare-managed media repo)
          var repoIsAdmin = api.selected.admin;
          var pidx = _.findIndex(disk.cha.mediaRepos, {
            path: paths.data(paths.gitlab(repo))
          });
          if (pidx !== -1) {
            var tmpRepo = gitlab.defineProtocol(repo);
            tmpRepo.admin = repoIsAdmin;
            disk.cha.mediaRepos[pidx] = tmpRepo; // overwrite
          } else {
            // can't update it - it doesn't exist
            logger.warn("Attempted to update CHA media repo " + repo.name + " but the repo does't exist");
          }
        });
      }

      return deferred.promise;
    };

    api.deleteRepo = function() {
      var deferred = $q.defer();
      if (!api.selected) {
        return;
      }

      notifications.confirm("Delete media repo " + api.selected.name + " and remove media files from disk?", function(
        buttonIndex
      ) {
        if (buttonIndex === 1) {
          deleteMediaRepo(api.selected);
          api.selected = undefined;
        }
      });

      function deleteMediaRepo(m) {
        var msg;
        if (!disk.cha.mediaRepos) {
          msg =
            "Attempted to delete CHA media repo " +
            m.name +
            " but TabSINT currently has no CHA media repos stored on disk.";
          logger.error(msg);
          deferred.reject({
            code: 2201,
            msg: msg
          });
        }

        var idx = _.findIndex(disk.cha.mediaRepos, m);
        if (idx === -1) {
          msg = "Trying to delete CHA media repo " + m.name + ", but it does not exist";
          logger.error(msg);
          deferred.reject({
            code: 2202,
            msg: msg
          });
        }

        if (app.tablet) {
          try {
            logger.debug("Removing media files for repo: " + m.name + " at path: " + m.path);
            var root = m.path
              .split("/")
              .slice(0, -2)
              .join("/");
            var repoDir = m.path.split("/").slice(-2, -1)[0];
            $cordovaFile
              .removeRecursively(root, repoDir)
              .then(function() {
                // remove metadata from disk
                disk.cha.mediaRepos.splice(idx, 1);
                deferred.resolve();
              })
              .catch(function(e) {
                msg =
                  "Failed to remove media files in directory " +
                  m.name +
                  " within root " +
                  m.path +
                  " for CHA media repo " +
                  m.name +
                  " with error: " +
                  angular.toJson(e);
                logger.error(msg);
                deferred.reject({
                  code: 2203,
                  msg: msg
                });
              });
          } catch (e) {
            msg =
              "Failed to remove media directory " +
              m.name +
              " from path " +
              m.path +
              " for reason: " +
              JSON.stringify(e);
            logger.debug(msg);
            deferred.reject({
              code: 2204,
              msg: msg
            });
          }
        }
      }

      return deferred.promise;
    };

    return api;
  });
