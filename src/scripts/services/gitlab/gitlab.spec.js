/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import "angular-mocks";
import "../../app";

beforeEach(angular.mock.module("tabsint"));

describe("Gitlab", function() {
  var gitlab, protocol, pm, logger, disk, config, version, $http, $httpBackend, $rootScope;

  beforeEach(
    angular.mock.inject(function(
      _gitlab_,
      _protocol_,
      _pm_,
      _logger_,
      _disk_,
      _config_,
      _version_,
      _$http_,
      _$httpBackend_,
      _$rootScope_
    ) {
      gitlab = _gitlab_;
      protocol = _protocol_;
      pm = _pm_;
      logger = _logger_;
      disk = _disk_;
      config = _config_;
      version = _version_;
      $http = _$http_;
      $httpBackend = _$httpBackend_;
      $rootScope = _$rootScope_;
      _$httpBackend_.whenGET("res/translations/translations.json").respond({});
    })
  );

  beforeEach(function() {
    config.gitlab = {
      host: "https://test.com/",
      group: "name",
      token: "token"
    };
    gitlab.$init();
    gitlab.initDefault();
  });

  describe("gitlab.$init", function() {
    it("should set disk variables based on config file", function() {
      expect(disk.servers.gitlab).toBeDefined();
      expect(disk.servers.gitlab.host).toEqual(config.gitlab.host);
      expect(disk.servers.gitlab.group).toEqual(config.gitlab.group);
      expect(disk.servers.gitlab.token).toEqual(config.gitlab.token);
    });

    it("should have no repositories on disk", function() {
      expect(disk.gitlab.repos.length).toEqual(0);
    });
  });

  describe("gitlab.initDefault", function() {
    beforeEach(function() {
      config.gitlab.group = "config-namespace";
    });

    it("reset disk values back to the config file values", function() {
      disk.servers.gitlab.group = "test-user-test";
      gitlab.initDefault();
      expect(disk.servers.gitlab.group).toEqual("config-namespace");
    });
  });

  describe("gitlab.get", function() {
    afterEach(function() {
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });

    it("should add token headers", function() {
      var setHeaders;
      var token = "token";
      var url = "host/" + gitlab.api + "api/point";
      $httpBackend
        .expectGET(url, {
          "PRIVATE-TOKEN": token,
          Accept: "application/json, text/plain, */*"
        })
        .respond(200);

      gitlab.get(url, token);
      $httpBackend.flush();
    });

    it("should reject a returned string object", function() {
      var fail = jasmine.createSpy();
      var token = "token";
      var url = "host/" + gitlab.api + "api/point";
      $httpBackend.expectGET(url).respond(200, "a string");

      gitlab.get(url, token).catch(fail);
      $httpBackend.flush();

      expect(fail).toHaveBeenCalled();
    });

    it("should return an object", function() {
      var token = "token";
      var url = "host/" + gitlab.api + "api/point";
      $httpBackend.expectGET(url).respond(200, '{"repository": "object"}');

      var data;
      gitlab.get(url, token).then(function(ret) {
        data = ret.data;
      });
      $httpBackend.flush();

      expect(data).toBeDefined();
      expect(data.repository).toEqual("object");
    });
  });

  describe("gitlab.post", function() {
    afterEach(function() {
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });

    it("should add token headers", function() {
      var headers;
      var token = "token";
      var url = "host/" + gitlab.api + "api/point";
      $httpBackend
        .expectPOST(
          url,
          {},
          {
            "PRIVATE-TOKEN": token,
            Accept: "application/json, text/plain, */*",
            "Content-Type": "application/json"
          }
        )
        .respond(200);

      gitlab.post(url, {}, token);
      $httpBackend.flush();
    });

    it("should reject a returned string object", function() {
      var fail = jasmine.createSpy();
      var token = "token";
      var url = "host/" + gitlab.api + "api/point";
      $httpBackend.expectPOST(url).respond(200, "a string");

      gitlab.post(url, {}, token).catch(fail);
      $httpBackend.flush();

      expect(fail).toHaveBeenCalled();
    });

    it("should return an object", function() {
      var token = "token";
      var url = "host/" + gitlab.api + "api/point";
      $httpBackend.expectPOST(url).respond(200, '{"repository": "object"}');

      var data;
      gitlab.post(url, {}, token).then(function(ret) {
        data = ret.data;
      });
      $httpBackend.flush();

      expect(data).toBeDefined();
      expect(data.repository).toEqual("object");
    });
  });

  describe("gitlab.add", function() {
    it("should GET the project id associated with the specified host, group, and repository", function() {
      var host = "host";
      var group = "group";
      var name = "protocol";
      var token = "token";

      // spyOn(gitlab, 'getCommit');

      var url = "https://" + host + "/" + gitlab.api + "projects/" + encodeURIComponent(group + "/" + name);
      $httpBackend.expectGET(url).respond(404);

      gitlab.add(host, group, name, token);
      $httpBackend.flush();

      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });
  });

  describe("gitlab.commit", function() {
    it("should POST a result package to the project id it received back", function() {
      var host = "host";
      var group = "group";
      var name = "resultsrepo";
      var token = "token";
      var data = {
        result: "object"
      };
      var toPath = "toPath";
      var msg = "message";

      // spyOn(gitlab, 'getCommit');

      var url = "https://" + host + "/" + gitlab.api + "projects/" + encodeURIComponent(group + "/" + name);
      $httpBackend.expectGET(url).respond(404);

      gitlab.commit(host, group, name, token, data, toPath, msg);
      $httpBackend.flush();

      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });

    it("should POST a result package to the project id it received back", function() {
      var host = "host";
      var group = "group";
      var name = "protocol";
      var token = "token";
      var data = {
        result: "object"
      };
      var toPath = "toPath";
      var msg = "message";

      spyOn(gitlab, "post");

      var url = "https://" + host + "/" + gitlab.api + "projects/" + encodeURIComponent(group + "/" + name);
      $httpBackend.expectGET(url).respond({ data: { id: 1234 } });

      // var posturl = 'https://' + host + '/' +gitlab.api+ 'projects/1234/repository/commits';
      // $httpBackend.expectPOST(posturl)
      //   .respond(200, '{"repository": "object"}');

      gitlab.commit(host, group, name, token, data, toPath, msg);

      // for GET
      $httpBackend.flush();

      // for POST
      // $rootScope.$digest();
      // $httpBackend.flush();

      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
      expect(gitlab.post).toHaveBeenCalled();
    });
  });
});
