/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

angular
  .module("tabsint.services.disk", [])

  .factory("disk", function($localStorage) {
    // persistent data store
    var disk = $localStorage.$default({
      // configuration saved from qrcode. used in config.load
      qrcodeConfig: {},

      // these disk values can be configured in config.json or QRCode
      // defaults defined in config service
      debugMode: undefined,
      disableLogs: undefined,
      pin: undefined,
      disableVolume: undefined,
      requireEncryptedResults: undefined,
      recordTestLocation: undefined,
      adminSkipMode: undefined,
      autoUpload: undefined,
      exportCSV: undefined,
      disableAudioStreaming: true,
      server: undefined, // one of 'tabsintServer', 'localServer', 'gitlab'
      tabletGain: undefined,

      // the rest of these parameters can't be configured from config service and need defaults here
      cha: {},
      externalMode: false,
      appDeveloperMode: false,
      appDeveloperModeCount: 0,
      uploadSummary: [],
      suppressAlerts: false,
      showUploadSummary: true,
      resultsMode: 1, // Upload Only
      preventUploads: false,
      preventExports: true,
      reloadingBrowser: false,
      downloadInProgress: false,
      lastReleaseCheck: undefined,
      validateProtocols: false,
      completedResults: [],
      currentResults: undefined,
      tabletLocation: {
        latitude: undefined,
        longitude: undefined,
        accuracy: undefined
      },
      gitlab: {
        repos: [],
        useTagsOnly: true,
        useSeperateResultsRepo: false
      },
      protocol: {}, // metadata of the currently active protocol
      protocols: [], // metadata of the protocols currently downloaded and available
      mediaRepos: [], // metadata of the media repos currently downloaded and available
      output: undefined, // one of 'tabsintServer', 'localServer', 'gitlab'
      servers: {
        tabsintServer: {
          url: undefined,
          username: undefined,
          password: undefined,
          site: undefined
        },
        localServer: {
          resultsDir: undefined // default defined in config service
        },
        gitlab: {
          repository: undefined,
          version: undefined,
          host: undefined,
          token: undefined,
          group: undefined,
          resultsGroup: undefined,
          resultsRepo: "results"
        }
      },
      // cha: {},  // TODO: Replaces L85 in TabSINT 3.4
      headset: undefined,
      language: undefined,
      interApp: {
        appName: undefined,
        dataIn: undefined,
        dataOut: undefined
      },
      init: true //when set to true, this is the initial opening of the app and disclaimer should be displayed
    });

    // COMPAT
    // disk.servers.gitlab.namespace -> disk.servers.gitlab.group
    if (disk.servers.gitlab.namespace) {
      disk.servers.gitlab.group = disk.servers.gitlab.namespace;
      delete disk.servers.gitlab.namespace;
    }

    // disk.plugins.cha -> disk.cha
    if (disk.plugins && disk.plugins.cha) {
      disk.cha = disk.plugins.cha;
      delete disk.plugins.cha;
    }

    return disk;
  });
