/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";
import "angular-mocks";
import * as tv4 from "tv4";

import "../../app";

beforeEach(angular.mock.module("tabsint"));

describe("Protocol", function() {
  var adminLogic, schema, protocol, pm, notifications, logger, json, disk, config, version, plugins, $scope;

  beforeEach(
    angular.mock.inject(function(
      _$rootScope_,
      _$httpBackend_,
      _adminLogic_,
      _protocol_,
      _pm_,
      _notifications_,
      _logger_,
      _json_,
      _disk_,
      _config_,
      _version_,
      _plugins_
    ) {
      adminLogic = _adminLogic_;
      protocol = _protocol_;
      pm = _pm_;
      notifications = _notifications_;
      logger = _logger_;
      json = _json_;
      disk = _disk_;
      config = _config_;
      version = _version_;
      plugins = _plugins_;
      $scope = _$rootScope_;
      _$httpBackend_.whenGET("res/translations/translations.json").respond({});
    })
  );

  describe("protocol.$init", function() {
    beforeEach(function() {
      protocol.$init();
    });

    it("should load local protocols", function() {
      expect(pm.local.length).toEqual(6);
    });

    it("should store meta data for local protocols and any plugin protocols onto the disk", function() {
      expect(disk.protocols.length).toEqual(pm.local.length + plugins.elems.localProtocols.length);
    });

    it("should set the disk.server protocol source variable based on the configuration file", function() {
      expect(disk.server).toEqual(config.server.type === "tabsintServer" ? "tabsintServer" : "localServer");
    });
  });

  describe("protocol.validate", function() {
    beforeEach(function() {
      protocol.$init();
    });

    it("built in protocols (including plugin protocols) should pass protocol schema validation.", function() {
      _.forEach(disk.protocols, function(meta) {
        var p = json.load(meta.path + "/protocol.json");
        var validationResult = protocol.validate(p);
        //Added a more valuable error message. Note that this syntax is not documented or supported,
        //and may break in the future, but including this message makes debugging MUCH easier.
        expect(validationResult.valid).toBeTruthy("Protocol validation failed for : " + meta.path);
      });
    });
  });

  describe("protocol.load", function() {
    beforeEach(function() {
      protocol.$init();
      config.load();
      version.load();
    });

    it("should notice when the minimum tabsint version is greater than the current version", function(done) {
      protocol.load({ name: "mock", path: "base/www/res/protocol/mock" }).then(function() {
        expect(pm.root.protocolTabsintOutdated).toBeTruthy();
        done();
      });
      $scope.$apply();
    });

    it("should be OK when the minimum tabsint version is lower than the current version", function(done) {
      protocol.load({ name: "cha-test", path: "base/www/res/protocol/cha-test" }).then(function() {
        expect(pm.root.protocolTabsintOutdated).toBeFalsy();
        done();
      });
      $scope.$apply();
    });

    it("should alert when a calibration file exists without version fields.", function(done) {
      spyOn(notifications, "alert");
      protocol
        .load({ name: "mock", path: "base/www/res/protocol/mock" }) // doesn't have version fields
        .then(function() {
          expect(notifications.alert).toHaveBeenCalled();
          done();
        });
      $scope.$apply();
    });

    it("should not alert when a calibration file exists with version fields.", function(done) {
      spyOn(notifications, "alert");
      protocol
        .load({ name: "cha-test", path: "base/www/res/protocol/cha-test" }) // does have version fields
        .then(function() {
          expect(notifications.alert).not.toHaveBeenCalled();
          done();
        });
      $scope.$apply();
    });

    it("should alert when a protocol's minTabsintVersion is higher than the TabSINT version.", function(done) {
      spyOn(notifications, "alert");
      protocol
        .load({ name: "mock", path: "base/www/res/protocol/mock" }) // doesn't have version fields
        .then(function() {
          expect(notifications.alert).toHaveBeenCalled();
          done();
        });
      $scope.$apply();
    });

    it("should not alert when a protocol's minTabsintVersion is lower than the TabSINT version.", function(done) {
      spyOn(notifications, "alert");
      protocol
        .load({ name: "cha-test", path: "base/www/res/protocol/cha-test" }) // does have version fields
        .then(function() {
          expect(notifications.alert).not.toHaveBeenCalled();
          done();
        });
      $scope.$apply();
    });
  });
});
