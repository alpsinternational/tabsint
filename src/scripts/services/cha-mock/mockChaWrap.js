/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

/* jshint ignore:start */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";

angular
  .module("tabsint.services.cha.mockChaWrap", [])

  .service("mockChaWrap", function($timeout, app) {
    if (!app.tablet) {
      return buildMockCha();
    }
    function buildMockCha() {
      var ChaWrap = {
        startChaSearch: undefined,
        cancelChaSearch: undefined
      };

      var mockCha = {
        // According to Bill's API 12-17-2014
        getName: undefined,
        connect: undefined,
        disconnect: undefined,
        setListener: undefined,
        removeListener: undefined,
        requestId: undefined,
        requestProbeId: undefined,
        requestCalibrationEntry: undefined,
        requestStatus: undefined,
        queueExam: undefined,
        requestResults: undefined,
        setSoftwareButtonState: undefined,
        abortExams: undefined,
        noiseFeatureStart: undefined,
        noiseFeatureStop: undefined
      };

      // mock objects
      var mock = {};

      mock.id = [
        "Id",
        {
          serialNumber: 8675309,
          description: "SN#123457 2020-07-31 12:00:00",
          buildDateTime: "xx/xx/xx",
          cpuID: "cpuId",
          board: "BOARD"
        }
      ];

      mock.probeId = [
        "ProbeId",
        {
          description: "probeId",
          serialNumber: 123456
        }
      ];

      mock.a2dp = [
        "AssociatedA2dpDiscovered",
        {
          description: "description",
          A2DP_CHA: {}
        }
      ];

      var readyStatus = [
        "Status",
        {
          State: 1,
          flags: undefined,
          lastCtrlError: 0,
          mcl: 20,
          vUsb: 4.15,
          vBattery: 3.9,
          samplesPlayed: 0
        }
      ];

      var examStatus = [
        "Status",
        {
          State: 2,
          flags: undefined,
          lastCtrlError: 0,
          mcl: 20,
          vUsb: 4.15,
          vBattery: 3.9,
          samplesPlayed: 0
        }
      ];

      mock.status = readyStatus;

      mockCha.json = {
        name: "MOCK CHA Name"
      };

      // Function Definitions

      mockCha.getName = function() {
        return "FAKE CHA";
      };

      mockCha.connect = function(connectSuccess, errorCallback) {
        console.log("mockCha.connect");
        //          $timeout(function(){
        //            console.log('mockCha.connect success');
        connectSuccess();
        //          },500);
      };

      mockCha.disconnect = function(disconnectSuccess, errorCallback) {
        disconnectSuccess();
      };

      mockCha.setListener = function(eventListener, okCallback, errorCallback) {
        mockCha.listener = eventListener; // set event listener
        okCallback();
      };

      mockCha.removeListener = function(okCallback, errorCallback) {
        mockCha.listener = undefined;
        okCallback();
      };

      mockCha.requestId = function(okCallback, errorCallback) {
        okCallback();
        mockCha.listener(mock.id);
      };

      mockCha.requestProbeId = function(okCallback, errorCallback) {
        okCallback();
        mockCha.listener(mock.probeId);
      };

      mockCha.requestAssociatedA2DP = function(okCallback, errorCallback) {
        okCallback();
        mockCha.listener(mock.a2dp);
      };

      mockCha.noiseFeatureStart = function(params, okCallback, errorCallback) {
        okCallback();
      };

      mockCha.noiseFeatureChangeLevel = function(params, okCallback, errorCallback) {
        okCallback();
      };

      mockCha.noiseFeatureStop = function(okCallback, errorCallback) {
        okCallback();
      };

      mockCha.requestStatus = function(okCallback, errorCallback) {
        okCallback();
        mockCha.listener(mock.status);
      };

      mockCha.queueExam = function(type, args, okCallback, errorCallback) {
        initResults(args);
        if (_.includes(["BekesyFrequency", "BekesyLike", "BekesyMLD"], type)) {
          okCallback();
          mock.status = examStatus;
          setTimeout(function() {
            results.queue.push(results[type]);
            mock.status = readyStatus;
          }, 4000);
        } else if (_.includes(["HughsonWestlake", "HughsonWestlakeFrequency"], type)) {
          okCallback();
          mock.status = examStatus;
          setTimeout(function() {
            results.queue.push(results[type]);
            mock.status = readyStatus;
          }, 4000);
        } else if (type === "ThreeDigit") {
          okCallback();
          if (args.nPresentations) {
            _.times(args.nPresentations, function() {
              results.queue.push(results.threeDigit);
            });
          } else {
            _.times(50, function() {
              results.queue.push(results.threeDigit);
            });
          }
        } else if (type === "ThirdOctaveBands") {
          okCallback();
          mock.status = examStatus;
          setTimeout(function() {
            results.queue.push(results[type]);
            mock.status = readyStatus;
          }, 4000);
        } else if (type === "ToneGeneration") {
          okCallback();
          mock.status = examStatus;
          setTimeout(function() {
            mock.status = readyStatus;
          }, args.ToneDuration);
        } else if (type === "DPOAE") {
          okCallback();
          mock.status = examStatus;
          setTimeout(function() {
            results.queue.push(results[type]);
            mock.status = readyStatus;
          }, 4000);
        } else if (type === "HINT") {
          okCallback();
          mock.status = examStatus;
          //            setTimeout(function () {
          results.queue.push(results[type]);
          mock.status = readyStatus;
          //            }, 100);
        } else if (type === "MLD") {
          okCallback();
          mock.status = examStatus;
          setTimeout(function() {
            results.queue.push(["Result", { State: "PLAYING" }]);
            mock.status = readyStatus;
          }, 2000);
          setTimeout(function() {
            results.queue.push(["Result", { State: "WAITING_FOR_RESULT" }]);
            mock.status = readyStatus;
          }, 4000);
        } else if (type === "TAT") {
          //step through blocks in a single presentation
          okCallback();
          mock.status = examStatus;
          setTimeout(function() {
            results.queue.push([
              "Result",
              {
                State: "1",
                CurrentPresentation: "1",
                CurrentBlock: "0",
                ToneIndex: "2"
              }
            ]); //PLAYING
            mock.status = readyStatus;
          }, 2000);
          setTimeout(function() {
            results.queue.push([
              "Result",
              {
                State: "1",
                CurrentPresentation: "1",
                CurrentBlock: "1",
                ToneIndex: "2"
              }
            ]); //PLAYING
            mock.status = readyStatus;
          }, 4000);
          setTimeout(function() {
            results.queue.push([
              "Result",
              {
                State: "1",
                CurrentPresentation: "1",
                CurrentBlock: "2",
                ToneIndex: "2"
              }
            ]); //PLAYING
            mock.status = readyStatus;
          }, 6000);
          setTimeout(function() {
            results.queue.push([
              "Result",
              {
                State: "1",
                CurrentPresentation: "1",
                CurrentBlock: "3",
                ToneIndex: "2"
              }
            ]); //PLAYING
            mock.status = readyStatus;
          }, 8000);
          setTimeout(function() {
            results.queue.push(["Result", { State: "4", CurrentPresentation: "1", ToneIndex: "2" }]); //WAITING_FOR_SUBMISSION
            mock.status = readyStatus;
          }, 10000);
          setTimeout(function() {
            results.queue.push(["Result", { State: "3", CurrentPresentation: "1", ToneIndex: "2" }]); //DONE
            mock.status = readyStatus;
          }, 15000);
        } else {
          errorCallback();
        }
      };

      mockCha.requestResults = function(okCallback) {
        okCallback();
        if (results.queue.length > 0) {
          mockCha.listener(results.queue.splice(0, 1)[0]);
        }
      };

      mockCha.setSoftwareButtonState = function(btnState, okCallback) {
        if (okCallback) {
          okCallback();
        }
      };

      mockCha.abortExams = function(okCallback) {
        okCallback();
      };

      mockCha.examSubmission = function(type, args, okCallback) {
        okCallback();
      };

      mockCha.requestCalibrationEntry = function(type, args, okCallback) {
        okCallback({});
      };

      // Mock Results
      var results = {};
      initResults();
      function initResults(args) {
        results.queue = [];
        results.BekesyLike = [
          "Result",
          {
            ResultType: 0,
            Threshold: 20, // user threshold (dB HL)
            Units: 1,
            RetSPL: 100, // equivalent threshold sound pressure (db SPL)
            L: [8, 6, 7, 5, 3, 0, 9], // presented levels (db HL)
            MaximumExcursion: 10 // difference between consecutive user responses (dB)
          }
        ];
        results.BekesyMLD = [
          "Result",
          {
            ResultType: 0,
            Threshold: 20, // user threshold (dB HL)
            Units: 1,
            RetSPL: 100, // equivalent threshold sound pressure (db SPL)
            L: [8, 6, 7, 5, 3, 0, 9], // presented levels (db HL)
            MaximumExcursion: 10 // difference between consecutive user responses (dB)
          }
        ];
        results.BekesyFrequency = [
          "Result",
          {
            Threshold: 1500, // user threshold (Hz)
            F: [1500, 1200, 1600, 1400, 500, 2000, 125], // presented frequencies
            MaximumExcursion: 10 // difference between consecutive user responses (dB)
          }
        ];
        results.threeDigit = [
          "Result",
          {
            currentPresentation: "filename",
            currentDigits: 124,
            State: 0,
            digitScore: 80,
            presentationScore: 10
          }
        ];
        var resultType;
        if (args && args.Screener) {
          var nPresentations = Math.floor(10 * Math.random());
          var numCorrectResp = Math.floor(3 * Math.random());
          var lArray = [],
            fpArray = [];
          for (var j = 0; j < nPresentations; j++) {
            lArray.push(args.Lstart || 40);
            fpArray.push(0);
          }
          if (numCorrectResp === 2) {
            resultType = 0;
          } else {
            resultType = 2;
          }
          results.HughsonWestlake = [
            "Result",
            {
              //ResultType: newResultType, // for randomly failing tests
              //            ResultType: resultType,
              NumCorrectResp: numCorrectResp,
              ResultType: 0,
              Units: 0,
              L: lArray, // presented levels (db HL)
              FalsePositive: fpArray
            }
          ];
        } else {
          resultType = Math.floor(10 * Math.random());
          if (resultType <= 5) {
            resultType = 0;
          } else {
            resultType = 1;
          }
          var threshList = [40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90];
          var threshInd = Math.floor(threshList.length * Math.random());
          var thresh = [threshList[threshInd], NaN, NaN];
          //var freq = [500, 1000, 2000, 3000, 4000, 6000, 8000, 9000, 10000, 11200, 12500, 14000, 16000, 18000, 20000];
          //var thresh =[20, 25,  30,   35,   40,   50,   60,   70,    80,    'NaN',   95,   'NaN',   'NaN',   'NaN',  'NaN'];
          //var resultType = [0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 2, 2, 2, 2];
          //var newThresh = undefined;
          //var newResultType = undefined;
          //if (args && args.F && freq.indexOf(args.F) > -1) {
          //  newThresh = thresh[freq.indexOf(args.F)];
          //  newResultType = resultType[freq.indexOf(args.F)];
          //}
          results.HughsonWestlake = [
            "Result",
            {
              //ResultType: newResultType, // for randomly failing tests
              //            ResultType: resultType,
              ResultType: 0,
              Units: 0,
              //              Threshold: thresh[resultType],  // for randomly failing tests
              Threshold: 65, // user threshold (dB HL)
              //Threshold: newThresh, // for a series of thresholds
              L: [85, 75, 65, 75, 70, 65, 70], // presented levels (db HL)
              MaximumExcursion: 10 // difference between consecutive user responses (dB)
            }
          ];
        }
        results.HughsonWestlakeFrequency = [
          "Result",
          {
            Threshold: 750, // user threshold (Hz)
            Units: 2,
            F: [2000, 1500, 1000, 500, 250, 125, 250, 500, 1000, 750, 500, 750], // presented levels (db HL)
            MaximumExcursion: 10 // difference between consecutive user responses (dB)
          }
        ];
        var fTmp = [],
          lTmp = [];
        for (var i = -9; i < 11; i++) {
          fTmp[i + 9] = 1000 * Math.pow(Math.pow(2, 1 / 3), i);
          lTmp[i + 9] = 5 + 25 * Math.random();
        }
        results.ThirdOctaveBands = [
          "Result",
          {
            ResultType: 0,
            Frequencies: fTmp, // third octave frequency bands (dB HL)
            Leq: lTmp // measureded levels (db HL)
          }
        ];
        results.DPOAE = [
          "Result",
          {
            ResultType: 0,
            DpLow: {
              Frequency: args ? (2 * args.F2) / 1.21 - args.F2 : 250 + 750 * Math.random(),
              Amplitude: 10 + 10 * Math.random(),
              Phase: 32,
              NoiseFloor: 0 + 10 * Math.random()
            }
          }
        ];
        results.HINT = [
          "Result",
          {
            State: 0,
            CurrentSentenceIndex: 1,
            CurrentSentence: "Sniper southwest twenty hundred meters, above the bakery"
          }
        ];
        results.MLD = [
          "Result",
          {
            State: 0,
            CurrentSentenceIndex: 1,
            CurrentSentence: "Sniper southwest twenty hundred meters, above the bakery"
          }
        ];
        results.TAT = [
          "Result",
          {
            Score: 100,
            presentation: 1,
            UserResponse: 2,
            CorrectResponse: 2,
            correct: true
          }
        ];
      }

      // ChaWrap functions
      ChaWrap.startChaSearch = function(interfaceString, chaFoundCallback, errorAndCompletionCallback) {
        if (window.__karma__) {
          chaFoundCallback(mockCha); // async testing hard with karma
        } else {
          setTimeout(function() {
            chaFoundCallback(mockCha);
          }, 1000);
        }
      };

      ChaWrap.cancelChaSearch = function(okCallback, errorCallback) {
        okCallback();
      };

      ChaWrap.getBluetoothAdapterState = function(okCallback, errorCallback) {
        okCallback("Bluetooth On");
      };

      window.ChaWrap = ChaWrap;

      // return the mockCha object for testing
      return mockCha;
    }
  });

/* jshint ignore:end */
