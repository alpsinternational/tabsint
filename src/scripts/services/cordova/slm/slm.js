/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

/*global SLM */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";

angular
  .module("tabsint.services.cordova.slm", [])

  .factory("slm", function($timeout, tabsintNative, logger, notifications, disk, $q) {
    var slm = {};

    /**
     * Recording API point.
     * Will be set true when the SLM is recording
     * @type {Boolean}
     */
    slm.recording = false;

    /**
     * Init method for each page
     * @param  {object} page - page object from the protocol page
     */
    slm.init = function(page) {
      if (!installed()) {
        logger.error("Attempting to start slm on page, but SLM cordova plugin is not available");
        notifications.alert(
          "This page is requesting the sound level meter, but the SLM plugin is not included in this build of TabSINT. Please hand the device back to an Administrator."
        );
        return;
      }

      if (page.slm.microphone && page.slm.microphone !== "internal") {
        if (page.slm.microphone === "dayton") {
          SLM.initDaytonAudioMic(
            function() {
              logger.debug("Successfully init the dayton mic");
              record();
            },
            function(e) {
              logger.error(`Failed to init dayton mic with error: ${JSON.stringify(e)}`);
              notifications.alert(
                "This page is requesting the sound level meter using the Dayton microphone, but TabSINT failed to initialize the microphone. Please hand the device back to an Administrator."
              );
            }
          );
        } else if (page.slm.microphone === "studio6") {
          SLM.initUSBprecisionMicStudioSixDigital(
            function() {
              logger.debug("Successfully init the studio6 mic");
              record();
            },
            function(e) {
              logger.error(`Failed to init studio6 mic with error: ${JSON.stringify(e)}`);
              notifications.alert(
                "This page is requesting the sound level meter using the Studio 6 microphone, but TabSINT failed to initialize the microphone. Please hand the device back to an Administrator."
              );
            }
          );
        }
      } else {
        SLM.initInternalMic(
          function() {
            logger.debug("Successfully init the internal mic");
            record();
          },
          function(e) {
            logger.error("Failed to init internal mic");
            notifications.alert(
              "This page is requesting the sound level meter using the internal microphone, but TabSINT failed to initialize the microphone. Please hand the device back to an Administrator."
            );
          }
        );
      }
    };

    /**
     * Stop recording
     * @return {promise}
     */
    slm.stop = function() {
      if (!installed()) {
        return $q.reject();
      }

      var q = $q.defer();

      SLM.stop(
        function() {
          logger.info("Successfully stopped recording with SLM");
          slm.recording = false;
          q.resolve();
        },
        function() {
          logger.error("Failed to stop recording with SLM");
          q.reject({
            msg:
              "TabSINT encountered an issue while trying to stop the sound level meter recording. Please hand the device to an Administrator"
          });
        }
      );

      return q.promise;
    };

    /**
     * Return parameters from sound level meter for a given sound
     * @param  {object} page - page object defining slm parameters
     * @return {promise}     - promise that resolves with the result object
     */
    slm.getSLM = function(page) {
      if (!installed()) {
        return $q.reject({ msg: "not installed" });
      }

      var defaultParams = ["recordingStartTime", "recordingDuration", "numberOfReports", "timePoints", "SPL_A_mean"];
      var minParams = ["SPL_A_slow", "meanSpectrum", "bandCenterFrequencies"]; // these parameters must be included to calculate band level and overallAmbientNoise

      page.slm.ignoreResults = page.slm.ignoreResults || []; // Set default (empty array per page schema).

      // assign requested parameters based on inputs or defaults.  Take the union of the resulting params with minParams to make sure we get the right fields
      var params = page.slm.parameters || defaultParams;
      // Put the default params get back onto the page object if being used.
      page.slm.parameters = params;
      // Add a superset of params to get from SLM.
      var paramsQuery = _.union(params, minParams);

      return slm
        .stop()
        .catch(function(e) {
          // Sensimetrics seems to have a bug with slm.stop, which propagates here.
          // In the meantime, we don't want to throw an alert here, log instead.
          logger.error("slm.getSLM: When getting slm data: ", e.msg);
        })
        .then(function() {
          return getParams(paramsQuery);
        })
        .then(function(result) {
          var firstDigit, levels;

          // convert bandCenterFrequencies message into Frequencies
          firstDigit = result.bandCenterFrequencies.match(/\d/);
          result.Frequencies = result.bandCenterFrequencies
            .substring(firstDigit.index)
            .split(/(\s+)/)
            .map(function(item) {
              return parseFloat(item);
            })
            .filter(function(item) {
              return !Number.isNaN(parseFloat(item));
            });

          // calculate Leq
          firstDigit = result.meanSpectrum.match(/\d/);
          var temp = result.meanSpectrum.substring(firstDigit.index).split(";");
          var timeSlices = [];
          temp.forEach(function(item) {
            timeSlices.push(
              item
                .split(/(\s+)/)
                .map(function(item) {
                  return parseFloat(item);
                })
                .filter(function(item) {
                  return !Number.isNaN(parseFloat(item));
                })
            );
          });

          var Leq = [];
          timeSlices.forEach(function(levels) {
            // sum up each level at each frequency band at each timeSlice
            levels.forEach(function(level, idx) {
              Leq[idx] = (Leq[idx] || 0) + Math.pow(level, 2);
            });
          });

          result.Leq = Leq.map(function(L) {
            // take the average of each Leq
            return Math.pow(L / timeSlices.length, 0.5);
          });

          // calculate LeqA from SPL_A_slow
          firstDigit = result.SPL_A_slow.match(/\d/);
          var sum = 0;
          levels = result.SPL_A_slow.substring(firstDigit.index)
            .split(/(\s+)/)
            .map(function(item) {
              return parseFloat(item);
            })
            .filter(function(item) {
              return !Number.isNaN(parseFloat(item));
            });

          levels.forEach(function(element) {
            sum = sum + Math.pow(element, 2);
          });
          result.LeqA = Math.pow(sum / levels.length, 0.5);

          // return the result object - this will pass through the "finally" block below
          return result;
        })
        .catch(function(e) {
          if (e && e.msg) {
            notifications.alert(e.msg);
          }
        })
        .finally(function() {
          // results will pass through close
          slm.close();
        });
    };

    /**
     * Calculate the band level.
     * SPL_A_slow and meanSpectrum comes from SLM as a long mixed string from an object value,
     * so we need to parse them first
     */
    slm.calculateBandLevel = function(slmResult, F) {
      // Values for noise calculations
      var thirdOctaveBands = slmResult.Frequencies;

      // Find 1/3 octave band indexes and noise levels
      var idx = thirdOctaveBands.indexOf(F);

      if (idx > 0 && idx < slmResult.Leq.length - 1) {
        var Lcenter = slmResult.Leq[idx];
        var Llower = slmResult.Leq[idx - 1];
        var Lupper = slmResult.Leq[idx + 1];

        // Now we can do the band level calculation
        // @val - what happens at the outer bounds? idx === 0 and idx === L.length?
        return 10 * Math.log10(Math.pow(10, Llower / 10) + Math.pow(10, Lcenter / 10) + Math.pow(10, Lupper / 10));
      } else {
        return undefined;
      }
    };

    /**
     * Close an open slm connection
     */
    slm.close = function() {
      if (!installed()) {
        return;
      }

      SLM.close(
        function() {
          logger.info("Successfully closed SLM connection");
          slm.recording = false;
        },
        function(e) {
          logger.error("Failed to close SLM connection properly");
        }
      );
    };

    /**
     * Convience function to check if SLM is installed
     * @return {boolean}
     */
    function installed() {
      return typeof SLM !== "undefined";
    }

    /**
     * Start recording with the previously init SLM
     */
    function record() {
      SLM.record(
        function() {
          logger.info("Successfully started recording with SLM");
          slm.recording = true;
        },
        function() {
          logger.error("Failed to start recording with SLM");
          slm.recording = false;
        }
      );
    }

    /**
     * Get parameters from SLM
     * @param  {object} params - parameters to get
     * @return {promise} - promise that resolves with object containing all the parameters
     */
    function getParams(params) {
      var promises = [];
      var result = {};

      _.forEach(params, function(param) {
        if (typeof SLM[param] === "function") {
          var sq = $q.defer();
          SLM[param](
            function(values) {
              result[param] = values;
              sq.resolve();
            },
            function(e) {
              logger.error(`Failed to get slm parameter ${param} with error ${JSON.stringify(e)}`);
              result[param] = null;
              sq.resolve();
            }
          );

          promises.push(sq.promise);
        }
      });

      // wait for all the promises to resolve, then return the result
      return $q.all(promises).then(function() {
        return result;
      });
    }

    return slm;
  });
