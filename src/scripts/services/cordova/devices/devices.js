/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

angular.module("tabsint.services.cordova.devices", []).factory("devices", function(logger, $cordovaFile) {
  /*
       Loads in window.device for access to device properties like uuid, model

       If window.device is not available, default to 'Browser'
       */
  var devices = {
    name: "Browser",
    cordova: "Browser",
    platform: "Browser",
    UUID: "Browser",
    shortUUID: "Browser",
    version: "Browser",
    model: "Browser",
    diskSpace: "Broswer",
    load: undefined
  };

  devices.load = function() {
    if (window.device) {
      devices.name = window.device.name;
      devices.cordova = window.device.cordova;
      devices.platform = window.device.platform.toLowerCase();
      devices.UUID = window.device.uuid;
      devices.shortUUID = devices.UUID.substring(devices.UUID.length - 5);
      devices.version = window.device.version;
      devices.model = window.device.model;
      devices.getDiskSpace();

      logger.param.uuid = devices.UUID;
      logger.param.os = devices.version;
      logger.param.model = devices.model;
      logger.param.platform = devices.platform;

      logger.debug('"Device" defined. uuid = ' + devices.UUID + ", model = " + devices.model);
    } else {
      console.log('WARNING: "Device" undefined. "Browser" will be used for all values.');
    }
  };

  devices.getDiskSpace = function() {
    // get disk space
    $cordovaFile.getFreeDiskSpace().then(function(diskSpace) {
      devices.diskSpace = parseInt(parseInt(diskSpace) / 1000); // Integer of MB
    });
  };

  return devices;
});
