/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

/* global CordovaSphinx, FileError */

"use strict";

import * as angular from "angular";

angular
  .module("tabsint.services.cordova.network.model", [])

  .factory("networkModel", function(logger, $window) {
    var nm = {
      setOffline: undefined,
      setOnline: undefined,
      status: false,
      type: undefined
    };

    /**
     * Manually set the network connection to offline.
     * This happens when we fail to reach a server, or we only have access to a 2G connection.
     */
    nm.setOffline = function() {
      var connectionType;
      if (angular.isDefined($window.navigator) && angular.isDefined($window.navigator.connection)) {
        connectionType = navigator.connection.type;
      }

      if (nm.status) {
        logger.info("Tablet set to offline with connection type: " + connectionType);
      }

      nm.status = false;
      nm.type = undefined;
    };

    nm.setOnline = function() {
      var connectionType;
      if (angular.isDefined($window.navigator) && angular.isDefined($window.navigator.connection)) {
        connectionType = navigator.connection.type;
      }

      if (!nm.status) {
        logger.info("Tablet set to online with connection type: " + connectionType);
      }

      nm.status = true;
      nm.type = connectionType;
    };

    return nm;
  });
