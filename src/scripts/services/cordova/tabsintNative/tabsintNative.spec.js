/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import "angular-mocks";
import "../../../app";

beforeEach(angular.mock.module("tabsint"));

// beforeEach(module('tabsintNative'));

describe("tabsintNative wrapper tests", function() {
  var tabsintNative;

  beforeEach(
    angular.mock.inject(function($injector, _tabsintNative_) {
      tabsintNative = _tabsintNative_;
    })
  );

  it("should define 'onVolumeError'", function() {
    expect(tabsintNative.onVolumeError).toBeDefined();
  });

  it("should provide a 'onVolumeError' method", function() {
    expect(tabsintNative.onVolumeError).toEqual(jasmine.any(Function));
  });
});
