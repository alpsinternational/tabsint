/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

/*global cordova */

"use strict";

import * as angular from "angular";

angular
  .module("tabsint.services.cordova.permissions", [])

  .factory("permissions", function(app, logger, $q, config, notifications) {
    // api
    var permissions = {};

    /**
     * Request an array of permissions
     *
     * Note this always resolves so that the load process always proceeds.
     * @param  {array} permissions
     * @return {promise}
     */
    permissions.request = function(permissions) {
      var q = $q.defer();

      if (app.tablet) {
        // make sure permissions is a list that has entries
        if (typeof permissions === "string") {
          permissions = [permissions];
        } else if (typeof permissions === "object" && !permissions.length) {
          // note we need the console.log because in the app load process the logger has not loaded yet.
          console.log("-- requested permissions are not a list: " + JSON.stringify(permissions));
          logger.error("requested permissions are not a list: " + JSON.stringify(permissions));
          return $q.resolve();
        }

        cordova.plugins.permissions.requestPermissions(
          permissions,
          function(result) {
            if (!result.hasPermission) {
              q.resolve();
            } else {
              console.log("-- successfully requested permissions"); // happens before logger is prepared
              logger.info("successfully requested permissions");
              q.resolve();
            }
          },
          function(e) {
            // swallow JSON error on older devices
            if (e === "JSON error") {
              q.resolve();
            }
            // note we need the console.log because in the app load process the logger has not loaded yet.
            console.log("-- failed to request permissions because of error: " + JSON.stringify(e));
            logger.error("failed to request permissions because of error: " + JSON.stringify(e));

            q.resolve();
          }
        );
      } else {
        console.log("-- successfully requested permission in browser");
        q.resolve();
      }

      return q.promise;
    };

    /**
     * Request permissions from the tabsint build config file
     * @return {promise}
     */
    permissions.requestConfig = function() {
      if (app.tablet && config.permissions.length > 0) {
        var permissionKeys = Object.keys(cordova.plugins.permissions);

        // make sure all the permissions written in the file do exist in the cordova.plugins.permissions object
        var cperm = config.permissions.filter(function(p, idx) {
          if (permissionKeys.indexOf(p) === -1) {
            logger.error("config permission value does not exist in android-permissions plugin");
          }

          return permissionKeys.indexOf(p) > -1;
        });

        cperm = cperm.map(function(p) {
          return cordova.plugins.permissions[p];
        });
        return permissions.request(cperm);
      } else {
        return $q.resolve();
      }
    };

    /**
     * Request all default permissions
     * @return {promise}
     */
    permissions.requestAll = function() {
      let requiredPermissions = [];

      if (app.tablet) {
        requiredPermissions = [cordova.plugins.permissions.CAMERA, cordova.plugins.permissions.WRITE_EXTERNAL_STORAGE];
      }

      return permissions.request(requiredPermissions);
    };

    /**
     * Request location specific permissions
     * @return {promise}
     */
    permissions.requestLocationPermission = function() {
      let requiredPermissions = [];

      if (app.tablet) {
        requiredPermissions = [
          cordova.plugins.permissions.ACCESS_COARSE_LOCATION,
          cordova.plugins.permissions.ACCESS_FINE_LOCATION
        ];
      }

      return permissions.request(requiredPermissions);
    };

    /**
     * Request camera specific permissions
     * @return {promise}
     */
    permissions.requestCameraPermission = function() {
      let requiredPermissions = [];

      if (app.tablet) {
        requiredPermissions = [cordova.plugins.permissions.CAMERA];
      }

      return permissions.request(requiredPermissions);
    };

    return permissions;
  });
