/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

angular.module("tabsint.services.gain", []).factory("gain", function(devices, disk) {
  /*
       Loads in devices for access to devices.model, which determines the gain to apply to wav files
       */
  var gain = {
    reset: undefined
  };

  // Note: these gain values are relative to that of the Nexus7.
  var tabletGains = {
    Browser: -1,
    Nexus7: 0,
    SamsungTabE: -8.56,
    EPHD1: 0,
    SamsungTabA: -9.5596
  };

  /**
   * Load the gain factor on app load
   * Only utilized the first time tabsint is loaded
   */
  gain.load = function() {
    if (!disk.tabletGain) {
      gain.reset();
    }
  };

  /**
   * Reset the custom gain parameter for the current tablet
   */
  gain.reset = function() {
    disk.tabletGain = undefined;

    if (disk.headset === "EPHD1") {
      disk.tabletGain = tabletGains.EPHD1;
    } else if (Object.keys(tabletGains).indexOf(devices.model) > -1) {
      disk.tabletGain = tabletGains[devices.model];
    } else if (devices.model === "SAMSUNG-SM-T377A") {
      disk.tabletGain = tabletGains.SamsungTabE;
    } else if (devices.model === "SM-T380") {
      disk.tabletGain = tabletGains.SamsungTabA;
    } else {
      disk.tabletGain = tabletGains.Nexus7;
    }
  };

  return gain;
});
