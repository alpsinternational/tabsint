/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import "angular-mocks";
import * as tv4 from "tv4";

import "../../app";

beforeEach(angular.mock.module("tabsint"));

describe("Cha", function() {
  // Skip over modals required by CHA
  beforeEach(
    angular.mock.module(function($provide) {
      $provide.value("authorizeAdmin", {
        modalAuthorize: function(targetFn) {
          targetFn();
        } // assume authorized.
      });
    })
  );
  beforeEach(
    angular.mock.module(function($provide) {
      $provide.value("chooseCha", {
        discover: function() {}
      });
    })
  );

  describe("Cha Setup", function() {
    var cha, config, mockCha, json;
    beforeEach(
      angular.mock.inject(function(_cha_, _config_, _mockChaWrap_, _json_, _$httpBackend_) {
        cha = _cha_;
        config = _config_;
        mockCha = _mockChaWrap_;
        json = _json_;
        _$httpBackend_.whenGET("res/translations/translations.json").respond(200, "a string");
      })
    );

    it("should have connect functions and start disconnected", function() {
      expect(cha.connect).toBeDefined();
      expect(cha.state).toBe("disconnected");
    });
  });

  describe("Functionality", function() {
    var cha, chaExams, config, mockCha, disk, protocol, $rootScope, timeout, json;

    beforeEach(
      angular.mock.inject(function(
        _cha_,
        _chaExams_,
        _disk_,
        _config_,
        _mockChaWrap_,
        _protocol_,
        _$rootScope_,
        _json_,
        $injector,
        _$httpBackend_
      ) {
        cha = _cha_;
        chaExams = _chaExams_;
        disk = _disk_;
        config = _config_;
        mockCha = _mockChaWrap_;
        protocol = _protocol_;
        $rootScope = _$rootScope_;
        json = _json_;
        timeout = $injector.get("$timeout");
        _$httpBackend_.whenGET("res/translations/translations.json").respond(200, "a string");
      })
    );

    function flushActivateTimeout() {
      timeout.flush(5001);
    }

    beforeEach(function() {
      $rootScope.$digest();
    });

    beforeEach(function() {
      disk.cha.myCha = "FAKE CHA";
      cha.connect();
      $rootScope.$digest();
    });

    it("should be in a connected state", function() {
      expect(cha.state).toBe("connected");
    });

    it("should have set the info object of the cha", function() {
      expect(cha.myCha.id).toBeDefined();
      expect(cha.myCha.id.serialNumber).toEqual(8675309);
    });

    it("should have set the info object of the cha", function() {
      expect(cha.myCha.id).toBeDefined();
      expect(cha.myCha.id.serialNumber).toEqual(8675309);
    });

    it("should parse calibration date if set in description", function() {
      spyOn(mockCha, "requestId").and.callFake(function() {
        mockCha.listener([
          "Id",
          {
            serialNumber: 8675309,
            description: "SN#1234567 2020-07-31",
            buildDateTime: "xx/xx/xx",
            cpuID: "cpuId",
            board: "BOARD"
          }
        ]);
      });
      mockCha.requestId();
      expect(cha.myCha.id.chaCalibrationDate).toEqual("2020-07-31");
    });

    it("should parse calibration datetime if set in description", function() {
      spyOn(mockCha, "requestId").and.callFake(function() {
        mockCha.listener([
          "Id",
          {
            serialNumber: 8675309,
            description: "SN#1234567 2020-07-31 12:00:00",
            buildDateTime: "xx/xx/xx",
            cpuID: "cpuId",
            board: "BOARD"
          }
        ]);
      });
      mockCha.requestId();
      expect(cha.myCha.id.chaCalibrationDate).toEqual("2020-07-31");
    });

    it("should leave calibration date undefined if not in description", function() {
      spyOn(mockCha, "requestId").and.callFake(function() {
        mockCha.listener([
          "Id",
          {
            serialNumber: 8675309,
            description: "SN#1234567",
            buildDateTime: "xx/xx/xx",
            cpuID: "cpuId",
            board: "BOARD"
          }
        ]);
      });
      mockCha.requestId();
      expect(cha.myCha.id.chaCalibrationDate).toBeUndefined();
    });

    it("should be able to disconnect", function() {
      cha.disconnect();
      $rootScope.$digest();
      expect(cha.state).toBe("disconnected");
    });

    describe("Event Listener", function() {
      beforeEach(
        angular.mock.inject(function(_$httpBackend_) {
          _$httpBackend_.whenGET("res/translations/translations.json").respond(200, "a string");
        })
      );

      it("should listen for status events", function() {
        var statusPromise = cha.requestStatus();
        $rootScope.$digest();
        expect(statusPromise.$$state.value.State).toEqual(1); // nested within the resolved promise
      });

      it("should listen for error events", function() {
        mockCha.listener(["Error", { code: 1, msg: "Test Error Message" }]);
      });

      //it('should listen for result events - though no will be ', function () {
      //  var resultPromise = cha.requestResults();
      //  $rootScope.$digest();
      //  expect(resultPromise.$$state.status).toEqual(0);   // nested within the resolved promise
      //});
    });

    describe("Cha Response Areas ", function() {
      var examLogic, scope;

      beforeEach(
        angular.mock.inject(function(_examLogic_, $controller, $rootScope, _$httpBackend_) {
          examLogic = _examLogic_;
          scope = $rootScope.$new(); // start a new scope

          $rootScope.$digest();
          protocol.override("res/protocol/cha-unit-test").then(examLogic.reset);
          $rootScope.$digest();

          examLogic.begin();
          flushActivateTimeout();
          scope.$digest();
          scope.dm = examLogic.dm;
          _$httpBackend_.whenGET("res/translations/translations.json").respond(200, "a string");

          // TODO - Note that this controller can normally only be loaded via exam view if the current page is a cha responseArea
          // the controller grabs the page type, slices of the first three letters (chaHughsonWestlake => HughsonWestlake)
          // then attempts to use that type for preparation and exam queueing.
          // If the test protocol first page isn't a cha response area, all of this will break in a cryptic way.
          $controller("ChaResponseAreaCtrl", {
            $scope: scope
          });
        })
      );

      it("should appropriately queue Exam", function() {
        //examLogic.testing.goToId('ToneGeneration1');
        scope.$digest();
        scope.startExam(chaExams.examType, chaExams.examProperties);
        scope.$digest();
        expect(scope.chaExams.state).toBe("exam");
      });

      it("should have reset to a clean state", function() {
        scope.$digest();
        expect(scope.chaError).toBeFalsy();
        expect(scope.chaExams.state).toBe("start");
      });

      it("should see errors from below", function() {
        var err = { code: 1, msg: "Test Error Message" };
        mockCha.listener(["Error", err]);
        scope.$digest();
        expect(scope.errorMessage).toBe("Error: " + angular.toJson(err.msg));
      });
    });
  });
});
