/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

angular
  .module("tabsint.services.remote", [])

  .factory("remote", function($http, $window, $q, $timeout, app, logger, networkModel) {
    var remote = {};

    /**
     * Default properties for remote calls
     * @property {number} timeout - default timeout for $http calls
     * @instance
     */
    remote.defaults = {
      timeout: 10000
    };

    /**
     * Method to open a local link in a new window
     * @param  {string} url - url to open
     */
    remote.link = function(url) {
      logger.info("Opening link: " + url);
      var ref = $window.open(url, "_system");
    };

    /**
     * Method to ping the github site to see if we are online. Only gets called when we're on a tablet
     * @return {promise} - promise to ping the github server
     */
    remote.ping = function() {
      var deferred = $q.defer();
      var pingTimeout;
      if (app.tablet) {
        $http
          .get("https://logs.tabsint.org/ping", {})
          .then(function() {
            $timeout.cancel(pingTimeout);
            networkModel.setOnline();
            deferred.resolve();
          })
          .catch(function(errMsg) {
            $timeout.cancel(pingTimeout);
            networkModel.setOffline();
            deferred.reject(errMsg);
          });

        // If no response in 15 seconds, fail and reject
        pingTimeout = $timeout(function() {
          var msg =
            'TabSINT is trying to check the internet connection, but the ping timed out.  Setting connection to "offline"';
          networkModel.setOffline(); // At least now the user will know, by seeing the indicator in the toolbar change.
          logger.warn("" + msg);
          deferred.reject({ code: 17, msg: msg });
        }, 15000);
      } else {
        deferred.resolve();
      }

      return deferred.promise;
    };

    /**
     * Light wrapper over the $http.get method
     * @param {string} url - url to get
     * @param {object} conf - configuration object to send along with the http request.
     * @param {object} conf.headers - (required) Authentication header to send with request
     * @param {object} [conf.timeout=remote.defaults.timeout] - timeout for the request
     * @instance
     * @returns {Promise} Returns a promise from the http call
     */
    remote.get = function(url, conf) {
      if (!networkModel.status) {
        logger.debug("No network connection found while trying to get remote");
        return $q.reject();
      }

      conf = remote.formatConfig(conf);
      // try to ping first.  should take very little time and confirm connection prior to actual get
      return remote
        .ping()
        .then(function() {
          return $http.get(url, conf);
        })
        .catch(function(ret) {
          return remote.error(ret, url);
        });
    };

    /**
     * Light wrapper over the $http.post method
     * @param {string} url - url to get
     * @param {object} data - json object to post to url
     * @param {object} conf - configuration object to send along with the http request
     * @param {object} conf.headers - (required) Authentication header to send with request
     * @param {object} [conf.timeout=remote.defaults.timeout] - timeout for the request
     * @returns {Promise} Returns a promise from the http call
     */
    remote.post = function(url, data, conf) {
      if (!networkModel.status) {
        logger.debug("No network connection found while trying to post remote");
        return $q.reject();
      }

      conf = remote.formatConfig(conf);
      // try to ping first.  should take very little time and confirm connection prior to actual post
      return remote
        .ping()
        .then(function() {
          return $http.post(url, data, conf);
        })
        .catch(function(ret) {
          return remote.error(ret, url);
        });
    };

    /**
     * Light wrapper over the $http.head method
     * @param {string} url - url to get
     * @returns {Promise} Returns a promise from the http call
     */
    remote.getHead = function(url, conf) {
      if (!networkModel.status) {
        logger.debug("No network connection found while trying to get head");
        return $q.reject();
      }

      conf = remote.formatConfig(conf);
      return remote
        .ping()
        .then(function() {
          return $http.head(url, conf);
        })
        .catch(function(ret) {
          return remote.error(ret, url);
        });
    };

    /**
     * Error handler for remote methods
     * @param {object} err - error object being returned
     * @param {string} url - the url trying to get called
     * @returns {Promise} This method returns a promise resolution or rejection
     */
    remote.error = function(err, url) {
      var rej = {
        status: err.status || null,
        data: err.data || null,
        url: url
      };

      if (rej.status === -1) {
        rej.msg =
          "The tablet is having trouble connecting to the TabSINT Server.  Please check the TabSINT Server Configuration.";
        // networkModel.setOffline();
      } else if (err.code && err.code === 17) {
        rej.msg = err.msg; // ping failed prior to attempting to get or post
      } else {
        rej.msg = "Failure while trying to access remote server";
      }

      logger.error("Remote error: " + angular.toJson(rej));
      return $q.reject(rej);
    };

    /**
     * HTTP Config sanitization and validation
     * @param {object} config - config object to validate. See the top level api points ({@link remote.get}, {@link remote.post} for more information)
     * @returns {object} This method returns a promise rejection with an error object if the config is invalid, or it returns the validated configuration object
     */
    remote.formatConfig = function(c) {
      // reject if no headers are present
      if (!c) {
        return {};
      }

      var conf = {
        headers: c.headers,
        timeout: c.timeout || remote.defaults.timeout
      };

      return conf;
    };

    return remote;
  });
