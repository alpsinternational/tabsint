/**
 * Created by bpf on 7/14/2016.
 */

"use strict";

import * as angular from "angular";

angular
  .module("tabsint.services.semver", [])

  .factory("semver", function() {
    var api = {
      compare: undefined
    };

    /**
     * Compare semver - this is non-trivial, as gitlab returns a list of tags in alphabetical order, not semantic order!
     * @param {strings} v1 - a semantic version string (i.e. '1' or '3.5.3')
     * @param {strings} v2 - a semantic version string (i.e. '1' or '3.5.3')
     * @returns {integer} - 1 if v1 > v2, 0 if v1=v2, -1 if v1 > v2
     */
    api.compare = function(v1, v2) {
      var result = false,
        p1 = normalizeSemVer(v1),
        p2 = normalizeSemVer(v2);

      if (
        p1[0] > p2[0] ||
        (p1[0] === p2[0] && p1[1] > p2[1]) ||
        (p1[0] === p2[0] && p1[1] === p2[1] && p1[2] > p2[2])
      ) {
        result = 1;
      } else if (p1[0] === p2[0] && p1[1] === p2[1] && p1[2] === p2[2]) {
        result = 0;
      } else {
        result = -1;
      }

      return result;

      function normalizeSemVer(v) {
        if (v.length < 1) {
          return "0.0.0";
        }
        var p = v.toString().split(".");
        if (p.length < 2) {
          p[1] = "0";
        }
        if (p.length < 3) {
          p[2] = "0";
        }
        return [Number(p[0]), Number(p[1]), Number(p[2])];
      }
    };

    return api;
  });
