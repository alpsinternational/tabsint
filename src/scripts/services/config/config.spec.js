/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import "angular-mocks";
import * as tv4 from "tv4";

import "../../app";

beforeEach(angular.mock.module("tabsint"));

describe("Config", function() {
  describe("Inputs", function() {
    var configSchema, config, json, paths;
    beforeEach(
      angular.mock.inject(function(_json_, _paths_) {
        json = _json_;
        paths = _paths_;

        // Load config schema (sync).
        configSchema = json.load(paths.www("config_schema.json"));
        config = json.load(paths.www("tabsintConfig.json"));
      })
    );

    it("should pass Config JSON-SCHEMA validation.", function() {
      var valid = tv4.validate(config, configSchema);
      if (!valid) {
        console.log(angular.toJson(tv4.error, true));
      }
      expect(valid).toBeTruthy();
    });
  });

  describe("Processing", function() {
    var config;
    beforeEach(
      angular.mock.inject(function(_config_) {
        config = _config_;
      })
    );

    it("should have values defined for plugins, even if they are defaults", function() {
      config.load();
      expect(config.tabsintPlugins).toBeDefined();
    });
  });
});
