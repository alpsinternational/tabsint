/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";
import * as qrcode from "qrcode";

angular
  .module("tabsint.services.config", [])
  .factory("config", function($cordovaFile, disk, file, gain, gettextCatalog, json, logger, notifications, paths, cha) {
    var config = {};

    const DEFAULTS = {
      load: undefined,
      reset: undefined,
      generateQRCode: undefined,
      qRCodeUrl: undefined,
      build: "",
      description: undefined,
      platform: undefined,
      permissions: [],
      releases: undefined,
      cordovaPlugins: {},
      tabsintPlugins: {},
      adminSettings: {
        defaultHeadset: "None",
        language: "English",
        adminMode: false,
        adminPin: 7114,
        disableLogs: true,
        maxLogRows: 1000,
        disableAutomaticVolumeControl: false,
        enableSkipExam: false,
        automaticallyOutputTestResults: false,
        resultsMode: "Upload Only",
        exportResultsCSV: false,
        localResultsDirectory: "tabsint-results",
        tabletGain: -8.56,
        headsetType: "Bluetooth 3.0",
        disableAudioStreaming: true,
        ignoreFirmwareUpdates: false,
        enableHeadsetMediaManagement: false,
        requireEncryptedResults: false,
        recordTestLocation: false
      },
      server: "localServer",
      gitlab: {
        host: "https://gitlab.com/",
        group: "",
        repository: undefined,
        version: undefined,
        onlyTrackTags: true,
        resultsGroup: undefined,
        resultsRepo: "results",
        token: ""
      },
      tabsintServer: {
        site: undefined,
        url: "https://hffd.crearecomputing.com/",
        username: undefined,
        password: undefined
      },
      overrideAutomaticGain: false
    };

    /**
     * Reset config api data model to defaults
     */
    config.reset = function() {
      // overwrite api keys with DEFAULTS
      _.merge(config, DEFAULTS);
    };

    /**
     * Load tabsintConfig.json on start-up or QrCode on scanning code and load it into the config object
     */
    config.load = function(configObject, resetDisk) {
      try {
        var objectToLoad = {};

        // if input is defined, it is a QRCode, save it to disk
        // Merge QRCode with tabsintConfig.json, letting QRCode take precendence.
        if (configObject) {
          disk.qrcodeConfig = JSON.parse(configObject);
        }
        _.merge(objectToLoad, json.load("tabsintConfig.json"), disk.qrcodeConfig);

        // reset data model
        config.reset();

        // merge object to load with api - DO NOT ALLOW FUNCTIONS TO GET OVERWRITTEN
        _.mergeWith(config, objectToLoad, function(configValue, objValue, key) {
          if (key === "ERROR") {
            logger.error("Error processing config object: " + angular.toJson(objValue));
          }

          // do not allow api methods to be overwritten
          if (typeof configValue === "function") {
            return configValue;
          }
        });

        // COMPAT
        // make sure "config.gitlab.namespace" ends up as "config.gitlab.group"
        if (config.gitlab.namespace) {
          config.gitlab.group = config.gitlab.namespace;
          delete config.gitlab.namespace;
        }
      } catch (err) {
        notifications.alert(
          gettextCatalog.getString("An error occurred while loading the TabSINT configuration: ") +
            err +
            ". " +
            gettextCatalog.getString("Please try again.")
        );
      }

      // set disk parameters based on config, if they are not already defined
      // if first load, then reset disk and overwrite
      if (disk.init) {
        config.resetDisk(true);
      } else {
        config.resetDisk(resetDisk);
      }

      // run updates
      config.changeHeadset(config.adminSettings.defaultHeadset);
      config.changeLanguage(config.adminSettings.language);
      config.changeResultsMode(config.adminSettings.resultsMode);
      cha.changeBluetoothType(config.adminSettings.headsetType);

      // put the build name in the logger parameters
      logger.param.build = config.build;
      logger.info("Config object processed: " + angular.toJson(config));
    };

    /**
     * Reset the disk-related configuration parameters based on config
     * @param  {boolean} overwrite - overwrite disk parameters with config values. Defaults to false
     */
    config.resetDisk = function(overwrite) {
      _writeDisk("debugMode", config.adminSettings.adminMode, overwrite);
      _writeDisk("pin", config.adminSettings.adminPin, overwrite);
      _writeDisk("disableLogs", config.adminSettings.disableLogs, overwrite);
      _writeDisk("maxLogRows", config.adminSettings.maxLogRows, overwrite);
      _writeDisk("disableVolume", config.adminSettings.disableAutomaticVolumeControl, overwrite);
      _writeDisk("adminSkipMode", config.adminSettings.enableSkipExam, overwrite);
      _writeDisk("autoUpload", config.adminSettings.automaticallyOutputTestResults, overwrite);
      _writeDisk("exportCSV", config.adminSettings.exportResultsCSV, overwrite);
      _writeDisk("disableAudioStreaming", config.adminSettings.disableAudioStreaming, overwrite);
      _writeDisk("server", config.server, overwrite);
      _writeDisk("requireEncryptedResults", config.adminSettings.requireEncryptedResults, overwrite);
      _writeDisk("recordTestLocation", config.adminSettings.recordTestLocation, overwrite);

      if (config.overrideAutomaticGain) {
        _writeDisk("tabletGain", config.adminSettings.tabletGain, overwrite);
      }

      // for nested disk objects we need to do this manually (without _writeDisk function)
      if (!disk.cha.enableHeadsetMedia || overwrite) {
        disk.cha.enableHeadsetMedia = _.cloneDeep(config.adminSettings.enableHeadsetMediaManagement);
      }
      if (!disk.servers.localServer.resultsDir || overwrite) {
        disk.servers.localServer.resultsDir = _.cloneDeep(config.adminSettings.localResultsDirectory);
      }
      if (!disk.cha.ignoreFirmwareUpdates || overwrite) {
        disk.cha.ignoreFirmwareUpdates = _.cloneDeep(config.adminSettings.ignoreFirmwareUpdates);
      }
    };

    // assign a default to disk if its not already defined
    function _writeDisk(key, defaultValue, overwrite) {
      if (disk[key] === undefined || overwrite) {
        disk[key] = _.cloneDeep(defaultValue);
      }
    }

    /**
     * Language selection method
     * @param  {string} language - language selected from the dropdown
     */
    config.changeLanguage = function(language) {
      disk.language = language;
      gettextCatalog.setCurrentLanguage(language);
      logger.info('Changed language to "' + disk.language + '" and saved it to disk');
    };

    /**
     * Headset selection method
     * @param headset
     */
    config.changeHeadset = function(headset) {
      if (headset === "None") {
        disk.headset = undefined;
      } else {
        disk.headset = headset;
      }
      if (!config.overrideAutomaticGain) {
        gain.reset();
      }
      logger.info('Changed headset to "' + disk.headset + '" and saved it to disk');
    };

    /**
     * Change results mode method
     * @param resultsMode
     */
    config.changeResultsMode = function(resultsMode) {
      if (resultsMode === 0 || resultsMode === "0" || resultsMode === "Upload/Export") {
        disk.resultsMode = 0;
        disk.preventUploads = false;
        disk.preventExports = false;
      } else if (resultsMode === 1 || resultsMode === "1" || resultsMode === "Upload Only") {
        disk.resultsMode = 1;
        disk.preventUploads = false;
        disk.preventExports = true;
      } else if (resultsMode === 2 || resultsMode === "2" || resultsMode === "Export Only") {
        disk.resultsMode = 2;
        disk.preventUploads = true;
        disk.preventExports = false;
      }
    };

    // Generate a QR code containing all the information about the current configuration settings.
    config.generateQRCode = function() {
      var mapResultsMode = { 0: "Upload/Export", 1: "Upload Only", 2: "Export Only" };
      // Write the json object containing all the information about the current configuration settings.
      // Omit any field that is the TabSINT default.
      var text = "{";
      text = text + _writeText("build", DEFAULTS.build, config.build);
      text = text + _writeText("description", DEFAULTS.description, config.description);
      text = text + '"adminSettings": {';
      text = text + _writeText("defaultHeadset", DEFAULTS.adminSettings.defaultHeadset, disk.headset);
      text = text + _writeText("language", DEFAULTS.adminSettings.language, disk.language);
      text = text + _writeText("adminMode", DEFAULTS.adminSettings.adminMode, disk.debugMode);
      text = text + _writeText("adminPin", DEFAULTS.adminSettings.adminPin, disk.pin);
      text = text + _writeText("disableLogs", DEFAULTS.adminSettings.disableLogs, disk.disableLogs);
      text = text + _writeText("maxLogRows", DEFAULTS.adminSettings.maxLogRows, disk.maxLogRows);
      text =
        text +
        _writeText(
          "disableAutomaticVolumeControl",
          DEFAULTS.adminSettings.disableAutomaticVolumeControl,
          disk.disableVolume
        );
      text = text + _writeText("enableSkipExam", DEFAULTS.adminSettings.enableSkipExam, disk.adminSkipMode);
      text =
        text +
        _writeText(
          "automaticallyOutputTestResults",
          DEFAULTS.adminSettings.automaticallyOutputTestResults,
          disk.autoUpload
        );
      text = text + _writeText("resultsMode", DEFAULTS.adminSettings.resultsMode, mapResultsMode[disk.resultsMode]);
      text = text + _writeText("exportResultsCSV", DEFAULTS.adminSettings.exportResultsCSV, disk.exportCSV);
      text =
        text +
        _writeText(
          "localResultsDirectory",
          DEFAULTS.adminSettings.localResultsDirectory,
          disk.servers.localServer.resultsDir
        );
      text = text + _writeText("tabletGain", DEFAULTS.adminSettings.tabletGain, disk.tabletGain);
      text = text + _writeText("headsetType", DEFAULTS.adminSettings.headsetType, disk.cha.bluetoothType);
      text =
        text +
        _writeText("disableAudioStreaming", DEFAULTS.adminSettings.disableAudioStreaming, disk.disableAudioStreaming);
      text =
        text +
        _writeText(
          "ignoreFirmwareUpdates",
          DEFAULTS.adminSettings.ignoreFirmwareUpdates,
          disk.cha.ignoreFirmwareUpdates
        );
      text =
        text +
        _writeText(
          "enableHeadsetMediaManagement",
          DEFAULTS.adminSettings.enableHeadsetMediaManagement,
          disk.cha.enableHeadsetMedia
        );
      text =
        text +
        _writeText(
          "requireEncryptedResults",
          DEFAULTS.adminSettings.requireEncryptedResults,
          disk.requireEncryptedResults
        );
      text =
        text + _writeText("recordTestLocation", DEFAULTS.adminSettings.recordTestLocation, disk.recordTestLocation);
      text = text.slice(0, -1); // remove trailing coma
      text = text + "},";
      text = text + _writeText("server", DEFAULTS.server, disk.server);
      text = text + '"gitlab": { ';
      text = text + _writeText("host", DEFAULTS.gitlab.host, disk.servers.gitlab.host);
      text = text + _writeText("group", DEFAULTS.gitlab.group, disk.servers.gitlab.group);
      text = text + _writeText("repository", DEFAULTS.gitlab.repository, disk.servers.gitlab.repository);
      text = text + _writeText("version", DEFAULTS.gitlab.version, disk.servers.gitlab.version);
      text = text + _writeText("onlyTrackTags", DEFAULTS.gitlab.onlyTrackTags, disk.gitlab.useTagsOnly);
      text = text + _writeText("resultsGroup", DEFAULTS.gitlab.resultsGroup, disk.servers.gitlab.resultsGroup);
      text = text + _writeText("resultsRepo", DEFAULTS.gitlab.resultsRepo, disk.servers.gitlab.resultsRepo);
      text = text + _writeText("token", DEFAULTS.gitlab.token, disk.servers.gitlab.token);
      text = text.slice(0, -1); // remove trailing coma
      text = text + "},";
      text = text + '"tabsintServer": { ';
      text = text + _writeText("site", DEFAULTS.tabsintServer.site, disk.servers.tabsintServer.site);
      text = text + _writeText("url", DEFAULTS.tabsintServer.url, disk.servers.tabsintServer.url);
      text = text + _writeText("username", DEFAULTS.tabsintServer.username, disk.servers.tabsintServer.username);
      text = text + _writeText("password", DEFAULTS.tabsintServer.password, disk.servers.tabsintServer.password);
      text = text.slice(0, -1); // remove trailing coma
      text = text + "}";
      text = text + "}";
      logger.debug(text);
      qrcode.toDataURL(
        text,
        {
          correctLevel: 0
        },
        function(err, url) {
          config.qRCodeUrl = url;
          // Split the base64 string in data and contentType
          let block = url.split(";");
          // Get the content type
          let dataType = block[0].split(":")[1]; // In this case "image/png"
          // get the real base64 content of the file
          let realData = block[1].split(",")[1]; // In this case the QR code
          // Convert the base64 string in a Blob
          let dataBlob = b64toBlob(realData, dataType);

          // Save QR Code to disk
          let dir = "tabsint-configuration";
          let filename = "qrcode.png";
          file
            .createDirRecursively(paths.public(""), dir)
            .then($cordovaFile.writeFile(paths.public(dir), filename, dataBlob, dataType))
            .then(function() {
              notifications.alert(
                gettextCatalog.getString(
                  "Current TabSINT configuration saved as QR Code on your tablet: internal storage/tabsint-configuration/qrcode.png"
                )
              );
            })
            .catch(function(e) {
              notifications.alert(gettextCatalog.getString("Saving QR Code failed with error: " + e));
              logger.error("Saving QR Code failed with error: " + e);
            });
        }
      );
    };

    function _writeText(key, defaultValue, currentValue) {
      if (defaultValue === currentValue || currentValue === undefined) {
        return "";
      } else {
        if (currentValue === true || currentValue === false || key === "tabletGain") {
          return '"' + key + '": ' + currentValue + ",";
        } else {
          return '"' + key + '": "' + currentValue + '",';
        }
      }
    }

    /**
     * Convert a base64 string in a Blob according to the data and contentType.
     *
     * @param b64Data {String} Pure base64 string without contentType
     * @param contentType {String} the content type of the file i.e (image/jpeg - image/png - text/plain)
     * @param sliceSize {Int} SliceSize to process the byteCharacters
     * @see http://stackoverflow.com/questions/16245767/creating-a-blob-from-a-base64-string-in-javascript
     * @return Blob
     */
    function b64toBlob(b64Data, contentType, sliceSize) {
      contentType = contentType || "";
      sliceSize = sliceSize || 512;

      var byteCharacters = atob(b64Data);
      var byteArrays = [];

      for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
          byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
      }

      var blob = new Blob(byteArrays, { type: contentType });
      return blob;
    }

    return config;
  });
