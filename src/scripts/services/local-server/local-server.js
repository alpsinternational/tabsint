/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

angular
  .module("tabsint.services.local-server", [])

  .factory("localServer", function(
    $q,
    logger,
    tasks,
    file,
    fileChooser,
    notifications,
    disk,
    protocol,
    gettextCatalog
  ) {
    var api = {};

    /**
     * Add a protocol from the sd card
     * @param {string} pType - 'media' or 'protocol' to signify what type of protocol is getting loaded
     */
    api.addProtocol = function(pType) {
      pType = pType || "protocol";

      return fileChooser
        .choose("directory")
        .then(function(theFile) {
          var date;
          theFile.getMetadata(function(md) {
            var d = md.modificationTime.toISOString();
            d = d.slice(0, d.indexOf("."));
            date = d.replace(/T/, " ");
          });

          var meta = protocol.define({
            name: theFile.name,
            path: theFile.nativeURL,
            date: date,
            server: "localServer"
          });

          if (pType === "protocol") {
            protocol.store(meta);
          } else if (pType === "media") {
            protocol.storeMedia(meta);
          }
          return meta;
        })
        .catch(function(err) {
          var msg, type;
          if (err.msg === "ChooseFile modal cancelled") {
            logger.debug(pType + " load failed with error: " + angular.toJson(err));
            msg = gettextCatalog.getString("User cancelled local ") + pType + gettextCatalog.getString(" file chooser");
          } else if (err.msg === "ChooseFile modal did not choose a proper file") {
            logger.warn(pType + " load failed with error: " + angular.toJson(err));
            msg = gettextCatalog.getString("The item selected was not a proper file");
            notifications.alert(msg);
          } else {
            logger.error(pType + " load failed with error: " + angular.toJson(err));
            msg = gettextCatalog.getString(
              "Load failed for an unknown reason. Please verify the protocol syntax and location. For further troubleshooting help, please see the documentation at https://tabsint.org"
            );
            notifications.alert(msg);
          }

          return $q.reject(msg);
        });
    };

    api.changeResultsDirectory = function() {
      fileChooser.choose("directory").then(function(directory) {
        var tmp = directory.fullPath;
        if (tmp[0].indexOf("/") >= 0) {
          tmp = tmp.slice(1);
        }

        if (tmp[tmp.length - 1].indexOf("/") >= 0) {
          tmp = tmp.slice(0, -1);
        }
        disk.servers.localServer.resultsDir = tmp;
      });
    };

    if (window.tabsint) {
      window.tabsint.localServer = api;
    }

    return api;
  });
