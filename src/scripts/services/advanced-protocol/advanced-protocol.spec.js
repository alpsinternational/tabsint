/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import "angular-mocks";
import * as tv4 from "tv4";

import "../../app";

/* global tabsint */

beforeEach(angular.mock.module("tabsint"));

describe("Advanced Protocol Service", function() {
  var advancedProtocol, protocol, examLogic, config, $rootScope;
  beforeEach(
    angular.mock.inject(function($injector, _advancedProtocol_, _config_, _examLogic_, _protocol_, _$rootScope_) {
      advancedProtocol = _advancedProtocol_;
      config = _config_;
      examLogic = _examLogic_;
      protocol = _protocol_;
      $rootScope = _$rootScope_;
    })
  );

  it("should have an empty registry to start ", function() {
    expect(advancedProtocol.registry).toEqual({});
  });

  it("should define tabsint.register function", function() {
    expect(window.tabsint.register).toBeDefined();
  });

  it("should register functions", function() {
    tabsint.register("test", function() {});
    expect(advancedProtocol.registry.test).toBeDefined();
  });

  it("should not allow functions to be registered twice", function() {});

  describe("advancedProtocol.loadJs", function() {
    beforeEach(function(done) {
      config.load();
      protocol
        .override("res/protocol/mock", false)
        .then(examLogic.reset)
        .then(function() {
          $rootScope.$apply();
        });
    }, 1000);
  });
});
