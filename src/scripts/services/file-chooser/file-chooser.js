/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

angular
  .module("tabsint.services.file-chooser", [])

  .factory("fileChooser", function(app, notifications, $q, $uibModal, file, logger, gettextCatalog) {
    var api = {
      choose: undefined
    };

    api.choose = function(fileOrDirectory) {
      var deferred = $q.defer();

      if (!app.tablet) {
        notifications.alert(gettextCatalog.getString("File chooser disabled while debugging in the browser"));
        deferred.reject({ msg: "Cannot choose file in debug browser" });
      }

      fileOrDirectory = fileOrDirectory || "file";

      var FileChooserCtrl = function($scope, $uibModalInstance) {
        function getContents(path) {
          //console.log('-- fileChooser.getContents(path) with path: '+angular.toJson(path));
          file.getEntries(path).then(function(result) {
            $scope.files = result;
            if (path !== file.localFS.nativeURL) {
              $scope.files.unshift({ name: "[parent]" });
              file.getParentDirectory(path).then(function(result) {
                result.name = "[parent]";
                $scope.files[0] = result;
                //console.log('-- fileChooser new file list: ' + angular.toJson($scope.files));
              });
            } else {
              //console.log('-- fileChoser new file list: ' + angular.toJson($scope.files));
            }
          });
        }

        var currentDirectory = file.localFS.nativeURL;
        $scope.fileOrDirectory = fileOrDirectory;
        $scope.selectedFile = undefined;
        $scope.files = [];

        getContents(currentDirectory);

        //console.log('starting file list: '+angular.toJson($scope.files));

        $scope.createDirectory = function() {
          var newDirectoryName = window.prompt("New Directory Name");
          console.log("-- creating new directory" + newDirectoryName + ", at currentDirectory: " + currentDirectory);
          if (angular.isDefined(newDirectoryName) && newDirectoryName !== "") {
            file.createDirectory(currentDirectory, newDirectoryName).then(getContents(currentDirectory));
          }
        };

        $scope.clickFile = function(file) {
          $scope.selectedFile = file;
          //console.log('-- fileChooser.clickFile() '+angular.toJson(file));
          if (file.isDirectory) {
            currentDirectory = file.nativeURL;
            getContents(file.nativeURL);
          }
        };

        // Footer button logic (Select, Cancel)
        $scope.select = function(chosenFile) {
          //logger.debug('fileChooser.select() '+angular.toJson(chosenFile));
          $uibModalInstance.close(chosenFile);
        };

        $scope.cancel = function() {
          deferred.reject({ code: 502, msg: "ChooseFile modal cancelled" });
          //console.log('-- chose file cancelled');
          $uibModalInstance.dismiss("cancel");
        };
      };

      // Modal controllers and logic
      var modalInstance = $uibModal.open({
        templateUrl: "scripts/services/views/filechooser.html",
        controller: FileChooserCtrl,
        backdrop: "static"
      });

      // function to call upon $uibModalInstance.close()
      modalInstance.result.then(function(chosenFile) {
        if (fileOrDirectory === "file") {
          if (chosenFile && chosenFile.isFile) {
            logger.debug("fileChooser chose file: " + angular.toJson(chosenFile));
            deferred.resolve(chosenFile);
          } else {
            logger.debug("fileChooser did not chose a proper file: " + angular.toJson(chosenFile));
            deferred.reject({
              code: 503,
              msg: "ChooseFile modal did not choose a proper file"
            });
          }
        } else if (fileOrDirectory === "directory") {
          if (chosenFile && chosenFile.isDirectory) {
            logger.debug("fileChooser chose directory: " + angular.toJson(chosenFile));
            deferred.resolve(chosenFile);
          } else {
            logger.debug("fileChooser did not chose a proper directory: " + angular.toJson(chosenFile));
            deferred.reject({
              code: 504,
              msg: "ChooseFile modal did not choose a proper directory"
            });
          }
        }
      });

      return deferred.promise;
    };

    return api;
  });
