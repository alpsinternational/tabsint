/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as d3 from "d3";

angular
  .module("tabsint.services.cha.plot", [])
  /* Angular wrappers for d3 library to be used in conjunction with our response areas

     Goal to write directives/controllers that can be used across multiple response areas with common inputs/outputs
     */

  .factory("d3Services", function() {
    // factory/service to set global properties of certain graphic types
    var colorList = [
      "#1f77b4",
      "#ff7f0e",
      "#2ca02c",
      "#d62728",
      "#9467bd",
      "#8c564b",
      "#e377c2",
      "#7f7f7f",
      "#bcbd22",
      "#17becf"
    ];

    // If we need custom symbols, like the x
    var customSymbolTypes = d3.map({
      "thin-x": function(size) {
        return "M" + -size / 2 + "," + -size / 2 + "l" + size + "," + size + "m0," + -size + "l" + -size + "," + size;
      },
      bone_left: function(size) {
        return "M0," + size + "V-" + size + "h0" + size;
      },
      bone_right: function(size) {
        return "M0," + size + "V-" + size + "h0-" + size;
      }
    });

    // CREATE A CUSTOM SYMBOL FUNCTION MIRRORING THE BUILT-IN FUNCTIONALITY
    d3.svg.customSymbol = function() {
      var type,
        size = 64; // SET DEFAULT SIZE
      function symbol(d, i) {
        // GET THE SYMBOL FROM THE CUSTOM MAP
        return customSymbolTypes.get(type.call(this, d, i))(size.call(this, d, i)); // jshint ignore:line
        // todo: this is causing jshint to fail, figure out why
      }
      // DEFINE GETTER/SETTER FUNCTIONS FOR SIZE AND TYPE
      symbol.type = function(_) {
        if (!arguments.length) {
          return type;
        }
        type = d3.functor(_);
        return symbol;
      };
      symbol.size = function(_) {
        if (!arguments.length) {
          return size;
        }
        size = d3.functor(_);
        return symbol;
      };
      return symbol;
    };

    function getSymbol(type, size) {
      // SIZE DEFAULTS TO 64 IF NOT GIVEN
      size = size || 64;
      // IF TYPE IS ONE OF THE BUILT-IN TYPES, CALL THE BUILT-IN FUNCTION
      if (d3.svg.symbolTypes.indexOf(type) !== -1) {
        return d3.svg
          .symbol()
          .type(type)
          .size(size)();
      }
      // OTHERWISE, CALL THE CUSTOM SYMBOL FUNCTION
      else {
        return d3.svg
          .customSymbol()
          .type(type)
          .size(size)();
      }
    }

    var api = {
      template: undefined,
      scatterplot: undefined
    };

    api.template = function() {};

    api.scatterPlot = function(domElem, dataStruct) {
      var data = [],
        xValue,
        xScale,
        xMap,
        xAxis,
        yValue,
        yScale,
        yMap,
        yAxis;

      var minY = angular.isDefined(dataStruct.minY) ? dataStruct.minY : 0;
      var minX = angular.isDefined(dataStruct.minX) ? dataStruct.minX : 5;
      var maxY = angular.isDefined(dataStruct.maxY) ? dataStruct.maxY : 200;
      var plotHeight = angular.isDefined(dataStruct.plotHeight) ? dataStruct.plotHeight : 400;
      var plotWidth = angular.isDefined(dataStruct.plotWidth) ? dataStruct.plotWidth : 400;

      var margin = {
          top: 40,
          right: 10,
          bottom: 40,
          left: 65
        },
        width = plotWidth - margin.left - margin.right,
        height = plotHeight - margin.top - margin.bottom;

      if (dataStruct.xLabel === angular.undefined) {
        dataStruct.xLabel = "x-axis";
      }
      if (dataStruct.yLabel === angular.undefined) {
        dataStruct.yLabel = "y-axis";
      }
      if (dataStruct.title === angular.undefined) {
        dataStruct.title = "Title";
      }

      if (dataStruct.y === angular.undefined) {
        // fill with fake data for testing
        dataStruct.y = [1000, 950, 900, 850, 800, 750, 800, 760, 790, 760, 780, 760, 780, 770, 775];
      }

      var dataMin = Math.min.apply(Math, dataStruct.y);
      if (dataMin < 0) {
        minY = -20 * Math.ceil(Math.abs(dataMin) / 20);
      }

      minX = Math.max(dataStruct.y.length, minX);

      if (dataStruct.x === angular.undefined) {
        // just y data, use index
        for (var j = 0; j < dataStruct.y.length; j++) {
          data[j] = {
            x: 0,
            y: 0
          };
          data[j].y = dataStruct.y[j];
        }

        xValue = function(d) {
          return d;
        };
        xScale = d3.scale
          .linear()
          .range([0, width])
          .domain([0, minX]);
        xMap = function(d, i) {
          return xScale(xValue(i) + 1);
        };
        xAxis = d3.svg
          .axis()
          .scale(xScale)
          .orient("bottom")
          .tickFormat(d3.format("d"));

        yValue = function(d, i) {
          return d;
        };
        yScale = d3.scale
          .linear()
          .range([height, 0])
          .domain([minY, maxY]);
        yMap = function(d) {
          return yScale(yValue(d.y));
        };
        yAxis = d3.svg
          .axis()
          .scale(yScale)
          .orient("left");
      } else {
        for (var k = 0; k < dataStruct.y.length; k++) {
          data[k] = {
            x: 0,
            y: 0
          };
          data[k].x = dataStruct.x[k];
          data[k].y = dataStruct.y[k];
        }

        xValue = function(d) {
          return d;
        };
        xScale = d3.scale
          .linear()
          .range([0, width])
          .domain([0, 200]);
        xMap = function(d, i) {
          return xScale(xValue(d.x));
        };
        xAxis = d3.svg
          .axis()
          .scale(xScale)
          .orient("bottom");

        yValue = function(d, i) {
          return d;
        };
        yScale = d3.scale
          .linear()
          .range([height, 0])
          .domain([minY, 200]);
        yMap = function(d) {
          return yScale(yValue(d.y));
        };
        yAxis = d3.svg
          .axis()
          .scale(yScale)
          .orient("left");
      }

      var mySVG = d3
        .select(domElem)
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

      // x-axis
      mySVG
        .append("g")
        .attr("class", "x axis")
        .attr("font-size", 16)
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

      mySVG
        .append("text")
        .attr("class", "label")
        .attr("font-size", 20)
        .attr("x", width / 2)
        .attr("y", height + 35)
        .style("text-anchor", "middle")
        .text(dataStruct.xLabel);

      // y-axis
      mySVG
        .append("g")
        .attr("class", "y axis")
        .attr("font-size", 16)
        .call(yAxis);

      mySVG
        .append("text")
        .attr("class", "label")
        .attr("font-size", 18)
        .attr("transform", "rotate(-90)")
        .attr("x", -height / 2)
        .attr("y", -50)
        .style("text-anchor", "middle")
        .text(dataStruct.yLabel);

      // title
      mySVG
        .append("text")
        .attr("class", "label")
        .attr("x", width / 2)
        .attr("y", 10)
        .style("text-anchor", "middle")
        .style("font-weight", "bold")
        .style("font-size", "20px")
        .text(dataStruct.title);

      mySVG
        .selectAll(".dot")
        .data(data)
        .enter()
        .append("circle")
        .attr("class", "dot")
        .attr("r", 5)
        .attr("cx", xMap)
        .attr("cy", yMap)
        .style("fill", colorList[0]);
    };

    api.thirdOctaveBandPlot = function(domElem, dataStruct) {
      var xLabel = "Frequency (Hz)";
      var yLabel = "Level (dB SPL)";
      var title = "Third-Octave Band Results";
      var legendData = [],
        bar,
        bar2;

      dataStruct = dataStruct || [];
      if (dataStruct === angular.undefined || dataStruct.length === 0) {
        return;
      }

      var dataLeft = dataStruct.left || [];
      var dataRight = dataStruct.right || [];
      var dataStandard = dataStruct.standard || {};

      var margin = {
          top: 20,
          right: 100,
          bottom: 60,
          left: 50
        },
        width = 550 - margin.left - margin.right,
        height = 450 - margin.top - margin.bottom;

      var xTicks = [250, 500, 1000, 2500, 5000, 10000];
      var yTicks = [0, 10, 20, 30, 40, 50, 60, 70, 80];

      var xScale = d3.scale
        .ordinal()
        .rangeBands([0, width], 0.2)
        .domain(
          dataLeft.map(function(d) {
            return d.freq;
          })
        );

      var yScale = d3.scale
        .linear()
        .domain([0, 80])
        .range([height, 0]);

      var xLog = d3.scale
        .log()
        .base(2)
        .domain([dataLeft[0].freq / Math.pow(2, 1 / 6), Math.pow(2, 1 / 6) * dataLeft[dataLeft.length - 1].freq])
        .range([0, width]);

      var yAxis = d3.svg
        .axis()
        .scale(yScale)
        .orient("left")
        .tickValues(yTicks)
        .tickSize(10, 0);
      var xAxis = d3.svg
        .axis()
        .scale(xLog)
        .orient("bottom")
        .tickValues(xTicks)
        .tickSize(10)
        .tickFormat(d3.format("d"));

      var mySVG = d3
        .select(domElem)
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

      // Handle different plotting for 1 or both channels
      if ((dataLeft.length > 0 && dataRight.length <= 0) || (dataRight.length > 0 && dataLeft.length <= 0)) {
        // Plot single channel, with above/below limit coloring if applicable
        var dataSingleSide;
        if (dataLeft.length > 0) {
          dataSingleSide = dataLeft;
        } else {
          dataSingleSide = dataRight;
        }
        bar = mySVG
          .selectAll("bars")
          .data(dataSingleSide)
          .enter()
          .append("rect")
          .attr("x", function(d) {
            return xScale(d.freq);
          })
          .attr("y", function(d) {
            return yScale(d.level);
          })
          .attr("width", function(d) {
            return xScale.rangeBand();
          })
          .attr("height", function(d) {
            return height - yScale(d.level);
          })
          .attr("stroke", "rgb(0,0,0)")
          .attr("fill", function(d) {
            if (angular.isDefined(dataStandard.data) && dataStandard.data.length > 0) {
              // plot is using a standard noise level defined in the protocol
              // make all green for now - we don't yet want to alarm if noise measurements are higher than standards allow
              return "green";
            } else {
              // plot is using limit bands defined here
              // use original red for above limit, green for below
              if (d.level > d.limit) {
                return "red";
              } else {
                return "green";
              }
              legendData.push({
                text: "Below Limit",
                color: "green"
              });
              legendData.push({
                text: "Above Limit",
                color: "red"
              });
            }
          });
      } else if (dataLeft.length > 0 && dataRight.length > 0) {
        // Plot both channels with separate colors, without color change for over-standard values
        bar = mySVG
          .selectAll("bars")
          .data(dataLeft)
          .enter()
          .append("rect")
          .attr("x", function(d) {
            return xScale(d.freq);
          })
          .attr("y", function(d) {
            return yScale(d.level);
          })
          .attr("width", function(d) {
            return xScale.rangeBand() / 2;
          })
          .attr("height", function(d) {
            return height - yScale(d.level);
          })
          .attr("stroke", "rgb(0,0,0)")
          .attr("fill", "blue");

        bar2 = mySVG
          .selectAll("bars")
          .data(dataRight)
          .enter()
          .append("rect")
          .attr("x", function(d) {
            return xScale(d.freq) + xScale.rangeBand() / 2;
          })
          .attr("y", function(d) {
            return yScale(d.level);
          })
          .attr("width", xScale.rangeBand() / 2)
          .attr("height", function(d) {
            return height - yScale(d.level);
          })
          //.attr("data-legend",function(d) { return 'right'})
          .attr("stroke", "rgb(0,0,0)")
          .attr("fill", "red");

        legendData.push({
          text: "Left",
          color: "blue"
        });
        legendData.push({
          text: "Right",
          color: "red"
        });
      }

      // plot standard if defined, otherwise plot limit bands
      if (angular.isDefined(dataStandard.data) && dataStandard.data.length > 0) {
        // add first and last 0 for closure:
        dataStandard.data.splice(0, 0, angular.copy(dataStandard.data[0]));
        dataStandard.data[0].L = 0;

        dataStandard.data.push(angular.copy(dataStandard.data[dataStandard.data.length - 1]));
        dataStandard.data[dataStandard.data.length - 1].L = 0;
        var xScale2 = d3.scale
          .log()
          .domain([250 - 31.25, 10000 + 1250])
          .range([0, width]);

        var line = d3.svg
          .line()
          .x(function(d) {
            return xScale2(d.F);
          })
          .y(function(d) {
            return yScale(d.L);
          });

        mySVG
          .append("svg:path")
          .datum(dataStandard.data)
          .attr("class", "line")
          .attr("clip-path", "url(#clip)")
          .attr("stroke", "black")
          .attr("fill", "gray")
          .style("opacity", 0.6)
          //.attr('data-legend',function(d) { return 'MPANL'})
          .attr("d", line);

        if (dataStandard.name) {
          legendData.push({
            text: dataStandard.name,
            color: "gray"
          });
        } else {
          legendData.push({
            text: "ANSI MPANL",
            color: "gray"
          });
        }
      } else {
        // limit bands
        var limitData;
        if (dataLeft.length > 0) {
          limitData = dataLeft;
        } else {
          limitData = dataRight;
        }
        mySVG
          .selectAll("limits")
          .data(limitData)
          .enter()
          .append("rect")
          .attr("x", function(d) {
            return xScale(d.freq) - 3;
          })
          .attr("y", function(d) {
            return yScale(d.limit);
          })
          .attr("width", xScale.rangeBand() + 6)
          .attr("height", 6)
          .attr("stroke", "rgb(0,0,0)")
          .attr("fill", "rgb(150, 150, 150)");

        legendData.push({
          text: "Limit",
          color: "gray"
        });
      }

      var legend = mySVG
        .append("g")
        .attr("class", "legend")
        //.attr("x", w - 65)
        //.attr("y", 50)
        .attr("transform", "translate(-80,10)");

      legend
        .selectAll("rect")
        .data(legendData)
        .enter()
        .append("rect")
        .attr("x", width - 65)
        .attr("y", function(d, i) {
          return i * 20;
        })
        .attr("width", 10)
        .attr("height", 10)
        .style("fill", function(d) {
          return d.color;
        });

      legend
        .selectAll("text")
        .data(legendData)
        .enter()
        .append("text")
        .attr("x", width - 52)
        .attr("y", function(d, i) {
          return i * 20 + 9;
        })
        .text(function(d) {
          return d.text;
        });

      // x-axis
      mySVG
        .append("g")
        .attr("class", "axisnoline")
        .attr("font-size", 18)
        .attr("format", "d")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis)
        .attr("font-size", 20)
        .append("text")
        .attr("class", "label")
        .attr("y", 50)
        .attr("x", width / 2)
        .style("text-anchor", "middle")
        .text(xLabel);

      // y-axis
      mySVG
        .append("g")
        .attr("class", "axisnoline")
        .attr("font-size", 18)
        .call(yAxis)
        .append("text")
        .attr("font-size", 20)
        .attr("class", "label")
        .attr("transform", "rotate(-90)")
        .attr("y", -40)
        .attr("x", -height / 2)
        .style("text-anchor", "middle")
        .text(yLabel);
    };

    api.mpanlsPlot = function(domElem, dataStruct) {
      var xLabel = "Frequency (Hz)";
      var yLabel = "Estimated Level Under the Earcup (dB SPL)";
      var title = "MPANLs Results";
      var legendData = [],
        bar,
        bar2;

      dataStruct = dataStruct || [];
      if (dataStruct === angular.undefined || dataStruct.length === 0) {
        console.log("ERROR: no data passed in to mpanlsPlot");
        return;
      }

      var margin = {
          top: 20,
          right: 90,
          bottom: 80,
          left: 60
        },
        width = 550 - margin.left - margin.right,
        height = 450 - margin.top - margin.bottom;

      var xTicks = dataStruct.map(function(d) {
        return d.freq;
      });
      var yTicks = [-10, 0, 10, 20, 30, 40, 50, 60];

      var xScale = d3.scale
        .ordinal()
        .rangeBands([-15, width + 15], 0.2)
        .domain(
          dataStruct.map(function(d) {
            return d.freq;
          })
        );

      var yScale = d3.scale
        .linear()
        .domain([-10, 80])
        .range([height, 0]);

      var xLog = d3.scale
        .log()
        .base(2)
        .domain([dataStruct[0].freq / Math.pow(2, 1 / 3), Math.pow(2, 1 / 3) * dataStruct[dataStruct.length - 1].freq])
        .range([0, width]);

      var yAxis = d3.svg
        .axis()
        .scale(yScale)
        .orient("left")
        .tickValues(yTicks)
        .tickSize(10, 0);
      var xAxis = d3.svg
        .axis()
        .scale(xLog)
        .orient("bottom")
        .tickValues(xTicks)
        .tickSize(10)
        .tickFormat(d3.format("d"));

      var mySVG = d3
        .select(domElem)
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

      bar = mySVG
        .selectAll("bars")
        .data(dataStruct)
        .enter()
        .append("rect")
        .attr("x", function(d) {
          return xScale(d.freq);
        })
        .attr("y", function(d) {
          return yScale(d.levelUnderWAHTS);
        })
        .attr("width", function(d) {
          return xScale.rangeBand();
        })
        .attr("height", function(d) {
          return height - yScale(d.levelUnderWAHTS);
        })
        .attr("stroke", "rgb(0,0,0)")
        .attr("fill", function(d) {
          // plot is using limit bands defined here
          // use original red for above limit, green for below, yellow for within 3 dB SPL
          if (d.levelUnderWAHTS > d.limit + 3) {
            return "red";
          } else if (d.levelUnderWAHTS > d.limit - 3) {
            return "yellow";
          } else {
            return "green";
          }
        });
      legendData.push({
        text: "Below Limit",
        color: "green"
      });
      legendData.push({
        text: "Within 3 dB SPL of Limit",
        color: "yellow"
      });
      legendData.push({
        text: "Above Limit",
        color: "red"
      });

      // plot standard limit bands
      mySVG
        .selectAll("limits")
        .data(dataStruct)
        .enter()
        .append("rect")
        .attr("x", function(d) {
          return xScale(d.freq) - 3;
        })
        .attr("y", function(d) {
          return yScale(d.limit);
        })
        .attr("width", xScale.rangeBand() + 6)
        .attr("height", 6)
        .attr("stroke", "rgb(0,0,0)")
        .attr("fill", "rgb(150, 150, 150)");
      legendData.push({
        text: "Limit",
        color: "gray"
      });

      // plot dosimeter noise floor bands
      mySVG
        .selectAll("noiseFloors")
        .data(dataStruct)
        .enter()
        .append("rect")
        .attr("x", function(d) {
          return xScale(d.freq) - 3;
        })
        .attr("y", function(d) {
          return yScale(d.noiseFloor - d.att);
        })
        .attr("width", xScale.rangeBand() + 6)
        .attr("height", 2)
        .attr("stroke", "rgb(0,0,0)")
        .attr("fill", "rgb(0, 0, 0)");
      legendData.push({
        text: "Dosimeter Noise Floor",
        color: "black"
      });

      var legend = mySVG
        .append("g")
        .attr("class", "legend")
        //.attr("x", w - 65)
        //.attr("y", 50)
        .attr("transform", "translate(-80,10)");

      legend
        .selectAll("rect")
        .data(legendData)
        .enter()
        .append("rect")
        .attr("x", width - 255)
        .attr("y", function(d, i) {
          return i * 25;
        })
        .attr("width", 10)
        .attr("height", 10)
        .style("fill", function(d) {
          return d.color;
        });

      legend
        .selectAll("text")
        .data(legendData)
        .enter()
        .append("text")
        .attr("x", width - 242)
        .attr("y", function(d, i) {
          return i * 25 + 12;
        })
        .text(function(d) {
          return d.text;
        });

      // x-axis
      mySVG
        .append("g")
        .attr("class", "axisnoline")
        .attr("font-size", 18)
        .attr("format", "d")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis)
        .attr("font-size", 20)
        .append("text")
        .attr("class", "label")
        .attr("y", 50)
        .attr("x", width / 2)
        .style("text-anchor", "middle")
        .text(xLabel);

      // y-axis
      mySVG
        .append("g")
        .attr("class", "axisnoline")
        .attr("font-size", 18)
        .attr("format", "d")
        .call(yAxis)
        .append("text")
        .attr("font-size", 20)
        .attr("class", "label")
        .attr("transform", "rotate(-90)")
        .attr("y", -50)
        .attr("x", -240)
        .style("text-anchor", "middle")
        .text(yLabel);
    };

    api.audiogramPlot = function(domElem, dataStruct) {
      var data = [],
        xValue,
        xScale,
        xScaleMinor,
        xMap,
        xAxis,
        xAxisMinor,
        xTicks,
        xTicksMinor,
        xGrid,
        xGridMinor,
        yValue,
        yScale,
        yMap,
        yAxis,
        yTicks,
        yGrid;
      var symbolMap, sizeMap, colorMap, strokeWidthMap;

      xTicks = [125, 250, 500, 1000, 2000, 4000, 8000, 16000];
      xTicksMinor = [750, 1500, 3000, 6000];
      yTicks = [-20, -10, 0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120];

      var aspectRatio = (yTicks.length - 1) / 2 / (xTicks.length - 1); // should be 20dB/octave
      var margin = {
          top: 55,
          right: 60,
          bottom: 60,
          left: 60
        },
        width = 520 - margin.left - margin.right, // x - 120 = 400
        height = width * aspectRatio; // x - 115 = 350

      if (dataStruct.xLabel === angular.undefined) {
        dataStruct.xLabel = "x-axis";
      }
      if (dataStruct.yLabel === angular.undefined) {
        dataStruct.yLabel = "y-axis";
      }
      if (dataStruct.title === angular.undefined) {
        dataStruct.title = "Title";
      }
      if (dataStruct.frequency === angular.undefined || dataStruct.level === angular.undefined) {
        return;
      }

      for (var k = 0; k < dataStruct.frequency.length; k++) {
        data[k] = {
          level: undefined,
          frequency: undefined,
          channel: undefined
        };
        data[k].level = dataStruct.level[k];
        data[k].frequency = dataStruct.frequency[k];
        data[k].channel = dataStruct.channel[k];
      }

      xValue = function(d) {
        return d;
      };
      xScale = d3.scale
        .log()
        .base(2)
        .range([0, width])
        .domain([93.75, 24000]);
      xMap = function(d, i) {
        return xScale(xValue(d.frequency));
      };
      xAxis = d3.svg
        .axis()
        .scale(xScale)
        .orient("top")
        .tickFormat(d3.format(",.0f"))
        .tickValues(xTicks)
        .tickSize(15, 0);

      xAxisMinor = d3.svg
        .axis()
        .scale(xScale)
        .orient("top")
        .tickFormat(d3.format(",.0f"))
        .tickValues(xTicksMinor)
        .tickSize(3, 0);

      yValue = function(d, i) {
        return d;
      };
      yScale = d3.scale
        .linear()
        .range([0, height])
        .domain([Math.min.apply(Math, yTicks), Math.max.apply(Math, yTicks)]);
      yMap = function(d) {
        return yScale(yValue(d.level));
      };
      yAxis = d3.svg
        .axis()
        .scale(yScale)
        .orient("left")
        .tickValues(yTicks)
        .tickSize(10, 0);

      xGrid = xAxis;
      xGridMinor = xAxisMinor;
      yGrid = yAxis.ticks(10);

      symbolMap = function(d) {
        if (d.channel === "left") {
          return "thin-x";
        } else if (d.channel === "right") {
          return "circle";
        } else if (d.channel === "bone_left") {
          return "bone_left";
        } else if (d.channel === "bone_right") {
          return "bone_right";
        }
      };
      sizeMap = function(d) {
        if (d.channel === "left") {
          return "16";
        } else if (d.channel === "right") {
          return "256";
        } else if (d.channel === "bone_left") {
          return "12"; // TODO: ??
        } else if (d.channel === "bone_right") {
          return "12";
        }
      };
      colorMap = function(d) {
        return d.channel.indexOf("left") > -1 ? "blue" : "red";
      };
      strokeWidthMap = function(d) {
        return "3";
      };

      var mySVG = d3
        .select(domElem)
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

      // x-axis
      mySVG
        .append("g")
        .attr("class", "x axis")
        .attr("font-size", 16)
        .call(xAxis);

      mySVG
        .append("text")
        .attr("class", "label")
        .attr("font-size", 20)
        .attr("x", width / 2)
        .attr("y", -40)
        .style("text-anchor", "middle")
        .text(dataStruct.xLabel);

      //        // x-axis minor
      //        mySVG.append('g')
      //          .attr('class', 'x axis')
      //          .attr('font-size',12)
      //          //.attr('transform', 'translate(0,' + height + ')')
      //          .call(xAxisMinor);

      // y-axis
      mySVG
        .append("g")
        .attr("class", "y axis")
        .attr("font-size", 16)
        .call(yAxis);

      mySVG
        .append("text")
        .attr("font-size", 20)
        .attr("class", "label")
        .attr("transform", "rotate(-90)")
        .attr("y", -60)
        .attr("x", -height / 2)
        .attr("dy", ".71em")
        .style("text-anchor", "middle")
        .text(dataStruct.yLabel);

      // border
      mySVG
        .append("rect")
        .attr("x", 0)
        .attr("y", 0)
        .attr("height", height)
        .attr("width", width)
        .style("stroke", "black")
        .style("fill", "none")
        .style("stroke-width", 2);

      // gridlines
      mySVG
        .append("g")
        .attr("class", "grid")
        .attr("transform", "translate(0," + height + ")")
        .call(xGrid.tickSize(height, 0, 0).tickFormat(""));

      mySVG
        .append("g")
        .attr("class", "grid")
        .attr("transform", "translate(0," + (height - height / 39) + ")")
        .style("stroke-dasharray", "" + height / 39 + "," + (2 * height) / 39 + "")
        .call(xGridMinor.tickSize(height - (2 * height) / 39, 0, 0).tickFormat(""));

      mySVG
        .append("g")
        .attr("class", "grid")
        .call(yGrid.tickSize(-width, 0, 0).tickFormat(""));

      var node = mySVG
        .selectAll(".node")
        .data(data)
        .enter()
        .append("g")
        .attr("class", "node")
        .attr("transform", function(d) {
          return "translate(" + xScale(d.frequency) + "," + yScale(d.level) + ")";
        });

      node
        .append("path")
        .attr("d", function(d) {
          return getSymbol(symbolMap(d), sizeMap(d));
        })
        .attr("stroke", colorMap)
        .attr("fill", "none")
        .attr("stroke-width", strokeWidthMap);
    };

    api.dpoaePlot = function(domElem, dataStruct) {
      // y1 is the amplitude, y2 is the noise floor
      // verify the results are being passed in correctly:

      var data,
        xValue,
        xScale,
        xMapLow,
        xMapHigh,
        xAxis,
        yValue,
        yScale,
        yMapLowAmp,
        yMapLowNoise,
        yMapHighAmp,
        yMapHighNoise,
        yAxis,
        i;

      //var xTicks = [750, 1000, 1500, 2000, 4000, 6000, 8000];
      var xTicks = [0, 1000, 2000, 3000, 4000, 5000];

      var margin = {
          top: 10,
          right: 10,
          bottom: 60,
          left: 65
        },
        width = 500 - margin.left - margin.right,
        height = 400 - margin.top - margin.bottom;

      if (dataStruct.xLabel === angular.undefined) {
        dataStruct.xLabel = "x-axis";
      }
      if (dataStruct.yLabel === angular.undefined) {
        dataStruct.yLabel = "y-axis";
      }
      if (dataStruct.title === angular.undefined) {
        dataStruct.title = "Title";
      }

      data = dataStruct.data;

      var yMin = -10,
        yMax = 40;
      for (i = 0; i < data.length; i++) {
        yMin = Math.min(yMin, data[i].DpLow.Amplitude);
        //          yMin = Math.min(yMin,data[i].DpHigh.Frequency);
      }
      //        var dataMin = Math.min(  Math.min.apply(Math,dataStruct.y), Math.min.apply(Math,dataStruct.y2) );
      //        if (dataMin < 0){
      //          minY = -20*Math.ceil(Math.abs(dataMin)/20);
      //        }

      var xMax = 6000;
      for (i = 0; i < data.length; i++) {
        xMax = Math.max(xMax, data[i].F2.Frequency);
        //          xMax = Math.max(xMax,data[i].DpHigh.Frequency);
      }
      //xMax = Math.round(xMax*1.2);

      //        var xMax = 1.2*Math.max.apply(Math,dataStruct.x);
      //
      //        for (var k = 0; k< dataStruct.y.length; k++){
      //          data[k]={x:0, y:0};
      //          data[k].x = dataStruct.x[k];
      //          data[k].y = dataStruct.y[k];
      //          data[k].y2 =dataStruct.y2[k];
      //        }

      var rectR = 5;

      xValue = function(d) {
        return d;
      };
      xScale = d3.scale
        .linear()
        .range([0, width])
        .domain([0, xMax]);
      xMapLow = function(d, i) {
        return xScale(xValue(d.F2.Frequency));
      };
      xMapHigh = function(d, i) {
        return xScale(xValue(d.DpHigh.Frequency));
      };
      xAxis = d3.svg
        .axis()
        .scale(xScale)
        .orient("bottom")
        .tickFormat(d3.format(",.0f"))
        .tickValues(xTicks)
        .tickSize(15, 0);

      yValue = function(d, i) {
        return d;
      };
      yScale = d3.scale
        .linear()
        .range([height, 0])
        .domain([yMin, yMax]);
      yMapLowAmp = function(d) {
        return yScale(yValue(d.DpLow.Amplitude));
      };
      yMapLowNoise = function(d) {
        return yScale(yValue(d.DpLow.NoiseFloor));
      };
      yMapHighAmp = function(d) {
        return yScale(yValue(d.DpHigh.Amplitude));
      };
      yMapHighNoise = function(d) {
        return yScale(yValue(d.DpHigh.NoiseFloor));
      };
      yAxis = d3.svg
        .axis()
        .scale(yScale)
        .orient("left");

      var symbolMap = function(d) {
        return d.channel === "left" ? "thin-x" : "circle";
      };
      var sizeMap = function(d) {
        return d.channel === "left" ? "16" : "256";
      };
      var colorMap = function(d) {
        return d.channel === "left" ? "blue" : "red";
      };
      var strokeWidthMap = function(d) {
        return d.channel === "left" ? "3" : "3";
      };

      var mySVG = d3
        .select(domElem)
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

      // x-axis
      mySVG
        .append("g")
        .attr("class", "x axis")
        .attr("font-size", 16)
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

      mySVG
        .append("text")
        .attr("class", "label")
        .attr("font-size", 20)
        .attr("x", width / 2)
        .attr("y", height + 48)
        .style("text-anchor", "middle")
        .text(dataStruct.xLabel);

      // y-axis
      mySVG
        .append("g")
        .attr("class", "y axis")
        .attr("font-size", 16)
        .call(yAxis);

      mySVG
        .append("text")
        .attr("class", "label")
        .attr("font-size", 18)
        .attr("transform", "rotate(-90)")
        .attr("x", -height / 2)
        .attr("y", -50)
        .style("text-anchor", "middle")
        .text(dataStruct.yLabel);

      // title
      //        mySVG.append('text')
      //          .attr('class', 'label')
      //          .attr('x', width/2)
      //          .attr('y', 10)
      //          .style("text-anchor", "middle")
      //          .style('font-weight', 'bold')
      //          .style('font-size','20px')
      //          .text(dataStruct.title);

      var node = mySVG
        .selectAll(".node")
        .data(data)
        .enter()
        .append("g")
        .attr("class", "node");

      node
        .append("path")
        .attr("transform", function(d) {
          return "translate(" + xScale(d.F2.Frequency) + "," + yScale(yValue(d.DpLow.Amplitude)) + ")";
        })
        .attr("d", function(d) {
          return getSymbol(symbolMap(d), sizeMap(d));
        })
        .attr("stroke", colorMap)
        .attr("fill", "none")
        .attr("stroke-width", strokeWidthMap);

      // Plotting the Amplitude DpHigh
      //        node.append('circle')
      //          .attr('class', 'dot')
      //          .attr('r', 5)
      //          .attr('cx', xMapHigh)
      //          .attr('cy', yMapHighAmp)
      //          .style('fill', colorList[1])
      //        ;

      // Plotting the NoiseFloor DpLow
      node
        .append("rect")
        .attr("x", xMapLow)
        .attr("y", yMapLowNoise)
        .attr("width", 2 * rectR)
        .attr("height", 2 * rectR)
        .style("fill", colorList[2])
        .attr("transform", function() {
          return "translate(" + -rectR + "," + -rectR + ")";
        });

      // Plotting the NoiseFloor DpHigh
      //        node.append('rect')
      //          .attr('x', xMapHigh)
      //          .attr('y', yMapHighNoise)
      //          .attr('width', 2*rectR)
      //          .attr('height',2*rectR)
      //          .style('fill', colorList[2])
      //          .attr('transform', function() {return 'translate('+(-rectR)+','+(-rectR)+')';})
      //        ;

      var legData = [
        //          { c: colorList[0],text: 'Level'},
        //          { c: colorList[1],text: 'DpHigh'},
        {
          c: colorList[2],
          text: "NoiseFloor"
        }
      ];

      // draw legend
      var legend = mySVG
        .selectAll(".legend")
        .data(legData)
        .enter()
        .append("g")
        .attr("class", "legend")
        .attr("transform", function(d, i) {
          return "translate(0," + i * 20 + ")";
        });

      // draw legend colored rectangles
      legend
        .append("rect")
        .attr("x", width - 18)
        .attr("width", 18)
        .attr("height", 18)
        .style("fill", function(d) {
          return d.c;
        });

      // draw legend text
      legend
        .append("text")
        .attr("x", width - 24)
        .attr("y", 9)
        .attr("dy", ".35em")
        .style("text-anchor", "end")
        .text(function(d) {
          return d.text;
        });
    };

    (api.calibrationPlot = function(dataStruct) {
      var createPlot = function(domElem, dataStruct) {
        var margin = {
            top: 70,
            right: 70,
            bottom: 120,
            left: 100
          },
          width = 600 - margin.left - margin.right,
          height = 450 - margin.top - margin.bottom;

        var x = d3.scale.ordinal().rangeRoundBands([0, width], 0.05);

        var y = d3.scale.linear().range([height, 0]);

        var xAxis = d3.svg
          .axis()
          .scale(x)
          .orient("bottom");

        var yAxis = d3.svg
          .axis()
          .scale(y)
          .orient("left")
          .ticks(10);

        var svg = d3
          .select(domElem)
          .append("svg")
          .attr("width", width + margin.left + margin.right)
          .attr("height", height + margin.top + margin.bottom)
          .append("g")
          .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        var data = dataStruct.measured.map((item, index) => {
          return {
            frequency: dataStruct.frequencies[index],
            measured: item
          };
        });
        x.domain(dataStruct.frequencies);
        y.domain([0, d3.max(dataStruct.measured)]);

        svg
          .append("g")
          .attr("class", "x axis")
          .attr("transform", "translate(0," + height + ")")
          .call(xAxis)
          .selectAll("text")
          .style("text-anchor", "end")
          .attr("dx", "-.8em")
          .attr("dy", "-.55em")
          .attr("transform", "rotate(-90)");

        svg
          .append("text")
          .attr("class", "label")
          .attr("font-size", 16)
          .attr("x", width / 2)
          .attr("y", height + 100)
          .style("text-anchor", "middle")
          .text(dataStruct.xLabel);

        svg
          .append("g")
          .attr("class", "y axis")
          .call(yAxis)
          .append("text")
          .attr("font-size", 16)
          .attr("transform", "rotate(-90)")
          .attr("y", height / 2)
          .attr("dy", "-11em")
          .style("text-anchor", "end")
          .text(dataStruct.yLabel);

        // title
        svg
          .append("text")
          .attr("x", width / 2)
          .attr("y", 0 - margin.top / 2)
          .attr("text-anchor", "middle")
          .style("font-size", "16px")
          .style("font-weight", "bold")
          .text(dataStruct.title);

        svg
          .selectAll("bar")
          .data(data)
          .enter()
          .append("rect")
          // Color the bars according to passing, warning, and failing measuements.
          .attr("fill", function(d) {
            if (d.measured >= 10) {
              return "red";
            } else if (d.measured >= 5) {
              return "yellow";
            }
            return "green";
          })
          .attr("x", function(d) {
            return x(d.frequency);
          })
          .attr("width", x.rangeBand())
          .attr("y", function(d) {
            return y(d.measured);
          })
          .attr("height", function(d) {
            return height - y(d.measured);
          });
      };
      createPlot("#calibrationPlotLeftWindow", dataStruct.left);
      createPlot("#calibrationPlotRightWindow", dataStruct.right);
    }),
      (api.gapResultsPlot = function(domElem, dataStruct) {
        var data = [],
          xValue,
          xScale,
          xMap,
          xAxis,
          yValue,
          yScale,
          yMap,
          yAxis;

        var minY = angular.isDefined(dataStruct.minY) ? dataStruct.minY : 0;
        var minX = angular.isDefined(dataStruct.minX) ? dataStruct.minX : 5;
        var maxY = angular.isDefined(dataStruct.maxY) ? dataStruct.maxY : 200;
        var plotHeight = angular.isDefined(dataStruct.plotHeight) ? dataStruct.plotHeight : 400;
        var plotWidth = angular.isDefined(dataStruct.plotWidth) ? dataStruct.plotWidth : 400;

        var margin = {
            top: 40,
            right: 10,
            bottom: 40,
            left: 65
          },
          width = plotWidth - margin.left - margin.right,
          height = plotHeight - margin.top - margin.bottom;

        if (dataStruct.xLabel === angular.undefined) {
          dataStruct.xLabel = "x-axis";
        }
        if (dataStruct.yLabel === angular.undefined) {
          dataStruct.yLabel = "y-axis";
        }
        if (dataStruct.title === angular.undefined) {
          dataStruct.title = "Title";
        }

        if (dataStruct.y === angular.undefined) {
          // fill with fake data for testing
          dataStruct.y = [1000, 950, 900, 850, 800, 750, 800, 760, 790, 760, 780, 760, 780, 770, 775];
        }

        var dataMin = Math.min.apply(Math, dataStruct.y);
        if (dataMin < 0) {
          minY = -20 * Math.ceil(Math.abs(dataMin) / 20);
        }

        minX = Math.max(dataStruct.y.length, minX);

        for (var j = 0; j < dataStruct.y.length; j++) {
          data[j] = {
            x: 0,
            y: 0
          };
          data[j].y = dataStruct.y[j];
        }

        xValue = function(d) {
          return d;
        };
        xScale = d3.scale
          .linear()
          .range([0, width])
          .domain([0, minX]);
        xMap = function(d, i) {
          return xScale(xValue(i) + 1);
        };
        xAxis = d3.svg
          .axis()
          .scale(xScale)
          .orient("bottom")
          .tickFormat(d3.format("d"));

        yValue = function(d, i) {
          return d;
        };
        yScale = d3.scale
          .linear()
          .range([height, 0])
          .domain([minY, maxY]);
        yMap = function(d) {
          return yScale(yValue(d.y));
        };
        yAxis = d3.svg
          .axis()
          .scale(yScale)
          .orient("left");

        var mySVG = d3
          .select(domElem)
          .append("svg")
          .attr("width", width + margin.left + margin.right)
          .attr("height", height + margin.top + margin.bottom)
          .append("g")
          .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        // x-axis
        mySVG
          .append("g")
          .attr("class", "x axis")
          .attr("font-size", 16)
          .attr("transform", "translate(0," + height + ")")
          .call(xAxis);

        mySVG
          .append("text")
          .attr("class", "label")
          .attr("font-size", 20)
          .attr("x", width / 2)
          .attr("y", height + 35)
          .style("text-anchor", "middle")
          .text(dataStruct.xLabel);

        // y-axis
        mySVG
          .append("g")
          .attr("class", "y axis")
          .attr("font-size", 16)
          .call(yAxis);

        mySVG
          .append("text")
          .attr("class", "label")
          .attr("font-size", 18)
          .attr("transform", "rotate(-90)")
          .attr("x", -height / 2)
          .attr("y", -50)
          .style("text-anchor", "middle")
          .text(dataStruct.yLabel);

        // title
        mySVG
          .append("text")
          .attr("class", "label")
          .attr("x", width / 2)
          .attr("y", 10)
          .style("text-anchor", "middle")
          .style("font-weight", "bold")
          .style("font-size", "20px")
          .text(dataStruct.title);

        if (dataStruct.GapThreshold) {
          var line = d3.svg
            .line()
            .x(function(d) {
              return xScale(d.x);
            })
            .y(function(d) {
              return yScale(d.y);
            });

          var gapThreshold = [
            {
              x: 0,
              y: dataStruct.GapThreshold
            },
            {
              x: minX,
              y: dataStruct.GapThreshold
            }
          ];

          mySVG
            .append("svg:path")
            .datum(gapThreshold)
            .attr("class", "line")
            .attr("clip-path", "url(#clip)")
            .attr("stroke", "black")
            .attr("fill", "gray")
            .style("stroke-dasharray", "4,4")
            .attr("data-legend", function(d) {
              return "Threshold";
            })
            .attr("d", line);
        }

        mySVG
          .selectAll(".dot")
          .data(data)
          .enter()
          .append("circle")
          .attr("class", "dot")
          .attr("r", 5)
          .attr("cx", xMap)
          .attr("cy", yMap)
          .style("fill", function(d, i) {
            if (dataStruct.reversals[i]) {
              return "red";
            }
            return colorList[0];
          });
      });

    return api;
  })
  .directive("levelProgression", function() {
    return {
      restrict: "E",
      template: '<div id="levelProgressionPlotWindow"></div>',
      controller: "LevelProgressionPlotCtrl",
      scope: {
        data: "="
      }
    };
  })

  .controller("LevelProgressionPlotCtrl", function($scope, d3Services) {
    d3Services.scatterPlot("#levelProgressionPlotWindow", $scope.data);
  });
