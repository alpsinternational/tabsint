/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";

angular
  .module("tabsint.services.subject-history", [])

  .factory("subjectHistory", function(app, file, tabsintServer, $q, disk, json, logger) {
    // api object
    var subjectHistory = {
      data: undefined,
      dateModified: undefined,
      nativeURL: undefined
    };

    /**
     * load subject history from protocol path
     * @return {promise}             promise to load subject history
     */
    subjectHistory.load = function() {
      if (!disk.protocol.path) {
        logger.error("No protocol path while loading subject history");
        return $q.reject();
      }

      subjectHistory.path = disk.protocol.path + "subjecthistory.json";

      // internal
      function getFile() {
        return file
          .ready()
          .then(function() {
            return file.getFile(subjectHistory.path);
          })
          .then(
            function(fileEntry) {
              logger.debug("Local subject history found");
              subjectHistory.nativeURL = fileEntry.nativeURL;
            },
            function() {
              return $q.reject();
            }
          );
      }

      function getDateModified() {
        return file
          .ready()
          .then(function() {
            return file.getMetadata(subjectHistory.path);
          })
          .then(
            function(metadata) {
              subjectHistory.dateModified = metadata.modificationTime;
            },
            function() {
              logger.debug("Failed to get date modified of subject history");
              subjectHistory.dateModified = undefined;
            }
          )
          .then(function() {
            // tabsintServer specific functionality
            if (disk.server === "tabsintServer") {
              // check that subject history is not too old
              if (subjectHistory.dateModified) {
                var refreshTime = 24 * 60 * 60 * 1000; // milliseconds in 24 hours, default refresh ime
                var currentDate = new Date().getTime();
                if (currentDate - refreshTime > subjectHistory.dateModified) {
                  logger.debug("Subject history is out of date, attempting to download newer version");
                  return $q.reject();
                }
              } else {
                logger.debug("Subject history dateModified not found, attempting to download");
                return $q.reject();
              }
            }
          });
      }

      function loadLocal() {
        return json.loadAsync(subjectHistory.nativeURL).then(function(data) {
          subjectHistory.data = data;
        });
      }

      if (app.tablet) {
        return getFile() // will reject if no local file is found
          .then(getDateModified)
          .catch(function() {
            // try to download from tabsintServer if rejected above
            if (disk.server === "tabsintServer") {
              return tabsintServer
                .downloadSubjectHistory(disk.protocol.siteName, subjectHistory.path) // return true if no subject history needs to be downloaded
                .then(subjectHistory.load);

              // if not tabsintServer, keep rejecting
            } else {
              return $q.reject();
            }
          })
          .then(loadLocal)
          .catch(function() {}); // eat all rejections to subjectHistory calls so it always resolves -  we don't want this to hold up tests

        // browser and testing
      } else {
        return json
          .loadAsync(disk.protocol.path + "/subjecthistory.json")
          .then(function(data) {
            logger.info("Local subject history found");
            subjectHistory.data = data;
          })
          .catch(function() {
            logger.error("Subject history failed to load");
          });
      }
    };

    return subjectHistory;
  });
