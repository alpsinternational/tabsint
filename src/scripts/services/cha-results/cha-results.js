/**
 * This file is subject to the terms and conditions defined in
 * file 'LICENSE.txt', which is part of this source code package.
 */

"use strict";

import * as angular from "angular";
import * as _ from "lodash";
import * as $ from "jquery";

angular
  .module("tabsint.services.cha.results", [])
  .factory("chaResults", function($q, results, page, cha, chaCheck, logger, gettextCatalog) {
    // convenience function to splice substrings out of a string
    // s: string - the string that may contain substrings to be removed
    // rem: array[str,str,str,...] - array of strings to remove from s if present
    // returns: new s, sans substrings in rem
    function removeStrings(s, rem) {
      for (var i = 0; i < rem.length; i++) {
        var r = rem[i];
        var ind = s.indexOf(r);

        if (ind === 0) {
          s = s.slice(ind + r.length);
        } else if (ind > 0 && ind + r.length < s.length) {
          var tmp = s.slice(0, ind);
          tmp += s.slice(ind + r.length);
          s = tmp;
        } else if (ind > 0) {
          s = s.slice(0, ind);
        }
      }
      return s;
    }

    var api = {};

    // convenience function to grab specific results from the stack
    // idList: array of string - partial or full match presentationIds
    // matchFields: array of obj of type {field: str, list [val, val, val] }
    //    field is the field in the result object to match
    //    list contains the values you are looking for
    // returns: array of matched result objects
    api.getPastResults = function(idList, matchFields) {
      // retrieve old results, add current response
      var responses = angular.copy(results.current.testResults.responses);
      if (angular.isDefined(page.result.examType)) {
        responses.push(page.result);
      }

      var resultsList = [];
      if (idList !== angular.undefined && idList.length > 0) {
        _.forEach(idList, function(id) {
          resultsList = resultsList.concat(
            _.filter(responses, function(res) {
              var hasExamType = angular.isDefined(res.examType); // if there isn't an examType, it's not a cha exam and shouldn't be tabulated
              var hasPresentationId = res.presentationId.indexOf(id) >= 0;
              return hasExamType && hasPresentationId;
            })
          );
        });
      }

      if (matchFields && matchFields.length > 0) {
        _.forEach(matchFields, function(matchSet) {
          resultsList = _.filter(resultsList, function(res) {
            return res[matchSet.field] && matchSet.list.indexOf(res[matchSet.field]) > -1;
          });
        });
      }

      return resultsList;
    };

    api.addTOBResults = function(tobResults) {
      if (tobResults.Frequencies === angular.undefined || tobResults.Leq === angular.undefined) {
        tobResults.dataError = "No results returned";
        page.result = $.extend({}, page.result, { tobResults: tobResults });
      } else {
        // round floating point issues
        var roundedFrequencies = tobResults.Frequencies.map(function(f) {
          return Math.round(f);
        });

        var roundedLevels = tobResults.Leq.map(function(leq) {
          return Math.round(leq * 1000) / 1000;
        });
        // push all results onto the dm
        page.result = $.extend({}, page.result, {
          tobResults: { Frequencies: roundedFrequencies, Leq: roundedLevels }
        });
      }
    };

    api.createAudiometryResults = function(idList) {
      var audiogramData = {
        frequency: [],
        level: [],
        channel: [],
        xLabel: undefined,
        yLabel: undefined,
        title: undefined
      };
      var matchList, units;

      // audiogram passes in idList from protocol, displays at the end of audiometry exams do not - it must be created
      if (angular.isUndefined(idList)) {
        if (
          angular.isDefined(page.dm.responseArea.plotProperties) &&
          angular.isDefined(page.dm.responseArea.plotProperties.displayAudiogram)
        ) {
          var plotProperties = page.dm.responseArea.plotProperties || {};
          idList = plotProperties.displayAudiogram;
        } else if (angular.isDefined(page.dm.responseArea.displayIds)) {
          idList = page.dm.responseArea.displayIds;
        } else {
          idList = [""];
        }
      }

      var resultsList;
      if (angular.isDefined(page.result.examType)) {
        matchList = [{ field: "examType", list: [page.result.examType] }];
        resultsList = api.getPastResults(idList, matchList);
        units = page.result.Units || gettextCatalog.getString("dB HL");
      } else {
        // we are tabling previous results - only use the idList
        resultsList = api.getPastResults(idList, null);
        if (resultsList.length > 0 && resultsList[0].Units) {
          units = resultsList[0].Units;
        } else {
          units = "dB HL";
        }
      }

      if (resultsList.length > 0) {
        for (var i = 0; i < resultsList.length; i++) {
          pushResult(resultsList[i]);
        }

        audiogramData.yLabel = gettextCatalog.getString("Hearing Level") + " (" + units + ")";
        audiogramData.xLabel = gettextCatalog.getString("Frequency (Hz)");

        return [resultsList, audiogramData];
      } else {
        return [undefined, undefined];
      }

      function pushResult(result) {
        if (
          angular.isDefined(result) &&
          angular.isDefined(result.examProperties) &&
          angular.isDefined(result.presentationId && angular.isDefined(result.examType))
        ) {
          var F, L, channel, id, passFail, index;
          try {
            channel = result.examProperties.OutputChannel;
            id = result.presentationId;
            index = result.presentationIndex;

            // check left/right/bone
            if (channel === "HPL0" || channel === "HPL1") {
              channel = "left";
            } else if (channel === "HPR0" || channel === "HPR1") {
              channel = "right";
            } else if (channel === "LINEL0 NONE") {
              // bone conduction left
              channel = "bone_left";
            } else if (channel === "NONE LINEL0") {
              // bone conduction right
              channel = "bone_right";
            }

            if (chaCheck.isLevelExam(result.examType)) {
              if (result.examProperties.Screener || result.ResponseType === "pass-fail") {
                F = angular.isNumber(result.examProperties.F) ? result.examProperties.F : "-";
                passFail = result.response ? result.response : result.ResultType;
              } else {
                F = angular.isNumber(result.examProperties.F) ? result.examProperties.F : 1000;
                L = isFinite(result.Threshold) ? result.Threshold : "-";

                if (!_.includes([F, L], "-") && !isNaN(L)) {
                  audiogramData.frequency.push(F);
                  audiogramData.level.push(L);
                  audiogramData.channel.push(channel);
                }
              }
            } else if (chaCheck.isFrequencyExam(result.examType)) {
              F = angular.isNumber(result.Threshold) ? result.Threshold : "-";
              L = angular.isNumber(result.examProperties.Level) ? result.examProperties.Level : 65;

              if (!_.includes([F, L], "-") && !isNaN(F)) {
                audiogramData.frequency.push(F);
                audiogramData.level.push(L);
                audiogramData.channel.push(channel);
              }
            }
          } catch (err) {
            logger.error("error while trying to create audiometry figure: " + angular.toJson(err));
          }
        }
      }
    };

    api.createLevelProgressionData = function(lpResults) {
      var levelProgressionData = {
        x: undefined,
        y: undefined,
        xLabel: gettextCatalog.getString("Iteration") + " #",
        yLabel: gettextCatalog.getString("Level") + " (" + lpResults.Units + ")",
        title: gettextCatalog.getString("Level Progression")
      };
      if (!_.includes([lpResults.L], "-")) {
        levelProgressionData.y = lpResults.L;
      }
      return levelProgressionData;
    };

    api.createGapData = function(gapResults) {
      var gapData = {
        x: undefined,
        y: undefined,
        xLabel: gettextCatalog.getString("Presentation") + " #",
        yLabel: gettextCatalog.getString("Gap Length") + " (ms)",
        title: gettextCatalog.getString("Gap Detection Results")
      };
      var maxY = 0;
      gapResults.GapLengthArray.forEach(function(entry) {
        maxY = entry > maxY ? entry : maxY;
      });
      maxY = maxY === 0 ? 200 : maxY + 10;
      gapData.y = gapResults.GapLengthArray;
      gapData.hit = gapResults.HitOrMissArray;
      gapData.maxY = maxY;
      gapData.GapThreshold = 0;
      if (gapResults.GapThreshold && !gapResults.GapThreshold.isNaN) {
        gapData.GapThreshold = gapResults.GapThreshold;
      }
      gapData.reversals = gapResults.ReversalUsedForThresholdArray;
      return gapData;
    };

    api.createDPOAEData = function(dpoaeResults) {
      var resultsList = [];

      var ret = {
        data: [],
        channel: dpoaeResults.channel ? dpoaeResults.channel : "unknown",
        xLabel: "F2 Frequency, Hz",
        yLabel: "Level, dB SPL",
        title: "DPOAE"
      };
      var plotProperties = page.dm.responseArea.plotProperties || {};

      ret.data = dpoaeResults.resultList;

      for (var i = 0; i < ret.data.length; i++) {
        ret.data[i].channel = ret.channel;
      }

      return ret;
    };

    api.createTOBFigures = function(results, standard) {
      var startFreq = 250 - 1;
      var endFreq = 10080 + 1;
      var tobData = {};

      // One limit set
      var limitSet1 = [
        { freq: 125, limit: 24 },
        { freq: 250, limit: 16 },
        { freq: 500, limit: 11 },
        { freq: 800, limit: 10 },
        { freq: 1000, limit: 8 },
        { freq: 1600, limit: 9 },
        { freq: 2000, limit: 9 },
        { freq: 3150, limit: 8 },
        { freq: 4000, limit: 6 },
        { freq: 6300, limit: 8 },
        { freq: 8000, limit: 9 }
      ];

      // choose limit set to use
      var limits = limitSet1;

      if (angular.isDefined(standard)) {
        tobData.standard = standard;
      }

      for (var i = 0; i < results.length; i++) {
        var tmp = results[i];
        if (tmp.examProperties.InputChannel === "SMICR1") {
          addData(tmp, "right");
        } else if (tmp.examProperties.InputChannel === "SMICR0") {
          addData(tmp, "left");
        }
      }

      function addData(data, channel) {
        tobData[channel] = [];
        for (var i = 0; i < data.Frequencies.length; i++) {
          // reduce data set to window of interest
          if (data.Frequencies[i] >= startFreq && data.Frequencies[i] <= endFreq) {
            // handling mapping the frequencies that don't match with the limit frequencies
            var freqMap = Math.round(data.Frequencies[i]);
            var limit = 1000; // default to put unset limits off-secreen
            for (var j = 0; j < limits.length; j++) {
              var freqDiff = Math.abs(freqMap - limits[j].freq) / freqMap;
              if (freqDiff < 0.05) {
                // seems to always be within 2%
                limit = limits[j].limit; // set limit if it exists
              }
            }

            tobData[channel].push({
              freq: freqMap,
              level: data.Leq[i] > 0 ? data.Leq[i] : 0, // min of 0
              limit: limit
            });
          }
        }
      }

      // may want tables some day, keeping same api as createAudiometryResults
      var tables = {};

      return [tables, tobData];
    };

    return api;
  });
